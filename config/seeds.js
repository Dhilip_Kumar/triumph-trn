/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */

/*
	Model Notes:
	============

	emostateassessment - collection that contains questions that needs to be asked for emostateassessment
	energystate - collection that stores list of energy states and its definition
	statequestionassociation - collection that stores mapping for all 15 emostates with associated values and low, medium, high score results
	valueassessment - collection that contains questions that needs to be asked for valueassessment
	valuemapping - collection that stores value properties such as name, hint, definition, benefits
*/

module.exports.seeds = {

	disable: true,

	emostateassessment: {
		data: [
			{
			   "questionId":1,
			   "question":"When I see someone in pain, I want to do something to help take their pain away.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Compassion",
			   "reviseScore":false
			},
			{
			   "questionId":2,
			   "question":"In the last several days, I helped others even when it was inconvenient to me.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Kindness \u0026 Caring",
			   "reviseScore":false
			},
			{
			   "questionId":3,
			   "question":"The bigger the decision to make, the greater the amount of stress it causes me.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Understanding",
			   "reviseScore":true
			},
			{
			   "questionId":4,
			   "question":"Past or anticipated failures demotivate me from pursuing interesting initiatives.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Courage",
			   "reviseScore":true
			},
			{
			   "questionId":5,
			   "question":"I find myself thinking about the 'paths not taken' and 'what if' scenarios pertaining to past events.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Letting Go",
			   "reviseScore":true
			},
			{
			   "questionId":6,
			   "question":"I don't like it when someone takes more than they give.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Inclusivity",
			   "reviseScore":true
			},
			{
			   "questionId":7,
			   "question":"Sometimes, I associate with people or do things that do not align with who I am or what I stand for.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Authenticity & Truthfulness",
			   "reviseScore":true
			},
			{
			   "questionId":8,
			   "question":"I find myself in complicated or overwhelming situations more often than I'd like.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Simplicity",
			   "reviseScore":true
			},
			{
			   "questionId":9,
			   "question":"It bothers me when someone raises concerns or objections with regard to my likes, ideas, or work.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Humility",
			   "reviseScore":true
			},
			{
			   "questionId":10,
			   "question":"There are certain traits or behaviors of people that annoy me.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Accepting & Non-Judgement",
			   "reviseScore":true
			},
			{
			   "questionId":11,
			   "question":"The idea of pursuing big or audacious goals makes me uncomfortable.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Abundance",
			   "reviseScore":true
			},
			{
			   "questionId":12,
			   "question":"I tend to drift into thoughts unrelated to what I'm doing currently.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Being Present",
			   "reviseScore":true
			},
			{
			   "questionId":13,
			   "question":"I'm able to change my habits and behavior in order to achieve larger objectives.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Dedication \u0026 Surrender",
			   "reviseScore":false
			},
			{
			   "questionId":14,
			   "question":"People or circumstances get in the way of accomplishing my goals more often than I'd like.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Being Responsible",
			   "reviseScore":true
			},
			{
			   "questionId":15,
			   "question":"When I run into major hurdles, I find ways to work around them without getting too anxious.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Positivity & Spirit",
			   "reviseScore":false
			},
			{
			   "questionId":16,
			   "question":"I have feelings (e.g. anxiousness, anger, irritation) without a very clear cause at times.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Self-Awareness",
			   "reviseScore":true
			},
			{
			   "questionId":17,
			   "question":"I get unhappy when certain people are upset with me or when I lose something I treasure.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Freedom",
			   "reviseScore":true
			},
			{
			   "questionId":18,
			   "question":"I say or do things that I regret from time to time.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Self-Mastery",
			   "reviseScore":true
			},
			{
			   "questionId":19,
			   "question":"Changes to my already established work, routine, surroundings, or relationships make me uncomfortable.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Openness",
			   "reviseScore":true
			},
			{
			   "questionId":20,
			   "question":"I need to watch my own back when interacting with others as everyone's looking out for themselves.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Innocence",
			   "reviseScore":true
			},
			{
				"questionId":21,
				"question":"I strive to make every interaction deep and meaningful.",
				"score":0,
				"isAnswered":false,
				"associatedValue":"Excellence",
				"reviseScore":false
			},
			{
			   "questionId":22,
			   "question":"It’s frustrating when those in a position to help don’t.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Compassion",
			   "reviseScore":true
			},
			{
			   "questionId":23,
			   "question":"My words and actions are usually gentle and tender.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Kindness \u0026 Caring",
			   "reviseScore":false
			},
			{
			   "questionId":24,
			   "question":"It feels like others don’t get me or I don’t get them.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Understanding",
			   "reviseScore":true
			},
			{
			   "questionId":25,
			   "question":"The more others disagree with my decision, the more likely I am to give up.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Courage",
			   "reviseScore":true
			},
			{
			   "questionId":26,
			   "question":"Past events and experiences have an ongoing effect on me and my relationships.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Letting Go",
			   "reviseScore":true
			},
			{
			   "questionId":27,
			   "question":"I have people with a lot of different backgrounds and personalities in my circles.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Inclusivity",
			   "reviseScore":false
			},
			{
			   "questionId":28,
			   "question":"Sometimes, I do inappropriate, unfavorable or untruthful things that I would not feel good about telling anyone.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Authenticity & Truthfulness",
			   "reviseScore":true
			},
			{
			   "questionId":29,
			   "question":"I tend to have more stuff than I have the need, space, or capacity for.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Simplicity",
			   "reviseScore":true
			},
			{
			   "questionId":30,
			   "question":"It bothers me when people do not appreciate my value or contributions.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Humility",
			   "reviseScore":true
			},
			{
			   "questionId":31,
			   "question":"There are many things about myself that I’m unhappy about.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Accepting & Non-Judgement",
			   "reviseScore":true
			},
			{
			   "questionId":32,
			   "question":"I wish I had a bigger safety net to give me the assurance and peace of mind I need.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Abundance",
			   "reviseScore":true
			},
			{
			   "questionId":33,
			   "question":"My day's usually filled with many moments of happiness, small or big.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Being Present",
			   "reviseScore":false
			},
			{
			   "questionId":34,
			   "question":"Making personal sacrifices now and then to do what I do everyday does not burn me out.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Dedication \u0026 Surrender",
			   "reviseScore":false
			},
			{
			   "questionId":35,
			   "question":"I tend to be a step or two (or few) behind what I need to meet my responsibilities effortlessly.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Being Responsible",
			   "reviseScore":true
			},
			{
			   "questionId":36,
			   "question":"I'm grateful for so many things—I think I'm very fortunate.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Positivity & Spirit",
			   "reviseScore":false
			},
			{
			   "questionId":37,
			   "question":"I wish I didn't get overwhelmed (e.g. stress, anxiety, confusion) so quickly.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Self-Awareness",
			   "reviseScore":true
			},
			{
			   "questionId":38,
			   "question":"I feel bad for myself or get somewhat envious when I see someone else enjoying success in life.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Freedom",
			   "reviseScore":true
			},
			{
			   "questionId":39,
			   "question":"Sometimes, I am unable to do or avoid doing things that I know would be good for me or for others involved.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Self-Mastery",
			   "reviseScore":true
			},
			{
			   "questionId":40,
			   "question":"I’m constantly learning about things beyond the scope of my identified areas of study or work.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Openness",
			   "reviseScore":false
			},
			{
			   "questionId":41,
			   "question":"I frequently get a wondrous feeling as I see or experience things working so flawlessly, almost magically.",
			   "score":0,
			   "isAnswered":false,
			   "associatedValue":"Innocence",
			   "reviseScore":false
			},
			{
				"questionId":42,
				"question":"I strive to discover and learn about well-done works within as well as outside my areas of interest and expertise.",
				"score":0,
				"isAnswered":false,
				"associatedValue":"Excellence",
				"reviseScore":false
			},
		],
		unique: ['questionId'],
		overwrite: true,
	}, // End of emostateassessment

	energystate: {
		data: [
			{
				"state":"Balanced",
				"definition":"In this Energy State, life can be blissful and productive. You can have a greater level of clarity about things that matter and know what you want out of life. You know you’re going to reach your lofty objectives, if you haven’t already, and don’t need others to say nice things to make you feel good about yourself. The deeper the state of Balance, the more every moment is joyful and every person wonderful, and the more you want to share the richness of life with everyone and everything around you. Balanced is the Energy State in which we operate the best.",
				"score":0,
				"sortOrder":1
			},
			{
				"state":"Hyper",
				"definition":"In this Energy State, you can be wasteful, inefficient, and insensitive. You might be driven to pursue the things you do not have while losing sight of the things you already possess. You see your goals so clearly and approach them with such energy that if others get in your way, it can make you mad. You may feel that others are mostly either with you or against you. It can also become annoying or even frustrating when others bring up contrary or opposing ideas.",
				"score":0,
				"sortOrder":2
			},
			{
				"state":"Inert",
				"definition":"In this Energy State, you can be weak and lack initiative. You might experience confusion and lack the ability to make confident decisions. You might feel pressures from life that block you from taking positive action. The more time you spend in the Inert Energy State, the more you feel stalled—unable or unwilling to progress—preventing you from living your life to its fullest potential.",
				"score":0,
				"sortOrder":3
			}
		],
		unique: ['state'],
		overwrite: true,
	}, // End of energystate

	statequestionassociation: {
		data: [
			{
				"stateName":"Fear & Insecurity",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					20,
					41,
					11,
					32,
					17,
					38,
					19,
					40
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Innocence",
					"Abundance",
					"Freedom",
					"Openness"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Inclusivity", "Openness", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Compassion", "Kindness & Caring", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Excellence", "Courage"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Defensiveness",
					"Timidity",
					"Low Self-Confidence"
				],
				"mediumScoreDefinition":"When in this EmoState, you’re less likely to take the lead, preferring to go with the flow, or keeping to the sides of the room to try and not to be too noticeable. You sometimes have doubts in your abilities and feel you have yet to prove yourself.",
				"highScoreResults":[
					"Distrust",
					"Insecure",
					"Suspicious",
					"Phobias",
					"Defensiveness",
					"Timidity",
					"Low Self-Confidence"
				],
				"highScoreDefinition":"When in this EmoState, you usually prefer keeping to yourself, mostly speaking when it is required and saying what you think others want to hear. You might dislike others knowing your business and often keep your guard up when around other people. In this state, you can find it hard to trust others and not feel safe, no matter what.",
				"energyState":"Inert",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Shame",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					8,
					29,
					14,
					35,
					20,
					41,
					19,
					40,
					17,
					38,
					15,
					36
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Simplicity",
					"Being Responsible",
					"Innocence",
					"Openness",
					"Freedom",
					"Positivity & Spirit"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Letting Go", "Accepting & Non-Judgement", "Openness", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Kindness & Caring", "Inclusivity", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Positivity & Spirit", "Innocence", "Courage"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Shyness",
					"Introversion",
					"Low Self-Confidence",
					"Perfectionist Tendencies"
				],
				"mediumScoreDefinition":"When in this EmoState, you second-guess yourself and take very little pride in your work. You might avoid interacting with other people, and when you do interact, you often feel your contributions are not worth much.",
				"highScoreResults":[
					"Withdrawn",
					"Rigidity",
					"Extremism",
					"Righteousness",
					"Shyness",
					"Introversion",
					"Low Self-Confidence"
				],
				"highScoreDefinition":"When in this EmoState, nothing you do is good enough, and you start to feel as though you’re incapable of doing anything worthwhile. You might feel as though you are a shell of who you once were, and the decisions you make now feel foreign and forced. In this state, you prefer being left alone rather than being dragged into discussions or debates.",
				"energyState":"Inert",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Guilt",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					19,
					40,
					5,
					26,
					13,
					34,
					12,
					33
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Openness",
					"Letting Go",
					"Dedication \u0026 Surrender",
					"Being Present"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Positivity & Spirit", "Innocence", "Openness"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Remorse",
					"Overthinking",
					"Being On Edge",
					"Defensiveness"
				],
				"mediumScoreDefinition":"When in this EmoState, you tend to blame yourself and have trouble forgiving yourself when things go wrong. You carry some of your past indiscretions with you, unable to let them go.",
				"highScoreResults":[
					"Self-recrimination",
					"Victimhood",
					"Accident-proneness",
					"Remorse",
					"Overthinking",
					"Being On Edge"
				],
				"highScoreDefinition":"When in this EmoState, you usually have your guard up, acting ‘on-edge’ and nervous. You are likely to overthink even simple situations and usually jump straight to the worst case scenario, feeling that others are waiting for you to make a mistake. In this state, it can feel as if it’s you versus yourself.",
				"energyState":"Inert",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName" : "Doubt",
				"associatedQuestions": [
					11, 32, 14, 35, 5, 26, 10, 31, 19, 40, 4, 25, 8, 29, 13, 34
				],
				"associatedValues": ["Abundance", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Openness", "Courage", "Simplicity", "Dedication & Surrender"],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Openness", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Freedom", "Kindness & Caring", "Being Present", "Positivity & Spirit", "Innocence", "Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "When in this EmoState, it can be easy to second-guess yourself and not feel as confident about your choices. Or when the path to be taken is clear, you might feel ill-prepared for the journey ahead.",
				"highScoreResults": [],
				"highScoreDefinition": "When in this EmoState, it can be easy to second-guess yourself and not feel as confident about your choices. Or when the path to be taken is clear, you might feel ill-prepared for the journey ahead. Sometimes, it can get frustrating, especially when clarity keeps eluding you despite trying and having a strong desire to find a satisfactory resolution promptly.",
				"energyState": "Inert",
				"reviseScore": true,
				"type": "User"
			},
			{
				"stateName":"Despair",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					15,
					36,
					11,
					32,
					19,
					40,
					17,
					38,
					13,
					34,
					12,
					33,
					18,
					39,
					20,
					41
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Positivity & Spirit",
					"Abundance",
					"Openness",
					"Freedom",
					"Dedication \u0026 Surrender",
					"Being Present",
					"Self-Mastery",
					"Innocence"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Innocence", "Openness", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Freedom", "Compassion", "Kindness & Caring", "Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Pessimism",
					"Humorless",
					"Tiredness",
					"Carelessness",
					"Confusion",
					"Discouragement"
				],
				"mediumScoreDefinition":"When in this EmoState, it can be hard for you to be inspired by anything you do, as sometimes, you just don’t see the point. In this state, you don’t laugh as much as you used to, and simple things often take more willpower than before.",
				"highScoreResults":[
					"Hoplessness",
					"Victimhood",
					"Dependence",
					"Depression",
					"Defeat",
					"Pessimism",
					"Humorless",
					"Tiredness",
					"Carelessness",
					"Confusion",
					"Discourgement"
				],
				"highScoreDefinition":"When in this EmoState, it can be hard for you to be inspired by anything you do, as sometimes, you just don’t see the point. In this state, you don’t laugh as much as you used to, and simple things often take more willpower than before. During more severe bouts, you’d much rather do nothing at all.",
				"energyState":"Inert",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Regret and Grief",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					11,
					32,
					5,
					26,
					17,
					38,
					12,
					33
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Abundance",
					"Letting Go",
					"Freedom",
					"Being Present"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Courage", "Simplicity"],
					"Tier2": ["Kindness & Caring", "Innocence", "Openness", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Disappointment",
					"Regret",
					"Longing"
				],
				"mediumScoreDefinition":"When in this EmoState, if things don’t go your way, you can quickly become disappointed. You occasionally have moments where you feel like abandoning your work altogether, and starting something new. In this state, you often focus on what ‘could have been’ too often when you should be looking at ‘what could be.’",
				"highScoreResults":[
					"Pain",
					"Anguish",
					"Disappointment",
					"Regret",
					"Longing"
				],
				"highScoreDefinition":"When in this EmoState, you periodically feel like so much time has passed you by, yet you’ve hardly accomplished anything you said or thought you would. On bad days, you cannot help but be burdened by your past failures, allowing yourself to feel the weight of every poor decision and every missed opportunity.",
				"energyState":"Inert",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Frustration",
				"associatedQuestions":[
					16,
					37,
					8,
					29,
					17,
					38,
					19,
					40,
					10,
					31,
					3,
					24,
					9,
					30,
					12,
					33,
					1,
					22
				],
				"associatedValues":[
					"Self-Awareness",
					"Simplicity",
					"Freedom",
					"Openness",
					"Accepting & Non-Judgement",
					"Understanding",
					"Humility",
					"Being Present",
					"Compassion"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Simplicity"],
					"Tier2": ["Inclusivity", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Impatience"
				],
				"mediumScoreDefinition":"When in this EmoState, you prefer jumping into action rather than spending the extra effort to put together a solid plan first. In this state, if you can’t complete something quickly, you are likely to lose interest and look for another route to your goal, or sometimes, a new goal entirely.",
				"highScoreResults":[
					"Indignation",
					"Contempt",
					"Impatience"
				],
				"highScoreDefinition":"When in this EmoState, you prefer jumping into action rather than spending the extra effort to put together a solid plan first. If you can’t complete something quickly, you are likely to lose interest and look for another route to your goal, or sometimes, a new goal entirely. You can get annoyed when others are not on the same page as you. In this state, if others don’t understand what you are doing straight away, then you might view them as a problem rather than a resource.",
				"energyState":"Hyper",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Desire & Greed",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					17,
					38,
					11,
					32,
					6,
					27
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Freedom",
					"Abundance",
					"Inclusivity"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Kindness & Caring", "Inclusivity", "Being Present", "Courage", "Simplicity"],
					"Tier2": ["Compassion", "Authenticity & Truthfulness", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Innocence", "Openness"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Control",
					"Selfishness"
				],
				"mediumScoreDefinition":"When in this EmoState, you can feel uncomfortable when others do things differently to you. You occasionally ignore instructions in favor of your own methods or pursuits that especially benefit or appeal to you, even if others disagree.",
				"highScoreResults":[
					"Obsession",
					"Possessiveness",
					"Control",
					"Selfishness"
				],
				"highScoreDefinition":"When in this EmoState, you are highly driven towards your ultimate goal, and so the smaller achievements you accomplish along the way may mean very little. In this state, you can become impatient, ignore advice, and not take loss or defeat—even when small—well. Competing ideas and suggestions for alternatives to your identified objectives may not sit well with you. At times, others find working with you to be a demanding undertaking.",
				"energyState":"Hyper",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Competing",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					17,
					38,
					11,
					32,
					1,
					22,
					2,
					23,
					6,
					27
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Freedom",
					"Abundance",
					"Compassion",
					"Kindness \u0026 Caring",
					"Inclusivity"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Being Present", "Being Responsible", "Humility", "Courage", "Simplicity"],
					"Tier2": ["Authenticity & Truthfulness", "Letting Go", "Accepting & Non-Judgement", "Innocence", "Openness"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Envy",
					"Jealousy"
				],
				"mediumScoreDefinition":"While in this EmoStaste, when others succeed, your first reaction is likely to compare their success to your own. If you are ahead, you take it as validation of your own success, but if you are behind, you can feel as though you need to step up your game.",
				"highScoreResults":[
					"Greed",
					"Selfishness",
					"Envy",
					"Jealousy"
				],
				"highScoreDefinition":"When in this EmoState, you often compare yourself to others, and feel insecure that you are underachieving. You often compete with others purely to win even if that success doesn’t stand to add any significant material value to your life. In this state, when you gain something, you want more of it and won’t feel satisfied until you have it.",
				"energyState":"Hyper",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Anger",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					18,
					39,
					1,
					22,
					3,
					24,
					9,
					30,
					14,
					35,
					17,
					38,
					12,
					33,
					10,
					31
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Self-Mastery",
					"Compassion",
					"Understanding",
					"Humility",
					"Being Responsible",
					"Freedom",
					"Being Present",
					"Accepting & Non-Judgement"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Courage", "Simplicity"],
					"Tier2": ["Compassion", "Kindness & Caring", "Inclusivity", "Authenticity & Truthfulness", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Sarcasm",
					"Stubbornness",
					"Pouting",
					"Meanness"
				],
				"mediumScoreDefinition":"When in this EmoState, you can be quick to meet disagreements with offensive or sarcastic remarks. Sometimes you don’t want to change your mind and feel as though why should you? In this state, you often think it is as simple as you are right and they are wrong.",
				"highScoreResults":[
					"Revenge",
					"Outrage",
					"Hatred",
					"Rebellion",
					"Sarcasm",
					"Stubbornness",
					"Pouting",
					"Meanness"
				],
				"highScoreDefinition":"When in this EmoState, you can be quick to meet disagreements with offensive or sarcastic remarks. Sometimes you don’t want to change your mind and feel as though why should you? You might think it is as simple as you are right and they are wrong. In this EmoState, you can be inclined to not let even a slight expression of disagreement, antagonism, or any other kind of untoward behavior directed at you go unpunished.",
				"energyState":"Hyper",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName":"Pride",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					9,
					30,
					6,
					27,
					17,
					38,
					8,
					29,
					10,
					31,
					13,
					34,
					19,
					40,
					20,
					41,
					2,
					23
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Humility",
					"Inclusivity",
					"Freedom",
					"Simplicity",
					"Accepting & Non-Judgement",
					"Dedication \u0026 Surrender",
					"Openness",
					"Innocence",
					"Kindness \u0026 Caring"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Freedom", "Kindness & Caring", "Inclusivity", "Accepting & Non-Judgement", "Humility", "Simplicity"],
					"Tier2": ["Authenticity & Truthfulness", "Letting Go", "Innocence", "Openness"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Arrogance",
					"Judging",
					"Over-valuation"
				],
				"mediumScoreDefinition":"When in this EmoState, you tend to misplace value on things you identify with while undervaluing the rest, thereby resulting in varying degrees of bias in different aspects of your life. You might, for example, believe your hard work deserves praise—regardless of whether the results are good or not—while downplaying somebody else in a similar situation. In this state, if others critique something close to you, you can be quick to take offense and slow to forgive.",
				"highScoreResults":[
					"Denial",
					"Boastfulness",
					"Vain",
					"Self-centeredness",
					"Prejudice",
					"Unforgiving",
					"Pious",
					"Bigotry",
					"Selfishness",
					"Aloof",
					"Smug",
					"Arrogance",
					"Judging",
					"Over-valuation"
				],
				"highScoreDefinition":"When in this EmoState, you tend to misplace value on things you identify with while undervaluing the rest, thereby resulting in varying degrees of bias in different aspects of your life. You might, for example, believe your hard work deserves praise—regardless of whether the results are good or not—while downplaying somebody else in a similar situation. In this state, if others critique something close to you, you can be quick to take offense and slow to forgive. Others’ views are often less important than your own, and if your views change, so should theirs, too.",
				"energyState":"Hyper",
				"reviseScore":true,
				"type": "User"
			},
			{
				"stateName" : "Contempt & Spite",
				"associatedQuestions": [
					11, 32, 17, 38, 1, 22, 2, 23, 6, 27, 12, 33, 5, 26, 10, 31, 9, 30, 20, 41, 19, 40, 8, 29 
				],
				"associatedValues": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Being Present", "Letting Go", "Accepting & Non-Judgement", "Humility", "Innocence", "Openness", "Simplicity"],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Being Present", "Letting Go", "Accepting & Non-Judgement", "Humility", "Innocence", "Openness", "Simplicity"],
					"Tier2": ["Being Responsible", "Positivity & Spirit", "Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "",
				"mediumScoreResults": [
					"Disrespect", "Bitterness", "Aversion"
				],
				"mediumScoreDefinition": "When in this EmoState, you tend to find others unreasonable, irresponsible, or hostile. You might see malice in others’ words or actions even when known was intended. If this EmoState persists too long, you might develop bitterness or aversion towards the people in question.",
				"highScoreResults": [
					"Offending, annoying, hindering, humiliating or even hurting others", "Disrespect", "Bitterness", "Aversion"
				],
				"highScoreDefinition": "When in this EmoState, you tend to find others unreasonable, irresponsible, or hostile. You might see malice in others’ words or actions even when known was intended. If this EmoState persists too long, you might develop bitterness or aversion towards the people in question, and possibly engage in activities meant to offend, annoy, or hinder them.",
				"energyState": "Hyper",
				"reviseScore": true,
				"type": "User"
			},
			{
				"stateName":"Resourcefulness & Self-Sufficiency",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					17,
					38,
					8,
					29,
					12,
					33,
					7,
					28,
					9,
					30,
					18,
					39,
					15,
					36
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Freedom",
					"Simplicity",
					"Being Present",
					"Authenticity & Truthfulness",
					"Humility",
					"Self-Mastery",
					"Positivity & Spirit"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Innocence", "Openness", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Self-Awareness", "Self-Mastery", "Understanding"],
					"Tier3": []
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Humor",
					"Taking Action",
					"Cheerfulness"
				],
				"mediumScoreDefinition":"When in this EmoState, a tight spot is no problem for you, and you’re usually able to take it in your stride. After all, this isn't the first time you’ve weathered the storm, and you know it won't be the last. In this EmoState, where others stop, you are able to push on.",
				"highScoreResults":[
					"Confidence",
					"Clarity",
					"Independence",
					"Self-sufficiency",
					"Resilience",
					"Resourcefulness",
					"Humor",
					"Taking Action",
					"Cheerfulness"
				],
				"highScoreDefinition":"When in this EmoState, a tight spot is no problem for you, and you’re usually able to take it in your stride. After all, this isn't the first time you’ve weathered the storm, and you know it won't be the last. Where others might show signs of stopping, you’re able to grit your teeth, smile and push on. In this EmoState, you’re able to go from chaos and confusion to clarity and confidence with a little effort. You are unafraid to take the lead, and new opportunities fill you with excitement.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Enthusiasm",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					7,
					28,
					17,
					38,
					3,
					24,
					10,
					31,
					19,
					40,
					20,
					41,
					9,
					30,
					15,
					36
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Authenticity & Truthfulness",
					"Freedom",
					"Understanding",
					"Accepting & Non-Judgement",
					"Openness",
					"Innocence",
					"Humility",
					"Positivity & Spirit"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Innocence", "Openness", "Courage", "Dedication & Surrender"],
					"Tier2": ["Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Motivation",
					"Taking Action",
					"Growth Orientation",
					"Friendliness"
				],
				"mediumScoreDefinition":"When in this EmoStaste, you want every day to be better than the last, but understand that good results require work. The idea of growth and learning inspires you. In this EmoState, you bond easily with others and share your growth mentality with them.",
				"highScoreResults":[
					"Resilience",
					"Confidence",
					"Creators",
					"Humility",
					"Motivation",
					"Taking Action",
					"Growth Orientation",
					"Friendliness"
				],
				"highScoreDefinition":"When in this EmoStaste, new experiences excite and entertain you, especially when creating something either by yourself or as part of a group. Those around you are powerless to resist and get caught up in your confident enthusiasm, feeling more inspired and motivated. In this EmoState, when the going gets tough, you’re the one who keeps a smile on your face and the train on the tracks.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Acceptance",
				"associatedQuestions":[
					16,
					37,
					4,
					25,
					1,
					22,
					3,
					24,
					7,
					28,
					9,
					30,
					6,
					27,
					19,
					40,
					20,
					41,
					11,
					32,
					5,
					26,
					12,
					33
				],
				"associatedValues":[
					"Self-Awareness",
					"Courage",
					"Compassion",
					"Understanding",
					"Authenticity & Truthfulness",
					"Humility",
					"Inclusivity",
					"Openness",
					"Innocence",
					"Abundance",
					"Letting Go",
					"Being Present"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion"],
					"Tier2": ["Being Responsible", "Positivity & Spirit", "Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Connectedness",
					"Warmth",
					"Mellow"
				],
				"mediumScoreDefinition":"When in this EmoState, you’re warm to those around you and treat others with respect. You try not to let what others are doing affect your own decisions, as you understand we are all different. In this EmoState, you like the person you are, but feel that you still have some room to grow.",
				"highScoreResults":[
					"Caring",
					"Self-worthiness",
					"Soft",
					"Gentle",
					"Natural",
					"Connectedness",
					"Warmth",
					"Mellow"
				],
				"highScoreDefinition":"When in this EmoStste, creating connections with the people you interact with comes naturally because a deep understanding of your own strengths and weaknesses means you are better able to accept those of others. You try not to let what others are doing affect your own decisions, as you understand we are all different. Others notice that you feel at ease with yourself, and so feel they can be at ease around you, too. Unlike some, you don’t need validation to feel good about yourself.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "Internal"
			},
			{
				"stateName":"Love",
				"associatedQuestions":[
					"1",
					"2",
					"3",
					"4",
					"5",
					"6",
					"7",
					"8",
					"9",
					"10",
					"11",
					"12",
					"13",
					"14",
					"15",
					"16",
					"17",
					"18",
					"19",
					"20",
					"21",
					"22",
					"23",
					"24",
					"25",
					"26",
					"27",
					"28",
					"29",
					"30",
					"31",
					"32",
					"33",
					"34",
					"35",
					"36",
					"37",
					"38",
					"39",
					"40",
					"41",
					"42"
				],
				"associatedValues":[
					"Compassion",
					"Kindness \u0026 Caring",
					"Understanding",
					"Courage",
					"Letting Go",
					"Inclusivity",
					"Authenticity & Truthfulness",
					"Simplicity",
					"Humility",
					"Accepting & Non-Judgement",
					"Abundance",
					"Being Present",
					"Dedication \u0026 Surrender",
					"Being Responsible",
					"Positivity & Spirit",
					"Self-Awareness",
					"Freedom",
					"Self-Mastery",
					"Openness",
					"Innocence",
					"Excellence"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Excellence", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Self-Awareness", "Self-Mastery", "Understanding"],
					"Tier3": []
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Gratitude",
					"Humility",
					"Collaborative"
				],
				"mediumScoreDefinition":"When in this EmoState, you are quick to allow others into your life, and you enjoy working with people from diverse backgrounds and having different kinds of personalities. You usually focus on a person’s positives rather than their negatives. In this EmoState, you find it easy to apologize when you are in the wrong.",
				"highScoreResults":[
					"Purity",
					"Generosity",
					"Steadfast",
					"Forgiving",
					"Complete",
					"Gratitude",
					"Humility",
					"Collaborative"
				],
				"highScoreDefinition":"When in this EmoState, you are quick to allow others into your life, and you enjoy working with people from diverse backgrounds and having different kinds of personalities. You usually focus on a person’s positives rather than their negatives. In this EmoState, you find it easy to apologize when you are in the wrong. You care deeply about the needs and feelings of others, and strive to bring joy to their lives. Sights, sounds and smells trigger positive memories and emotions, and remind you how grateful you are to feel this way.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Peace & Serenity",
				"associatedQuestions":[
					"1",
					"2",
					"3",
					"4",
					"5",
					"6",
					"7",
					"8",
					"9",
					"10",
					"11",
					"12",
					"13",
					"14",
					"15",
					"16",
					"17",
					"18",
					"19",
					"20",
					"22",
					"23",
					"24",
					"25",
					"26",
					"27",
					"28",
					"29",
					"30",
					"31",
					"32",
					"33",
					"34",
					"35",
					"36",
					"37",
					"38",
					"39",
					"40",
					"41"
				],
				"associatedValues":[
					"Compassion",
					"Kindness \u0026 Caring",
					"Understanding",
					"Courage",
					"Letting Go",
					"Inclusivity",
					"Authenticity & Truthfulness",
					"Simplicity",
					"Humility",
					"Accepting & Non-Judgement",
					"Abundance",
					"Being Present",
					"Dedication \u0026 Surrender",
					"Being Responsible",
					"Positivity & Spirit",
					"Self-Awareness",
					"Freedom",
					"Self-Mastery",
					"Openness",
					"Innocence"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Humility", "Innocence", "Openness", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Compassion", "Kindness & Caring", "Inclusivity", "Positivity & Spirit"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Serenity"
				],
				"mediumScoreDefinition":"When in this EmoState, you’re able to remain calm and think things through even when life takes a negative turn. Some of your friends turn to you for direction in times of need, while others get frustrated that you are not more upset. They don’t understand that you won't let worry control your decisions. In this state, you understand that tomorrow is a new day, and most situations aren't as bad as they seem.",
				"highScoreResults":[
					"Timelessness",
					"Unity",
					"Total Freedom",
					"Serenity"
				],
				"highScoreDefinition":"When in this EmoState, every day holds new possibilities, and in your mind, you’re free to make whatever choices you feel are best for you. Your mistakes aren't mistakes; they're opportunities to learn, grow, and improve. You forgive rather than forget and understand instead of judge. In this state, you’re able to remain calm and think things through even when life takes a negative turn.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Clarity",
				"associatedQuestions":[
					16,
					37,
					8,
					29,
					3,
					24,
					12,
					33,
					7,
					28,
					20,
					41,
					17,
					38,
					19,
					40,
					21,
					42,
					5,
					26,
					9,
					30
				],
				"associatedValues":[
					"Self-Awareness",
					"Simplicity",
					"Understanding",
					"Being Present",
					"Authenticity & Truthfulness",
					"Innocence",
					"Freedom",
					"Openness",
					"Excellence",
					"Letting Go",
					"Humility"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Inclusivity", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Humility", "Innocence", "Openness", "Simplicity"],
					"Tier2": ["Kindness & Caring", "Positivity & Spirit", "Excellence", "Courage", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Ability to sort out complicated situations",
					"Ability to set priorities",
					"Creative thinking",
					"Eliminate waste"
				],
				"mediumScoreDefinition":"When in this EmoState, you're able to get things sorted through clear thinking when faced with tricky or overwhelming situations. You're able to set clear priorities and employ creativity to great effect. What's more is that, in this state, you're able to minimize wasteful use of time and resources on account of your well thought-out approach.",
				"highScoreResults":[
					"Very clear priorities",
					"Ability to do more with less",
					"Decisiveness in midst of chaos",
					"Ability to prevent complicated situations",
					"Innovation"
				],
				"highScoreDefinition":"When in this EmoState, you're usually successful at preventing things from getting too complicated or overwhelming. The same trait also helps you be decisive when brought into chaotic situations. In this EmoState, you're able to do more with less and to drive innovation by embracing constraints rather than resenting them.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Inspiration",
				"associatedQuestions":[
					16,
					37,
					21,
					42,
					8,
					29,
					13,
					34,
					11,
					32,
					19,
					40,
					15,
					36,
					12,
					33,
					5,
					26,
					7,
					28,
					9,
					30,
					17,
					38,
					6,
					27,
					18,
					39
				],
				"associatedValues":[
					"Self-Awareness",
					"Excellence",
					"Simplicity",
					"Dedication \u0026 Surrender",
					"Abundance",
					"Openness",
					"Positivity & Spirit",
					"Being Present",
					"Letting Go",
					"Authenticity & Truthfulness",
					"Humility",
					"Freedom",
					"Inclusivity",
					"Self-Mastery"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Authenticity & Truthfulness", "Openness", "Excellence", "Simplicity"],
					"Tier2": ["Letting Go", "Positivity & Spirit", "Courage", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Enjoying the journey",
					"Meaning & Purpose",
					"Fulfillment",
					"Internally motivated"
				],
				"mediumScoreDefinition":"When in this EmoState, you find meaning in most things you do. Pursuing purposeful things helps bring fulfillment to your life. What's more is that, in this state, you find motivation from within yourself to do the things that you do, and that helps you enjoy your life journey even more.",
				"highScoreResults":[
					"Inspiring vision for self and others",
					"Inclusive",
					"Inspire and lead others",
					"Inspire self and others to achieve Greatness"
				],
				"highScoreDefinition":"When in this EmoState, you’re more likely to have an inspiring vision that infuses all aspects of your life. Furthermore, others that come in contact with you are also drawn to your compelling vision. In this state, you're able to inspire others to achieve greatness, bringing immense amounts of meaning, purpose, fulfillment, and joy in everybody's lives along the way.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Patience",
				"associatedQuestions":[
					11, 32, 17, 38, 1, 22, 6, 27, 12, 33, 14, 35, 5, 26, 10, 31, 15, 36, 9, 30, 20, 41, 19, 40, 8, 29
				],
				"associatedValues":[
					"Abundance", "Freedom", "Compassion", "Inclusivity", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Simplicity"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Compassion", "Inclusivity", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Simplicity"],
					"Tier2": ["Kindness & Caring", "Authenticity & Truthfulness", "Excellence", "Courage", "Dedication & Surrender"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Not getting frustrated when running into a challenge or an unexpected turn of events",
					"Giving people the chance they need"
				],
				"mediumScoreDefinition":"When in this EmoState, you don’t get frustrated too easily. Even when you run into an unexpected challenge, you find a way not to lose your calm. In this state, you’re more likely to give others as well as yourself the chance they need.",
				"highScoreResults":[
					"Others can be themselves in your presence",
					"Making others feel comfortable asking for help",
					"Not getting frustrated when running into a challenge or an unexpected turn of events",
					"Giving people the chance they need"
				],
				"highScoreDefinition":"When in this EmoState, you don’t get frustrated too easily. Even when you run into an unexpected challenge, you find a way not to lose your calm. In this state, you’re more likely to give others as well as yourself the chance they need. Others can be themselves around you and they feel comfortable asking you for help.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName":"Perseverance",
				"associatedQuestions":[
					11, 32, 17, 38, 14, 35, 4, 25, 8, 29, 13, 34
				],
				"associatedValues":[
					"Abundance", "Freedom", "Being Responsible", "Courage", "Simplicity", "Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": ["Abundance", "Freedom", "Being Responsible", "Courage", "Simplicity", "Dedication & Surrender"],
					"Tier2": ["Excellence"],
					"Tier3": ["Self-Awareness", "Self-Mastery", "Understanding"]
				},
				"lowScoreResults":[

				],
				"lowScoreDefinition":"",
				"mediumScoreResults":[
					"Seeing things through",
					"Not losing sight of your objectives, even when they span longer periods of time"
				],
				"mediumScoreDefinition":"When in this EmoState, you see things through to completion. Once the decision has been made, you don’t lose sight of your objectives that easily. In this state, you can commit to longer term objectives, even when they’re difficult and you’re somewhat uncomfortable.",
				"highScoreResults":[
					"Staying Disciplines",
					"Keeping distractions to a minimum",
					"Seeing things through",
					"Not losing sight of your objectives, even when they span longer periods of time"
				],
				"highScoreDefinition":"When in this EmoState, you see things through to completion. Once the decision has been made, you don’t lose sight of your objectives that easily. In this state, you can commit to longer term objectives, even when they’re difficult and you’re somewhat uncomfortable. You can stay disciplined and keep distractions to a minimum if your pursuit demands as such.",
				"energyState":"Balanced",
				"reviseScore":false,
				"type": "User"
			},
			{
				"stateName": "Grit",
				"associatedQuestions": [],
				"associatedValues": [
					"Being Responsible",
					"Positivity & Spirit",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Being Responsible",
						"Positivity & Spirit",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Grit is the passion and perseverance for long term goals.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Grit is the passion and perseverance for long term goals.",
				"highScoreResults": [],
				"highScoreDefinition": "Grit is the passion and perseverance for long term goals.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Generalized Anxiety Disorder",
				"associatedQuestions": [],
				"associatedValues": [
					"Freedom",
					"Inclusivity",
					"Being Present",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Humility",
					"Simplicity"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Inclusivity",
						"Being Present",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Humility",
						"Simplicity"
					],
					"Tier2": [
						"Compassion",
						"Kindness & Caring",
						"Authenticity & Truthfulness",
						"Being Responsible",
						"Positivity & Spirit",
						"Innocence",
						"Openness",
						"Courage",
						"Dedication & Surrender"
					],
					"Tier3": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Generalized Anxiety Disorder involves excessive worrying and fear, often accompanied by irritability and other symptoms.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Generalized Anxiety Disorder involves excessive worrying and fear, often accompanied by irritability and other symptoms.",
				"highScoreResults": [],
				"highScoreDefinition": "Generalized Anxiety Disorder involves excessive worrying and fear, often accompanied by irritability and other symptoms.",
				"energyState": null,
				"reviseScore": true,
				"type": "Facilitator"
			},
			{
				"stateName": "Generalized Social Anxiety Disorder",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Compassion",
					"Kindness & Caring",
					"Inclusivity",
					"Being Present",
					"Being Responsible",
					"Positivity & Spirit",
					"Humility",
					"Innocence",
					"Openness",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Compassion",
						"Kindness & Caring",
						"Inclusivity",
						"Being Present",
						"Being Responsible",
						"Positivity & Spirit",
						"Humility",
						"Innocence",
						"Openness",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Generalized Social Anxiety Disorder is intense anxiety or fear of being judged, negatively evaluated, or rejected in a social or performance situation. It makes activities such as public speaking difficult.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Generalized Social Anxiety Disorder is intense anxiety or fear of being judged, negatively evaluated, or rejected in a social or performance situation. It makes activities such as public speaking difficult.",
				"highScoreResults": [],
				"highScoreDefinition": "Generalized Social Anxiety Disorder is intense anxiety or fear of being judged, negatively evaluated, or rejected in a social or performance situation. It makes activities such as public speaking difficult.",
				"energyState": null,
				"reviseScore": true,
				"type": "Facilitator"
			},
			{
				"stateName": "Generalized Self-Efficacy",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Being Present",
					"Being Responsible",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Positivity & Spirit",
					"Innocence",
					"Openness",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Being Present",
						"Being Responsible",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Positivity & Spirit",
						"Innocence",
						"Openness",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Generalized Self-Efficacy is the belief that one can perform novel or difficult tasks, or cope with adversity -- in various domains of human functioning.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Generalized Self-Efficacy is the belief that one can perform novel or difficult tasks, or cope with adversity -- in various domains of human functioning.",
				"highScoreResults": [],
				"highScoreDefinition": "Generalized Self-Efficacy is the belief that one can perform novel or difficult tasks, or cope with adversity -- in various domains of human functioning.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Depression",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Being Present",
					"Being Responsible",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Positivity & Spirit",
					"Innocence",
					"Openness",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Being Present",
						"Being Responsible",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Positivity & Spirit",
						"Innocence",
						"Openness",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Compassion",
						"Kindness & Caring",
						"Inclusivity",
						"Excellence"
					],
					"Tier3": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Depression—also called “clinical depression” or a “depressive disorder”—is a mood disorder that causes distressing symptoms that affect how you feel, think, and handle daily activities, such as sleeping, eating, or working.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Depression—also called “clinical depression” or a “depressive disorder”—is a mood disorder that causes distressing symptoms that affect how you feel, think, and handle daily activities, such as sleeping, eating, or working.",
				"highScoreResults": [],
				"highScoreDefinition": "Depression—also called “clinical depression” or a “depressive disorder”—is a mood disorder that causes distressing symptoms that affect how you feel, think, and handle daily activities, such as sleeping, eating, or working.",
				"energyState": null,
				"reviseScore": true,
				"type": "Facilitator"
			},
			{
				"stateName": "Big Five - Conscientiousness",
				"associatedQuestions": [],
				"associatedValues": [
					"Being Responsible",
					"Excellence",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Being Responsible",
						"Excellence",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Conscientiousness is a tendency to display self-discipline, act dutifully, and strive for achievement against measures or outside expectations.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Conscientiousness is a tendency to display self-discipline, act dutifully, and strive for achievement against measures or outside expectations.",
				"highScoreResults": [],
				"highScoreDefinition": "Conscientiousness is a tendency to display self-discipline, act dutifully, and strive for achievement against measures or outside expectations.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Big Five - Agreeableness",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Compassion",
					"Kindness & Caring",
					"Inclusivity",
					"Authenticity & Truthfulness",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Humility",
					"Innocence",
					"Openness",
					"Simplicity"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Compassion",
						"Kindness & Caring",
						"Inclusivity",
						"Authenticity & Truthfulness",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Humility",
						"Innocence",
						"Openness",
						"Simplicity"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "The agreeableness trait reflects individual differences in general concern for social harmony.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "The agreeableness trait reflects individual differences in general concern for social harmony.",
				"highScoreResults": [],
				"highScoreDefinition": "The agreeableness trait reflects individual differences in general concern for social harmony.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Big Five - Extraversion",
				"associatedQuestions": [],
				"associatedValues": [
					"Inclusivity",
					"Positivity & Spirit",
					"Innocence",
					"Openness",
					"(Inverse) Freedom"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Inclusivity",
						"Positivity & Spirit",
						"Innocence",
						"Openness",
						"(Inverse) Freedom"
					],
					"Tier2": [
						"(Inverse) Self-Awareness",
						"(Inverse) Self-Mastery",
						"(Inverse) Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Extraversion is characterized by breadth of activities (as opposed to depth), surgency from external activity/situations, and energy creation from external means.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Extraversion is characterized by breadth of activities (as opposed to depth), surgency from external activity/situations, and energy creation from external means.",
				"highScoreResults": [],
				"highScoreDefinition": "Extraversion is characterized by breadth of activities (as opposed to depth), surgency from external activity/situations, and energy creation from external means.",
				"energyState": null,
				"reviseScore": false,
				"customReviseScore": true,
				"type": "Facilitator"
			},
			{
				"stateName": "Big Five - Openness to Experience",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Innocence",
					"Openness",
					"Excellence"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Innocence",
						"Openness",
						"Excellence"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Openness to experience is a general appreciation for art, emotion, adventure, unusual ideas, imagination, curiosity, and variety of experience.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Openness to experience is a general appreciation for art, emotion, adventure, unusual ideas, imagination, curiosity, and variety of experience.",
				"highScoreResults": [],
				"highScoreDefinition": "Openness to experience is a general appreciation for art, emotion, adventure, unusual ideas, imagination, curiosity, and variety of experience.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Big Five - Neuroticism",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Compassion",
					"Kindness & Caring",
					"Inclusivity",
					"Authenticity & Truthfulness",
					"Being Present",
					"Being Responsible",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Positivity & Spirit",
					"Humility",
					"Innocence",
					"Openness",
					"Excellence",
					"Courage",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Compassion",
						"Kindness & Caring",
						"Inclusivity",
						"Authenticity & Truthfulness",
						"Being Present",
						"Being Responsible",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Positivity & Spirit",
						"Humility",
						"Innocence",
						"Openness",
						"Excellence",
						"Courage",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Neuroticism is the tendency to experience negative emotions, such as anger, anxiety, or depression.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Neuroticism is the tendency to experience negative emotions, such as anger, anxiety, or depression.",
				"highScoreResults": [],
				"highScoreDefinition": "Neuroticism is the tendency to experience negative emotions, such as anger, anxiety, or depression.",
				"energyState": null,
				"reviseScore": true,
				"type": "Facilitator"
			},
			{
				"stateName": "Entrepreneurial Vision",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Innocence",
					"Openness",
					"Excellence"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Innocence",
						"Openness",
						"Excellence"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Entrepreneurial Vision.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Entrepreneurial Vision.",
				"highScoreResults": [],
				"highScoreDefinition": "Entrepreneurial Vision.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Entrepreneurial Simplicity & Focus",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Letting Go",
					"Accepting & Non-Judgement",
					"Humility",
					"Simplicity",
					"Dedication & Surrender"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Letting Go",
						"Accepting & Non-Judgement",
						"Humility",
						"Simplicity",
						"Dedication & Surrender"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Entrepreneurial Simplicity & Focus.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Entrepreneurial Simplicity & Focus.",
				"highScoreResults": [],
				"highScoreDefinition": "Entrepreneurial Simplicity & Focus.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			},
			{
				"stateName": "Entrepreneurial Leadership",
				"associatedQuestions": [],
				"associatedValues": [
					"Abundance",
					"Freedom",
					"Compassion",
					"Kindness & Caring",
					"Inclusivity",
					"Authenticity & Truthfulness",
					"Being Responsible",
					"Positivity & Spirit",
					"Humility",
					"Courage"
				],
				"associatedValuesV2andV3": {
					"Tier1": [
						"Abundance",
						"Freedom",
						"Compassion",
						"Kindness & Caring",
						"Inclusivity",
						"Authenticity & Truthfulness",
						"Being Responsible",
						"Positivity & Spirit",
						"Humility",
						"Courage"
					],
					"Tier2": [
						"Self-Awareness",
						"Self-Mastery",
						"Understanding"
					]
				},
				"lowScoreResults": [],
				"lowScoreDefinition": "Entrepreneurial Leadership.",
				"mediumScoreResults": [],
				"mediumScoreDefinition": "Entrepreneurial Leadership.",
				"highScoreResults": [],
				"highScoreDefinition": "Entrepreneurial Leadership.",
				"energyState": null,
				"reviseScore": false,
				"type": "Facilitator"
			}
		],
		unique: ['stateName'],
		overwrite: true,
	}, // End of statequestionassociation

	valuemapping: {
		data: [
			{
			   "valueName":"Abundance",
			   "valueHint":"Generosity, Win-Win, Big Thinking, Vision",
			   "definition":"People with an abundance attitude take pleasure in sharing their own knowledge, life lessons, and even resources with others, as they are fully aware that there is plenty in this world for everyone to be able to succeed. Those who value it believe in a win-win approach; understanding the infinite potential we all hold, and knowing that through generosity we are able to think bigger, and achieve so much more.",
			   "benefits":[
				  "Unlock the power of generosity, and experience it for yourself!",
				  "Understand your own potential.",
				  "Build your network and abilities by sharing and building with others"
			   ],
			   "prerequisites": [
			   ],
			   "categoryID": 1,
			   "valueChecklist": "List_2",
			   "infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Abundance.mp4",
			   "infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Abundance.mp4',
			   "quote": "Our life is abundant, but only if we breathe, think, and act as such.",
			   "author": "Sneharshi",
			   "iAmStatement" : "have Abundance"
			},
			{
			   "valueName":"Accepting & Non-Judgement",
			   "valueHint":"Not Judging",
			   "definition":"People who value acceptance are able to acknowledge that we all have our differences, and these are our strengths. They are able to observe without judgment and evaluate reality for what it is. When we accept our world (and those in it) for what it truly is, we are able to work together, look forward, and become stronger for it. When we work with those who are different than ourselves, we complement each others’ strengths and cover each others’ weaknesses. Discovery comes from accepting who we are and allows us to begin creating an amazing future for ourselves as well as for our world.",
			   "benefits":[
				  "Learn how to observe and understand your world.",
				  "Discover the real you, and accept yourself in all your glory!",
				  "Filter through the arbitrary and understand what is."
			   ],
			   "prerequisites": [
				],
				"categoryID": 16,
				"valueChecklist": "List_17",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Accepting_NonJudgement.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Accepting_NonJudgement.mp4',
				"quote": "Defer judgements until you have the Truth. Once you have Truth, there will be no space for judgements.",
				"author": "Sneharshi",
				"iAmStatement" : "am Accepting & Non-judgemental."
			},
			{
			   "valueName":"Being Present",
			   "valueHint":"Focus, Concentration, Presence, Mindfulness",
			   "definition":"For those who value being present, they understand that it is better to live in the here and now. Being present when with someone else makes connecting and bonding much more natural and effortless, and in the context of work, improves our focus and concentration and allows us to do our best work. Those who value being present understand that past mistakes are there to be learned from and not dwelled on, and that more often than not, what we worry about in the future never actually happens. They value what they can control. They value what they can enjoy right now. They value the present.",
			   "benefits":[
				  "Don't just live life - experience it.",
				  "Increase your capacity to learn.",
				  "Increase your social skills and relationship building.",
				  "Allow yourself to forgive and forget past mistakes."
			   ],
			   "prerequisites": [
				],
				"categoryID": 2,
				"valueChecklist": "List_3",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Being_Present.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Being_Present.mp4',
				"quote": "Be present to experience the wholeness of your existence.",
				"author": "Sneharshi",
				"iAmStatement" : "am Present"
			},
			{
			   "valueName":"Being Responsible",
			   "valueHint":"Practice, Proactivity, Taking Responsibility, Excellence, Taking Action, Getting Things Done",
			   "definition":"The path of responsibility is harder to tread due to the pressure involved, but usually leads to much greater rewards. Those who value responsibility understand that in most situations, someone is required to step up and take the helm, even when that helm is a burden. We all have limited resources specific to ourselves, and being responsible means using them mindfully. Being Responsible starts with accepting that our own thoughts, words, and actions shape our lives. External circumstances are merely the backdrop, so blaming them gets us nowhere. Being responsible is about accepting power and treating it with the respect it deserves.",
			   "benefits":[
				  "Learn how your actions and words shape your life!",
				  "Learn how to better leverage your resources and abilities.",
				  "Have more energy and enthusiasm in your work and life."
			   ],
			   "prerequisites": [
				   "Positivity & Spirit",
				   "Dedication & Surrender",
				   "Being Present"
				],
				"categoryID": 19,
				"valueChecklist": "List_20",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Being_Responsible.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Being_Responsible.mp4',
				"quote": "There always are outside forces at play, but don't lose sight of the agency you possess. Using it to realize your potential, that's your responsibility.",
				"author": "Sneharshi",
				"iAmStatement" : "am Responsible"
			},
			{
			   "valueName":"Compassion",
			   "valueHint":"Feeling others’ pain, Desire to help",
			   "definition":"Those who value compassion attempt to feel what others are going through so they may offer help. They seek to expand their awareness beyond their own problems and needs, as they understand we thrive as a collective when we are compassionate. However, self-compassion is equally as important, as this allows us to also thrive as individuals. Compassion is having the desire to help and acting on it, whether that be with someone else, or our own internal needs.",
			   "benefits":[
				  "Expand your awareness beyond your own.",
				  "Learn how to thrive within a group setting.",
				  "Feel good while assisting others."
			   ],
			   "prerequisites": [
				],
				"categoryID": 13,
				"valueChecklist": "List_14",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Compassion.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Compassion.mp4',
				"quote": "Practice compassion to get in touch with your inner self.",
				"author": "Sneharshi",
				"iAmStatement" : "am Compassionate"
			},
			{
			   "valueName":"Self-Mastery",
			   "valueHint":"Discipline, Patience, Reflection, Composure",
			   "definition":"Life is never linear; it is made of many twists, turns, ups, downs, and obstacles. Those who value Self-Mastery seek composure even when at their lowest, darkest points, but also to be able to experience all of the joy from their highest, greatest achievements. It is a blend of discipline, patience, deep inner reflection, and resolve. This keen knowledge of all of their workings means they are able to weather any storm, which ultimately means they are capable of anything.",
			   "benefits":[
				  "Hone your self-discipline to an expert level.",
				  "Access your ability to keep perfect composure.",
				  "Learn how to reflect and grow."
			   ],
			   "prerequisites": [
					"Positivity & Spirit",
					"Being Present"
				],
				"categoryID": 9,
				"valueChecklist": "List_10",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Self_Mastery.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Self_Mastery.mp4',
				"quote": "Self-Mastery is the ability to identify things that are not you and to avoid or discard them. It's the capacity to reach your inner being and keep it unobstructed.",
				"author": "Sneharshi",
				"iAmStatement" : "have Self-Mastery"
			},
			{
			   "valueName":"Courage",
			   "valueHint":"Overcoming Fear, Resilience, Grit, Determination",
			   "definition":"Courage is the ability to do what you are too afraid to do, and those who value courage understand that courage is the gateway to a more fulfilled life. When we fail to act, we miss out on so many potential opportunities. Until you step outside your comfort zone, you have no idea what could be waiting for you on the other side. Remember - fortune favors the brave.",
			   "benefits":[
				  "Do things you never thought possible by learning what you are capable of.",
				  "Gain awareness of opportunities all around you and insights on how to take them.",
				  "Step outside your comfort zone."
			   ],
			   "prerequisites": [
					"Positivity & Spirit",
					"Dedication & Surrender"
				],
				"categoryID": 17,
				"valueChecklist": "List_18",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Courage.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Courage.mp4',
				"quote": "The greater the clarity about who you are and what you stand for, the greater the courage that arises naturally from within.",
				"author": "Sneharshi",
				"iAmStatement" : "am Courageous"
			},
			{
			   "valueName":"Dedication \u0026 Surrender",
			   "valueHint":"Meaning, Purpose, Motivation, Drive, loyalty",
			   "definition":"To truly dedicate oneself to a task or a cause is to surrender to it; by which we mean we do what is needed to be done rather than simply sticking to things that we like or prefer doing. When you commit to something bigger than yourself, something you truly believe in, you’re able to rise to a much higher level of awareness and existence. Those who value dedication understand that true attainment and success come from a willingness to walk the path, no matter what the circumstances.",
			   "benefits":[
				  "Connect with the work you do so stepping outside your comfort zone can happen effortlessly.",
				  "Submit yourself to worthy goals and 10x your achievements.",
				  "Break down mental barriers and hit your targets."
			   ],
			   "prerequisites": [
				],
				"categoryID": 8,
				"valueChecklist": "List_9",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Dedication.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Dedication.mp4',
				"quote": "Let go of the smaller you and find the bigger you for effortless success in the pursuit of meaningful goals.",
				"author": "Sneharshi",
				"iAmStatement" : "Dedicate and Surrender"
			},
			{
			   "valueName":"Letting Go",
			   "valueHint":"Forgiveness, Moving On",
			   "definition":"It is easier to forget than it is to forgive, but letting go is to move past the things that hold us back so we can thrive and truly blossom. We all make mistakes from time to time, and those who value Letting Go understand that holding onto resentment can actually hinder our progress as it draws our focus to unhealthy negativity, rather than to the task at hand. The same goes for yourself. Forgive yourself your past digressions, move on, and become a better version of yourself.",
			   "benefits":[
				  "Remove self-inflicted burdens from your life.",
				  "Gain clarity from the present by accepting the past.",
				  "Forgive yourself and learn how to move forwards."
			   ],
			   "prerequisites": [
					"Positivity & Spirit",
					"Being Present",
					"Compassion",
					"Abundance",
					"Freedom",
					"Humility"
				],
				"categoryID": 3,
				"valueChecklist": "List_4",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Letting_Go.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Letting_Go.mp4',
				"quote": "Let go of what you're not to become who you are: boundless, conscious bliss. That is the Truth.",
				"author": "Sneharshi",
				"iAmStatement" : "Let Go"
			},
			{
			   "valueName":"Freedom",
			   "valueHint":"Freedom from Greed, Non-attachment, Relaxation, Non-possessiveness",
			   "definition":"Freedom is a way of thinking and living outside the limiting confines and structures we create around ourselves. When we don’t link our identity, state of mind, thoughts, and happiness to external things, we are able to enjoy them even more. It is one of the core things we seek in our lives, and ironically, it’s something that only we have the ability to give ourselves. Those who value freedom value the ability to be able to choose—or in many cases, carve out—their own path in life. Freedom is the ability to ask ‘why not?’ and to be grounded in truth.",
			   "benefits":[
				  "Turn on your inner switch and gain control like never before.",
				  "Remove your reliance on material objects and focus on the right things.",
				  "Open doorways to success you were unable to see before!"
			   ],
			   "prerequisites": [
					"Positivity & Spirit",
					"Abundance"
				],
				"categoryID": 10,
				"valueChecklist": "List_11",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Freedom.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Freedom.mp4',
				"quote": "Freedom is transitioning from 'musts' to conscious choices. It's your choice. It always is.",
				"author": "Sneharshi",
				"iAmStatement" : "am Free"
			},
			{
			   "valueName":"Authenticity & Truthfulness",
			   "valueHint":"Truthfulness, Integrity, Morality, Self-Belief",
			   "definition":"Authenticity & Truthfulness begin with being honest with ourselves, but this can be hard as many times we are not even aware when we’re being self-deceptive. It’s by replacing this kind of self-deception with authenticity and truthfulness that we can truly start living with integrity (being true to who we are) and kick our growth engine into high gear. Stretching the truth is like building on a weak foundation. Not being authentic can lead to negative feelings about ourselves, which in turn affects our confidence and belief in our own abilities.",
			   "benefits":[
				  "Improve decision making by removing negative options.",
				  "Improve your social skills.",
				  "Understand what you find important in yourself and others.",
				  "Build a strong career."
			   ],
			   "prerequisites": [
				],
				"categoryID": 4,
				"valueChecklist": "List_5",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Authenticity.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Authenticity.mp4',
				"quote": "Be grounded in your own real self to find your unique place of strength and grace in this world.",
				"author": "Sneharshi",
				"iAmStatement" : "am Authentic"
			},
			{
			   "valueName":"Humility",
			   "valueHint":"Openness to Feedback, Humbleness, Modesty",
			   "definition":"It is important to understand our value, but humility allows us to see others’ true worth, which goes a long way in terms of building strong personal and professional relationships. Those who value humility understand the importance of completing accomplishments for oneself and are more likely to take pleasure in the task itself, rather than seeking validation from others. Without humility, we’re blindsided by our shortcomings and errors, which in turn hinders our own progress.",
			   "benefits":[
				  "Stop hindering your own progress with ego-driven decision making.",
				  "Understand your true worth.",
				  "Remove the need for validation."
			   ],
			   "prerequisites": [
				],
				"categoryID": 5,
				"valueChecklist": "List_6",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Humility.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Humility.mp4',
				"quote": "Pride curbs your attention to a minuscule portion of yourself. Humility affords the opportunity to discern your entirety.",
				"author": "Sneharshi",
				"iAmStatement" : "am Humble"
			},
			{
			   "valueName":"Innocence",
			   "valueHint":"No Prejudice",
			   "definition":"When we talk of Innocence, we remove prejudice, judgment, ego, and malice from our thoughts. Those who value innocence have an inherent belief in the goodness of others. There is simplicity in thinking and a sense of wonder—the beginner's mindset. We may fumble and tumble, but we get up and try again. There is no bruised ego to nurse and no entitlement to preferred outcomes created from layers of ‘grown-up’ habits.",
			   "benefits":[
				  "Feel joy and wonder in everyday moments.",
				  "Remove your ego to build your resolve.",
				  "Greatly improve your ‘human experience’ by living life without entitlement."
			   ],
			   "prerequisites": [
				],
				"categoryID": 6,
				"valueChecklist": "List_7",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Innocence.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Innocence.mp4',
				"quote": "Innocence allows us to see the glass half full in every situation, every person, and everything, including ourselves.",
				"author": "Sneharshi",
				"iAmStatement" : "am Innocent"
			},
			{
			   "valueName":"Kindness \u0026 Caring",
			   "valueHint":"Helping, Being Gentle, Mentoring",
			   "definition":"Kindness is a virtue, and if passed on, can have tremendous knock-on effects. A simple smile or helping hand to a stranger down on their luck can have huge positive implications for both them and the people they meet. Caring, in turn, emerges from knowing the value and importance of other people and things beyond ourselves, and the more kind and caring we are to others, the more kindness and caring we find in our own lives. True kindness is the willingness to help even when it’s not convenient to do so.",
			   "benefits":[
				  "Learn how helping others is key to helping ourselves.",
				  "Understand the value of others and why this is so important.",
				  "Create stronger, lasting relationships built on foundations of kindness."
			   ],
			   "prerequisites": [
				],
				"categoryID": 14,
				"valueChecklist": "List_15",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Kindness_Caring.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Kindness_Caring.mp4',
				"quote": "Kindness is the touchstone for Love.",
				"author": "Sneharshi",
				"iAmStatement" : "am Kind and Caring"
			},
			{
			   "valueName":"Openness",
			   "valueHint":"Exploration, Learning, Growth, Curiosity, Adaptability",
			   "definition":"When someone practices openness, they are less likely to tie their fate to certain pre-specified outcomes. Thus, the more open we are, the freer and happier we can become. Openness can be driven by (and can lead to) curiosity, so those who practice it are usually more open to new experiences. This means new doors become open to us, and we understand that there is more to life than what we’ve seen and experienced so far.",
			   "benefits":[
				  "Gain access to new experiences.",
				  "Create new, important relationships.",
				  "Spark your curiosity, and learn more about yourself.",
				  "Remove fear from your decision."
			   ],
			   "prerequisites": [
				],
				"categoryID": 18,
				"valueChecklist": "List_19",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Openness.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Openness.mp4',
				"quote": "Be open, for if you're not, you're closed. And closed systems can thrive only so much.",
				"author": "Sneharshi",
				"iAmStatement" : "am Open"
			},
			{
			   "valueName":"Positivity & Spirit",
			   "valueHint":"Energy, Hope, Confidence, Positive Thinking, Enjoying the Journey, Enthusiasm, Gratitude",
			   "definition":"Those who embrace positivity and exude spirit take everything in their stride, and are able to view any situation through a positive lens. Rather than forever looking for what they don’t have, they are able to be grateful for what they do. Those who value positivity and spirit can see the huge importance this simple change of perspective can have: defeats become learning experiences, obstacles become growth opportunities, and successes become internalized.",
			   "benefits":[
				  "Change every defeat into a learning experience.",
				  "Become grateful for the positives in your life.",
				  "Accept your inner power and use it to build and grow."
			   ],
			   "prerequisites": [
				],
				"categoryID": 12,
				"valueChecklist": "List_13",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Positivity.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Positivity.mp4',
				"quote": "Positivity is our innate nature. Thus, being positive is simply the practice of centering oneself i.e. returning to one's true essence whenever thrown off-balance.",
				"author": "Sneharshi",
				"iAmStatement" : "am Positive and Spirited"
			},
			{
			   "valueName":"Inclusivity",
			   "valueHint":"Sharing, Not Self-Centered",
			   "definition":"It is a wonderful virtue to take pleasure in helping others while also taking care of our own selves. Those who value inclusivity understand that people who receive help are much more likely to pay it forward. To practice inclusivity is to set in motion a continuous positive impact which improves the human condition. It is not about ignoring our own needs, however, but rather putting others’ needs above our non-essential wants.",
			   "benefits":[
				  "Find deep pleasure in helping others.",
				  "Understand what you need to thrive and what is excess.",
				  "Leverage your strengths to create a better environment for yourself and others."
			   ],
			   "prerequisites": [
				],
				"categoryID": 7,
				"valueChecklist": "List_8",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Inclusivity.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Inclusivity.mp4',
				"quote": "Inclusivity is the magic ingredient for forging authentic connections and lasting bonds.",
				"author": "Sneharshi",
				"iAmStatement" : "am Inclusive"
			},
			{
			   "valueName":"Simplicity",
			   "valueHint":"Clear Thinking, Clear Priorities, Minimalism, Decluttering",
			   "definition":"It is easy to overcomplicate most aspects of our lives. We are encouraged to keep building our empires and collecting things as we pursue a myriad of different possibilities at the same time. Those who value simplicity understand that it’s about getting to the heart of what matters while ignoring the peripheral. Simplicity is about not spreading ourselves too thin so that we can truly enjoy what’s essential.",
			   "benefits":[
				  "Understand what really matters and learn how to stay focused.",
				  "Gain clarity in your thought processes.",
				  "Consolidate and appreciate what you have, creating space in your life."
			   ],
			   "prerequisites": [
				],
				"categoryID": 11,
				"valueChecklist": "List_12",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Simplicity.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Simplicity.mp4',
				"quote": "Complexity keeps your intellect occupied and distracted. Simplify so you don't lose touch with the essential, starting with your Self.",
				"author": "Sneharshi",
				"iAmStatement" : "am a Simplifier"
			},
			{
			   "valueName":"Understanding",
			   "valueHint":"Listening, Wisdom, Preparation",
			   "definition":"Sometimes, all it requires is a sympathetic ear and a sensitivity to another’s feelings. We may all be cut from the same cloth, but the experiences we all go through can differ wildly. Those who value understanding are able to think outside the confines of their own experiences and appreciate not every circumstance is the same. When we understand ourselves, we are better able to ignite our own growth by making more grounded life decisions. By understanding the world we live in, we can see our place in it, giving us greater significance and a stronger feel of purpose.",
			   "benefits":[
				  "Understand yourself to drive your growth more effectively.",
				  "Understand others so as to have thriving and synergistic relationships.",
				  "Make optimal decisions based on your own insights."
			   ],
			   "prerequisites": [
				],
				"categoryID": 15,
				"valueChecklist": "List_16",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Understanding.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Understanding.mp4',
				"quote": "The most effective tool is worthless in your hands if you don't understand it. Seek to understand Life—beginning with your Self—and flourish or be prepared for remorse and grief at the end of it.",
				"author": "Sneharshi",
				"iAmStatement" : "am Understanding"
			},
			{
			   "valueName":"Self-Awareness",
			   "valueHint":"Self-Awareness, Self-Discovery, Conscious Goal Selection, Conscious Motives, Core Value Awareness, Emo-State Awareness, Energy Awareness",
			   "definition":"Without awareness, there can be no understanding. And without understanding, we can’t be in the driver’s seat, can we? Our ability to do the best in our lives is predicated on our ability to understand ourselves, and that cannot happen without a sufficient level of Self-Awareness. It’s the common thread underlying all other values such as Understanding, Self-Mastery, Courage, Being Responsible, Being Present, and so on.",
			   "benefits":[
				  "Break down boundaries by acknowledging and leveraging your own abilities.",
				  "Experience life as the incredible individual you are!",
				  "Bring about deep and impactful shifts with greater clarity and confidence."
			   ],
			   "prerequisites": [
					"Positivity & Spirit",
					"Being Present",
					"Self-Mastery"
				],
				"categoryID": 21,
				"valueChecklist": "List_22",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Self_Awareness.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Self_Awareness.mp4',
				"quote": "No awareness, no understanding; No understanding, no conscious progress; No conscious progress, no lasting joy; No lasting joy, misery.",
				"author": "Sneharshi",
				"iAmStatement" : "am Self-Aware"
			},
			{
				"valueName":"Excellence",
				"valueHint":"Setting High Standards, Pushing Boundaries, Doing Your Best",
				"definition":"When we love someone or something, we want to do better than good, don't we? That's excellence at play right there. Here we're not excited by the idea of cutting corners, but instead, we're motivated to set high standards and to push boundaries. That's because we care and want to do our best.",
				"benefits":[
					"Start actualizing your potential at a faster rate.",
					"Be outstanding.",
					"Nurture people and initiatives so they reach their pinnacle."
				],
				"prerequisites": [
				],
				"categoryID": 20,
				"valueChecklist": "List_21",
				"infoVideo": "https://d2qoy7rbg08fi8.cloudfront.net/Values/Excellence.mp4",
				"infoVideoS3URL":'https://s3.us-east-2.amazonaws.com/triumph-staging-contents/Values/Excellence.mp4',
				"quote": "From the ongoing practice of doing your best and expanding your boundaries emerges Excellence.",
				"author": "Sneharshi",
				"iAmStatement" : "pursue Excellence"
			}
		],
		unique: ['valueName'],
		overwrite: true,
	}, // End of valuemapping

	trnorgsurvey: {
		data: [
			{
				'sectionVersion': 1,
				'sectionID': '1',
				'isDefault': true,
				'sectionTitle': 'Generalized Self-Efficacy survey',
				'overviewBlock': "",
				'sectionIntro': 'How much do you agree with the following statements?',
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': false,
				'scaleUnits': [
					{
						"label": "Not at all true",
						"value": 1
					},
					{
						"label": "Hardly true",
						"value": 2
					},
					{
						"label": "Moderately true",
						"value": 3
					},
					{
						"label": "Exactly true",
						"value": 4
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'I can always manage to solve difficult problems if I try hard enough.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'If someone opposes me, I can find the means and ways to get what I want.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'It is easy for me to stick to my aims and accomplish my goals.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I am confident that I could deal efficiently with unexpected events.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Thanks to my resourcefulness, I know how to handle unforeseen situations.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I can solve most problems if I invest the necessary effort.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I can remain calm when facing difficulties because I can rely on my coping abilities.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'When I am confronted with a problem, I can usually find several solutions.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'If I am in trouble, I can usually think of a solution.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I can usually handle whatever comes my way.',
						'requiresScoreInversion': false
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '2',
				'isDefault': true,
				'sectionTitle': 'Grit-S survey',
				'overviewBlock': "",
				'sectionIntro': "<p>Here are a number of statements that may or may not apply to you.</p> <p> For the most accurate score, when responding, think of how you compare to most people -- not just the people you know well, but most people in the world. </p><p style='margin-bottom: 0px'> There are no right or wrong answers, so just answer honestly!<p>",
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': false,
				'scaleUnits': [
					{
						"label": "Not like me at all",
						"value": 1
					},
					{
						"label": "Not much like me",
						"value": 2
					},
					{
						"label": "Somewhat like me",
						"value": 3
					},
					{
						"label": "Mostly like me",
						"value": 4
					},
					{
						"label": "Very much like me",
						"value": 5
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'New ideas and projects sometimes distract me from previous ones.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Setbacks don’t discourage me.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I have been obsessed with a certain idea or project for a short time but later lost interest.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I am a hard worker.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I often set a goal but later choose to pursue a different one.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I have difficulty maintaining my focus on projects that take more than a few months to complete.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I finish whatever I begin.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I am diligent.',
						'requiresScoreInversion': false
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '3',
				'isDefault': true,
				'sectionTitle': 'GAD-7 survey',
				'overviewBlock': "",
				'sectionIntro': 'Over the last <u>2 weeks</u>, how often have you been bothered by the following problems?',
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': false,
				'scaleUnits': [
					{
						"label": "Not at all true",
						"value": 0
					},
					{
						"label": "Several days",
						"value": 1
					},
					{
						"label": "More than half the days",
						"value": 2
					},
					{
						"label": "Nearly every day",
						"value": 3
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'Feeling nervous, anxious or on edge',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Not being able to stop or control worrying',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Worrying too much about different things',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Trouble relaxing',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Being so restless that it is hard to sit still',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Becoming easily annoyed or irritable',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Feeling afraid as if something awful might happen',
						'requiresScoreInversion': false
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '4',
				'isDefault': true,
				'sectionTitle': 'Mini-SPIN for GSAD survey',
				'overviewBlock': "",
				'sectionIntro': 'Rate or answer the following prompts based on the <u>past week.</u>',
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': false,
				'scaleUnits': [
					{
						"label": "Not at all",
						"value": 0
					},
					{
						"label": "A little bit",
						"value": 1
					},
					{
						"label": "Somewhat",
						"value": 2
					},
					{
						"label": "Very much",
						"value": 3
					},
					{
						"label": "Extremely",
						"value": 4
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'Fear of embarrassment causes me to avoid doing things or speaking to people.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I avoid activities in which I am the center of attention.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Being embarrassed or looking stupid are among my worst fears.',
						'requiresScoreInversion': false
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '5',
				'isDefault': true,
				'sectionTitle': 'Beck Depression Inventory (BDI)',
				'overviewBlock': "",
				'sectionIntro': "<p>This questionnaire consists of 21 groups of statements. </p> <p>Please read each group of statements carefully. And then pick out the one statement in each group that best describes the way you have been feeling during the <u>past two weeks</u>, including today.</p><p style='margin-bottom:0px'> If several statements in the group seem to apply equally well, select the highest number for that group.</p>",
				'introTipText': '',
				'responseFormat': 'multiple-choice-radio-options',
				'statementsAndPrompts': [
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I do not feel sad",
								"value": 0
							},
							{
								"label": "I feel sad much of the time",
								"value": 1
							},
							{
								"label": "I am sad all the time",
								"value": 2
							},
							{
								"label": "I am so sad or unhappy that I can't stand it",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I am not discouraged about my future",
								"value": 0
							},
							{
								"label": "I feel more discouraged about my future than I used to",
								"value": 1
							},
							{
								"label": "I do not expect things to work out for me",
								"value": 2
							},
							{
								"label": "I feel my future is hopeless and will only get worse",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I do not feel like a failure",
								"value": 0
							},
							{
								"label": "I have failed more than I should have",
								"value": 1
							},
							{
								"label": "As I look back, I see a lot of failures",
								"value": 2
							},
							{
								"label": "I feel I am a total failure as a person",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I get as much pleasure as I ever did from the things I enjoy",
								"value": 0
							},
							{
								"label": "I don't enjoy things as much as I used to",
								"value": 1
							},
							{
								"label": "I get very little pleasure from the things I used to enjoy",
								"value": 2
							},
							{
								"label": "I can't get any pleasure from the things I used to enjoy",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I don't feel particularly guilty",
								"value": 0
							},
							{
								"label": "I feel guilty over many things I have done or should have done",
								"value": 1
							},
							{
								"label": "I feel quite guilty most of the time",
								"value": 2
							},
							{
								"label": "I feel guilty all of the time",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I don't feel I am being punished",
								"value": 0
							},
							{
								"label": "I feel I may be punished",
								"value": 1
							},
							{
								"label": "I expect to be punished",
								"value": 2
							},
							{
								"label": "I feel I am being punished",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I feel the same about myself as ever",
								"value": 0
							},
							{
								"label": "I have lost confidence in myself",
								"value": 1
							},
							{
								"label": "I am disappointed in myself",
								"value": 2
							},
							{
								"label": "I dislike myself",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I don't criticize or blame myself more than usual",
								"value": 0
							},
							{
								"label": "I am more critical of myself than I used to be",
								"value": 1
							},
							{
								"label": "I criticize myself for all of my faults",
								"value": 2
							},
							{
								"label": "I blame myself for everything bad that happens",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I don't have any thoughts of killing myself",
								"value": 0
							},
							{
								"label": "I have thoughts of killing myself, but I would not carry them out",
								"value": 1
							},
							{
								"label": "I would like to kill myself",
								"value": 2
							},
							{
								"label": "I would kill myself if I had the chance",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I don't cry anymore than I used to",
								"value": 0
							},
							{
								"label": "I cry more than I used to",
								"value": 1
							},
							{
								"label": "I cry over every little thing",
								"value": 2
							},
							{
								"label": "I feel like crying, but I can't",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I am no more restless or wound up than usual",
								"value": 0
							},
							{
								"label": "I feel more restless or wound up than usual",
								"value": 1
							},
							{
								"label": "I am so restless or agitated, it's hard to stay still",
								"value": 2
							},
							{
								"label": "I am so restless or agitated that I have to keep moving or doing something",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I have not lost interest in other people or activities",
								"value": 0
							},
							{
								"label": "I am less interested in other people or things than before",
								"value": 1
							},
							{
								"label": "I have lost most of my interest in other people or things",
								"value": 2
							},
							{
								"label": "It's hard to get interested in anything",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I make decisions about as well as ever",
								"value": 0
							},
							{
								"label": "I find it more difficult to make decisions than usual",
								"value": 1
							},
							{
								"label": "I have much greater difficulty in making decisions than I used to",
								"value": 2
							},
							{
								"label": "I have trouble making any decisions",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I do not feel I am worthless",
								"value": 0
							},
							{
								"label": "I don't consider myself as worthwhile and useful as I used to",
								"value": 1
							},
							{
								"label": "I feel more worthless as compared to others",
								"value": 2
							},
							{
								"label": "I feel utterly worthless",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I have as much energy as ever",
								"value": 0
							},
							{
								"label": "I have less energy than I used to have",
								"value": 1
							},
							{
								"label": "I don't have enough energy to do very much",
								"value": 2
							},
							{
								"label": "I don't have enough energy to do anything",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I have not experienced any change in my sleeping",
								"value": 0
							},
							{
								"label": "I sleep somewhat more than usual",
								"value": 1.1
							},
							{
								"label": "I sleep somewhat less than usual",
								"value": 1.2
							},
							{
								"label": "I sleep a lot more than usual",
								"value": 2.1
							},
							{
								"label": "I sleep a lot less than usual",
								"value": 2.2
							},
							{
								"label": "I sleep most of the day",
								"value": 2.3
							},
							{
								"label": "I wake up 1-2 hours early and can't get back to sleep",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I am not more irritable than usual",
								"value": 0
							},
							{
								"label": "I am more irritable than usual",
								"value": 1
							},
							{
								"label": "I am much more irritable than usual",
								"value": 2
							},
							{
								"label": "I am irritable all the time",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I have not experienced any change in my appetite",
								"value": 0
							},
							{
								"label": "My appetite is somewhat less than usual",
								"value": 1.1
							},
							{
								"label": "My appetite is somewhat greater than usual",
								"value": 1.2
							},
							{
								"label": "My appetite is much less than before",
								"value": 2.1
							},
							{
								"label": "My appetite is much greater than usual",
								"value": 2.2
							},
							{
								"label": "I have no appetite at all",
								"value": 3.1
							},
							{
								"label": "I crave food all the time",
								"value": 3.2
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I can concentrate as well as ever",
								"value": 0
							},
							{
								"label": "I can't concentrate as well as usual",
								"value": 1
							},
							{
								"label": "It's hard to keep my mind on anything for very long",
								"value": 2
							},
							{
								"label": "I find I can't concentrate on anything",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I am no more tired or fatigued than usual",
								"value": 0
							},
							{
								"label": "I get more tired or fatigued more easily than usual",
								"value": 1
							},
							{
								"label": "I am too tired or fatigued to do a lot of the things I used to do",
								"value": 2
							},
							{
								"label": "I am too tired or fatigued to do most of the things I used to do",
								"value": 3
							}
						]
					},
					{
						'prompt': '',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "I have not noticed any recent change in my interest in sex",
								"value": 0
							},
							{
								"label": "I am less interested in sex than I used to be",
								"value": 1
							},
							{
								"label": "I am much less interested in sex now",
								"value": 2
							},
							{
								"label": "I have lost interest in sex completely",
								"value": 3
							}
						]
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '6',
				'isDefault': true,
				'sectionTitle': 'Life Mastery Scale',
				'overviewBlock': "",
				'sectionIntro': "<p>Here are a number of statements that may or may not apply to you.</p><p> For example, do you agree that you are someone who has great physical health?</p> <p style='font-family:Pangram-Light; font-size:16px; margin-bottom:0px'> Please rate these statements on the 7-point scale ranging from <i>Very Untrue</i> to <i>Very True</i> based on how much they are true for you.</p>",
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': true,
				'scaleUnits': [
					{
						"label": "Very Untrue",
						"value": 0
					},
					{
						"label": "Mostly Untrue",
						"value": 1
					},
					{
						"label": "Somewhat Untrue",
						"value": 2
					},
					{
						"label": "Neutral",
						"value": 3
					},
					{
						"label": "Somewhat True",
						"value": 4
					},
					{
						"label": "Mostly True",
						"value": 5
					},
					{
						"label": "Very True",
						"value": 6
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'I have the necessary knowledge to be successful in life.',
						'requiresScoreInversion': false,
					},
					{
						'prompt': "My physical health is great with no physical conditions that limit my other abilities.",
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I have the physical energy and capacity to do what I need or want to do everyday.',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'I have people, places, knowledge, or experiences that help me stay grounded and true e.g. getting back on track whenever I get lost.',
						'requiresScoreInversion': false
					},
					{
						'prompt': "I know I'll be fine no matter what.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I have built up or gathered enough assets, goodwill, or capabilities to fuel the rest of my life journey.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am able to easily adapt to changing circumstances in order to meet my personal and professional obligations even when I have many conflicting demands.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I keep encountering challenges on my professional journey.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am extremely pleased with what I've accomplished professionally so far.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am doing exactly the kind of work that I know I should be doing.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I have clearly defined professional and career-related goals that I'm very excited about pursuing.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I know I will do well professionally in the coming years.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I devote the necessary amount of time, attention, and effort to my work.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I keep having problems with accummulating or growing my financial wealth.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am extremely pleased with my current financial standing.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am very purposeful in terms of how I earn, spend, and save money.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I have a solid financial plan that takes into account my short and long term obligations as well as aspirations.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I know I will do well financially in the coming years.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I devote the necessary amount of time, attention, and effort to managing my finances",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I experience troubles in my close relationships again and again.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I am extremely pleased with the network of people I've built relationships with.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I have several uplifting relationships.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I have a very clear sense about what I'd like my key relationships to be like in the longer term.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I know I will thrive socially in the coming years.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I devote the necessary amount of time, attention, and effort to my relationships.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I'm able to bring about growth in any aspect of my life as necessary.",
						'requiresScoreInversion': false
					},
					{
						'prompt': "I keep expanding the depth and range of things I learn and do.",
						'requiresScoreInversion': false
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '7',
				'isDefault': true,
				'sectionTitle': 'General Demographics',
				'overviewBlock': "",
				'sectionIntro': 'Please answer the questions below.',
				'introTipText': '',
				'responseFormat': 'open-ended-questions',
				'statementsAndPrompts': [
					{
						'prompt': "What is your age?",
						'tip': "Please enter a number in the age field to continue",
						'responseFormat': 'single-line-text-input',
						'inputType': 'number'
					},
					{
						'prompt': "What's your gender?",
						'tip': "",
						'responseFormat': 'single-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Male",
								"value": "Male"
							},
							{
								"label": "Female",
								"value": "Female"
							},
							{
								"label": "Other",
								"value": "Other"
							}
						],
					},
					{
						'prompt': "What is your ethnicity?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "American Indian or Alaska Native",
								"value": "American Indian or Alaska Native"
							},
							{
								"label": "Asian",
								"value": "Asian"
							},
							{
								"label": "Black or African American",
								"value": "Black or African American"
							},
							{
								"label": "Hispanic or Latino",
								"value": "Hispanic or Latino"
							},
							{
								"label": "Native Hawaiian or Other Pacific Islander",
								"value": "Native Hawaiian or Other Pacific Islander"
							},
							{
								"label": "White",
								"value": "White"
							}
						]
					},
					{
						'prompt': "What is the highest degree or level of school you have completed?",
						'tip': "If you're currently enrolled in school, please indicate the highest degree you have received.",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Less than a high school diploma",
								"value": "Less than a high school diploma"
							},
							{
								"label": "High school degree or equivalent",
								"value": "High school degree or equivalent"
							},
							{
								"label": "Bachelor's degree (e.g. BA, BS)",
								"value": "Bachelor's degree (e.g. BA, BS)"
							},
							{
								"label": "Master's degree (e.g. MA, MS, MEd)",
								"value": "Master's degree (e.g. MA, MS, MEd)"
							},
							{
								"label": "Doctorate (e.g. PhD, EdD)",
								"value": "Doctorate (e.g. PhD, EdD)"
							},
							{
								"label": "Other",
								"value": "Other"
							}
						]
					},
					{
						'prompt': "What is your current employment status?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Employed full-time (40+hours per week)",
								"value": "Employed full-time (40+hours per week)"
							},
							{
								"label": "Employed part-time (less than 40 hours per week)",
								"value": "Employed part-time (less than 40 hours per week)"
							},
							{
								"label": "Unemployed (currently looking for work)",
								"value": "Unemployed (currently looking for work)"
							},
							{
								"label": "Unemployed (NOT currently looking for work)",
								"value": "Unemployed (NOT currently looking for work)"
							},
							{
								"label": "Student",
								"value": "Student"
							},
							{
								"label": "Retired",
								"value": "Retired"
							},
							{
								"label": "Self-employed",
								"value": "Self-employed"
							},
							{
								"label": "Unable to work",
								"value": "Unable to work"
							}
						]
					},
					{
						'prompt': "What is your marital status?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Single (never married)",
								"value": "Single (never married)"
							},
							{
								"label": "Married",
								"value": "Married"
							},
							{
								"label": "In a domestic partnership",
								"value": "In a domestic partnership"
							},
							{
								"label": "Divorced",
								"value": "Divorced"
							},
							{
								"label": "Widowed",
								"value": "Widowed"
							}
						]
					},
					{
						'prompt': "What is your household income?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Below $10k",
								"value": "Below $10k"
							},
							{
								"label": "$10k-$50k",
								"value": "$10k-$50k"
							},
							{
								"label": "$50-$100k",
								"value": "$50-$100k"
							},
							{
								"label": "$100k-$150k",
								"value": "$100k-$150k"
							},
							{
								"label": "$150k-$250k",
								"value": "$150k-$250k"
							},
							{
								"label": "Over $250k",
								"value": "Over $250k"
							}
						]
					},
					{
						'prompt': "Which of the following best describes your current occupation?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Life, Physical, and Social Science Occupations",
								"value": "Life, Physical, and Social Science Occupations"
							},
							{
								"label": "Legal Occupations",
								"value": "Legal Occupations"
							},
							{
								"label": "Education, Training, and Library Occupations",
								"value": "Education, Training, and Library Occupations"
							},
							{
								"label": "Personal Care and Service Occupations",
								"value": "Personal Care and Service Occupations"
							},
							{
								"label": "Healthcare Support Occupations",
								"value": "Healthcare Support Occupations"
							},
							{
								"label": "Installation, Maintenance, and Repair Occupations",
								"value": "Installation, Maintenance, and Repair Occupations"
							},
							{
								"label": "Management Occupations",
								"value": "Management Occupations"
							},
							{
								"label": "Protective Service Occupations",
								"value": "Protective Service Occupations"
							},
							{
								"label": "Computer and Mathematical Occupations",
								"value": "Computer and Mathematical Occupations"
							},
							{
								"label": "Architecture and Engineering Occupations",
								"value": "Architecture and Engineering Occupations"
							},
							{
								"label": "Construction and Extraction Occupations",
								"value": "Construction and Extraction Occupations"
							},
							{
								"label": "Sales and Related Occupations",
								"value": "Sales and Related Occupations"
							},
							{
								"label": "Healthcare Practitioners and Technical Occupations",
								"value": "Healthcare Practitioners and Technical Occupations"
							},
							{
								"label": "Food Preparation and Serving Related Occupations",
								"value": "Food Preparation and Serving Related Occupations"
							},
							{
								"label": "Business and Financial Operations Occupations",
								"value": "Business and Financial Operations Occupations"
							},
							{
								"label": "Office and Administrative Support Occupations",
								"value": "Office and Administrative Support Occupations"
							},
							{
								"label": "Production Occupations",
								"value": "Production Occupations"
							},
							{
								"label": "Community and Social Service Occupations",
								"value": "Community and Social Service Occupations"
							},
							{
								"label": "Farming, Fishing, and Forestry Occupations",
								"value": "Farming, Fishing, and Forestry Occupations"
							},
							{
								"label": "Building and Grounds Cleaning and Maintenance Occupations",
								"value": "Building and Grounds Cleaning and Maintenance Occupations"
							},
							{
								"label": "Arts, Design, Entertainment, Sports, and Media Occupations",
								"value": "Arts, Design, Entertainment, Sports, and Media Occupations"
							},
							{
								"label": "Transportation and Materials Moving Occupations",
								"value": "Transportation and Materials Moving Occupations"
							},
							{
								"label": "Other",
								"value": "Other"
							}
						]
					},
					{
						'prompt': "Which best describes your experience level?",
						'tip': "",
						'responseFormat': 'multi-line-radio-options',
						'requiresScoreInversion': false,
						'scaleUnits': [
							{
								"label": "Intern",
								"value": "Intern"
							},
							{
								"label": "Entry-level",
								"value": "Entry-level"
							},
							{
								"label": "Intermediate",
								"value": "Intermediate"
							},
							{
								"label": "Senior-level",
								"value": "Senior-level"
							},
							{
								"label": "Director or Principal",
								"value": "Director or Principal"
							},
							{
								"label": "Vice President",
								"value": "Vice President"
							},
							{
								"label": "Senior Vice President",
								"value": "Senior Vice President"
							},
							{
								"label": "C-level executive",
								"value": "C-level executive"
							},
							{
								"label": "President or CEO",
								"value": "President or CEO"
							},
							{
								"label": "Owner",
								"value": "Owner"
							},
							{
								"label": "Other",
								"value": "Other"
							}
						]
					},
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '8',
				'isDefault': true,
				'sectionTitle': 'Open Ended Section',
				'overviewBlock': "",
				'sectionIntro': "<p>Please answer the following questions <u>in as many or as few lines as you'd like</u>.</p><p style='margin-bottom:0px'>And please do not enter any personally identifiable information.</p>",
				'introTipText': '',
				'responseFormat': 'open-ended-questions',
				'statementsAndPrompts': [
					{
						'prompt': "Can you please describe your life journey or any specific segments or aspects of it?",
						'tip': "Please take a moment to reflect and then jot down your responses below.",
						'responseFormat': 'multi-line-text-input',
						'numberOfAllowedRows': 3
					},
					{
						'prompt': "In general, what's terrific about your life right now?",
						'tip': "Please take a moment to reflect and then jot down your responses below.",
						'responseFormat': 'multi-line-text-input',
						'numberOfAllowedRows': 3
					},
					{
						'prompt': "Where are you most stuck in your life at the moment?",
						'tip': "Please take a moment to reflect and then jot down your responses below.",
						'responseFormat': 'multi-line-text-input',
						'numberOfAllowedRows': 3
					},
					{
						'prompt': "What are some of the top of mind things for you right now?",
						'tip': "Please take a moment to reflect and then jot down your responses below.",
						'responseFormat': 'multi-line-text-input',
						'numberOfAllowedRows': 3
					}
				]
			},
			{
				'sectionVersion': 1,
				'sectionID': '9',
				'isDefault': true,
				'sectionTitle': 'Big Five Personality Traits survey',
				'overviewBlock': "",
				'sectionIntro': "<p>Here are a number of statements that may or may not apply to you.</p><p>For example, do you agree that you are someone who likes to spend time with others?</p><p style='font-family:Pangram-Light; font-size:16px;'>Please select the option next to each statement to indicate the extent to which you agree or disagree with that statement.</p><p style='margin-bottom:0px'>I see Myself as Someone Who...</p>",
				'introTipText': '',
				'responseFormat': 'matrix-table-radio-options',
				'useMobileLayoutForDesktop': false,
				'scaleUnits': [
					{
						"label": "Disagree strongly",
						"value": 1
					},
					{
						"label": "Disagree a little",
						"value": 2
					},
					{
						"label": "Neither agree nor disagree",
						"value": 3
					},
					{
						"label": "Agree a little",
						"value": 4
					},
					{
						"label": "Agree Strongly",
						"value": 5
					}
				],
				'statementsAndPrompts': [
					{
						'prompt': 'Is talkative',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Tends to find fault with others',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Does a thorough job',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is depressed, blue',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is original, comes up with new ideas',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is reserved',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is helpful and unselfish with others',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Can be somewhat careless',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is relaxed, handles stress well',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is curious about many different things',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is full of energy',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Starts quarrels with others',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is a reliable worker',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Can be tense',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is ingenious, a deep thinker',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Generates a lot of enthusiasm',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Has a forgiving nature',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Tends to be disorganized',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Worries a lot',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Has an active imagination',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Tends to be quiet',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is generally trusting',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Tends to be lazy',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is emotionally stable, not easily upset',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is inventive',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Has an assertive personality',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Can be cold and aloof',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Perseveres until the task is finished',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Can be moody',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Values artistic, aesthetic experiences',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is sometimes shy, inhibited',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is considerate and kind to almost everyone',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Does things efficiently',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Remains calm in tense situations',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Prefers work that is routine',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is outgoing, sociable',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is sometimes rude to others',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Makes plans and follows through with them',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Gets nervous easily',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Likes to reflect, play with ideas',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Has few artistic interests',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Likes to cooperate with others',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is easily distracted',
						'requiresScoreInversion': false
					},
					{
						'prompt': 'Is sophisticated in art, music, or literature',
						'requiresScoreInversion': false
					}
				]
			},
		],
		unique: ['sectionID'],
		overwrite: true,
	}, // End of trnorgsurvey
}