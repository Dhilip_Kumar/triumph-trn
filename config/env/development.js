/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {
  	connection: 'devMongodbServer',
  },

  mailgun: {
    apiKey: 'c18ab458472e133369af741e39dfcc73-985b58f4-52e3f063',
    domain: 'mg.triumphunt.com',
    baseUrl: 'https://api.mailgun.net/v3/mg.triumphhq.com'
  },

  hostURL: 'http://localhost:1337/',

  ANALYTICS: {
    SEGMENT: {
        endpoint: 'https://api.segment.io/v1/',
        write_key: 'WFFFbERsd3NEcFhVU1ZST05RQ0xyUDg1QVY2aHVHWk4='
    }
  },

  hookTimeout: 100000
};
