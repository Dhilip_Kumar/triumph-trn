/**
 * TRNSurveyResults.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var randomize = require('randomatic');

module.exports = {

    attributes: {

        openEndedResposes : {
            collection : 'TRNOpenEndedSurveyResult',
            via : 'trnSurveyResultsID'
        },
        reportID: {
            type: 'string',
            required: true
        },
        completionCode: {
            type: 'string'
        },
        trnOrganizationID: {
            type: 'string'
        },
        groupID: {
            type: 'string'
        }
    },

    /* Generate a six digit alpha numeric random string. Ref: https://www.npmjs.com/package/randomatic. */

    generateRandomCode: function() {    
        return randomize('A0', 6);
    }, // End of generateRandomCode

    getSurveyCompletionCode: function(callback) {

        /* async forever method is used here to handle async functions within the loop. This is a self invoking function and it will be triggered with the help of next() fn. */

        async.forever(
            
            function(next) {
                
                var randomCode = TRNSurveyResults.generateRandomCode();

                TRNSurveyResults.findOne({completionCode: randomCode}).exec((findError, foundRecord) => {

                    if (findError) return callback(findError);

                    if (!foundRecord) {
                        return callback(null, randomCode); // The generated random code is unique. Terminate the loop and pass the code to the invoking function.
                    }
                    else {
                        next(); // If the generated code already exists, iterate the loop until we get a unique code.
                    }
                });
            }
        );
    }, // End of getSurveyCompletionCode

    getSupportedReportVersionForDataExport: () => {
		return 1
    }, // End of getSupportedReportVersionForDataExport

    /* Compute reports for the Survey results based on sectionID  */
    computeReports: (sectionID, surveyResults, internalEmoStates, callback) => {

        var reports = [];

        async.eachSeries(Object.keys(surveyResults), (ind, innerCallback) => {

            if (sectionID == 'IIA') { // compute IIA results

                if (!surveyResults[ind].userResponses) {
                    return innerCallback();
                };

                TRNSurveyResults.constructAssementReponseInRow(surveyResults[ind], internalEmoStates, (err,report) => {

                    if (err) {
                        return innerCallback(err);
                    };
                    reports.push(report);
                    return innerCallback();
                });
            }
            else { // compute other type of surveyResults

                if (!surveyResults[ind].statementsAndPrompts) {
                    return innerCallback();
                };

                TRNSurveyResults.constructSurveyReponseInRow(surveyResults[ind], (err,report) => {

                    if (err) {
                        return innerCallback(err);
                    };
                    reports.push(report);
                    return innerCallback();
                });
            }
        }, 
        (err) => {

            if (err) {
                return callback(err);
            };

            return callback (null, reports);
        }); 
    }, // End of computeReports

    /* Construct Survey reponse in row */
    constructSurveyReponseInRow: (surveyResult, outerCallback) => {

		async.auto({
			
			// process survey response
			processSurveyResponse: function(callback) {

				var data = {};

				async.eachSeries(Object.keys(surveyResult.statementsAndPrompts), (ind, innerCallback) => {

                    var key = 'PROMPT-' + (parseInt(ind) + 1);
					data[key] = surveyResult.statementsAndPrompts[ind].prompt;
					data[key + '-USER-RESPONSE'] = surveyResult.statementsAndPrompts[ind].selectedResponse;
					data[key + '-FINAL-RESPONSE'] = surveyResult.statementsAndPrompts[ind].finalResponse;
					data[key + '-TEXT-RESPONSE'] = surveyResult.statementsAndPrompts[ind].textResponse || '';

					innerCallback();
				},
				() => {
					return callback(null, data);
				});
            },

            // process finalResult
			finalResult: ['processSurveyResponse', function (callback, asyncData) {

				var finalResponseObj = 	_.merge(
					{},
					asyncData.processSurveyResponse,
					{   
						"REPORT-ID": surveyResult.reportID,
						"REPORT-VERSION": surveyResult.reportVersion,
						"COMPLETION-CODE": surveyResult.completionCode,
						"TRN-ORG-ID": surveyResult.trnOrganizationID,
						"GROUP-ID": surveyResult.groupID,
						"CHANNEL": surveyResult.channel,
                        "SECTION-START-TIME": surveyResult.sectionStartTime,
					    "SECTION-END-TIME": surveyResult.sectionEndTime,
                        "SURVEY-START-TIME": surveyResult.surveyStartTime,
					    "SURVEY-END-TIME": surveyResult.surveyEndTime,
                        "TOTAL-SURVEY-DURATION-IN-SECONDS": surveyResult.totalSurveyDurationInSeconds,
                        "CREATED-AT" : surveyResult.createdAt,
                    }
				);

				return callback (null, finalResponseObj);
			}]
		}, 
		function allDone (err, asyncData) {
			return outerCallback(null, asyncData.finalResult);
		});
	}, // End of constructSurveyReponseInRow

    /* Constract IIA response in a row */
	constructAssementReponseInRow: (assessmentResult, internalEmoStates, outerCallback) => {

		async.auto({
			
			// process IIA response
			processIIAResponse: function(callback) {

				var data = {};

				async.eachSeries(Object.keys(assessmentResult.userResponses), (ind, innerCallback) => {
				
					var key = 'PROMPT-' + assessmentResult.userResponses[ind].questionId;
					data[key] = assessmentResult.userResponses[ind].question;
					data[key + '-USER-RESPONSE'] = assessmentResult.userResponses[ind].userSelectedAnswer;
					data[key + '-FINAL-RESPONSE'] = assessmentResult.userResponses[ind].answer;
					data[key + '-TEXT-RESPONSE'] = assessmentResult.userResponses[ind].textResponse || '';

					innerCallback();
				},
				() => {
					return callback(null, data);
				});
			},

			// process value score
			processValueScore: function(callback) {

				var data = {};

				async.eachSeries(Object.keys(assessmentResult.valueScores), (ind, innerCallback) => {
					data['VALUE-' + (assessmentResult.valueScores[ind].valueName).replace(/\s/g,'-').toUpperCase()] = assessmentResult.valueScores[ind].valueScore;
					innerCallback();
				},
				() => {
					return callback(null, data);
				});	
			},

			// process Energy states
			processEnergyStates: function(callback) {
	
				var data = {};

				async.eachSeries(Object.keys(assessmentResult.energyProfile), (ind, innerCallback) => {
					data['ENERGY-' + (assessmentResult.energyProfile[ind].state).replace(/\s/g,'-').toUpperCase()] = assessmentResult.energyProfile[ind].average;
					innerCallback();
				},
				() => {
					return callback(null, data);
				});		
			},

			// process Emo states
			processEmoStates: function(callback) {
	
				var data = {};

				async.eachSeries(Object.keys(assessmentResult.stateAverages), (ind, innerCallback) => {
					data['EMO-' + (assessmentResult.stateAverages[ind].stateName).replace(/\s/g,'-').toUpperCase()] = assessmentResult.stateAverages[ind].average;
					innerCallback();
				},
				() => {
					return callback(null, data);
				});	
			},
			
			// compute EmoStates: 'Internal' and 'Facilitator'
			processInternalEmoStates: function(callback) {

				var data = {};

				EmostateAssessment.computeEmoStateResults(internalEmoStates, assessmentResult, (err, emoStates) => {
					async.eachSeries(Object.keys(emoStates), (ind, innerCallback) => {
						data['EMO-' + (emoStates[ind].stateName).replace(/\s/g,'-').toUpperCase()] = emoStates[ind].avgFromApproach4;
						innerCallback();
					},
					() => {
						return callback(null, data);
					});
				});
			},

            // process thrive meter
			processThriveMeter: function(callback) {

				var data = {}
	
				Tracks.getRecommendedTracks(assessmentResult, function (err, result) {
					
					if (err) return callback(err);

					data['TM-'+"CORE-ENABLER"] = result.coreEnablerGroupAvg;
					data['TM-'+"INNER-COMPASS"] = result.innerCompassGroupAvg;
					data['TM-'+"GOOD-LIVING"] = result.goodLivingGroupAvg;
					data['TM-'+"BEING-EXTRAORDINARY"] = result.beingExtraordinaryGroupAvg;

					return callback(null, data);
				});
			},

			// process finalResult
			finalResult: ['processIIAResponse', 'processEmoStates', 'processEnergyStates', 'processValueScore', 'processThriveMeter', 'processInternalEmoStates', function (callback, asyncData) {

				var finalResponseObj = 	_.merge(
					{},
					asyncData.processIIAResponse,
					asyncData.processValueScore,
					asyncData.processEmoStates,
					asyncData.processInternalEmoStates,
					asyncData.processEnergyStates,
					asyncData.processThriveMeter,
					{
						"REPORT-ID": assessmentResult.reportID,
						"REPORT-VERSION": assessmentResult.reportVersion,
						"COMPLETION-CODE": assessmentResult.completionCode,
						"TRN-ORG-ID": assessmentResult.trnOrganizationID,
						"GROUP-ID": assessmentResult.groupID,
						"CHANNEL": assessmentResult.channel,
                        "SECTION-START-TIME": assessmentResult.sectionStartTime,
						"SECTION-END-TIME": assessmentResult.sectionEndTime,
						"SURVEY-START-TIME": assessmentResult.surveyStartTime,
					    "SURVEY-END-TIME": assessmentResult.surveyEndTime,
                        "TOTAL-SURVEY-DURATION-IN-SECONDS": assessmentResult.totalSurveyDurationInSeconds,
                        "CREATED-AT" : assessmentResult.createdAt
                    }
				);

				return callback (null, finalResponseObj);
			}]
		}, 
		function allDone (err, asyncData) {
	
			if (err) {
				return outerCallback(err);
			};
	
			return outerCallback(null, asyncData.finalResult);
		});
	} // End of constructAssementReponseInRow
};

