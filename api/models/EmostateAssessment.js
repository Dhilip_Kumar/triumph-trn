/**
 * EmostateAssessment.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
*/

module.exports = {

    attributes: {

    },

    calculateScoreAndMakeResult: function(response, outerCallback) {

        async.auto({

            /*
                - Each Value is associated with two questions
                - So the score of a particular value will be average of answers of two questions
                - Question 1 and 21 are associated to same value, 2 and 22 are associated to same value and so on..
                - Store these results seperately so that it can be used in value assessment
            */
            storeValueScores: function(callback) {

                var valueArray = [];

                ValueMapping.find().exec((findErr, values) => {

                    if (findErr) return callback(findErr, null);

                    for (var ind = 0; ind <= 21; ind++) {
                        
                        if (ind == 21) {
                            /* 
                            - Async eachSeries, parallel methods doesn't support iterator max limit.
                            - As a workaround, for loop will be used and forcefully callback will be called upon reaching the desired limit.
                            */
                            return callback(null, valueArray);
                        }
                        else {

                            var foundObj = _.find(values, {valueName: response[ind].associatedValue});
                            
                            var valueObject = {};
                            valueObject['valueName']  = response[ind].associatedValue;
                            valueObject['valueScore'] = (parseInt(response[ind].score) + parseInt(response[ind + 21].score)) / 2; // 1 and 21 associated to same value
                            valueObject['definition'] = foundObj.definition;
                            valueArray.push(valueObject);
                        };
                    }; 
                });
            },

            /*
                - Store responses of individual questions
            */
            storeUserResponse: function(callback) {

                var userResponse = [];

                async.eachSeries(Object.keys(response), (ind, innerCallback) => {

                    var responseObject = {};
                    responseObject.questionId = (parseInt(ind) + 1).toString();
                    responseObject.question = response[ind].associatedQuestion;
                    responseObject.answer = parseInt(response[ind].score);
                    responseObject.userSelectedAnswer = parseInt(response[ind].selectedAnswer);
                    responseObject.textResponse = response[ind].textResponse;
                    userResponse.push(responseObject);

                    innerCallback();
                }, () => {
                    callback(null, userResponse);
                });
            },

            /* Fetch the emostate and associated questions list

            sampleRecord:
            ------------

            "stateName" : "Shame",
            "associatedQuestions" : ['20', '5', '12'],
            "associatedValues" : ['Courage', 'Simplicity', 'Openness'],
            "lowScoreResults" : [],
            "lowScoreDefinition" : "",
            "mediumScoreResults" : [
                "Shyness",
                "Introvertion",
            ],
            "mediumScoreDefinition" : "You repeatedly second-guess yourself and take little pride in your work. You avoid others where possible and feel your contributions are worth very little.",
            "highScoreResults" : [
                "Withdrawn Attitude",
                "Rigidity",
            ],
            "highScoreDefinition" : "Nothing you do is ever good enough, and you're beginning to be unsure if you’re even capable of anything worthwhile. You sometimes feel as though you are a shell of who you once were, and the decisions you make now feel foreign and forced. Your past decisions make it hard for you to move forward, as you focus more on your mistakes than your triumphs.",
            "energyState" : "Inert",
            "reviseScore" : true */

            fetchMapList: function(callback) {
                StateQuestionAssociation.find({type:'User'}).exec(callback);
            },

            /* calculate the score that needs to be shown for user for each emostate */
            manipulateScore: ['fetchMapList', 'storeValueScores', function(callback, asyncData) {

                var mapList = asyncData.fetchMapList || [];
                var valueScores = asyncData.storeValueScores || [];

                // map each emostate with below calculations.
                async.map(mapList, function(state, innerCallback) {

                    var average = 0; // Initialize EmoState average as 0
                    var associatedValuesV2andV3 = state.associatedValuesV2andV3 || [];

                    var tierOneValueReports = _.filter(valueScores, (valueObj) => {
                        if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier1']) {
                            return associatedValuesV2andV3['Tier1'].indexOf(valueObj.valueName) > -1;
                        };
                    });
        
                    var tierTwoValueReports = _.filter(valueScores, (valueObj) => {
                        if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier2']) {
                            return associatedValuesV2andV3['Tier2'].indexOf(valueObj.valueName) > -1;
                        };
                    });
        
                    var tierThreeValueReports = _.filter(valueScores, (valueObj) => {
                        if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier3']) {
                            return associatedValuesV2andV3['Tier3'].indexOf(valueObj.valueName) > -1;
                        };
                    });

                    var tierOneValScore = _.sum(tierOneValueReports, 'valueScore');
                    var tierTwoValScore = _.sum(tierTwoValueReports, 'valueScore');
                    var tierThreeValScore = _.sum(tierThreeValueReports, 'valueScore');

                    // Compute Average
                    var tierOneValAvg = tierOneValueReports.length > 0 ? parseFloat(tierOneValScore / tierOneValueReports.length) : 0;
                    var tierTwoValAvg = tierTwoValueReports.length > 0 ? parseFloat(tierTwoValScore / tierTwoValueReports.length) : 0;
                    var tierThreeValAvg = tierThreeValueReports.length > 0 ? parseFloat(tierThreeValScore / tierThreeValueReports.length) : 0;

                    // Compute results using V4

                    /* Case 1: There are 3 tiers of values for the given EmoState */
                    if (tierOneValueReports.length > 0 && tierTwoValueReports.length > 0 && tierThreeValueReports.length > 0) {
                        average = ((tierOneValAvg * 2/3) + (tierTwoValAvg * 2/9) + (tierThreeValAvg * 1/9)).toFixed(2);
                    }

                    /* Case 2: There are 2 tiers of values */
                    else if (tierOneValueReports.length > 0 && tierTwoValueReports.length > 0) {
                        average = ((tierOneValAvg * 2/3) + (tierTwoValAvg * 1/3)).toFixed(2);
                    }

                    /* Case 3: There are only 1 tier of values */
                    else if (tierOneValueReports.length > 0) {
                        average = tierOneValAvg.toFixed(2);
                    };

                    /* Invert the scores for "Inert" & "Hyper" EmoStates */
                    if (state.reviseScore == true) {
                        average = (6.00 - average).toFixed(2)
                    };

                    var resultObject = {};
                    resultObject['stateName'] = state.stateName;
                    resultObject['average'] = average;
                    resultObject['energyState'] = state.energyState;

                    // Categorize the level based on scores
                    if (average <= 1.5) {
                        resultObject['stateDefinition'] = state.lowScoreDefinition;
                    }
                    else if (average >1.5 && average <4.5) {
                        resultObject['stateDefinition'] = state.mediumScoreDefinition;
                    }
                    else if (average >= 4.5) {
                        resultObject['stateDefinition'] = state.highScoreDefinition;
                    };

                    innerCallback(null, resultObject);

                }, callback);
            }],

            /* sort emostates based on score and energy state corresponding to it */
            sortEmostate: ['manipulateScore', function(callback, asyncData){

                var sortedArray = _.sortBy(asyncData.manipulateScore, 'average');

                // "Peace & Serenity" needs to be shown only if user hits maximum ratings for each question (average = 6.00 -> 42*6 = 252)
                _.remove(sortedArray, function (resultObject) {
                    return (resultObject.stateName == 'Peace & Serenity' && resultObject.average < 6.00);
                });

                sortedArray = sortedArray.reverse(); // sort emostate based on score from high to low

                /* handler to handle below logic
                list the emostates based on their energy state with below order
                    - Top three Balanced energy state
                    - Three from Hyper and Inert energy states
                */

                EmostateAssessment.sortFunction(sortedArray, callback);
            }],

            /* Calculate energy profile */
            calculateEnergyState: ['storeValueScores', function(callback, asyncData) {

                var valueScores = asyncData.storeValueScores || [];
                var associatedValuesV6 = EnergyState.getAssociatedValuesForV6();

                EnergyState.calculateEnergyStateByValueTierApproachV6(associatedValuesV6, valueScores, (calculateErr, results) => { // Calculate using V6 version

                    if (calculateErr) return callback(calculateErr, null);

                    EnergyState.find().exec((energyStateFindErr, energyStates) => {

                        if (energyStateFindErr) return callback(energyStateFindErr, null);

                        async.map(energyStates, function(energyState, innerCallback) {

                            if (energyState.state == 'Balanced') {
                                energyState.average = results.balancedStateAverage;
                            }
                            else if (energyState.state == 'Inert') {
                                energyState.average = results.inertStateAverage;
                            }
                            else if (energyState.state == 'Hyper') {
                                energyState.average = results.hyperStateAverage;
                            };
                            innerCallback();
                        }, 
                        () => {
                            callback(null, energyStates);
                        });
                    });
                });
            }],
        }, 
        function allDone (err, asyncData) {

            if (err) return outerCallback(err, null);

            var responseObject = {
                assessmentVersion: 5,
                stateAverages: asyncData.manipulateScore,
                emostateResponses: asyncData.sortEmostate,
                energyProfile: asyncData.calculateEnergyState,
                valueScores: asyncData.storeValueScores,
                userResponses: asyncData.storeUserResponse,
            }
            return outerCallback(null, responseObject);
        });
    },

    /* sort the resultant list based on energy state */
    sortFunction: function(listArray, callback) {

        var resultArray = []; // Array that will store final result
        var tempArray = []; // Array that will hold entries excluding Balanced energy state entries

        var grouped = _.groupBy(listArray, function(data) {
            return data.energyState;
        });

        // List Top three Balanced energy state first
        if (grouped.hasOwnProperty('Balanced')) {
            resultArray.push(grouped.Balanced[0]);
            resultArray.push(grouped.Balanced[1]);
            resultArray.push(grouped.Balanced[2]);
        }

        if (grouped.hasOwnProperty('Hyper')) {

            for (var i=0; i < grouped.Hyper.length; i++) {
                tempArray.push(grouped.Hyper[i])
            }
        }

        if (grouped.hasOwnProperty('Inert')) {

            for (var i=0; i < grouped.Inert.length; i++) {
                tempArray.push(grouped.Inert[i])
            }
        }

        var sortedArray = _.sortBy(tempArray, 'average');
        sortedArray = sortedArray.reverse();

        // Push Top 3 high scored state from 'Hyper' and 'Inert' energy state
        for (var i=0; i < 3; i++) {
            resultArray.push(sortedArray[i])
        }

        callback(null, resultArray);
    },

    /* Function to invert the value score */
    invertValueScore: function (value) { 
        return (6 - parseFloat(value.valueScore));
    },

    /* EmoState results from alternate approaches */

    computeEmoStateResults: function(emoStates, assessmentResult, callback) {

        var valueScores = assessmentResult.valueScores || [];
        var responseArray = [];

        async.map(emoStates, (emoState, nestedCb) => {

            var associatedValuesV1 = emoState.associatedValues || [];
            var associatedValuesV2andV3 = emoState.associatedValuesV2andV3 || [];
            var versionOneInverseValueReports = [];
            var tierOneInverseValueReports = [];
            var tierTwoInverseValueReports = [];
            var tierThreeInverseValueReports = [];
            var tierOneLength, tierTwoLength, tierThreeLength = 0;

            var versionOneValueReports = _.filter(valueScores, (valueObj) => {
                return associatedValuesV1.indexOf(valueObj.valueName) > -1;
            });

            var tierOneValueReports = _.filter(valueScores, (valueObj) => {
                if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier1']) {
                    return associatedValuesV2andV3['Tier1'].indexOf(valueObj.valueName) > -1;
                };
            });

            var tierTwoValueReports = _.filter(valueScores, (valueObj) => {
                if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier2']) {
                    return associatedValuesV2andV3['Tier2'].indexOf(valueObj.valueName) > -1;
                };
            });

            var tierThreeValueReports = _.filter(valueScores, (valueObj) => {
                if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier3']) {
                    return associatedValuesV2andV3['Tier3'].indexOf(valueObj.valueName) > -1;
                };
            });

            tierOneLength = tierOneValueReports.length;
            tierTwoLength = tierTwoValueReports.length;
            tierThreeLength = tierThreeValueReports.length;

            var versionOneValScore = _.sum(versionOneValueReports, 'valueScore');
            var tierOneValScore = _.sum(tierOneValueReports, 'valueScore');
            var tierTwoValScore = _.sum(tierTwoValueReports, 'valueScore');
            var tierThreeValScore = _.sum(tierThreeValueReports, 'valueScore');

            if (emoState.customReviseScore) {

                /* Compute inversion logic for the associatedValues that contains '(Inverse) ' string */

                const INVERSE = '(Inverse) '; 

                var versionOneInverseValueReports = _.filter(valueScores, (valueObj) => {
                    return associatedValuesV1.indexOf(INVERSE + valueObj.valueName) > -1;
                });

                var tierOneInverseValueReports = _.filter(valueScores, (valueObj) => {
                    if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier1']) {
                        return associatedValuesV2andV3['Tier1'].indexOf(INVERSE + valueObj.valueName) > -1;
                    };
                });

                var tierTwoInverseValueReports = _.filter(valueScores, (valueObj) => {
                    if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier2']) {
                        return associatedValuesV2andV3['Tier2'].indexOf(INVERSE + valueObj.valueName) > -1;
                    };
                });

                var tierThreeInverseValueReports = _.filter(valueScores, (valueObj) => {
                    if (associatedValuesV2andV3 && associatedValuesV2andV3['Tier3']) {
                        return associatedValuesV2andV3['Tier3'].indexOf(INVERSE + valueObj.valueName) > -1;
                    };
                });

                tierOneLength += tierOneInverseValueReports.length;
                tierTwoLength += tierTwoInverseValueReports.length;
                tierThreeLength += tierThreeInverseValueReports.length;

                // Compute inverted score
                var versionOneInverseValScore = _.sum(versionOneInverseValueReports, EmostateAssessment.invertValueScore);
                var tierOneInverseValScore = _.sum(tierOneInverseValueReports, EmostateAssessment.invertValueScore);
                var tierTwoInverseValScore = _.sum(tierTwoInverseValueReports, EmostateAssessment.invertValueScore);
                var tierThreeInverseValScore = _.sum(tierThreeInverseValueReports, EmostateAssessment.invertValueScore);

                // Add inverted score
                versionOneValScore += versionOneInverseValScore;
                tierOneValScore += tierOneInverseValScore;
                tierTwoValScore += tierTwoInverseValScore;
                tierThreeValScore += tierThreeInverseValScore;
            }

            // Compute Average
            var versionOneValAvg = versionOneValueReports.length > 0 || versionOneInverseValueReports.length > 0 ? parseFloat(versionOneValScore / (versionOneValueReports.length + versionOneInverseValueReports.length)) : 0;
            var tierOneValAvg = tierOneLength > 0 ? parseFloat(tierOneValScore / tierOneLength) : 0;
            var tierTwoValAvg = tierTwoLength > 0 ? parseFloat(tierTwoValScore / tierTwoLength) : 0;
            var tierThreeValAvg = tierThreeLength > 0 ? parseFloat(tierThreeValScore / tierThreeLength) : 0;

            var avgFromApproach1, avgFromApproach2, avgFromApproach3, avgFromApproach4, avgFromApproach5, avgFromApproach6, avgFromApproach7, avgFromApproach8, avgFromApproach9 = 0;

            // Compute results using V1 (Avg from V1 set of values)
            avgFromApproach1 = versionOneValAvg.toFixed(2);
        
            // Compute results using V2 (Average across all the tiers of values)
            avgFromApproach2 = ((tierOneValAvg + tierTwoValAvg + tierThreeValAvg) / 3).toFixed(2);

            // Compute results using V3

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach3 = ((tierOneValAvg * 1/2) + (tierTwoValAvg * 1/3) + (tierThreeValAvg * 1/6)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength >0) {
                avgFromApproach3 = ((tierOneValAvg * 2/3) + (tierTwoValAvg * 1/3)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach3 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach3 = 0;
            }

            // Compute results using V4

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach4 = ((tierOneValAvg * 2/3) + (tierTwoValAvg * 2/9) + (tierThreeValAvg * 1/9)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach4 = ((tierOneValAvg * 2/3) + (tierTwoValAvg * 1/3)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach4 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach4 = 0;
            }

            // Compute results using V5

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach5 = ((tierOneValAvg * 0.7) + (tierTwoValAvg * 0.2) + (tierThreeValAvg * 0.1)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach5 = ((tierOneValAvg * 0.7) + (tierTwoValAvg * 0.3)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach5 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach5 = 0;
            }

            // Compute results using V6

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach6 = ((tierOneValAvg * 0.8) + (tierTwoValAvg * 0.14) + (tierThreeValAvg * 0.06)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach6 = ((tierOneValAvg * 0.8) + (tierTwoValAvg * 0.2)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach6 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach6 = 0;
            }

            // Compute results using V7

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach7 = ((tierOneValAvg * 0.5) + (tierTwoValAvg * 0.4) + (tierThreeValAvg * 0.1)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach7 = ((tierOneValAvg * 0.9) + (tierTwoValAvg * 0.1)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach7 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach7 = 0;
            }

            // Compute results using V8

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach8 = ((tierOneValAvg * 0.6) + (tierTwoValAvg * 0.3) + (tierThreeValAvg * 0.1)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach8 = ((tierOneValAvg * 0.9) + (tierTwoValAvg * 0.1)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach8 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach8 = 0;
            }

            // Compute results using V9

            /* Case 1: There are 3 tiers of values for the given EmoState */
            if (tierOneLength > 0 && tierTwoLength > 0 && tierThreeLength > 0) {
                avgFromApproach9 = ((tierOneValAvg * 0.9) + (tierTwoValAvg * 0) + (tierThreeValAvg * 0.1)).toFixed(2);
            }

            /* Case 2: There are 2 tiers of values */
            else if (tierOneLength > 0 && tierTwoLength > 0) {
                avgFromApproach9 = ((tierOneValAvg * 0.9) + (tierTwoValAvg * 0.1)).toFixed(2);
            }

            /* Case 3: There are only 1 tier of values */
            else if (tierOneLength > 0) {
                avgFromApproach9 = tierOneValAvg.toFixed(2);
            }
            else {
                avgFromApproach9 = 0;
            }

            /* Invert the scores for "Inert" & "Hyper" EmoStates */
            if (emoState.reviseScore == true) {
                avgFromApproach1 = (6.00 - avgFromApproach1).toFixed(2);
                avgFromApproach2 = (6.00 - avgFromApproach2).toFixed(2);
                avgFromApproach3 = (6.00 - avgFromApproach3).toFixed(2);
                avgFromApproach4 = (6.00 - avgFromApproach4).toFixed(2);
                avgFromApproach5 = (6.00 - avgFromApproach5).toFixed(2);
                avgFromApproach6 = (6.00 - avgFromApproach6).toFixed(2);
                avgFromApproach7 = (6.00 - avgFromApproach7).toFixed(2);
                avgFromApproach8 = (6.00 - avgFromApproach8).toFixed(2);
                avgFromApproach9 = (6.00 - avgFromApproach9).toFixed(2);
            };

            responseArray.push({
                stateName: emoState.stateName,
                energyState: emoState.energyState,
                avgFromApproach1: avgFromApproach1,
                avgFromApproach2: avgFromApproach2,
                avgFromApproach3: avgFromApproach3,
                avgFromApproach4: avgFromApproach4,
                avgFromApproach5: avgFromApproach5,
                avgFromApproach6: avgFromApproach6,
                avgFromApproach7: avgFromApproach7,
                avgFromApproach8: avgFromApproach8,
                avgFromApproach9: avgFromApproach9
            });
            nestedCb();
        }, 
        () => {
            responseArray = _.sortBy(responseArray, 'energyState');
            return callback(null, responseArray);
        });
    }, // End of computeEmoStateResults
};

