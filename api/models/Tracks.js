/**
 * Tracks.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

    },

    getRecommendedTracks: function(assessmentReport, callback) {

        var recommendedValues = [];
        var valueScores = assessmentReport ? assessmentReport.valueScores : [];

        var coreEnablerGroupValues = ['Self-Awareness', 'Understanding', 'Self-Mastery'];
        var innerCompassGroupValues = ['Abundance', 'Freedom', 'Compassion', 'Kindness & Caring', 'Inclusivity', 'Authenticity & Truthfulness'];
        var goodLivingGroupValues = ['Being Present', 'Being Responsible', 'Letting Go', 'Accepting & Non-Judgement', 'Positivity & Spirit', 'Humility'];
        var beingExtraordinaryGroupValues = ['Innocence', 'Openness', 'Excellence', 'Courage', 'Simplicity', 'Dedication & Surrender'];

        var coreEnablerGroupResults = _.filter(valueScores, (valueObj) => {
            return coreEnablerGroupValues.indexOf(valueObj.valueName) > -1;
        });
        
        var innerCompassGroupResults = _.filter(valueScores, (valueObj) => {
            return innerCompassGroupValues.indexOf(valueObj.valueName) > -1;
        });

        var goodLivingGroupResults = _.filter(valueScores, (valueObj) => {
            return goodLivingGroupValues.indexOf(valueObj.valueName) > -1;
        });

        var beingExtraordinaryGroupResults = _.filter(valueScores, (valueObj) => {
            return beingExtraordinaryGroupValues.indexOf(valueObj.valueName) > -1;
        });

        /* Sort the values from low to high based on the values score. */
        coreEnablerGroupResults = _.sortBy(coreEnablerGroupResults, 'valueScore');
        innerCompassGroupResults = _.sortBy(innerCompassGroupResults, 'valueScore');
        goodLivingGroupResults = _.sortBy(goodLivingGroupResults, 'valueScore');
        beingExtraordinaryGroupResults = _.sortBy(beingExtraordinaryGroupResults, 'valueScore');

        /* Present the lowest scored values from each group as the recommended values. */
        if (coreEnablerGroupResults[0] && coreEnablerGroupResults[0].valueName) {
            recommendedValues.push(coreEnablerGroupResults[0].valueName);
        };
        if (innerCompassGroupResults[0] && innerCompassGroupResults[0].valueName) {
            recommendedValues.push(innerCompassGroupResults[0].valueName);
        };
        if (goodLivingGroupResults[0] && goodLivingGroupResults[0].valueName) {
            recommendedValues.push(goodLivingGroupResults[0].valueName);
        };
        if (beingExtraordinaryGroupResults[0] && beingExtraordinaryGroupResults[0].valueName) {
            recommendedValues.push(beingExtraordinaryGroupResults[0].valueName);
        };

        /* Compute the avg. for each group. */
        var coreEnablerGroupAvg = _.sum(coreEnablerGroupResults, 'valueScore') / coreEnablerGroupResults.length;
        var innerCompassGroupAvg = _.sum(innerCompassGroupResults, 'valueScore') / innerCompassGroupResults.length;
        var goodLivingGroupAvg = _.sum(goodLivingGroupResults, 'valueScore') / goodLivingGroupResults.length;
        var beingExtraordinaryGroupAvg = _.sum(beingExtraordinaryGroupResults, 'valueScore') / beingExtraordinaryGroupResults.length;

        /* Add 1 to the resultant avg.score to make it as a 7pt scale */
        coreEnablerGroupAvg += 1;
        innerCompassGroupAvg += 1;
        goodLivingGroupAvg += 1;
        beingExtraordinaryGroupAvg += 1;

        /* Convert 7pt to a 10pt scale */
        coreEnablerGroupAvg = ((coreEnablerGroupAvg / 7) * 10).toFixed(1);
        innerCompassGroupAvg = ((innerCompassGroupAvg / 7) * 10).toFixed(1);
        goodLivingGroupAvg = ((goodLivingGroupAvg / 7) * 10).toFixed(1);
        beingExtraordinaryGroupAvg = ((beingExtraordinaryGroupAvg / 7) * 10).toFixed(1);

        var responseObj = {
            coreEnablerGroupResults: coreEnablerGroupResults,
            innerCompassGroupResults: innerCompassGroupResults,
            goodLivingGroupResults: goodLivingGroupResults,
            beingExtraordinaryGroupResults: beingExtraordinaryGroupResults,
            coreEnablerGroupAvg: coreEnablerGroupAvg,
            innerCompassGroupAvg: innerCompassGroupAvg,
            goodLivingGroupAvg: goodLivingGroupAvg,
            beingExtraordinaryGroupAvg: beingExtraordinaryGroupAvg,
            recommendedValues: recommendedValues
        };

        callback(null, responseObj);

    }, // End of getRecommendedTracks
}