/**
 * TRNReportWidgets.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        widgetID: {
            type: 'integer',
            unique: 'true',
            index: true,
            primaryKey: true
        }
    },

    beforeCreate: function(values, next) {
        
        TRNReportWidgets.find({}).sort('widgetID DESC')
            .limit(1).exec(function(err, recentWidget) {
            
            if (err) next(err);
            
            else {

                if (!values.widgetID) {

                    var nextWidgetID = 1;

                    if (recentWidget && recentWidget[0] && recentWidget[0].widgetID) {
                        nextWidgetID = recentWidget[0].widgetID + 1;
                    };
                    values.widgetID = nextWidgetID;
                }
                values.isActive = true;
                next(null);
            }
        });
    },

    updateWidget: function(action, widgetObj, callback) {

        if (action == 'add') {
            TRNReportWidgets.create(widgetObj).exec(callback);
        }
        else if (action == 'edit') {
            TRNReportWidgets.update({widgetID: widgetObj.widgetID}, widgetObj).exec(callback);
        }
        else if (action == 'delete') {
            TRNReportWidgets.update({widgetID: widgetObj.widgetID}, {isActive: false}).exec(callback);
        }
        else {
            callback('Invalid Request.', null);
        }
    }, // End of updateWidget

    getDataDimensionalReport: function(dataDimensionResults, options, callback) {

        // Initialize response array
        var allTextResponses = [];
        var textResponses = [];
        var focusAreaResponses = [];
        var topicModels = [];
        var response = {};

        async.auto({

            init: function(cb) {

                async.eachSeries(Object.keys(dataDimensionResults), (ind, innerCb) => {

                    if (!dataDimensionResults[ind].trnSurveyResultsID) return innerCb();
         
                    var openEndedResults = dataDimensionResults[ind].responseSet;
                    var trnSurveyResult = dataDimensionResults[ind].trnSurveyResultsID;
        
                    async.eachSeries(openEndedResults, (data, next) => {
    
                        if (data.textResponse) {
                            textResponses.push(data.textResponse);
                            allTextResponses.push({
                                trnSurveyResult : trnSurveyResult,
                                textResponse : data.textResponse
                            });
                            topicModels.push({
                                id: topicModels.length + 1, 
                                text: data.textResponse
                            });
                        };
                        
                        if (data.focusAreaResponse) {
                            focusAreaResponses.push(data.focusAreaResponse);
                        };
                        next();
                    },
                    () => { 
                        innerCb(); 
                    });
                }, 
                () => { 
                    cb(null, 1); 
                });
            },

            topicModeling: ['init', (cb, asyncData) => {
        
                // Append TopicModel responses
                TRNReportWidgets.computeTopicModelResponses(topicModels, options)
                .then((topicModelResponse) => {
                    return cb(null, topicModelResponse);
                })
                .catch((err) => {
                    console.log("Topic model compute err: ", err);
                    return cb(null, topicModelResponse);
                });
            }],

            getSurveyResultInfoForTopics: ['topicModeling', (cb, asyncData) =>{
                TRNReportWidgets.getSurveyResultInfoForTopics(asyncData.topicModeling, allTextResponses, cb);
            }],

            themesAndTopics: ['getSurveyResultInfoForTopics', (cb, asyncData) =>{
                TRNReportWidgets.computeThemesNdTopcis(asyncData.getSurveyResultInfoForTopics, cb);
            }],
        },
        (err, asyncData) => {
            
            if (err) return callback(err);

            response.textResponses = textResponses;
            response.focusAreaResponses = focusAreaResponses;
            response.topicModelingResponses = asyncData.topicModeling;
            response.peopleHeadCount = dataDimensionResults.length;
            response.topics = asyncData.themesAndTopics;

            return callback(null,response)
        });
    }, // End of getDataDimensionalReport

    getSurveyResultInfoForTopics: function (topicModeling, allTextResponses, callback) {

        var topics = [];

        async.eachSeries(topicModeling, (topic, nextTopic) => {

            var surveyResults = [];

            async.eachSeries(topic.documents, (item, nextDoc) => {

                async.eachSeries(allTextResponses, (data, nextResponse) => {

                    if (data.textResponse != item.text) return nextResponse();

                    let isExist = surveyResults.filter(surveyResult => surveyResult.id === data.trnSurveyResult.id);

                    if (isExist.length <= 0) {
                        surveyResults.push(data.trnSurveyResult);
                    }
                    return nextResponse();
                }, 
                () => { 
                    nextDoc(); 
                });
            },
            () => { 

                const newTopic = {...topic};
                newTopic.surveyResults = surveyResults;
                topics.push(newTopic);
                nextTopic(); 
            });
        },
        () => { 
            callback(null, topics); 
        });
    },

    computeThemesNdTopcis: function name(topics, callback) {

        async.eachSeries(Object.keys(topics), (key, nextTopic) => {

            var energyState = [], emoState = [], valueScores = [];

            async.eachSeries(topics[key].surveyResults, (surveyResult, nextRes) => {

                async.auto({

                    computeEnergyState: (cb) => {

                        surveyResult.energyProfile.forEach(data => {

                            var index = energyState.findIndex((e) => e.state == data.state);

                            if (index < 0) {
                                energyState.push({
                                    total: parseFloat(data.average), 
                                    state: data.state
                                });
                            }
                            else {
                                energyState[index].total += parseFloat(data.average);
                            }
                        });
                        cb(null, energyState)
                    },

                    computeEmoState: (cb) => {

                        surveyResult.stateAverages.forEach(data => {

                            var index = emoState.findIndex((e) => e.stateName == data.stateName);
                            
                            if (index < 0) {
                                emoState.push({
                                    total: parseFloat(data.average),
                                    stateName: data.stateName
                                });
                            }
                            else {
                                emoState[index].total += parseFloat(data.average);
                            }
                        });
                        cb(null, emoState);
                    },

                    computeValueScore: (cb) => {

                        surveyResult.valueScores.forEach(data => {

                            var index = valueScores.findIndex((v) => v.valueName == data.valueName);
                            
                            if (index < 0) {
                                valueScores.push({
                                    total: parseFloat(data.valueScore),
                                    valueName: data.valueName
                                });
                            }
                            else {
                                valueScores[index].total += parseFloat(data.valueScore);
                            }
                        });
                        cb(null, 1);
                    },
                },
                () => { 
                    nextRes(); 
                });
            },
            () => { 

                /** sort Energy State, EmoState and EnergyState */
                energyState = (_.sortBy(energyState, 'total')).reverse();
                emoState = _.sortBy(emoState, 'total');
                valueScores = _.sortBy(valueScores, 'total');

                topics[key].themesAndTopics = { 
                    energyState : energyState, 
                    emoState : emoState, 
                    valueScores : valueScores,
                    surveyHeadCount : topics[key].surveyResults.length
                };

                delete topics[key].surveyResults;
                nextTopic(); 
            });

        },() => { callback(null, topics); });
    }, //End of computeThemesNdTopcis

    getMultiDimensionalReport: function (inputs, callback) {

        async.auto({

            getInitialReport: function(innerCallback) { // compute > textResponse, focusArea, and topic Modeling for all Entries
                TRNReportWidgets.getInitialReportForMultiDimentionReport(inputs, innerCallback);
            },

            computeUniqueEntries: ['getInitialReport', (cb, asyncData) =>{
                TRNReportWidgets.computeUniqueEntriesNdTopicModeling(inputs, cb);
            }],

            computeCommonEntries: ['getInitialReport', (cb, asyncData) =>{
                TRNReportWidgets.computeCommonEntriesNdTopicModeling(inputs, cb);
            }],
        },
        (err, asyncData) => {
            
            if (err) return callback(err);

            return callback(null,inputs.innerDimensions);
        }); 
    },

    getInitialReportForMultiDimentionReport: function (inputs, callback) {

        var findCriteria = {
            trnOrganizationID: inputs.trnOrganizationID,
            prompt: inputs.dataDimension
        };
       
        if (inputs.surveyDataset && inputs.surveyDataset.length > 0 && inputs.surveyDataset.indexOf("all") == -1) 
            findCriteria.surveyID = inputs.surveyDataset;
        
        if (inputs.groupDataset && inputs.groupDataset.length > 0 && inputs.groupDataset.indexOf("all") == -1) 
            findCriteria.groupID = inputs.groupDataset;
        
        async.eachSeries(Object.keys(inputs.innerDimensions), (ind, cb) => {

            var category = inputs.innerDimensions[ind].category;

            TRNOpenEndedSurveyResult.find(findCriteria)
            .populate('trnSurveyResultsID')
            .exec((err, dataDimensionResults) => {
                
                if (err) return cb(err);
                
                if (dataDimensionResults && dataDimensionResults.length == 0) {
                    inputs.innerDimensions[ind].textResponses = [];
                    inputs.innerDimensions[ind].focusAreaResponses = [];
                    inputs.innerDimensions[ind].topicModelingResponses = [];
                    inputs.innerDimensions[ind].peopleHeadCount = 0;
                    return cb();
                };

                if (category == 'Energy State') {
                    TRNReportWidgets.getMultiDimensionalReportBasedOnEnergyState(dataDimensionResults, inputs.innerDimensions[ind], inputs.options, cb);
                }
                else if (category == 'EmoState') {
                    TRNReportWidgets.getMultiDimensionalReportBasedOnEmoState(dataDimensionResults, inputs.innerDimensions[ind], inputs.options, cb);
                }
                else if (category == 'Values') {
                    TRNReportWidgets.getMultiDimensionalReportBasedOnValues(dataDimensionResults, inputs.innerDimensions[ind], inputs.options, cb);
                }
                else {
                    return cb();
                }
            });
        },
        (err, results) => {
            
            if (err) return callback(err);
            
            return callback(null,inputs.innerDimensions);
        });
    },

    computeUniqueEntriesNdTopicModeling: function (inputs, callback) {
        
        async.eachSeries(Object.keys(inputs.innerDimensions), (key, processNext) => {

            let uniqueEntries = [],topicModels = [];

            async.eachSeries(inputs.innerDimensions[key].textResponsesWithSurveyResultID, (data, outerCallback) => {

                let matchedCount = 0;

                async.eachSeries(Object.keys(inputs.innerDimensions), (ind, innerCallback) => {

                    if(ind == key) return innerCallback();

                    async.eachSeries(inputs.innerDimensions[ind].textResponsesWithSurveyResultID, (item, cb) => {
                        
                        if (data.surveyResultID === item.surveyResultID) { 
                            matchedCount ++; 
                            return cb("break") 
                        };
                        cb();
                    },
                    (err) => {

                        if(!matchedCount) innerCallback();

                        else innerCallback("Break");
                    });
                }, 
                (err) => {
                    
                    if (!matchedCount) { 
                        uniqueEntries.push(data.textResponse);
                        topicModels.push({
                            id: topicModels.length + 1, 
                            text : data.textResponse
                        });
                    };
                    outerCallback();
                });
            }, 
            (err, results) => {

                // Append TopicModel responses
                TRNReportWidgets.computeTopicModelResponses(topicModels, inputs.options)
                .then((topicModelResponse) => {

                    inputs.innerDimensions[key].uniqueEntries = {
                        textResponses : uniqueEntries,
                        topicModelingResponses : topicModelResponse
                    }
                    return processNext();
                })
                .catch((err) => {
                    console.error("Topic model compute err: ", err);
                    inputs.innerDimensions[key].uniqueEntries = {
                        textResponses : uniqueEntries,
                        topicModelingResponses : []
                    };
                    return processNext();
                });
            });
        }, 
        () => { 
            callback(null,inputs); 
        });
    },

    computeCommonEntriesNdTopicModeling: function (inputs, callback) {

        let keys = Object.keys(inputs.innerDimensions);
        let commonEntries = [], topicModels = [];
        
        async.eachSeries(inputs.innerDimensions[keys[0]].textResponsesWithSurveyResultID, (data, outerCallback) => {

            let matchedCount = 1, processedCount = 0;

            async.eachSeries(keys, (key, innerCallback) => {

                processedCount++;

                if (key == keys[0]) return innerCallback();

                async.eachSeries(inputs.innerDimensions[key].textResponsesWithSurveyResultID, (item, cb) => {

                    if (data.surveyResultID === item.surveyResultID) { 
                        matchedCount ++; 
                        return cb("break") 
                    };
                    cb();
                },
                (err) => {

                    if (processedCount == matchedCount) innerCallback();

                    else innerCallback("Break");
                });
            }, 
            (err) => {
                
                if (processedCount == matchedCount) { 

                    commonEntries.push(data.textResponse);
                    topicModels.push({
                        id: topicModels.length + 1, 
                        text : data.textResponse
                    });
                };
                outerCallback();
            });
        },
        (err, results) => {
            
            async.auto({

                topicModeling: function(cb) {

                    var topicModelingResponses = [];

                    // Append TopicModel responses
                    TRNReportWidgets.computeTopicModelResponses(topicModels, inputs.options)
                        .then((topicModelResponse) => {
                            topicModelingResponses = topicModelResponse;
                            return cb(null, topicModelingResponses);
                        })
                        .catch((err) => {
                            console.error("Topic model compute err: ", err);
                            return cb(null, topicModelingResponses);
                        });

                },
    
                computeUniqueEntries: ['topicModeling', (cb, asyncData) =>{

                    async.eachSeries(keys, (ind, innerCallback) => {
                        inputs.innerDimensions[ind].commonEntries = {
                            textResponses : commonEntries,
                            topicModelingResponses : asyncData.topicModeling
                        }
                        innerCallback();
                    },
                    (err) => { 
                        cb();
                    });
                }],
            },
            (err, asyncData) => { 
                callback(null,inputs); 
            });
        });
    },

    getMultiDimensionalReportBasedOnEnergyState: function(dataDimensionResults, innerDimension, options, callback) {

        // Initialize response array
        var textResponsesWithSurveyResultID = [];
        var textResponses = [];
        var focusAreaResponses = [];
        var topicModels = [];

        /* No.of people fall under each innerDimension column of a widget */
        innerDimension.peopleHeadCount = 0;

        /*
        - Here is the EnergyState cutoffs
            Low: <=3.5
            Medium: >3.5 and <7.5
            High: >=7.5
        */

        async.eachSeries(Object.keys(dataDimensionResults), (ind, innerCb) => {

            if (!dataDimensionResults[ind].trnSurveyResultsID) return innerCb();
 
            var userEnergyStates = dataDimensionResults[ind].trnSurveyResultsID.energyProfile;
            var openEndedResults = dataDimensionResults[ind].responseSet;
            
            var filteredObj = null;
            var isUserReportMatchesFilterCriteria = false;

            if (innerDimension.filter == 'High') {
                filteredObj = _.find(userEnergyStates, (val) => {
                    return (val.state == innerDimension.dimension && parseFloat(val.average) >= 7.5); 
                });
            }
            else if (innerDimension.filter == 'Medium') {
                filteredObj = _.find(userEnergyStates, (val) => {
                    return (val.state == innerDimension.dimension && parseFloat(val.average) > 3.5 && parseFloat(val.average) < 7.5); 
                });
            }
            else if (innerDimension.filter == 'Low') {
                filteredObj = _.find(userEnergyStates, (val) => {
                    return (val.state == innerDimension.dimension && parseFloat(val.average) <= 3.5); 
                });
            }
            else if (innerDimension.filter == 'Highest for User') {

                userEnergyStates = _.sortBy(userEnergyStates, 'average'); // sortBy average from asc to desc
                userEnergyStates = userEnergyStates.reverse(); // sortBy average from desc to asc 
                
                /* Check whether the most dominating energy state of the user matches given Energy State dimension. */
                if (userEnergyStates[0].state == innerDimension.dimension) { 
                    filteredObj = userEnergyStates[0];
                }
                else {
                    var filteredEnergyState = _.filter(userEnergyStates, {'state': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple dominating Energy States, then check whether the given Energy State dimension is one among them. */
                    if (filteredEnergyState.length > 0 && parseFloat(filteredEnergyState[0].average) == parseFloat(userEnergyStates[0].average))
                        filteredObj = filteredEnergyState[0];
                };
            }
            else if (innerDimension.filter == 'Lowest for User') {
                
                userEnergyStates = _.sortBy(userEnergyStates, 'average'); // sortBy average from asc to desc
                
                /* Check whether the most weakest energy state of the user matches given Energy State dimension. */
                if (userEnergyStates[0].state == innerDimension.dimension) { 
                    filteredObj = userEnergyStates[0];
                }
                else {
                    var filteredEnergyState = _.filter(userEnergyStates, {'state': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple weak Energy States, then check whether the given Energy State dimension is one among them. */
                    if (filteredEnergyState.length > 0 && parseFloat(filteredEnergyState[0].average) == parseFloat(userEnergyStates[0].average))
                        filteredObj = filteredEnergyState[0];
                };
            }

            if (filteredObj) {

                async.eachSeries(openEndedResults, (data, next) => {

                    if (data.textResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        textResponses.push(data.textResponse);
                        topicModels.push({
                            id: topicModels.length + 1, 
                            text : data.textResponse
                        });
                        textResponsesWithSurveyResultID.push({
                            surveyResultID : dataDimensionResults[ind].trnSurveyResultsID.id,
                            textResponse: data.textResponse
                        });
                    };
                    
                    if (data.focusAreaResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        focusAreaResponses.push(data.focusAreaResponse);
                    };
                    next();
                },
                () => { 

                    if (isUserReportMatchesFilterCriteria === true) {
                        innerDimension.peopleHeadCount += 1;
                    };
                    innerCb(); 
                });
            } 
            else innerCb();
        }, 
        () => {

            innerDimension.textResponses = textResponses;
            innerDimension.focusAreaResponses = focusAreaResponses;
            innerDimension.topicModelingResponses = [];
            innerDimension.textResponsesWithSurveyResultID = textResponsesWithSurveyResultID;

            // Append TopicModel responses
            TRNReportWidgets.computeTopicModelResponses(topicModels, options)
                .then((topicModelResponse) => {
                    innerDimension.topicModelingResponses = topicModelResponse;
                    return callback(null, innerDimension);
                })
                .catch((err) => {
                    console.log("Topic model compute err: ", err);
                    return callback(null, innerDimension);
                });
        });
    }, // End of getMultiDimensionalReportBasedOnEnergyState

    getMultiDimensionalReportBasedOnEmoState: function(dataDimensionResults, innerDimension, options, callback) {

        // Initialize response array
        var textResponsesWithSurveyResultID = [];
        var textResponses = [];
        var focusAreaResponses = [];
        var topicModels = [];

        /* No.of people fall under each innerDimension column of a widget */
        innerDimension.peopleHeadCount = 0;

        /*
        - Here is the EmoState cutoffs
            Low: <=1.5
            Medium: >1.5 and <4.5
            High: >=4.5
        */

        async.eachSeries(Object.keys(dataDimensionResults), (ind, innerCb) => {

            if (!dataDimensionResults[ind].trnSurveyResultsID) return innerCb();
 
            var stateAverages = dataDimensionResults[ind].trnSurveyResultsID.stateAverages;
            var openEndedResults = dataDimensionResults[ind].responseSet;
            
            var filteredObj = null;
            var isUserReportMatchesFilterCriteria = false;

            if (innerDimension.filter == 'High') {
                filteredObj = _.find(stateAverages, (val) => {
                    return (val.stateName == innerDimension.dimension && parseFloat(val.average) >= 4.5); 
                });
            }
            else if (innerDimension.filter == 'Medium') {
                filteredObj = _.find(stateAverages, (val) => {
                    return (val.stateName == innerDimension.dimension && parseFloat(val.average) > 1.5 && parseFloat(val.average) < 4.5); 
                });
            }
            else if (innerDimension.filter == 'Low') {
                filteredObj = _.find(stateAverages, (val) => {
                    return (val.stateName == innerDimension.dimension && parseFloat(val.average) <= 1.5); 
                });
            }
            else if (innerDimension.filter == 'Highest for User') {

                stateAverages = _.sortBy(stateAverages, 'average'); // sortBy average from asc to desc
                stateAverages = stateAverages.reverse(); // sortBy average from desc to asc 
                
                /* Check whether the most dominating EmoState of the user matches given EmoState dimension. */
                if (stateAverages[0].stateName == innerDimension.dimension) { 
                    filteredObj = stateAverages[0];
                }
                else {
                    var filteredEmoState = _.filter(stateAverages, {'stateName': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple dominating EmoStates, then check whether the given EmoState dimension is one among them. */
                    if (filteredEmoState.length > 0 && filteredEmoState[0].average == stateAverages[0].average)
                        filteredObj = filteredEmoState[0];
                };
            }
            else if (innerDimension.filter == 'Lowest for User') {

                stateAverages = _.sortBy(stateAverages, 'average'); // sortBy average from asc to desc
                
                /* Check whether the most weakest EmoState of the user matches given EmoState dimension. */
                if (stateAverages[0].stateName == innerDimension.dimension) { 
                    filteredObj = stateAverages[0];
                }
                else {
                    var filteredEmoState = _.filter(stateAverages, { 'stateName': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple weak EmoStates, then check whether the given EmoState dimension is one among them. */
                    if (filteredEmoState.length > 0 && filteredEmoState[0].average == stateAverages[0].average)
                        filteredObj = filteredEmoState[0];
                };
            }
  
            if (filteredObj) {

                async.eachSeries(openEndedResults, (data, next) => {

                    if (data.textResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        textResponses.push(data.textResponse);
                        topicModels.push({
                            id: topicModels.length + 1, 
                            text : data.textResponse
                        });
                        textResponsesWithSurveyResultID.push({
                            textResponse : data.textResponse,
                            surveyResultID : dataDimensionResults[ind].trnSurveyResultsID.id
                        });
                    };
                    
                    if (data.focusAreaResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        focusAreaResponses.push(data.focusAreaResponse);
                    };
                    next();
                },
                () => { 

                    if (isUserReportMatchesFilterCriteria === true) {
                        innerDimension.peopleHeadCount += 1;
                    };
                    innerCb(); 
                });
            } 
            else innerCb();
        }, 
        () => {

            innerDimension.textResponses = textResponses;
            innerDimension.focusAreaResponses = focusAreaResponses;
            innerDimension.topicModelingResponses = [];
            innerDimension.textResponsesWithSurveyResultID = textResponsesWithSurveyResultID;

            // Append TopicModel responses
            TRNReportWidgets.computeTopicModelResponses(topicModels, options)
                .then((topicModelResponse) => {
                    innerDimension.topicModelingResponses = topicModelResponse;
                    return callback(null, innerDimension);
                })
                .catch((err) => {
                    console.log("Topic model compute err: ", err);
                    return callback(null, innerDimension);
                });
        });
    }, // End of getMultiDimensionalReportBasedOnEmoState

    getMultiDimensionalReportBasedOnValues: function(dataDimensionResults, innerDimension, options, callback) {

        // Initialize response array
        var textResponsesWithSurveyResultID = [];
        var textResponses = [];
        var focusAreaResponses = [];
        var topicModels = [];

        /* No.of people fall under each innerDimension column of a widget */
        innerDimension.peopleHeadCount = 0;

        /*
        - Here is the Value cutoffs
            Low: <=1.5
            Medium: >1.5 and <4.5
            High: >=4.5
        */

        async.eachSeries(Object.keys(dataDimensionResults), (ind, innerCb) => {

            if (!dataDimensionResults[ind].trnSurveyResultsID) return innerCb()
 
            var valueScores = dataDimensionResults[ind].trnSurveyResultsID.valueScores;
            var openEndedResults = dataDimensionResults[ind].responseSet;

            var filteredObj = null;
            var isUserReportMatchesFilterCriteria = false;

            if (innerDimension.filter == 'High') {
                filteredObj = _.find(valueScores, (val) => {
                    return (val.valueName == innerDimension.dimension && parseFloat(val.valueScore) >= 4.5); 
                });
            }
            else if (innerDimension.filter == 'Medium') {
                filteredObj = _.find(valueScores, (val) => {
                    return (val.valueName == innerDimension.dimension && parseFloat(val.valueScore) > 1.5 && parseFloat(val.valueScore) < 4.5); 
                });
            }
            else if (innerDimension.filter == 'Low') {
                filteredObj = _.find(valueScores, (val) => {
                    return (val.valueName == innerDimension.dimension && parseFloat(val.valueScore) <= 1.5); 
                });
            }
            else if (innerDimension.filter == 'Highest for User') {

                valueScores = _.sortBy(valueScores, 'valueScore'); // sortBy average from asc to desc
                valueScores = valueScores.reverse(); // sortBy average from desc to asc 
                
                /* Check whether the most dominating Values of the user matches given Value dimension. */
                if (valueScores[0].valueName == innerDimension.dimension) { 
                    filteredObj = valueScores[0];
                }
                else {
                    var filteredValueScore = _.filter(stateAverages, {'valueName': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple dominating Values, then check whether the given Value dimension is one among them. */
                    if (filteredValueScore.length > 0 && parseFloat(filteredValueScore[0].valueScore) == parseFloat(stateAverages[0].valueScore))
                        filteredObj = filteredValueScore[0];
                };
            }
            else if (innerDimension.filter == 'Lowest for User') {

                valueScores = _.sortBy(valueScores, 'valueScore'); // sortBy average from asc to desc
                
                /* Check whether the most dominating Values of the user matches given Value dimension. */
                if (valueScores[0].valueName == innerDimension.dimension) { 
                    filteredObj = valueScores[0];
                }
                else {
                    var filteredValueScore = _.filter(stateAverages, {'valueName': innerDimension.dimension});
                    
                    /* Handle tie scenarios. If the user has multiple weak Values, then check whether the given Value dimension is one among them. */
                    if (filteredValueScore.length > 0 && parseFloat(filteredValueScore[0].valueScore) == parseFloat(stateAverages[0].valueScore))
                        filteredObj = filteredValueScore[0];
                };
            }

            if (filteredObj) {

                async.eachSeries(openEndedResults, (data, next) => {

                    if (data.textResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        textResponses.push(data.textResponse);
                        topicModels.push({
                            id: topicModels.length + 1, 
                            text : data.textResponse
                        });
                        textResponsesWithSurveyResultID.push({
                            textResponse : data.textResponse,
                            surveyResultID : dataDimensionResults[ind].trnSurveyResultsID.id
                        });
                    };
                    
                    if (data.focusAreaResponse) {
                        isUserReportMatchesFilterCriteria = true;
                        focusAreaResponses.push(data.focusAreaResponse);
                    };
                    next();
                },
                () => { 

                    if (isUserReportMatchesFilterCriteria === true) {
                        innerDimension.peopleHeadCount += 1;
                    };
                    innerCb(); 
                });
            } 
            else innerCb();
        }, 
        () => {

            innerDimension.textResponses = textResponses;
            innerDimension.focusAreaResponses = focusAreaResponses;
            innerDimension.topicModelingResponses = [];
            innerDimension.textResponsesWithSurveyResultID = textResponsesWithSurveyResultID;

            // Append TopicModel responses
            TRNReportWidgets.computeTopicModelResponses(topicModels, options)
                .then((topicModelResponse) => {
                    innerDimension.topicModelingResponses = topicModelResponse;
                    return callback(null, innerDimension);
                })
                .catch((err) => {
                    console.log("Topic model compute err: ", err);
                    return callback(null, innerDimension);
                });
        });
    }, // End of getMultiDimensionalReportBasedOnValues

    computeTopicModelResponses: function(topicModels, customOptions) {

        return new Promise((resolve, reject) => {
            
            try {

                if (topicModels.length == 0) return resolve([]);
                
                const LDA = require('lda-topic-model');
                const options = {
                    displayingStopwords: false,
                    language: 'en',
                    numberTopics: customOptions.topics,
                    sweeps: 100,
                    stem: customOptions.stem == true || false ? customOptions.stem : false
                };
                const ldaResponse = new LDA(options, topicModels, []);
                return resolve(ldaResponse.getDocuments());
            }
            catch (error) { 
                return reject(error); 
            }
        });
    }, // End of computeTopicModelResponses

    getEnergyStateReport: function(surveyResults, callback) {

        var inertPercentiles = 0;
        var balancedPercentiles = 0;
        var hyperPercentiles = 0;

        async.eachSeries(Object.keys(surveyResults), (ind, innerCb) => {

            var userEnergyStates = surveyResults[ind].energyProfile;

            async.eachSeries(Object.keys(userEnergyStates), (i, nestedCb) => {
                
                if (userEnergyStates[i].state == 'Inert') {
                    inertPercentiles += parseFloat(userEnergyStates[i].average);
                }
                else if (userEnergyStates[i].state == 'Balanced') {
                    balancedPercentiles += parseFloat(userEnergyStates[i].average);
                }
                else if (userEnergyStates[i].state == 'Hyper') {
                    hyperPercentiles += parseFloat(userEnergyStates[i].average);
                };
                nestedCb();
            },
            () => {
                innerCb();
            });
        }, 
        () => {

            var response = [
                {
                    energyState: 'Hyper',
                    percent: (hyperPercentiles / surveyResults.length).toFixed(1)
                },
                {
                    energyState: 'Inert',
                    percent: (inertPercentiles / surveyResults.length).toFixed(1)
                },
                {
                    energyState: 'Balanced',
                    percent: (balancedPercentiles / surveyResults.length).toFixed(1)
                }
            ];
            var sortedResponse = _.sortBy(response, 'percent');
            sortedResponse = sortedResponse.reverse(); // Display results in High to low

            return callback(null, sortedResponse);
        });
    }, // End of getEnergyStateReport

    getEmoStateReport: function(surveyResults, callback) {
    
        var consolidatedScores = {};

        async.eachSeries(Object.keys(surveyResults), (ind, innerCb) => {

            var stateAverages = surveyResults[ind].stateAverages;

            for (var i=0; i < stateAverages.length; i++) {

                if (consolidatedScores[stateAverages[i].stateName]) {
                    consolidatedScores[stateAverages[i].stateName].score += parseFloat(stateAverages[i].average);
                }
                else {
                    consolidatedScores[stateAverages[i].stateName] = {
                        score: parseFloat(stateAverages[i].average),
                        energyState: stateAverages[i].energyState
                    };
                }
            }
            innerCb();
        },
        () => {

            StateQuestionAssociation.find({type:'User'}).exec((findErr, mapList) => {

                if (findErr) return callback(findErr, null);

                var resultantArray = [];

                for(var key in consolidatedScores) {
                    
                    var scoreAverage = (consolidatedScores[key].score / surveyResults.length).toFixed(2);
                    var emostateObj = _.find(mapList, {stateName: key});
                    var stateDefinition = '';

                    // Categorize the level based on scores
                    if (scoreAverage <= 1.5) {
                        stateDefinition = emostateObj.lowScoreDefinition;
                    }
                    else if (scoreAverage >1.5 && scoreAverage <4.5) {
                        stateDefinition = emostateObj.mediumScoreDefinition;
                    }
                    else if (scoreAverage >= 4) {
                        stateDefinition = emostateObj.highScoreDefinition;
                    };

                    resultantArray.push({
                        stateName: key,
                        average : scoreAverage,
                        energyState: consolidatedScores[key].energyState,
                        stateDefinition: stateDefinition
                    });
                };

                var sortedArray = _.sortBy(resultantArray, 'average');

                // Peace needs to be shown only if user hits maximum ratings for each question (average = 6.00 -> 42*6 = 252)
                _.remove(sortedArray, function (resultObject) {
                    return (resultObject.stateName == 'Peace' && resultObject.average < 6.00);
                });

                sortedArray = sortedArray.reverse(); // sort emostate based on score from high to low

                /* handler to handle below logic
                list the emostates based on their energy state with below order
                    - Top three Balanced energy state
                    - Three from Hyper and Inert energy states
                */

                EmostateAssessment.sortFunction(sortedArray, callback);
            });
        });
    }, // End of getEmoStateReport

    getValuesReport: function(surveyResults, callback) {

        var consolidatedScores = {};

        async.eachSeries(Object.keys(surveyResults), (ind, innerCb) => {

            var valueScores = surveyResults[ind].valueScores;

            for (var i=0; i < valueScores.length; i++) {

                if (consolidatedScores[valueScores[i].valueName]) {
                    consolidatedScores[valueScores[i].valueName].score += valueScores[i].valueScore;
                }
                else {
                    consolidatedScores[valueScores[i].valueName] = {
                        score: valueScores[i].valueScore,
                        individualsWithHighScore: 0,
                        individualsWithMediumScore: 0,
                        individualsWithLowScore: 0
                    };
                }

                if (valueScores[i].valueScore <= 1.5) {
                    consolidatedScores[valueScores[i].valueName].individualsWithLowScore += 1;
                }
                else if (valueScores[i].valueScore > 1.5 && valueScores[i].valueScore < 4.5) {
                    consolidatedScores[valueScores[i].valueName].individualsWithMediumScore += 1;
                }
                else if (valueScores[i].valueScore >= 4.5) {
                    consolidatedScores[valueScores[i].valueName].individualsWithHighScore += 1;
                }
            }
            innerCb();
        },
        () => {

            var resultantArray = [];

            for(var key in consolidatedScores) {
                
                resultantArray.push({
                    valueName: key,
                    avgScore : (consolidatedScores[key].score / surveyResults.length).toFixed(2),
                    individualsWithHighScore: consolidatedScores[key].individualsWithHighScore,
                    individualsWithMediumScore: consolidatedScores[key].individualsWithMediumScore,
                    individualsWithLowScore: consolidatedScores[key].individualsWithLowScore
                });
            };

            var sortedArray = _.sortBy(resultantArray, 'avgScore');
            sortedArray = sortedArray.reverse(); // Highest avg. to lowest avg.

            return callback(null, sortedArray);
        });
    }, // End of getValuesReport
};