/**
 * TRNOpenEndedSurveyResult.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        trnSurveyResultsID : {
            model : 'trnSurveyResults'
        }
    },

    saveOpenEndedResponse: function(records, trnSurveyResult, outerCallback) {

        async.eachSeries(records, (record, innerCallback) => {

            if (!record || !record.promptID) return innerCallback();

            var newRecord = (({ 
                promptID = null, 
                prompt = '', 
                responseSet = [] 
            }) => ({ 
                promptID, prompt, responseSet 
            }))(record);
            
            newRecord.responseSet = newRecord.responseSet.filter((res) => { 
                return (res && res.textResponse && res.focusAreaResponse); 
            });
            newRecord.trnOrganizationID = trnSurveyResult.trnOrganizationID;
            newRecord.surveyID = trnSurveyResult.surveyID;
            newRecord.groupID = trnSurveyResult.groupID;
            newRecord.trnSurveyResultsID = trnSurveyResult.id;

            TRNOpenEndedSurveyResult.create(newRecord).exec((err, createdObj) => {
                
                if (err) return innerCallback(err); 
                
                return innerCallback();
            });
            
        }, (err) => { 
            outerCallback(err); 
        });
    }, // End of saveOpenEndedResponse
};