/**
 * TRNOrgSurvey.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        trnOrganizationID: {
            model: 'TRNOrganization',
        },
        sectionID: {
            type: 'string',
            unique: 'true',
            index: true,
            primaryKey:true,
        },
    },
};