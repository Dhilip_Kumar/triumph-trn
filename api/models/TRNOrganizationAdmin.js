/**
 * TRNOrganizationAdmin.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        adminID: {
            type: 'string',
            unique: 'true',
            index: true,
            primaryKey: true,
        },
        trnOrganizationID: {
            model: 'TRNOrganization'
        },
        name: {
            type: 'string',
        },
        email: {
            type: 'string',
            index: true,
            unique: true
        },
        encryptedPassword:{
            type: 'string',
        },
        resetPasswordToken:{
            type:'string'
        },
        status: {
            type: 'string',
        },
        createdBy: {
            type: 'string',
        },
        toJSON: function () {
            var obj = this.toObject();
            delete obj.encryptedPassword;
            delete obj.resetPasswordToken;
            return obj;
        }
    },
    
    beforeCreate: function (values, next) {
        
        UtilService.generateUniqueID((id) => {
            values.adminID = id;
            next();
        });
    },
};