/**
 * TRNOrganization.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        trnOrganizationID: {
            type: 'string',
            unique: 'true',
            index: true,
            primaryKey: true,
        },
        organizationName: {
            type: 'string',
        },
        secret: {
            type: 'string',
            unique: true,
        },
        status: {
            type: 'string',
        },
        createdBy: {
            type: 'string',
        },
        updatedBy: {
            type: 'string',
        },
        trnOrgAdmins: {
            collection: 'TRNOrganizationAdmin',
            via: 'trnOrganizationID'
        }
    },

    toJSON: function () {
        var obj = this.toObject();
        delete obj.createdBy;
        return obj;
    },

    beforeCreate: function (values, next) {
        
        UtilService.generateUniqueID((id) => {
            values.trnOrganizationID = id
            next();
        });
    },
};