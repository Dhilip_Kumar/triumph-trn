/**
 * AuthenticationToken.js.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        deviceID: {
            type: 'string',
        },
        deviceName: {
            type: 'string',
        },
        token: {
            type: 'text',
            maxLength: 512
        },
        userID: {
            type: 'string'
        },
        userRole: {
            type: 'string'
        },
        revoked: {
            type: 'boolean',
            defaultsTo: false 
        }
    },

    createAuthenticationTokenForUser: function (userID, userRole, deviceID, deviceName) {

        return new Promise(function (resolve, reject) {

            var Strings = require('machinepack-strings');
            var token = Strings.random({}).execSync();

            var newAuthenticationToken = {
                token: token,
                deviceID: deviceID,
                userID: userID,
                userRole: userRole,
                deviceName: deviceName,
            };

            AuthenticationToken.create(newAuthenticationToken).exec(function (err, newAuthenticationTokenRecord) {
                
                if (err) return reject(err);
                
                return resolve(newAuthenticationTokenRecord);
            });
        });
    },
};