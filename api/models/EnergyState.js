/**
 * EnergyState.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

    },

    calculateEnergyState: function(valueScores, cb) {

        var balancedScore = 0;
        var inertScore = {coreGroup: 0, supportingGroup: 0};
        var hyperScore = {coreGroup: 0, supportingGroup: 0};

        /* Inert & Hyper Energy States has a negative causal relationship with following values respectively */
        var valuesAssociatedWithInertState = {
            coreGroup: ['Letting Go', 'Dedication & Surrender', 'Courage', 'Positivity & Spirit'],
            supportingGroup: ['Abundance', 'Accepting & Non-Judgement', 'Being Present', 'Being Responsible', 'Self-Awareness', 'Freedom', 'Understanding', 'Innocence', 'Openness', 'Self-Mastery', 'Simplicity', 'Authenticity & Truthfulness']
        };
        var valuesAssociatedWithHyperState = {
            coreGroup: ['Compassion', 'Humility', 'Kindness & Caring', 'Inclusivity'],
            supportingGroup: ['Abundance', 'Accepting & Non-Judgement', 'Being Present', 'Being Responsible', 'Self-Awareness', 'Freedom', 'Understanding', 'Innocence', 'Openness', 'Self-Mastery', 'Simplicity', 'Authenticity & Truthfulness']
        };
        
        async.map(valueScores, (value, nestedCb) => {

            if (valuesAssociatedWithInertState['coreGroup'].indexOf(value.valueName) > -1) {
                inertScore['coreGroup'] += (6 - parseFloat(value.valueScore)); // Due to negative causal relationship, revert the scores.
            };
            if (valuesAssociatedWithInertState['supportingGroup'].indexOf(value.valueName) > -1) {
                inertScore['supportingGroup'] += (6 - parseFloat(value.valueScore)); // Due to negative causal relationship, revert the scores.
            };
            if (valuesAssociatedWithHyperState['coreGroup'].indexOf(value.valueName) > -1) {
                hyperScore['coreGroup'] += (6 - parseFloat(value.valueScore)); // Due to negative causal relationship, revert the scores.
            };
            if (valuesAssociatedWithHyperState['supportingGroup'].indexOf(value.valueName) > -1) {
                hyperScore['supportingGroup'] += (6 - parseFloat(value.valueScore)); // Due to negative causal relationship, revert the scores.
            };
            balancedScore += parseFloat(value.valueScore);
            nestedCb();
        },
        () => {

            var inertCoreGroupAvg = inertScore['coreGroup'] / valuesAssociatedWithInertState['coreGroup'].length;
            var inertSupportingGroupAvg = inertScore['supportingGroup'] / valuesAssociatedWithInertState['supportingGroup'].length;

            var hyperCoreGroupAvg = hyperScore['coreGroup'] / valuesAssociatedWithHyperState['coreGroup'].length;
            var hyperSupportGroupAvg = hyperScore['supportingGroup'] / valuesAssociatedWithHyperState['supportingGroup'].length;

            /* Compute the avg.score */
            var balancedStateAvg = balancedScore / 21;
            var inertStateAvg = (inertCoreGroupAvg * 2/3) + (inertSupportingGroupAvg * 1/3);
            var hyperStateAvg = (hyperCoreGroupAvg * 2/3) + (hyperSupportGroupAvg * 1/3);

            /* Add 1 to the resultant avg.score to make it as a 7pt scale */
            balancedStateAvg += 1;
            inertStateAvg += 1;
            hyperStateAvg += 1;

            /* Convert 7pt to a 10pt scale */
            balancedStateAvg = ((balancedStateAvg / 7) * 10).toFixed(1);
            inertStateAvg = ((inertStateAvg / 7) * 10).toFixed(1);
            hyperStateAvg = ((hyperStateAvg / 7) * 10).toFixed(1);

            return cb(null, {
                inertStateAverage: inertStateAvg,
                balancedStateAverage: balancedStateAvg,
                hyperStateAverage: hyperStateAvg
            });
        });
    }, // End of computeEnergyStateResults

    /* Get associated values for individual Energy State to compute V6 results */

    getAssociatedValuesForV6: function () {

        var valuesAssociatedWithBalancedState = {
            'Tier1': ["Abundance", "Freedom", "Compassion", "Kindness & Caring", "Inclusivity", "Authenticity & Truthfulness", "Being Present", "Being Responsible", "Letting Go", "Accepting & Non-Judgement", "Positivity & Spirit", "Humility", "Innocence", "Openness", "Excellence", "Courage", "Simplicity", "Dedication & Surrender"],
            'Tier2': ["Self-Awareness", "Understanding", "Self-Mastery"]
        };

        var valuesAssociatedWithInertState = {
            'Tier1': ["Positivity & Spirit", "Innocence", "Courage", "Dedication & Surrender"],
            'Tier2': ["Excellence", "Abundance", "Freedom", "Being Present", "Being Responsible", "Letting Go", "Humility", "Openness", "Simplicity"],
            'Tier3': ["Self-Awareness", "Understanding", "Self-Mastery"]
        };

        var valuesAssociatedWithHyperState = {
            'Tier1': ["Compassion", "Kindness & Caring", "Inclusivity", "Accepting & Non-Judgement"],
            'Tier2': ["Authenticity & Truthfulness", "Abundance", "Freedom", "Being Present", "Being Responsible", "Letting Go", "Humility", "Openness", "Simplicity"],
            'Tier3': ["Self-Awareness", "Understanding", "Self-Mastery"]
        };

        return ({
            balanced : valuesAssociatedWithBalancedState,
            inert: valuesAssociatedWithInertState,
            hyper: valuesAssociatedWithHyperState,
        });
    },

    /* Calculate V5 [OR] V6 Energy State Results based on Value Tier approach
        * Balenced State - calculated using two Tier formula,
        * Inert & Hyper State - calculated using three Tier formula
        * Due to negative causal relationship, invert the scores for Inert & Hyper State. 
    */

    calculateEnergyStateByValueTierApproachV6: function (associatedValues, valueScores, cb) {

        var inertScore = { 'Tier1': 0, 'Tier2': 0, 'Tier3': 0 };
        var balancedScore = { 'Tier1': 0, 'Tier2': 0 };
        var hyperScore = { 'Tier1': 0, 'Tier2': 0, 'Tier3': 0 };

        async.eachSeries(valueScores, (value, nestedCb) => {

            /* 
                * Compute Tier1, Tier2 and Tier3 scores for "Inert" & "Hyper" energy state
                * Due to negative causal relationship, invert the scores. 
            */

            if (associatedValues.inert['Tier1'].indexOf(value.valueName) > -1) {
                inertScore['Tier1'] += (6 - parseFloat(value.valueScore));
            };

            if (associatedValues.inert['Tier2'].indexOf(value.valueName) > -1) {
                inertScore['Tier2'] += (6 - parseFloat(value.valueScore));
            };

            if (associatedValues.inert['Tier3'].indexOf(value.valueName) > -1) {
                inertScore['Tier3'] += (6 - parseFloat(value.valueScore));
            };

            if (associatedValues.hyper['Tier1'].indexOf(value.valueName) > -1) {
                hyperScore['Tier1'] += (6 - parseFloat(value.valueScore));
            };

            if (associatedValues.hyper['Tier2'].indexOf(value.valueName) > -1) {
                hyperScore['Tier2'] += (6 - parseFloat(value.valueScore));
            };

            if (associatedValues.hyper['Tier3'].indexOf(value.valueName) > -1) {
                hyperScore['Tier3'] += (6 - parseFloat(value.valueScore));
            };

            /* Compute Tier1 and Tier2 of "Balanced" energy state */
            if (associatedValues.balanced['Tier1'].indexOf(value.valueName) > -1) {
                balancedScore['Tier1'] += parseFloat(value.valueScore);
            };

            if (associatedValues.balanced['Tier2'].indexOf(value.valueName) > -1) {
                balancedScore['Tier2'] += parseFloat(value.valueScore);
            };

            nestedCb();
        },
        () => {

            /* Compute Tier1 and Tier2 averages for "Balanced" state */
            var tier1BalencedAvg = parseFloat(balancedScore['Tier1'] / associatedValues.balanced['Tier1'].length);
            var tier2BalencedAvg = parseFloat(balancedScore['Tier2'] / associatedValues.balanced['Tier2'].length);

            /* Compute Tier1, Tier2 and Tier3 averages for "Inert" state */
            var tier1InertAvg = parseFloat(inertScore['Tier1'] / associatedValues.inert['Tier1'].length);
            var tier2InertAvg = parseFloat(inertScore['Tier2'] / associatedValues.inert['Tier2'].length);
            var tier3InertAvg = parseFloat(inertScore['Tier3'] / associatedValues.inert['Tier3'].length);

            /* Compute Tier1, Tier2 and Tier3 averages for "Hyper" state */
            var tier1HyperAvg = parseFloat(hyperScore['Tier1'] / associatedValues.hyper['Tier1'].length);
            var tier2HyperAvg = parseFloat(hyperScore['Tier2'] / associatedValues.hyper['Tier2'].length);
            var tier3HyperAvg = parseFloat(hyperScore['Tier3'] / associatedValues.hyper['Tier3'].length);

            /* Compute formula for energy states */
            var balancedStateAvg = ((tier1BalencedAvg * 2/3) + (tier2BalencedAvg * 1/3));
            var inertStateAvg = ((tier1InertAvg * 2/3) + (tier2InertAvg * 2/9) + (tier3InertAvg * 1/9));
            var hyperStateAvg = ((tier1HyperAvg * 2/3) + (tier2HyperAvg * 2/9) + (tier3HyperAvg * 1/9));

            /* Add 1 to the resultant avg.score to make it as a 7pt scale */
            balancedStateAvg += 1;
            inertStateAvg += 1;
            hyperStateAvg += 1;

            /* Convert 7pt to a 10pt scale */
            balancedStateAvg = ((balancedStateAvg / 7) * 10).toFixed(1);
            inertStateAvg = ((inertStateAvg / 7) * 10).toFixed(1);
            hyperStateAvg = ((hyperStateAvg / 7) * 10).toFixed(1);

            /* Return V5 results */
            return cb(null, {
                balancedStateAverage: balancedStateAvg,
                inertStateAverage: inertStateAvg,
                hyperStateAverage: hyperStateAvg
            });
        });
    }// End of calculateEnergyStateByValueTierApproachV6
};