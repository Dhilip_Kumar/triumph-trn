/**
 * TRNOrgGroups.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        
        groupID: {
            type: 'string',
            unique: 'true',
            index: true,
            primaryKey:true,
        },
        trnOrganizationID: {
            model: 'TRNOrganization',
        },
        activeSurveyID:{
            model: 'TRNOrgSurvey'
        }
    },

    toJSON: function() {
        var obj = this.toObject();
        delete obj.createdBy;
        delete obj.updatedBy;
        return obj;
    },
    
    beforeCreate: function(values, next) {
        
        UtilService.generateUniqueID((id) => {
            values.groupID = id;
            next();
        });
    },

    updateTRNOrgGroups: function(orgGroupObjToUpdate, callback) {
        
        if (!orgGroupObjToUpdate) {
            return callback('Invalid Request', null);
        };

        if (orgGroupObjToUpdate.trnOrganizationID && orgGroupObjToUpdate.groupID) { // Update operation

            var searchCriteria = {
                trnOrganizationID: orgGroupObjToUpdate.trnOrganizationID,
                groupID: orgGroupObjToUpdate.groupID
            };

            TRNOrgGroups.update(searchCriteria, orgGroupObjToUpdate).exec((updateErr, updatedRecords) => {

                if (updateErr) return callback(updateErr, null);

                if (updatedRecords && updatedRecords.length == 1) {
                    return callback(null, updatedRecords[0]);
                }
                else {
                    return callback('Invalid Request', null);
                };
            });
        }
        else { // Create operation

            TRNOrgGroups.create(orgGroupObjToUpdate).exec((createErr, createdRecord) => {

                if (createErr) return callback(createErr, null);

                if (createdRecord) {
                    return callback(null, createdRecord);
                }
                else {
                    return callback('Invalid Request', null);
                };
            });
        };
    }, // End of updateTRNOrgGroups
};