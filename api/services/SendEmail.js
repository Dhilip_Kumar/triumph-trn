/**
 * SendEmail.js
 * @description :: Server-side logic for sending Email notifications
*/

var mailgun = require('mailgun-js') ({
    apiKey: sails.config.mailgun.apiKey,
    domain: sails.config.mailgun.domain
});

module.exports = {

    notifySurveyCompletionToAdmin: function(adminDetails, report, callback) {

        var messageTemplate = 'Hi '+ adminDetails.name + '! <br> <br>' +
                              'A User from your organization has just completed the Triumph Survey.<br> <br>' +
                              '<table style="min-width:50%; display:table; border: 1px solid #ddd; border-spacing:0; border-collapse: collapse"><tbody style="display:table-row-group; vertical-align:middle; border-color: inherit">';

        for (key in report) {
            messageTemplate += '<tr style="display: table-row; vertical-align:inherit; border-color:inherit">';
            messageTemplate += '<td style="border: 1px solid #ddd; max-width:50%; padding:5px 10px;"><b>' + key + '</b></td>';
            messageTemplate += '<td style="border: 1px solid #ddd; max-width:50%"; padding:5px 10px; align="center">' + report[key] + '</td>';
            messageTemplate += '</tr>';   
        };

        messageTemplate +=  '</tbody></table> <br>' +
                            'For all things Triumph, you can follow us on <a href="https://www.twitter.com/letstriumph?src=results-twitter">Twitter</a> and <a href="https://www.instagram.com/letstriumph?src=results-instagram">Instagram</a> where we regularly update with news, compelling articles, exercises and so much more! <br> <br>' +
                            'Best, <br>'+
                            'Team Triumph <br>'+
                            '<i>Actualize Potential. Live Joyously.</i> <br> <br>'+
                            'Our Website: https://www.triumphhq.com <br> <br>';

        var data = {
            from: 'Triumph <hello@triumphhq.com>',
            to: adminDetails.email,
            bcc: 'dhilip+trn@thinkcodesoft.com',
            subject: '[Triumph TRN] A User has completed the Survey',
            html: messageTemplate
        };

        mailgun.messages().send(data, callback);
    }, // End of notifySurveyCompletionToAdmin
}