module.exports = {

    USER_ROLE : { 
        TRN_SUPER_ADMIN : "super-admin",
        TRN_ORG_ADMIN : "admin",
        TRN_ORG_EMP : "employee",
    },

    TOPIC_MODELING : {
        MULTI_DIMENSION : {
            STEM: false, 
            TOPICS: 3
        },
        DATA_DIMENSION : {
            STEM: true, 
            TOPICS: 6
        },
    },
};  