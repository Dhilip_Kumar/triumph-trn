module.exports = {

    generateUniqueID: function(callback) {
        return callback(
            UtilService.generateGUID() 
            + UtilService.generateGUID() 
            + '-' + UtilService.generateGUID() 
            + '-' + UtilService.generateGUID() 
            + '-' + UtilService.generateGUID() 
            + '-' + UtilService.generateGUID() 
            + UtilService.generateGUID() 
            + UtilService.generateGUID()
        );
    },
    
    generateGUID: function() {
        return  Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
}