/**
 * SuperAdminController
 *
 * @description :: Server-side logic for managing superadmins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Passwords = require('machinepack-passwords');

module.exports = {

    fetchTRNOrganizations: function(req, res) {

        TRNOrganization.find({}).exec((findErr, foundOrganizations) => {

            if (findErr) return res.serverError(findErr);

            return res.json(foundOrganizations);
        });
    }, // End of fetchTRNOrganizations

    updateTRNOrganization: function(req, res) {

        var organizationObjToUpdate = req.body.organizationObjToUpdate;

        if (!organizationObjToUpdate) {
            return res.badRequest('Invalid Request');
        };

        if (organizationObjToUpdate.trnOrganizationID) { // Update operation

            var searchCriteria = {
                trnOrganizationID: organizationObjToUpdate.trnOrganizationID
            };

            TRNOrganization.update(searchCriteria, organizationObjToUpdate).exec((updateErr, updatedRecords) => {

                if (updateErr) return res.serverError(updateErr);

                if (updatedRecords && updatedRecords.length == 1) {
                    return res.json(updatedRecords[0]);
                }
                else {
                    return res.badRequest('Invalid Request');
                };
            });
        }
        else { // Create operation

            TRNOrganization.create(organizationObjToUpdate).exec((createErr, createdRecord) => {

                if (createErr) return res.serverError(createErr);

                if (createdRecord) {
                    return res.json(createdRecord);
                }
                else {
                    return res.badRequest('Invalid Request');
                };
            });
        };
    }, // End of updateTRNOrganization

    fetchTRNOrgAdmins: function(req, res) {

        var trnOrganizationID = req.body.trnOrganizationID;

        if (!trnOrganizationID) {
            return res.badRequest('Invalid Request');
        };

        TRNOrganizationAdmin.find({trnOrganizationID: trnOrganizationID}).exec((findErr, foundAdmins) => {

            if (findErr) return res.serverError(findErr);

            return res.json(foundAdmins);
        });
    }, // End of fetchTRNOrgAdmins

    updateTRNOrgAdmin: function(req, res) {

        var adminObjToUpdate = req.body.adminObjToUpdate;
        var trnOrganizationID = req.body.trnOrganizationID;

        if (!adminObjToUpdate || !trnOrganizationID) {
            return res.badRequest('Invalid Request');
        };

        if (adminObjToUpdate.trnOrganizationID && adminObjToUpdate.adminID) { // Update operation

            var searchCriteria = {
                trnOrganizationID: adminObjToUpdate.trnOrganizationID,
                adminID: adminObjToUpdate.adminID
            };

            var objToBeUpdated = {
                name: adminObjToUpdate.name, 
                status: adminObjToUpdate.status
            };

            TRNOrganizationAdmin.update(searchCriteria, objToBeUpdated).exec((updateErr, updatedRecords) => {

                if (updateErr) {

                    // Check for duplicate email
                    if (updateErr.invalidAttributes && updateErr.invalidAttributes.email[0].rule === 'unique') {
                        return res.send(409, 'Email address already exists, please try again.');
                    };
                    return res.serverError(updateErr);
                }

                if (updatedRecords && updatedRecords.length == 1) {
                    return res.json(updatedRecords[0]);
                }
                else {
                    return res.badRequest('Invalid Request');
                };
            });
        }
        else { // Create operation

            Passwords.encryptPassword({
                password: 'j9fD2$j9ds', // Until the introduction of the invitation accept flow, let's use some hardcoded string as password.
            }).exec({

                error: function(encryptionError) {
                    return res.serverError(encryptionError);
                },

                success: function(result) {

                    adminObjToUpdate.trnOrganizationID = trnOrganizationID; // Associate "trnOrganizationID" to the new admin
                    adminObjToUpdate.encryptedPassword = result;

                    TRNOrganizationAdmin.create(adminObjToUpdate).exec((createErr, createdRecord) => {

                        if (createErr) {

                            // Check for duplicate email
                            if (createErr.invalidAttributes && createErr.invalidAttributes.email[0].rule === 'unique') {
                                return res.send(409, 'Email address already exists, please try again.');
                            };
                            return res.serverError(createErr);
                        }

                        if (createdRecord) {
                            return res.json(createdRecord);
                        }
                        else {
                            return res.badRequest('Invalid Request');
                        };
                    });
                }
            });
        };
    }, // End of updateTRNOrgAdmin

    /* Get Data Export filter options
     - List of survey sections
     - List of TRN organizations
    */

    getDataExportFilterOptions: function(req, res) {

        async.auto({

            // fetch survey section
            surveySections: function(callback) {

                var searchCriteria = {
                    select: ['sectionID','sectionTitle']
                };

                /* Fetch TRNOrgSurvey */
                TRNOrgSurvey.find(searchCriteria).sort('sectionTitle ASC').exec(callback);
             },

            // fetch TRNOrganizations
            organizations: function (callback) {

                var searchCriteria = {
                    select: ['trnOrganizationID','organizationName']
                };

                /* Fetch TRNOrganization */
                TRNOrganization.find(searchCriteria).sort('organizationName').exec(callback);
            },

            // fetch TRNOrgGroups
            groups: function (callback) {

                var searchCriteria = {
                    select: ['trnOrganizationID', 'groupID', 'groupName']
                };

                /* Fetch TRNOrgGroups */
                TRNOrgGroups.find(searchCriteria).exec(callback);
            },
        }, 
        function allDone (err, asyncData) {

            if (err) {
                return res.serverError(err);
            };

            return res.json(asyncData);
        });    
    }, // End of getDataExportFilterOptions

    /*
     - This API will be invoked from the SuperAdmin dashboard.
     - This will export the survey report for the selected date range, organization and survey sections.
    */

    getSurveyReportsForAnalysis: function(req , res) {

        var startDate = req.body.startDate || moment().subtract(7, 'days').startOf('day').toISOString();
        var endDate = req.body.endDate || moment().endOf('day').toISOString();
        var sectionID = req.body.sectionID;
        var trnOrganizationID = req.body.trnOrganizationID;
        var trnOrgGroupID = req.body.trnOrgGroupID;
        
        if (!sectionID || !trnOrganizationID) {
            return res.badRequest('Invalid Request');
        };

        async.auto({
            
            fetchSurveyResults: function(callback) {
                
                var suportedReportVersion = TRNSurveyResults.getSupportedReportVersionForDataExport();
                var searchCriteria = {
                    reportVersion : suportedReportVersion,
                    sectionID: sectionID,
                    createdAt: {$gte: startDate, $lte: endDate},
                };

                if (trnOrganizationID.toUpperCase() != 'ALL') { 
                    searchCriteria.trnOrganizationID = trnOrganizationID; // filter based on organization
                };

                if (trnOrgGroupID.toUpperCase() != 'ALL') { 
                    searchCriteria.groupID = trnOrgGroupID; // filter based on specific group
                };
                
                /* Fetch TRNSurveyResults for the selected date range */
                TRNSurveyResults.find(searchCriteria).sort('reportID ASC').exec(callback); 
            },

            fetchInternalEmoStates: function (callback) {

                var searchCriteria = {
                    type : ['Internal', 'Facilitator'] 
                };

                /* Fetch "Internal" and "Facilitator" EmoStates */
                StateQuestionAssociation.find(searchCriteria).exec(callback);
            },

            formatResponse: ['fetchSurveyResults', 'fetchInternalEmoStates', (callback, asyncData) => {

                var surveyResults = asyncData.fetchSurveyResults;
                var internalEmoStates = asyncData.fetchInternalEmoStates;

                /* Compute Reports */
                TRNSurveyResults.computeReports(sectionID, surveyResults, internalEmoStates, callback);
            }],
        },
        function allDone (err, asyncData) {

            if (err) {
                return res.serverError(err);
            };

            return res.json(asyncData.formatResponse);
        });
    }, // End of getSurveyReportsForAnalysis
};