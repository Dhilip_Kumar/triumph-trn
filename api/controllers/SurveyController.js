/**
 * SurveyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
*/

module.exports = {

    getSurveyListForTheGroup: function(req, res) {

        var groupID = req.body.groupID;
        var trnOrganizationID = req.body.trnOrganizationID;
		
        if (!groupID || !trnOrganizationID) return res.badRequest('Invalid Request');

        var searchCriteria = {
            groupID: groupID,
            trnOrganizationID: trnOrganizationID
        };

        TRNOrgGroups.findOne(searchCriteria).exec((findErr, groupDetails) => {

            if (findErr) return res.serverError(findErr);

            if (!groupDetails) return res.badRequest('Invalid Request');

            /* SurveyList value of a group will be stored as an array of JSON objects. Pluck only ID's here for computation. */
            var surveyList = _.map(groupDetails.surveyList, 'sectionID');
            var processedSurveyList = []; // The final array that will be send to the frontend.

            async.eachSeries(Object.keys(surveyList), (key, cb) => {

                if (surveyList[key] == 'IIA') {

                    EmostateAssessment.find().sort('questionId ASC').exec((assmntErr, assmntQuestions) => {

                        if (assmntErr) return res.serverError(assmntErr);
            
                        processedSurveyList.push({
                            sectionID: 'IIA',
                            sectionTitle: 'Individual Readiness',
                            overviewBlock: 'Any team or organization can only be as prepared or effective as the individual members, and this section is designed to help get a sense about where the individual team members are at.',
                            statementsAndPrompts: assmntQuestions
                        });
                        return cb();
                    });
                }
                else {
                    
                    TRNOrgSurvey.findOne({sectionID: surveyList[key]}).exec((surveyFindErr, surveyObj) => {
                    
                        if (surveyFindErr) return res.serverError(surveyFindErr);

                        if (surveyObj) {
                            /* Delete mongo generated uniqueIDs to avoid conflicts when storing survey response. */
                            delete surveyObj.id;
                            delete surveyObj.createdAt;
                            delete surveyObj.updatedAt;
                            processedSurveyList.push(surveyObj);
                        };                        
                        return cb();
                    });
                }
            },
            () => {
                groupDetails.surveySections = processedSurveyList;
                return res.json(groupDetails);
            });         
        });
    }, // End of getSurveyListForTheGroup

    submitSurveyResponses: function (req, res) {

        var surveyResponses = req.body.surveyResponses || [];
        var trnOrganizationID = req.body.organizationID;
        var groupID = req.body.groupID;

        if (surveyResponses.length == 0 || !trnOrganizationID || !groupID) {
            return res.badRequest('Invalid Request');
        };

        /*
         - Boolean flag to determine whether IIA was part of the survey. 
         - The IIA report will be shown to the users based on this flag and organization configurations. 
        */
        var containsIIASection = false;
        var reportID = UtilService.generateGUID() + '-' + UtilService.generateGUID() + '-' + Date.now();

        /* Secondary organization and group validation check */
        async.auto({

            fetchOrganizationAdmin: function(cb) {
                TRNOrganizationAdmin.findOne({trnOrganizationID: trnOrganizationID}).exec(cb);
            },

            fetchGroupPreferences: function(cb) {
                TRNOrgGroups.findOne({trnOrganizationID: trnOrganizationID, groupID: groupID}).exec(cb);
            },

            genereteSurveyCompletionCode: function(cb) {
                TRNSurveyResults.getSurveyCompletionCode(cb);
            }
        },
        (validationError, asyncData) => {

            if (validationError) return res.serverError(validationError);

            var orgAdminDetails = asyncData.fetchOrganizationAdmin;
            var orgGroupDetails = asyncData.fetchGroupPreferences;

            if (!orgAdminDetails || !orgGroupDetails) {
                return res.badRequest('Invalid Request');
            };

            /* General info that needs to be stored on each section associated with the survey. */

            var metaData = {
                reportID: reportID,
                reportVersion: 1,
                completionCode: asyncData.genereteSurveyCompletionCode,
                deviceID: req.param('deviceID'),
                platform: req.param('platform'),
                client: req.param('client'),
                trnOrganizationID: trnOrganizationID,
                groupID: groupID,
                surveyStartTime: req.body.surveyStartTime,
                surveyEndTime: req.body.surveyEndTime,
                totalSurveyDurationInSeconds: req.body.totalSurveyDurationInSeconds,
                channel: req.body.channel
            };

            async.eachSeries(Object.keys(surveyResponses), (key, callback) => {

                var sectionData = surveyResponses[key];
    
                if (sectionData.sectionID == 'IIA') {
                    
                    EmostateAssessment.calculateScoreAndMakeResult(sectionData.statementsAndPrompts, (computeErr, result) => {
    
                        if (computeErr) return res.serverError(computeErr); 
    
                        result.sectionID = 'IIA';
                        result.assessmentId = reportID; // This field will be handy when centralizing the Triumph accounts
                        result = _.merge(result, metaData);
                        result.sectionStartTime = sectionData.sectionStartTime;
                        result.sectionEndTime = sectionData.sectionEndTime;
    
                        containsIIASection = true;
                        TRNSurveyResults.create(result).exec(callback);
                    });
                }
                else {
                    sectionData = _.merge(sectionData, metaData);
                    TRNSurveyResults.create(sectionData).exec(callback);
                }
            },
            (asyncErr, data) => {

                if (asyncErr) return res.serverError(asyncErr);

                var emailParams = {
                    'Group Name': orgGroupDetails.groupName,
                    'Report ID': reportID,
                    'Completion Code': asyncData.genereteSurveyCompletionCode,
                    'Survey Sections': _.map(surveyResponses, 'sectionTitle').join(', '),
                    'Total Survey Duration': req.body.totalSurveyDurationInSeconds + ' secs',
                    'Channel': req.body.channel
                };

                SendEmail.notifySurveyCompletionToAdmin(orgAdminDetails, emailParams, () => {

                    var response = {
                        status: 'success'
                    };

                    if (containsIIASection == true && orgGroupDetails.showIIAReportToTheUser == true) {
                        response.reportID = reportID;
                    };

                    if (orgGroupDetails.showSurveyCompletionCodeToTheUser == true) {
                        response.surveyCompletionCode = asyncData.genereteSurveyCompletionCode;
                    };

                    return res.json(response);
                });
            });
        });
    }, // End of submitSurveyResponses

    getIIAReport: function(req, res) {

		var reportID = req.body.reportID;
		
		if (!reportID) return res.ok('Invalid Request');

		async.auto({

			fetchAssessmentResults: function(callback) {
				TRNSurveyResults.findOne({reportID: reportID, sectionID: 'IIA'}).exec(callback);
			},

			getRecommendedTracks: ['fetchAssessmentResults', function(callback, asyncData) {
				Tracks.getRecommendedTracks(asyncData.fetchAssessmentResults, callback);
			}],
		},
		(asyncError, asyncData) => {

			if (asyncError) return res.serverError(asyncError);

			return res.json({
				results: asyncData.fetchAssessmentResults,
				recommendedTracks: asyncData.getRecommendedTracks
			});
		});
    }, // End of getIIAReport
};

