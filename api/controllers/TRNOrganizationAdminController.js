/**
 * TRNOrganizationAdminController
 *
 * @description :: Server-side logic for managing Trnorganizationadmins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    
    getGroupManagementInfo: function(req, res) {

        var trnOrganizationID = req.body.trnOrganizationID;

        if (!trnOrganizationID) return res.badRequest('Invalid Request');

        async.auto({

            fetchGroups: function(cb) {
                TRNOrgGroups.find({trnOrganizationID: trnOrganizationID}).exec(cb);
            },

            fetchSurveyList: function(cb) {

                var searchCriteria = {
                    select: ['sectionID', 'sectionTitle']
                };

                TRNOrgSurvey.find(searchCriteria).sort('sectionTitle ASC').exec(cb);
            }
        },
        (asyncError, asyncData) => {

            if (asyncError) return res.serverError(asyncError);

            return res.json({
                orgGroups: asyncData.fetchGroups,
                surveyList: asyncData.fetchSurveyList
            });
        });
    }, // End of getGroupManagementInfo

    updateTRNOrgGroups: function(req, res) {

        var orgGroupObjToUpdate = req.body.orgGroupObjToUpdate;

        TRNOrgGroups.updateTRNOrgGroups(orgGroupObjToUpdate, (error, response) => {

            if (error) return res.serverError(error);

            if (response) {
                return res.json(response);
            }
            else {
                return res.ok();
            };
        });
    }, // End of updateTRNOrgGroups

    getOrganizationDashboardStats: function(req, res) {

        var trnOrganizationID = req.body.organizationID;

        if (!trnOrganizationID) return res.badRequest('Invalid Request');

        async.auto({

            getOrganizationSurveyResults: function(callback) {
                TRNSurveyResults.count({trnOrganizationID: trnOrganizationID}, callback);
            },
            getOrganizationWidgets: function(callback) {
                
                var searchCriteria = {
                    where: {
                        OR: [
                            { isDefault: true },
                            { trnOrganizationID: trnOrganizationID, isActive: true }
                        ]
                    }
                };
                TRNReportWidgets.find(searchCriteria).sort('widgetID ASC').exec(callback);
            }
        },
        (cbErr, asyncData) => {

            if (cbErr) return res.serverError(cbErr);

            return res.json({
                totalSurveyRecorded: asyncData.getOrganizationSurveyResults,
                widgets: asyncData.getOrganizationWidgets
            });
        });
    }, // End of getOrganizationDashboardStats

    /* Send list of available data dimensions and inner dimensions to the front-end to let admin add custom widgets. */
    
    getCustomWidgetDependencies: function(req, res) {

        var trnOrganizationID = req.body.organizationID;

        if (!trnOrganizationID) return res.badRequest('Invalid Request');

        async.auto({
            
            getEnergyStates: function(callback) {
                
                EnergyState.find().exec((err, energyStates) => {

                    if (err) return callback(err, null);

                    var energySelectors = [];

                    async.eachSeries(Object.keys(energyStates), (ind, innerCallback) => {
                        
                        energySelectors.push({
                            label: energyStates[ind].state,
                            value: energyStates[ind].state,
                            category: 'Energy State'
                        });
                        innerCallback();
                    },
                    () => {
                        callback(null, {
                            label: 'Energy States', 
                            options: energySelectors
                        });
                    });
                });
            },
            getEmoStates: function(callback) {

                StateQuestionAssociation.find({type:'User'}).exec((err, emoStates) => {

                    if (err) return callback(err, null);

                    var emoStateSelectors = [];

                    async.eachSeries(Object.keys(emoStates), (ind, innerCallback) => {
                        
                        emoStateSelectors.push({
                            label: emoStates[ind].stateName,
                            value: emoStates[ind].stateName,
                            category: 'EmoState'
                        });
                        innerCallback();
                    },
                    () => {
                        callback(null, {
                            label: 'EmoStates', 
                            options: emoStateSelectors
                        });
                    });
                });
            },
            getValues: function(callback) {

                ValueMapping.find().exec((err, values) => {

                    if (err) return callback(err, null);

                    var valueSelectors = [];

                    async.eachSeries(Object.keys(values), (ind, innerCallback) => {
                        
                        valueSelectors.push({
                            label: values[ind].valueName,
                            value: values[ind].valueName,
                            category: 'Values'
                        });
                        innerCallback();
                    },
                    () => {
                        callback(null, {
                            label: 'Values', 
                            options: valueSelectors
                        });
                    });
                });
            },
            getDataDimensions: function(callback) {

                TRNSurveyPrompts.find().sort('promptID ASC').exec((err, prompts) => {

                    if (err) return callback(err, null);

                    var dataDimensionSelectors = [];

                    async.eachSeries(Object.keys(prompts), (ind, innerCallback) => {
                        
                        dataDimensionSelectors.push({
                            label: prompts[ind].prompt,
                            value: prompts[ind].prompt,
                            category: 'DataDimension'
                        });
                        innerCallback();
                    },
                    () => callback(null, dataDimensionSelectors));
                });
            },
            getOrgGroups: function(callback) {
                
                TRNOrgGroups.find({trnOrganizationID: trnOrganizationID}).exec((err, groups) => {

                    if (err) return callback(err, null);

                    var groupSelectors = [{
                        label: 'ALL',
                        value: 'all',
                        category: 'OrgGroups'
                    }]; // The value 'all' will help admin to view reports across all the groups. 

                    async.eachSeries(Object.keys(groups), (ind, innerCallback) => {
                        
                        groupSelectors.push({
                            label: groups[ind].groupName,
                            value: groups[ind].groupID,
                            category: 'OrgGroups'
                        });
                        innerCallback();
                    },
                    () => callback(null, groupSelectors));
                });
            },
            getOrgSurvyes: function(callback) {

                TRNOrgSurvey.find({trnOrganizationID: trnOrganizationID}).exec((err, surveys) => {

                    if (err) return callback(err, null);

                    var surveySelectors = [{
                        label: 'ALL',
                        value: 'all',
                        category: 'OrgSurveys'
                    }]; // The value 'all' will help admin to view reports across all the surveys. 

                    async.eachSeries(Object.keys(surveys), (ind, innerCallback) => {
                        
                        surveySelectors.push({
                            label: surveys[ind].surveyName,
                            value: surveys[ind].surveyID,
                            category: 'OrgSurveys'
                        });
                        innerCallback();
                    },
                    () => callback(null, surveySelectors));
                });
            }
        },
        (cbErr, asyncData) => {

            if (cbErr) return res.serverError(cbErr);

            var innerDimensions = [];
            innerDimensions.push(asyncData.getEnergyStates);
            innerDimensions.push(asyncData.getEmoStates);
            innerDimensions.push(asyncData.getValues);

            return res.json({
                organizationID: trnOrganizationID,
                dataDimensions: asyncData.getDataDimensions,
                innerDimensions: innerDimensions,
                orgGroups: asyncData.getOrgGroups,
                orgSurveys: asyncData.getOrgSurvyes
            });
        });
    }, // End of getCustomWidgetDependencies

    getInnerDimensionWidgetReport: function(req, res) {

        var trnOrganizationID = req.body.organizationID;
        var section = req.body.section;
        var response = [];

        if (!trnOrganizationID || !section) return res.badRequest('Invalid Request');

        TRNSurveyResults.find({trnOrganizationID: trnOrganizationID}).exec((err, surveyResults) => {

            if (err) return res.serverError(err);

            if (surveyResults && surveyResults.length > 0) {

                /* Individual sections reports are dependend of other sections. Therefore fetch all section reports and process as per the requirements. */

                async.auto({
                    getEnergyStateReport: function(callback) {
                        TRNReportWidgets.getEnergyStateReport(surveyResults, callback);
                    },
                    getEmoStateReport: function(callback) {
                        TRNReportWidgets.getEmoStateReport(surveyResults, callback);
                    },
                    getValuesReport: function(callback) {
                        TRNReportWidgets.getValuesReport(surveyResults, callback);
                    }
                },
                (err, asyncData) => {

                    if (err) return res.serverError(err);

                    if (section == 'Energy State Report') {
                        response = asyncData.getEnergyStateReport;
                    }
                    else if (section == 'EmoState Report') {
                        response = {
                            emostateReport: asyncData.getEmoStateReport,
                            energyReport: asyncData.getEnergyStateReport
                        };
                    }
                    else if (section == 'Values Report') {
                        response = asyncData.getValuesReport;
                    }
                    return res.json(response);
                });
            }
            else {
                return res.json(response);
            }
        });
    }, // End of getInnerDimensionWidgetReport

    getMultiDimensionalWidgetReport: function(req, res) {

        var trnOrganizationID = req.body.organizationID;
        var dataDimension = req.body.dataDimension;
        var innerDimensions = req.body.innerDimensions;
        var groupDataset = req.body.groupDataset;
        var surveyDataset = req.body.surveyDataset;
        var options = req.body.options ? req.body.options : {};
        
        options.topics = GlobalsService.TOPIC_MODELING.MULTI_DIMENSION.TOPICS; // set default number of topics

        if (!trnOrganizationID || !dataDimension || !innerDimensions) 
            return res.badRequest('Invalid Request');

        var params = {
            trnOrganizationID: trnOrganizationID,
            dataDimension: dataDimension,
            innerDimensions: innerDimensions,
            groupDataset: groupDataset,
            surveyDatase: surveyDataset,
            options: options
        };

        TRNReportWidgets.getMultiDimensionalReport(params, (err, results) => {
            
            if (err) return res.serverError(err);
            
            return res.json(results);
        });
    }, // End of getMultiDimensionalWidgetReport

    getDataDimensionalWidgetReport: function(req, res) {

        var trnOrganizationID = req.body.trnOrganizationID;
        var dataDimension = req.body.dataDimension;
        var groupDataset = req.body.groupDataset;
        var surveyDataset = req.body.surveyDataset;
        var options = req.body.options ? req.body.options : {};
        options.topics = GlobalsService.TOPIC_MODELING.DATA_DIMENSION.TOPICS; // set default number of topics

        if (!trnOrganizationID || !dataDimension ) 
            return res.badRequest('Invalid Request');

        var findCriteria = {
            trnOrganizationID: trnOrganizationID,
            prompt: dataDimension
        };
       
        if (surveyDataset && surveyDataset.length > 0 && surveyDataset.indexOf("all") == -1) 
            findCriteria.surveyID = surveyDataset;
        
        if (groupDataset && groupDataset.length > 0 && groupDataset.indexOf("all") == -1) 
            findCriteria.groupID = groupDataset;

        async.auto({

            dataDimensionResults: function(cb) {

                TRNOpenEndedSurveyResult.find(findCriteria)
                .populate('trnSurveyResultsID')
                .exec((err, dataDimensionResults) => {
                    
                    if (err) return cb(err);
                    
                    cb(null, dataDimensionResults);
                });
            },

            getDataDimensionalReport: ["dataDimensionResults", (cb, asyncData) => {
                TRNReportWidgets.getDataDimensionalReport(asyncData.dataDimensionResults, options, cb);
            }],
        },
        (err, asyncData) => {

            if (err) return res.serverError(err);

            return res.json(asyncData.getDataDimensionalReport);
        });

    }, // End of getDataDimensionalWidgetReport

    saveCustomWidget: function(req, res) {

        var trnOrganizationID = req.body.organizationID;
        var action = req.body.action;
        var widgetToUpdate = req.body.widgetToUpdate;

        if (!trnOrganizationID || !action || !widgetToUpdate) return res.badRequest('Invalid Request');

        if (widgetToUpdate && Object.keys(widgetToUpdate).length > 0) {

            // Add trnOrganizationID
            widgetToUpdate.trnOrganizationID = trnOrganizationID;
            
            TRNReportWidgets.updateWidget(action, widgetToUpdate, (err, result) => {

                if (err) return res.serverError(err);

                return res.json({status: 'success'});
            });
        }
        else {
            return res.badRequest('Invalid Request.')
        }
    }, // End of saveCustomWidget
};