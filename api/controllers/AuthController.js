/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    TRNOrgAdminLogin: function (req, res) {

        if (!req.body.email || !req.body.password) {
            res.send(409, "Please enter username and password.");
        }

        var Passwords = require('machinepack-passwords');

        TRNOrganizationAdmin.findOne({
            email: req.body.email
        })
        .populate('trnOrganizationID')
        .exec(function (err, foundAdmin) {

            if (err) return res.negotiate(err);
            if (!foundAdmin) return res.send(409, 'Invalid credentials, please try again.');

            Passwords.checkPassword({
                passwordAttempt: req.body.password,
                encryptedPassword: foundAdmin.encryptedPassword
            })
            .exec({

                error: function (err) {
                    return res.negotiate(err);
                },

                incorrect: function () {
                    return res.send(409, 'Invalid credentials, please try again.');
                },

                success: function () {

                    if (foundAdmin.status.toString().toLowerCase() == "disabled") {
                        return res.forbidden("'Your account has been Disabled.  Please contact support.'");
                    }

                    AuthenticationToken.createAuthenticationTokenForUser(foundAdmin.adminID, GlobalsService.USER_ROLE.TRN_ORG_ADMIN, req.param('deviceID'), req.param('deviceName'))
                        .then((newAuthenticationTokenObj) => {

                            var returnObj = {
                                userID: foundAdmin.adminID,
                                accessToken: newAuthenticationTokenObj.token,
                                username: foundAdmin.name,
                                email: foundAdmin.email,
                                userRole: GlobalsService.USER_ROLE.TRN_ORG_ADMIN,
                                organizationID: foundAdmin.trnOrganizationID.trnOrganizationID,
                                organizationName: foundAdmin.trnOrganizationID.organizationName
                            };

                            return res.json(returnObj);
                        })
                        .catch((err) => {
                            return res.serverError(err);
                        });
                }
            }); // Passwords.checkPassword
        }); // TRNOrganizationAdmin.findOne
    }, // TRNOrgAdminLogin
    
    TRNEmployeeLogin: function (req, res) {

        if (!req.body.groupID || !req.body.groupSecret) {
            res.send(409, "Please enter group id and secret.");
        }

        TRNOrgGroups.findOne({
            groupID: req.body.groupID,
            groupSecret: req.body.groupSecret,
            status : "active"
        })
        .populate('trnOrganizationID')

            .exec(function (err, data) {

                if (err) { 
                    console.log(err);
                    return res.send(409, 'Something Bad Happened, please try again.');
                }

                else if (!data) return res.send(409, 'Invalid credentials, please try again.');

                else if (!data.trnOrganizationID || data.trnOrganizationID.status != "active" ) return res.send(409, 'Survey is closed now, please contact admin.');
                
                let userRole = GlobalsService.USER_ROLE.TRN_ORG_EMP;
                let trnOrg = data.trnOrganizationID;

                AuthenticationToken.createAuthenticationTokenForUser(data.groupID + "_" + "anonymous",userRole, req.param('deviceID'), req.param('deviceName'))
                    .then((newAuthenticationTokenObj) => {

                        var returnObj = {
                            userID: data.groupID + "_" + "anonymous",
                            accessToken: newAuthenticationTokenObj.token,
                            username: "anonymous user",
                            email: "",
                            organizationName: trnOrg.organizationName,
                            organizationID: trnOrg.trnOrganizationID,
                            groupID: data.groupID,
                            userRole: userRole
                        };
                        return res.json(returnObj);
                    })
                    .catch((err) => {
                        console.log(err);
                        return res.send(409, 'Something Bad Happened, please try again.');
                    });
            }); // TRNOrgGroups.findOne
    }, // TRNEmployeeLogin

    TRNlogout: function (req, res) {

        var accessToken = req.param('accessToken');

        AuthenticationToken.update({token: accessToken}, {revoked: true})
            .exec(function (err, updatedRecord) {
                
                if (err) return res.serverError(err);
                
                return res.ok();
            });
    }, // End of TRNlogout
}