/**
 * isAdmin
 *
 * @module      :: Policy
 * @description :: Policy to allow any authenticated user
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (sails.config.environment == "development" || req.userRole == GlobalsService.USER_ROLE.TRN_ORG_ADMIN ) {
        return next();
    }

    return res.forbidden('You are not permitted to perform this action.');
};