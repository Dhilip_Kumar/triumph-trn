/**
 * isSuperAdmin
 *
 * @module      :: Policy
 * @description :: Policy to allow any authenticated user
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

    if (sails.config.environment == "development" || req.userRole == GlobalsService.USER_ROLE.SUPER_ADMIN ) {
        return next();
    }

    return res.forbidden('You are not permitted to perform this action.');
};