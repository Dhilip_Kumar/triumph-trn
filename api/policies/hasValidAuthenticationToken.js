/**
 * isAdmin
 *
 * @module      :: Policy
 * @description :: Policy to allow any authenticated user
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (sails.config.environment == "development" ) {
        return next();
    }
    
    var accessToken = req.param('accessToken');

    AuthenticationToken.findOne({ token: accessToken, revoked: false})
    .exec(function (err, data) {

        if (err) return res.serverError(err);

        else if(!data) return res.forbidden('You are not permitted to perform this action.');

        req.userRole = data.userRole;
        
        return next();
    });
};