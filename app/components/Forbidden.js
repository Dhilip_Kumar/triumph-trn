import React, { Component } from 'react';
import SharedStyles from '../SharedStyles';
import NavBar from './NavBar';

class Forbidden extends Component {

    render() {
        
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackground}>   
                    <div className="container mobile-container">
                        <div style={SharedStyles.loaderComponent} className="containerWrapper">
                            <div className="loaderContainer" style={styles.innerContainerWrapper}>
                                <h3>Forbidden</h3>
                                <p>You either have an active session or you don't have permission to view this page with your current role.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const styles = {

    innerContainerWrapper: {
        textAlign:'center', 
        marginTop:'-80px'
    }
}

export default Forbidden;