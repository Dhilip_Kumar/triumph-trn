import React, { Component } from 'react';
import User from '../modules/User';
import Globals from '../modules/Globals';

class NavBar extends Component {

    navigate(link) {
        this.props.history.replace(link);
    }

    logout() {
        
        var confirmationMessage = 'Are you sure you want to logout?';

        var ASYNC_STORAGE_KEYS = Object.keys(localStorage);
        var hasUnsyncedSurveyReports = false;

        if (ASYNC_STORAGE_KEYS.indexOf(Globals.ASYNC_STORAGE_KEYS.IIA_DATA) > -1) {
            hasUnsyncedSurveyReports = true;
        };

        if (ASYNC_STORAGE_KEYS.filter(keys => keys.includes('Section_')).length > 0) {
            hasUnsyncedSurveyReports = true;
        };

        if (hasUnsyncedSurveyReports) {
            confirmationMessage += ' Kindly note that your survey data is not submitted to the server yet.';
        };

        var confirmation = confirm(confirmationMessage);
    
        if (confirmation == true) {
            this.proceedToLogout();
        } 
        else {
            return; // Do nothing!
        }
    }

    proceedToLogout() {

        localStorage.clear();
        
        if (this.props.history) {
            this.props.history.replace('/');
        }
        else {
            window.location.reload();
        }
    }

    renderNavbarLinks() {

        if (User.isLoggedIn()) {
            return (
                <div className="collapse navbar-collapse" id="myNavbar">
                    <ul className="nav navbar-nav">
                        <li><a onClick={this.logout.bind(this)}>Logout</a></li>
                    </ul>
                </div>
            )
        } 
        else {
            return (null); // Temporarily disable admin login
            return (
                <div className="collapse navbar-collapse" id="myNavbar">
                    <ul className="nav navbar-nav">
                        <li><a onClick={this.navigate.bind(this, '/')}>Employee</a></li>
                        <li><a onClick={this.navigate.bind(this, '/admin')}>Admin</a></li>
                        {/* <li><a onClick={this.navigate.bind(this, '/super-admin')}>Super Admin</a></li> */}
                    </ul>
                </div>
            )
        }
    }

    render() {
        
        return (
            <nav className="navbar navbar-light navbar-fixed-top navBarStyle" style={styles.headerContainer}>
                <div className="container">
                    <div className="navbar-header">
                        <div style={{float:'left'}}><img style={{height:'60px'}} src="/images/Triumph_Logo.png" alt="Triumph Logo" /></div>
                        <div className="header-text-container">
                            <span className="header-title">TrulyReadyNow</span>
                            <span className="header-tagline">Perfect and leverage the human dimension of your operation</span>
                        </div>
                        <button type="button" className="navbar-toggle" data-toggle="collapse" style={styles.navBar} data-target="#myNavbar">
                            <span className="icon-bar" style={styles.navBarIcon}></span>
                            <span className="icon-bar" style={styles.navBarIcon}></span>
                            <span className="icon-bar" style={styles.navBarIcon}></span>
                        </button>
                    </div>
                    {this.renderNavbarLinks()}
                </div>
            </nav>
        );
    }
}

const styles = {

    headerContainer: {
        borderBottom: '1px solid #EAE9E9',
        backgroundColor: '#FFF',
    },
    navBar: {
        backgroundColor:'#F79569',
        marginTop:'13px'
    },
    navBarIcon: {
        backgroundColor: '#FFF'
    },
    headerText: {
        fontFamily: 'Pangram-Light',
        color: '#7d7d7d',
        fontSize: '15pt',
        letterSpacing: '0.5pt',
        verticalAlign: 'middle'
    },
    logoCaption: {
        float: 'left',
        margin: '7px auto',
        display: 'block'
    }
}

export default NavBar;