import React, { Component } from 'react';
import ReactWordCloud from 'react-wordcloud';
import _ from 'lodash';
import sw from 'stopword';

let WordCloudOptions = {
    colors: [
        '#1f77b4',
        '#ff7f0e',
        '#2ca02c',
        '#d62728',
        '#9467bd',
        '#8c564b',
    ],
    enableTooltip: true,
    deterministic: false,
    fontFamily: 'Pangram-Regular',
    fontSizes: [12, 30],
    fontStyle: 'normal',
    fontWeight: 'normal',
    padding: 1,
    randomSeed: null,
    rotations: 3,
    rotationAngles: [0],
    scale: 'sqrt',
    spiral: 'archimedean',
    transitionDuration: 1000,
};

class WordCloud extends Component { 

    constructor(props) {
        super(props);
        this.state = {
            inputWords: []
        };
    }

    componentWillMount() {
        
        var words = this.props.words;
        
        if (words && words.length == 0) return;
        
        var clubbedResp = words.join(' '); // join the responses
        var splittedWords = clubbedResp.replace(/[^a-z\d\s]+/gi, "").split(' ');
        var noStopWords = sw.removeStopwords(splittedWords); // remove stopwords
        var countedWords = _.countBy(noStopWords);

        /*console.log('words', words);
        console.log('clubbedResp', clubbedResp);
        console.log('splittedWords', splittedWords);
        console.log('noStopWords', noStopWords);
        console.log('countedWords', countedWords);*/

        var inputWords = [];

        for (var key in countedWords) {
            inputWords.push({
                text: key,
                value: countedWords[key]
            });
        };

        this.setState({inputWords: inputWords});
    }

    render() {
        
        if (this.state.inputWords && this.state.inputWords.length > 0)        
            return (
                <ReactWordCloud 
                    words={this.state.inputWords} 
                    options={WordCloudOptions} />
            );
        else 
            return null
    }
}

export default WordCloud;