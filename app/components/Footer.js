import React, { Component } from 'react';

class Footer extends Component {

    render() {
        return (
            <footer id="footer">
                <div className="footer">
                    <span>© 2016-2018 RCCI Group</span>
                    <div className="terms-of-use">
                        <span style={{marginRight:"15px"}}><a target='_blank' href='https://www.triumphhq.com/terms-of-use/'>Terms of Use</a></span>
                        <span><a target='_blank' href='https://www.triumphhq.com/privacy-policy/'>Privacy Policy</a></span>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;