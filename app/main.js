import React from 'react';
import ReactDom from 'react-dom';

import Navigation from './Navigation';
import "react-toggle/style.css"; // toggle css

ReactDom.render(<Navigation />, document.getElementById('app'));