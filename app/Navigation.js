import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link , Redirect} from "react-router-dom";
import User from './modules/User';
import Globals from './modules/Globals';
import EmpLogin from './pages/employee/EmpLogin';
import Survey from './pages/employee/Survey';
import AdminLogin from './pages/admin/AdminLogin';
import AdminDashboard from './pages/admin/AdminDashboard';
import SuperAdminLogin from './pages/super-admin/SuperAdminLogin';
import SuperAdminDashboard from './pages/super-admin/SuperAdminDashboard';
import Forbidden from './components/Forbidden';
import IIAReport from './pages/IIA/IIAReport';

const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
        React.createElement(component, finalProps)
    );
};

const PubicRoute = ({component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            return User.isLoggedIn() === false ? (
                renderMergedProps(component, routeProps, rest)
            ) : (
                    <Forbidden />
                );
        }} />
    );
};

const PrivateRoute = ({ component, redirectTo, eligibleUserRole, ...rest }) => {
    
    return (
        
        <Route {...rest} render={routeProps => {
            
            if (User.isLoggedIn()) {
                
                if (User.getUserRole() === eligibleUserRole) {
                    return (renderMergedProps(component, routeProps, rest));
                }
                else {
                    return (<Forbidden />);
                }
            } 
            else {
                return (
                    <Redirect to={{
                        pathname: redirectTo,
                        state: { from: routeProps.location }
                    }} />
                );
           }}}
        />
    )
};

export default class Navigation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            coreData: []
        };
    }

    componentWillMount() {
        var coreData = User.getUserCoreData();
        this.setState({coreData: coreData});
    }

    render() {
        return (
            <Router coreData={this.state.coreData}>
                <Switch>
                    <PubicRoute path='/' exact component={EmpLogin} coreData={this.state.coreData} />
                    <PubicRoute path='/admin' exact component={AdminLogin} coreData={this.state.coreData} />
                    <PubicRoute path='/super-admin' exact component={SuperAdminLogin} coreData={this.state.coreData} />
                    <PrivateRoute path='/survey' exact component={Survey} redirectTo='/' coreData={this.state.coreData} eligibleUserRole={Globals.USER_ROLE.TRN_ORG_EMP} />
                    <PrivateRoute path='/assessment-report' exact component={IIAReport} coreData={this.state.coreData} eligibleUserRole={Globals.USER_ROLE.TRN_ORG_EMP} />
                    <PrivateRoute path='/admin-dashboard' exact component={AdminDashboard} redirectTo='/admin' coreData={this.state.coreData} eligibleUserRole={Globals.USER_ROLE.TRN_ORG_ADMIN} />
                    <PrivateRoute path='/super-admin-dashboard' exact component={SuperAdminDashboard} redirectTo='/super-admin' coreData={this.state.coreData} eligibleUserRole={Globals.USER_ROLE.TRN_SUPER_ADMIN}/>
                </Switch>
            </Router>
        )
    }
}