import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';

/*
var sampleResp = [
    {
        "stateName": "Compassion",
        "avgScore": "4.54",
        "individualsWithHighScore": 9,
        "individualsWithMediumScore": 3,
        "individualsWithLowScore": 0
    },
    {
        "stateName": "Excellence",
        "avgScore": "4.13",
        "individualsWithHighScore": 8,
        "individualsWithMediumScore": 3,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Authenticity & Truthfulness",
        "avgScore": "4.00",
        "individualsWithHighScore": 7,
        "individualsWithMediumScore": 5,
        "individualsWithLowScore": 0
    },
    {
        "stateName": "Courage",
        "avgScore": "3.96",
        "individualsWithHighScore": 8,
        "individualsWithMediumScore": 3,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Understanding",
        "avgScore": "3.75",
        "individualsWithHighScore": 6,
        "individualsWithMediumScore": 5,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Positivity & Spirit",
        "avgScore": "3.58",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 8,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Self-Awareness",
        "avgScore": "3.50",
        "individualsWithHighScore": 5,
        "individualsWithMediumScore": 6,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Being Responsible",
        "avgScore": "3.50",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 8,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Dedication & Surrender",
        "avgScore": "3.38",
        "individualsWithHighScore": 5,
        "individualsWithMediumScore": 5,
        "individualsWithLowScore": 2
    },
    {
        "stateName": "Simplicity",
        "avgScore": "3.33",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 8,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Openness",
        "avgScore": "3.29",
        "individualsWithHighScore": 4,
        "individualsWithMediumScore": 7,
        "individualsWithLowScore": 1
    },
    {
        "stateName": "Kindness & Caring",
        "avgScore": "3.13",
        "individualsWithHighScore": 6,
        "individualsWithMediumScore": 1,
        "individualsWithLowScore": 5
    },
    {
        "stateName": "Humility",
        "avgScore": "3.08",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 7,
        "individualsWithLowScore": 2
    },
    {
        "stateName": "Being Present",
        "avgScore": "3.04",
        "individualsWithHighScore": 4,
        "individualsWithMediumScore": 3,
        "individualsWithLowScore": 5
    },
    {
        "stateName": "Letting Go",
        "avgScore": "2.83",
        "individualsWithHighScore": 2,
        "individualsWithMediumScore": 7,
        "individualsWithLowScore": 3
    },
    {
        "stateName": "Self-Mastery",
        "avgScore": "2.79",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 4,
        "individualsWithLowScore": 5
    },
    {
        "stateName": "Accepting & Non-Judgement",
        "avgScore": "2.71",
        "individualsWithHighScore": 2,
        "individualsWithMediumScore": 4,
        "individualsWithLowScore": 6
    },
    {
        "stateName": "Innocence",
        "avgScore": "2.54",
        "individualsWithHighScore": 2,
        "individualsWithMediumScore": 5,
        "individualsWithLowScore": 5
    },
    {
        "stateName": "Freedom",
        "avgScore": "2.42",
        "individualsWithHighScore": 3,
        "individualsWithMediumScore": 5,
        "individualsWithLowScore": 4
    },
    {
        "stateName": "Inclusivity",
        "avgScore": "2.29",
        "individualsWithHighScore": 2,
        "individualsWithMediumScore": 3,
        "individualsWithLowScore": 7
    },
    {
        "stateName": "Abundance",
        "avgScore": "2.17",
        "individualsWithHighScore": 0,
        "individualsWithMediumScore": 6,
        "individualsWithLowScore": 6
    }
]
*/

class ValuesReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valuesReport: this.props.widget.response
        }
    }

    renderTableBody() {

        return(
            <tbody style={{textAlign:'center'}}>
                {this.state.valuesReport.map((valueObj, ind) => {
                    return(
                        <tr key={ind}>
                            <td>{ind+1}</td>
                            <td>{valueObj.valueName}</td>
                            <td>{valueObj.avgScore}</td>
                            <td>{valueObj.individualsWithHighScore}</td>
                            <td>{valueObj.individualsWithMediumScore}</td>
                            <td>{valueObj.individualsWithLowScore}</td>
                        </tr>
                    )
                })}
            </tbody>
        )
    }

    render() {

        return (
            
            <div style={SharedStyles.widgetWrapper}>
                
                <div style={styles.sectionHeading}>
                    <p><img src='/images/Value_Icon.png' width={100}></img></p>
                    <p>Values Report</p>
                </div>

                <p>In this section, we are going to go through the values.</p>

                <table className="table-curved" style={styles.tableStyle}>
                    <thead>
                        <tr>
                            <th className="col-md-1" style={styles.tableHeader}>#</th>
                            <th className="col-md-3" style={styles.tableHeader}>Value</th>
                            <th className="col-md-2" style={styles.tableHeader}>Avg. Score (0-6)</th>
                            <th className="col-md-2" style={styles.tableHeader}># of employees with high score (4.5 and above)</th>
                            <th className="col-md-2" style={styles.tableHeader}># of employees with medium score (1.5 to 4.5)</th>
                            <th className="col-md-2" style={styles.tableHeader}># of employees with low score (1.5 and below)</th>
                        </tr>
                    </thead>
                    {this.renderTableBody()}
                </table>
            </div>
        )
    }
}

export default ValuesReport;

const styles = {

    sectionHeading: {
        fontFamily: 'Pangram-Regular',
        color: '#a9dde2',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    tableStyle: {
        width:'100%',
        marginTop:'20px',
        marginBottom: '5px',
        overflowX: 'auto',
        backgroundColor: '#FFF'
    },
    tableHeader: {
        fontFamily: 'Pangram-Regular',
        fontSize: '15px',
        textAlign: 'center',
        backgroundColor: '#E9E9E9',
    }
}