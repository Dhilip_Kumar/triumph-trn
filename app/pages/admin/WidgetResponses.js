import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';
import DataDimensionReport from './DataDimensionReport';
import MultiDimensionReport from './MultiDimensionReport';
import InnerDimensionReport from './InnerDimensionReport';
import CustomWidget from './CustomWidget';

class WidgetResponses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            widgetToUpdate: null,
            showCustomWidgetModal: false
        }
    }

    renderInnerContainer(widget) {

        if (widget.widgetType == 'multi-dimension') {
            return(
                <MultiDimensionReport widget={widget} {...this.props} />
            )
        }
        else if (widget.widgetType == 'data-dimension') {
            return(
                <DataDimensionReport widget={widget} {...this.props} />
            )
        }
        else if (widget.widgetType == 'inner-dimension') {
            return(
                <InnerDimensionReport widget={widget} {...this.props} />
            )
        }
        else {
            return <div />;
        }
    }

    onEditWidgetClick(widget) {
        
        this.setState({
            widgetToUpdate: widget,
            showCustomWidgetModal: true
        });
        console.log('this props', this.props);
        //this.props.onEditWidgetClick(widget);
    }

    render() {

        return (
            <div style={SharedStyles.widgetResponseContainer}>
                
                <CustomWidget 
                    action={'edit'}
                    widgets={this.props.widgets}
                    widgetToUpdate={this.state.widgetToUpdate}
                    showCustomWidgetModal={this.state.showCustomWidgetModal}
                    onClose={() => this.setState({showCustomWidgetModal:false})}
                    {...this.props} />
                
                {
                    this.props.widgets.map((widget, index) => {
                        
                        return(
                            <div id={'widget_' + widget.widgetID} key={'widget_' + widget.widgetID}>
                                <div style={styles.titleWrapper}>
                                    <div style={SharedStyles.widgetTitle}>#{index+1}. {widget.widgetTitle}</div>
                                    {widget.isDefault != true ?
                                        <div style={{cursor: 'pointer'}} onClick={() => this.onEditWidgetClick(widget)}>
                                            <img src='/images/EditIcon.png' alt='Edit' width={20}/>
                                        </div>
                                    : null}
                                </div>
                                {this.renderInnerContainer(widget)}
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default WidgetResponses;

const styles = {

    titleWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
}