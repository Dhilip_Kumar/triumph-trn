import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';
import { Modal } from 'react-bootstrap';
/*
var sampleResp = [
    {
        energyState: 'Hyper',
        percent: 41.86
    },
    {
        energyState: 'Inert',
        percent: 30.60
    },
    {
        energyState: 'Balanced',
        percent: 27.54
    }
]
*/

class EnergyReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            energyResponses: this.props.widget.response,
            modalPopupShow: false
        }
    }

    modalClose() {
        this.setState({ modalPopupShow: false });
    }

    modalOpen() {
        this.setState({ modalPopupShow: true });
    }

    renderModal = () => {

        return (

            <div className="static-modal">

                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.state.modalPopupShow} 
                    onHide={this.modalClose.bind(this)}>

                    <Modal.Header closeButton>
                        <Modal.Title>Energy State Report</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={SharedStyles.modalConatiner}>   

                        <p>While the ideal is to always be in the Balanced energy state, for most of us the reality is that we flow from one state to another based on the situation and our tendencies. This report reflect the amount of time you spend in the various Energy States. As such, our aim would be to go lower and lower on Hyper and Inert, and higher on Balanced.</p>

                        <p>Let's look at what the different Energy States specifically mean:</p>
                        
                        {this.renderEnergyStateDescriptions()}
                    
                    </Modal.Body>
                    
                </Modal>

            </div>
        )
    }

    renderEnergyState(widget) {

        var energyState = {};

        {this.state.energyResponses.map((item, ind) => {
                    
            if (item.energyState == 'Balanced') {
                energyState.balanced = (
                    <div key={'overview_display_' + item.energyState}>
                        <div style={{textAlign:'center'}}>
                            <img src='/images/Balanced_State_Transparent.png' width={70} style={{}}></img>
                            <p style={styles.overviewHeaderText}>Balanced</p>
                        </div>
                        <p style={styles.energyStateText}>{item.percent} / 10</p>
                    </div>
                );
            }
            else if (item.energyState == 'Inert') {
                energyState.inert = (
                    <div key={'overview_display_' + item.energyState}>
                        <div style={{textAlign:'center'}}>
                        <img src='/images/Inert_State_Transparent.png' width={65} style={{marginBottom:'5px'}}></img>
                        <p style={styles.overviewHeaderText}>Inert</p>
                        </div>
                        <p style={styles.energyStateText}>{item.percent} / 10</p>
                    </div>
                );
            }
            else if (item.energyState == 'Hyper') {
                energyState.hyper = (
                    <div key={'overview_display_' + item.energyState}>
                        <div style={{textAlign:'center'}}>
                        <img src='/images/Hyper_State_Transparent.png' width={70}></img>
                        <p style={styles.overviewHeaderText}>Hyper</p>
                        </div>
                        <p style={styles.energyStateText}>{item.percent} / 10</p>
                    </div>
                )
            }
        })}
        
        return (
            <div style={styles.overviewContainer}>
                {energyState.inert}
                {energyState.balanced}
                {energyState.hyper}
            </div>
        )
    }

    renderBarFiller(widget) {
        
        return(
            
            <div style={{margin: '20px 0'}}>
    
                {this.state.energyResponses.map((item, ind) => {
                    
                    if (item.energyState == 'Balanced') {
                        return (
                            <div style={{margin: '10px 0'}} key={'energy_bar_' + item.energyState}>
                                <p style={{fontFamily: 'Pangram-Regular', color: '#AECF7A'}}>{item.energyState} - {item.percent}%</p>
                                <div style={{height: '15px', borderRadius: '8px', width: item.percent+'%', backgroundColor: '#AECF7A'}}></div>
                            </div>
                        )
                    }
                    else if (item.energyState == 'Inert') {
                        return (
                            <div style={{margin: '10px 0'}} key={'energy_bar_' + item.energyState}>
                                <p style={{fontFamily: 'Pangram-Regular', color: '#a9dde2'}}>{item.energyState} - {item.percent}%</p>
                                <div style={{height: '15px', borderRadius: '8px', width: item.percent+'%', backgroundColor: '#a9dde2'}}></div>
                            </div>
                        )
                    }
                    else if (item.energyState == 'Hyper') {
                        return (
                            <div style={{margin: '10px 0'}} key={'energy_bar_' + item.energyState}>
                                <p style={{fontFamily: 'Pangram-Regular', color: '#ED7499'}}>{item.energyState} - {item.percent}%</p>
                                <div style={{height: '15px', borderRadius: '8px', width: item.percent+'%', backgroundColor: '#ED7499'}}></div>
                            </div>
                        )
                    }
                })}
            </div>
        )
    }

    renderEnergyStateDescriptions(widget) {

        return(
            
            this.state.energyResponses.map((item, ind) => {
                    
                if (item.energyState == 'Balanced') {
                    return (
                        <div style={styles.balancedTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Balanced</p>
                                <img src='/images/Balanced_State_Transparent.png' width={70} style={{marginTop:'-10px'}}></img>
                            </div>
                            <div>In this Energy State, life can be blissful and productive. You can have a greater level of clarity about things that matter and know what you want out of life. You know you’re going to reach your lofty objectives, if you haven’t already, and don’t need others to say nice things to make you feel good about yourself. The deeper the state of Balance, the more every moment is joyful and every person wonderful, and the more you want to share the richness of life with everyone and everything around you. Balanced is the Energy State in which we operate the best.</div>
                        </div>
                    )
                }
                else if (item.energyState == 'Inert') {
                    return (
                        <div style={styles.inertTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Inert</p>
                                <img src='/images/Inert_State_Transparent.png' width={65} style={{marginBottom:'5px'}}></img>
                            </div>
                            <div>In this Energy State, you can be weak and lack initiative. You might experience confusion and lack the ability to make confident decisions. You might feel pressures from life that block you from taking positive action. The more time you spend in the Inert Energy State, the more you feel stalled—unable or unwilling to progress—preventing you from living your life to its fullest potential.</div>
                        </div>
                    )
                }
                else if (item.energyState == 'Hyper') {
                    return (
                        <div style={styles.hyperTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Hyper</p>
                                <img src='/images/Hyper_State_Transparent.png' width={70}></img>
                            </div>
                            <div>In this Energy State, you can be wasteful, inefficient, and insensitive. You might be driven to pursue the things you do not have while losing sight of the things you already possess. You see your goals so clearly and approach them with such energy that if others get in your way, it can make you mad. You may feel that others are mostly either with you or against you. It can also become annoying or even frustrating when others bring up contrary or opposing ideas. </div>
                        </div>
                    )
                }
            })
        )
    }

    render() {

        return (
            
            <div style={SharedStyles.widgetWrapper}>

                <div style={styles.sectionHeading}>
                    <p><img src='/images/EnergyState_Icon.png' width={100}></img></p>
                    <p>Energy State</p>
                </div>

                <p>Let's take a look at your organization's Energy State report and see where your team currently operate.</p>

                {this.renderEnergyState()}

                <p>An 'Energy State' is the place from which you approach your life at any given point in time. Your Energy State is either Inert, Balanced, or Hyper.</p>
                
                <p>The idea is that we should strive to operate in a Balanced Energy State, as this is our most efficient, psychologically healthy state. Making decisions when in a balanced state will be the best we are capable of, as we are able to exhibit calm and control. By initiating actions from the Balanced Energy State, we can achieve significantly more and better results with significantly less effort. Whichever state we operate in affects all other aspects of our life.</p>
                
                <div style={{textAlign:"right"}}>
                    <button className="btn" onClick={this.modalOpen.bind(this)} style={styles.viewMoreBtn}>View More ></button>
                </div>
                
                {/* {this.renderEnergyStateDescriptions()} */}

                {this.renderModal()}
                
            </div>
        )
    }
}

export default EnergyReport;

const styles = {

    sectionHeading: {
        fontFamily: 'Pangram-Regular',
        color: '#AECF7A',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    hyperTextContainer: {
        backgroundColor: '#ED7499',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    balancedTextContainer: {
        backgroundColor: '#AECF7A',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    inertTextContainer: {
        backgroundColor: '#a9dde2',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    headerText: {
        fontFamily: 'Pangram-Bold',
        fontSize: '18px',
        color:"#fff"
    },
    subHeaderText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '18px',
        margin: '20px 0px 5px'
    },
    energyStateText : {
        fontFamily: 'Pangram-Regular', 
        padding:"5px 25px", 
        borderRadius:"10px", 
        background:"white", 
        color: '#92d6e1', 
        minWidth:"150px",
        textAlign:"center"
    },
    overviewContainer: {
        margin: '20px 0', 
        background :'#92d6e1', 
        borderRadius : '12px', 
        padding : '20px', 
        display : 'flex', 
        justifyContent: 'space-around'
    },
    overviewHeaderText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '17px',
        color:'#fff'
    },
    viewMoreBtn: {
        background:"#AECF7A",
        color : "#fff",
        borderRadius: '8px',
        padding: '5px 15px',
        marginTop: '10px',
        marginBottom: '5px',
        fontFamily: 'Pangram-Regular'
    }
}