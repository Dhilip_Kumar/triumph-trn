import React, { Component, PureComponent } from 'react';
import SharedStyles from '../../SharedStyles';
import EnergyReport from './EnergyReport';
import EmoStateReport from './EmoStateReport';
import ValuesReport from './ValuesReport';
import OrgAdmin from '../../modules/OrgAdmin';

class InnerDimensionReport extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            widget: this.props.widget
        };
    }

    componentWillMount() {

        if (!this.state.widget.response) {

            OrgAdmin.getInnerDimensionWidgetReport(this.props.coreData, this.state.widget.widgetTitle, (err, widgetReport) => {

                if (err) {
                    alert('Network Error. Please try again later.');
                    return;
                }

                var widget = this.state.widget;
                widget.response = widgetReport;

                this.setState({
                    widget: widget,
                    showActivityIndicator: false
                },
                () => {
                    this.forceUpdate();
                });
            });
        }
    }

    renderInnerContainer() {

        var widget = this.state.widget;

        if (!widget) return <div />;

        if (!widget.response || this.state.showActivityIndicator) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="loaderContainer" style={{textAlign: 'center'}}>
                        <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                    </div>
                </div>
            )
        }
        else if (widget.response && widget.response.length == 0) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="text-center" style={SharedStyles.plainText}>No Survey data found.</div>
                </div>
            )
        }
        else {

            if (widget.widgetTitle == 'Energy State Report') {
                return <EnergyReport widget={widget} />;
            }   
            else if (widget.widgetTitle == 'EmoState Report') {
                return <EmoStateReport widget={widget} />;
            }
            else if (widget.widgetTitle == 'Values Report') {
                return <ValuesReport widget={widget} />;
            }
            else {
                return null;
            }
        }
    }

    render() {

        return (
            <div style={SharedStyles.widgetResponse}>
                {this.renderInnerContainer()}
            </div>
        )
    }
}

export default InnerDimensionReport;