import React, { Component, PureComponent } from 'react';
import SharedStyles from '../../SharedStyles';
import OrgAdmin from '../../modules/OrgAdmin';
import WordCloud from '../../components/WordCloud';
import { fa } from 'stopword';

class DataDimensionReport extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            widget: this.props.widget,
            showActivityIndicator: false
        }
    }

    componentWillMount() {

        if (!this.state.widget.response) {

            var params = {
                dataDimension: this.state.widget.dataDimension,
                groupDataset: this.state.widget.groupDataset,
                surveyDataset: this.state.widget.surveyDataset,
                options : this.state.widget.options ? this.state.widget.options : false,
                trnOrganizationID : this.props.coreData.userCoreProperties.organizationID
            };

            OrgAdmin.getDataDimensionalWidgetReport(this.props.coreData, params, (err, widgetReport) => {

                if (err || !widgetReport) {
                    return alert('Network Error. Please try again later.');
                }
                else {

                    var widget = this.state.widget;
                    widget.report = widgetReport;
                    widget.response = true;

                    this.setState({
                        widget: widget,
                        showActivityIndicator: false
                    },
                    () => {
                        this.forceUpdate();
                    });
                }
            });
        }
    }

    renderTopicModelings = (response, key) => {

        let count = 0;

        return (
            <td key={'id_topic_resp_' + key} style={{verticalAlign: 'baseline'}}>
                {
                    response.map((topic, ind) => { 
                        if (topic.documents.length) { 
                            return (
                                <div style={count >= 1 ? styles.topicWrapper: null} key={'topic_wrapper_' + key + '_' + ind}>
                                    {this.renderTopic(topic, count++)}
                                </div>
                            );
                        }
                    })
                }
            </td>
        )
    }

    renderEnergyState = (energyState, headCount) => {

        return (
            <React.Fragment>
                <p style={styles.entriesSectionTitle}>Energy State</p>
                <table className="table-curved" style={styles.tableStyle}>
                    <thead>
                        <tr>
                            <th className="col-md-1" style={styles.tableHeader}>#</th>
                            <th className="col-md-3" style={styles.tableHeader}>Energy State</th>
                            <th className="col-md-4" style={styles.tableHeader}>Avg. Score</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center'}}>
                        {energyState.map((data, ind) => {
                            return <tr key={ind}>
                                <td>{ind+1}</td>
                                <td>{data.state}</td>
                                <td>{(parseFloat(data.total) / headCount).toFixed(2)}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
                <br></br>
            </React.Fragment>
        )
    }

    renderEmoState = (emoState, headCount) => {

        var top3 = emoState.slice(-3).reverse();
        var last3 = emoState.slice(0,3)

        return (
            <React.Fragment>
                <p style={styles.entriesSectionTitle}>EmoState</p>
                <table className="table-curved" style={styles.tableStyle}>
                    <thead>
                        <tr>
                            <th className="col-md-1" style={styles.tableHeader}>#</th>
                            <th className="col-md-3" style={styles.tableHeader}>Top 3 EmoState</th>
                            <th className="col-md-2" style={styles.tableHeader}>Avg. Score</th>
                            <th className="col-md-3" style={styles.tableHeader}>Bottom 3 EmoState</th>
                            <th className="col-md-2" style={styles.tableHeader}>Avg. Score</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center'}}>
                        {[0,1,2].map((ind) => {
                            return <tr key={ind}>
                                <td>{ind+1}</td>
                                <td>{top3[ind].stateName}</td>
                                <td>{(parseFloat(top3[ind].total) / headCount).toFixed(2)}</td>
                                <td>{last3[ind].stateName}</td>
                                <td>{(parseFloat(last3[ind].total) / headCount).toFixed(2)}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
                <br></br>
            </React.Fragment>
        )
    }

    renderValuesState = (values, headCount) => {

        var top3 = values.slice(-3).reverse();
        var last3 = values.slice(0,3);

        return (
            <React.Fragment>
                <p style={styles.entriesSectionTitle}>Values </p>
                <table className="table-curved" style={styles.tableStyle}>
                    <thead>
                        <tr>
                            <th className="col-md-1" style={styles.tableHeader}>#</th>
                            <th className="col-md-3" style={styles.tableHeader}>Top 3 Value</th>
                            <th className="col-md-2" style={styles.tableHeader}>Avg. Score</th>
                            <th className="col-md-3" style={styles.tableHeader}>Bottom 3 Value</th>
                            <th className="col-md-2" style={styles.tableHeader}>Avg. Score</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center'}}>
                        {[0,1,2].map((ind) => {
                            return(
                                <tr key={ind}>
                                    <td>{ind+1}</td>
                                    <td>{top3[ind].valueName}</td>
                                    <td>{(parseFloat(top3[ind].total)/ headCount).toFixed(2)}</td>
                                    <td>{last3[ind].valueName}</td>
                                    <td>{(parseFloat(last3[ind].total)/ headCount).toFixed(2)}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }

    renderThemesNdTopics = (response, key) => {
        return (
            <React.Fragment>
                {this.renderEnergyState(response.energyState, response.surveyHeadCount)}
                {this.renderEmoState(response.emoState, response.surveyHeadCount)}
                {this.renderValuesState(response.valueScores, response.surveyHeadCount)}
                <br></br>
            </React.Fragment>
        )
    }

    renderTopic = (response, i) => {

        var keywords = " ";

        response.documentVocab.map((key, ind) => {
            (ind  == response.documentVocab.length - 1) ? keywords += key.word :
            keywords += key.word + " + ";
        });

        var renderThemesNdTopics =  response.themesAndTopics ? this.renderThemesNdTopics(response.themesAndTopics) : null

        return (
            <div>
                <p style={styles.topicSectionTitle}>
                    <span>Topic {i + 1}:</span>
                    <span>{keywords}</span>
                </p>

                {renderThemesNdTopics}

                <p style={styles.entriesSectionTitle}>Entries:</p>
                <ul style={styles.unorderedList}>
                    {response.documents.map((document, ind) => {
                        return(<li key={ind} style={styles.listStyle}>{document.text}</li>);
                    })}
                </ul>  
            </div>
        )
    }
    
    renderTableBody() {

        var report = this.state.widget.report;

        var key = this.state.widget.widgetID;
        
        var textResponsesRow = <td style={styles.wordCloudWrapper}>
            <WordCloud words={report.textResponses} />
        </td>;

        var focusAreaResponsesRow = <td style={styles.wordCloudWrapper}>
            <WordCloud words={report.focusAreaResponses}/>
        </td>;

        var themesNdTopicModelingResponsesRow = this.renderTopicModelings(report.topics, key)
        var topicModelingResponsesRow = this.renderTopicModelings(report.topicModelingResponses, key)

        var allResponsesRow = <td key={'id_resp_' + key} style={{verticalAlign: 'baseline'}}>
                <div style={styles.subHeading}>{"All Entries"}</div>
                <ul style={styles.unorderedList}>
                    {report.textResponses.map((textResponse, ind) => {
                        return(<li key={ind} style={styles.listStyle}>{textResponse}</li>);
                    })}
                </ul>    
            </td>;

        return(
            <tbody>
                <tr>{textResponsesRow}</tr>
                {report.focusAreaResponses.length > 0 ? <tr>{focusAreaResponsesRow}</tr> : null}
                {this.renderSubHeadingRow("Themes & Topics")}
                <tr>{themesNdTopicModelingResponsesRow}</tr>
                {/* <tr>{topicModelingResponsesRow}</tr> */}
                <tr>{allResponsesRow}</tr>
            </tbody>
        )
    }

    renderSubHeadingRow(text){
        
        return(
            <tr>
                <td style={{backgroundColor: '#F6F6F6'}}>
                    <div>
                        <div style={styles.subHeading}>{text}</div>
                    </div>
                </td>
            </tr>
        )
    }

    renderReportTable() {

        if (!this.state.widget.response || this.state.showActivityIndicator) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="loaderContainer" style={{textAlign: 'center'}}>
                        <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                    </div>
                </div>
            )
        }
        else if (this.state.widget.response && this.state.widget.report && this.state.widget.report.textResponses.length == 0) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="text-center" style={SharedStyles.plainText}>No Survey data found.</div>
                </div>
            )
        }
        else {
            return(
                <table className="table-curved" style={SharedStyles.tableStyle}>
                    <thead>
                        <tr>
                            <th 
                                colSpan={this.state.widget.innerDimensions.length} 
                                style={SharedStyles.tableHeading}>
                                    <span style={SharedStyles.plainText}>Viewing data for</span>&nbsp;
                                    {this.state.widget.dataDimension}
                                    <div className='text-center'>({this.state.widget.report.peopleHeadCount})</div>
                            </th>
                        </tr>
                    </thead>
                    {this.renderTableBody()}
                </table>
            )
        }
    }

    render() {

        return (
            <div style={SharedStyles.widgetResponse}>
                {this.renderReportTable()}
            </div>
        )
    }
}

export default DataDimensionReport;

const styles = {

    wordCloudWrapper: {
        maxWidth: '250px'
    },
    topicSectionTitle: {
        fontFamily: 'Pangram-Bold', 
        textDecoration:"underline"
    },
    entriesSectionTitle: {
        fontFamily: 'Pangram-Regular', 
        textDecoration:"underline", 
        display:"inline-flex", 
    },
    topicSeparator: { 
        width:"100px", 
        borderColor : '#e4e2e2'
    },
    unorderedList: {
        paddingInlineStart: '30px'
    },
    listStyle: {
        paddingBottom: '5px'
    },
    topicWrapper: {
        marginTop: '12px',
        paddingTop: '12px', 
        borderTop: '0.5px dashed #d2d0d0'
    },
    tableStyle: {
        width:'100%',
        marginTop:'20px',
        marginBottom: '5px',
        overflowX: 'auto',
        backgroundColor: '#FFF'
    },
    tableHeader: {
        fontFamily: 'Pangram-Regular',
        fontSize: '15px',
        textAlign: 'center',
        backgroundColor: '#E9E9E9',
    },
    subHeading: {
        textAlign:'center',
        fontFamily:'Pangram-Bold',
        minWidth: '33.33%',
    },
}