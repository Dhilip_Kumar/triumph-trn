import React, { Component, PureComponent } from 'react';
import SharedStyles from '../../SharedStyles';
import OrgAdmin from '../../modules/OrgAdmin';
import WordCloud from '../../components/WordCloud';

class MultiDimensionReport extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            widget: this.props.widget,
            showActivityIndicator: false
        }
    }

    componentWillMount() {

        if (!this.state.widget.response) {

            var params = {
                dataDimension: this.state.widget.dataDimension,
                innerDimensions: this.state.widget.innerDimensions,
                groupDataset: this.state.widget.groupDataset,
                surveyDataset: this.state.widget.surveyDataset,
                options : this.state.widget.options ? this.state.widget.options : false
            };

            OrgAdmin.getMultiDimensionalWidgetReport(this.props.coreData, params, (err, widgetReport) => {

                if (err) {
                    alert('Network Error. Please try again later.');
                    return;
                }

                var widget = this.state.widget;
                widget.innerDimensions = widgetReport;
                widget.response = true;

                this.setState({
                    widget: widget,
                    showActivityIndicator: false
                },
                () => {
                    this.forceUpdate();
                });
            });
        }
    }

    renderTopicModelings = (response, key, colSpan=1) => {

        let count = 0;

        return (
            <td colSpan={colSpan} key={'id_topic_resp_' + key} style={{verticalAlign: 'baseline'}}>
                <div style={styles.subHeading}> { response.length ? "TOPICS" : null }</div>
                <br></br>
                {
                    response.map((topic, ind) => { 
                        if (topic.documents.length) { 
                            return (
                                <div style={count >= 1 ? styles.topicWrapper: null} key={'topic_wrapper_' + key + '_' + ind}>
                                    {this.renderTopic(topic, count++)}
                                </div>
                            );
                        }
                    })
                }
            </td>
        )
    }

    renderTopic = (response, i) => {

        var keywords = " ";

        response.documentVocab.map((key, ind) => {
            (ind  == response.documentVocab.length - 1) ? keywords += key.word :
            keywords += key.word + " + ";
        });

        return (
            <div>
                <p style={styles.topicSectionTitle}>
                    <span>Topic {i + 1}:</span>
                    <span>{keywords}</span>
                </p>
                <p style={styles.entriesSectionTitle}>Entries:</p>
                <ul style={styles.unorderedList}>
                    {response.documents.map((document, ind) => {
                        return(<li key={ind} style={styles.listStyle}>{document.text}</li>);
                    })}
                </ul>  
                {/* <hr style={styles.topicSeparator}></hr> */}
            </div>
        )
    }
    
    renderTableBody() {

        var innerDimensions = this.state.widget.innerDimensions;

        var innerDimensionHeadings = [];    // Inner Dimension Headings
        var textResponsesRow = [];    // WordCloud based on Data Dimension text response
        var focusAreaResponsesRow = [];  // WordCloud based on Data Dimension focusArea response
        var topicModeling4UniqeResponsesRow = [];   // Listing Topic Modeling for unique responses
        var uniqueResponsesRow = [];   // Listing Data Dimension for unique text responses
        var topicModeling4CommonResponsesRow = [];   // Listing Topic Modeling for common responses
        var commonResponsesRow = [];   // Listing Data Dimension for common text responses
        var topicModelingResponsesRow = [];   // Listing Topic Modeling responses
        var allResponsesRow = [];   // Listing Data Dimension text responses

        let processOnceForCommonEntries = false, showFocusAreaResponses = false;

        innerDimensions.forEach((innerDimension, key) => {
            
            innerDimensionHeadings.push(
                <td key={'id_headings_' + key}>
                    <div>
                        <div style={SharedStyles.innerDimensionHeading}>{innerDimension.filter + ' ' + innerDimension.dimension}</div>
                        <div className='text-center'>({innerDimension.peopleHeadCount})</div>
                    </div>
                </td>
            );

            textResponsesRow.push(
                <td key={'wc_text_resp_row_' + key} style={styles.wordCloudWrapper}><WordCloud words={innerDimension.textResponses} /></td>
            );
            
            if (innerDimension.focusAreaResponses.length > 0) showFocusAreaResponses = true;
            
            focusAreaResponsesRow.push(
                <td key={'wc_fa_resp_row_' + key} style={styles.wordCloudWrapper}>
                    {innerDimension.focusAreaResponses.length > 0 ? <WordCloud words={innerDimension.focusAreaResponses}/> : null}
                </td>
            );
            
            /* Compute Unique Entries Row */
            topicModeling4UniqeResponsesRow.push(
                this.renderTopicModelings(innerDimension.uniqueEntries.topicModelingResponses, key)
            );

            uniqueResponsesRow.push(
                <td key={'id_resp_' + key} style={{verticalAlign: 'baseline'}}>
                    <div style={styles.subHeading}>
                    {innerDimension.uniqueEntries.textResponses.length ? "ENTRIES" : null }</div>
                    <br></br>
                    <ul style={styles.unorderedList}>
                        {innerDimension.uniqueEntries.textResponses.map((textResponse, ind) => {
                            return(<li key={ind} style={styles.listStyle}>{textResponse}</li>);
                        })}
                    </ul>    
                </td>
            );

            /* Compute Common Entries Row */
            if(!processOnceForCommonEntries) { 

                processOnceForCommonEntries = !processOnceForCommonEntries;

                if (innerDimension.commonEntries.topicModelingResponses.length) {
                    topicModeling4CommonResponsesRow.push(
                        this.renderTopicModelings(innerDimension.commonEntries.topicModelingResponses, key, innerDimensions.length)
                    );
                }
                
                if (innerDimension.commonEntries.textResponses.length) {
                    commonResponsesRow.push(
                        <td colSpan={innerDimensions.length} key={'id_resp_' + key} style={{verticalAlign: 'baseline'}}>
                            <div style={styles.subHeading}>{"ALL ENTRIES"}</div>
                            <br></br>
                            <ul style={styles.unorderedList}>
                                {innerDimension.commonEntries.textResponses.map((textResponse, ind) => {
                                    return(<li key={ind} style={styles.listStyle}>{textResponse}</li>);
                                })}
                            </ul>    
                        </td>
                    );
                }
                else {
                    commonResponsesRow.push(
                        <td colSpan={innerDimensions.length}> {""} </td>
                    );
                }
            }

            /* Compute All Entries Row */
            topicModelingResponsesRow.push(
                this.renderTopicModelings(innerDimension.topicModelingResponses, key)
            );

            allResponsesRow.push(
                <td key={'id_resp_' + key} style={{verticalAlign: 'baseline'}}>
                    <div style={styles.subHeading}>
                    {innerDimension.textResponses.length ? "ALL ENTRIES" : null }</div>
                    <br></br>
                    <ul style={styles.unorderedList}>
                        {innerDimension.textResponses.map((textResponse, ind) => {
                            return(<li key={ind} style={styles.listStyle}>{textResponse}</li>);
                        })}
                    </ul>    
                </td>
            );
        });

        return(
            <tbody>
                <tr>{innerDimensionHeadings}</tr>
                <tr>{textResponsesRow}</tr>
                {showFocusAreaResponses?<tr>{focusAreaResponsesRow}</tr>:null}

                {/* Unique Entries Section */}
                {this.renderSubHeadingRow("Unique Entries",innerDimensions.length)}
                <tr>{topicModeling4UniqeResponsesRow}</tr>
                <tr>{uniqueResponsesRow}</tr>

                {/* Common Entries Section */}
                {this.renderSubHeadingRow("Common Entries",innerDimensions.length)}
                <tr>{topicModeling4CommonResponsesRow}</tr>
                {<tr>{commonResponsesRow}</tr>}

                {/* All Entries Section */}
                {this.renderSubHeadingRow("All Entries",innerDimensions.length)}
                <tr>{topicModelingResponsesRow}</tr>
                <tr>{allResponsesRow}</tr>
            </tbody>
        )
    }

    /*  var sampleResponse = {
            widgetID: 4,
            widgetTitle: 'Top Of Mind Things Across Energy States',
            widgetType: 'multi-dimension',
            dataDimension: 'What’s on your mind?',
            innerDimensions: [
                {
                    category : 'Energy State',
                    dimension: 'Inert',
                    filter   : 'High',
                    textResponses: ['Unable to control my emotions', 'Negative thinking filled my mind', 'Shyness and inferiority complex halting my progress'],
                    focusAreaResponses: [],
                    "topicModelingResponses": [
                        {
                            "topic": 1,
                            "documents": [
                                {
                                    "id": 1,
                                    "text": "Unable to control my emotions",
                                    "score": 0.15714285714285714
                                }
                            ],
                            "documentVocab": [
                                {
                                    "word": "control",
                                    "count": 1,
                                    "specificity": 1
                                }
                            ]
                        },
                        {
                            "topic": 1,
                            "documents": [],
                            "documentVocab": []
                        },
                    ]
                },
                {
                    category : 'Energy State',
                    dimension: 'Balanced',
                    filter   : 'High',
                    textResponses: ['Plan for a trip with my friends', 'Do exercise regularly and get fit', 'Listen music and do yoga.'],
                    focusAreaResponses: [],
                    "topicModelingResponses": [
                        {
                            "topic": 0,
                            "documents": [],
                            "documentVocab": []
                        }
                    ]
                },
                {
                    category : 'Energy State',
                    dimension: 'Hyper',
                    filter   : 'High',
                    textResponses: ['Nothing executed as per my plan', 'Fear of failure', 'Expectations makes me down'],
                    focusAreaResponses: [],
                    "topicModelingResponses": [
                        {
                            "topic": 0,
                            "documents": [],
                            "documentVocab": []
                        }
                    ]
                }
            ]
        };
    */

    renderReportTable() {

        if (!this.state.widget.response || this.state.showActivityIndicator) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="loaderContainer" style={{textAlign: 'center'}}>
                        <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                    </div>
                </div>
            )
        }
        else if (this.state.widget.response && this.state.widget.innerDimensions.length == 0) {

            return (
                <div style={SharedStyles.widgetWrapper}>
                    <div className="text-center" style={SharedStyles.plainText}>No Survey data found.</div>
                </div>
            )
        }
        else {
            return(
                <table className="table-curved" style={SharedStyles.tableStyle}>
                    <thead>
                        <tr>
                            <th 
                                colSpan={this.state.widget.innerDimensions.length} 
                                style={SharedStyles.tableHeading}>
                                    <span style={SharedStyles.plainText}>Viewing data for</span>&nbsp;
                                    {this.state.widget.dataDimension}
                            </th>
                        </tr>
                    </thead>
                    {this.renderTableBody()}
                </table>
            )
        }
    }

    renderSubHeadingRow(text, colSpan){
        
        return(
            <tr>
                <td colSpan={colSpan} style={{backgroundColor: '#F6F6F6'}}>
                    <div>
                        <div style={styles.subHeading}>{text}</div>
                    </div>
                </td>
            </tr>
        )
    }

    render() {

        return (
            <div style={SharedStyles.widgetResponse}>
                {this.renderReportTable()}
            </div>
        )
    }
}

export default MultiDimensionReport;

const styles = {

    wordCloudWrapper: {
        maxWidth: '250px'
    },
    topicSectionTitle: {
        fontFamily: 'Pangram-Bold', 
        textDecoration:"underline"
    },
    entriesSectionTitle: {
        fontFamily: 'Pangram-Regular', 
        textDecoration:"underline", 
        display:"inline-flex", 
        //padding: "10px 0"
    },
    topicSeparator: { 
        width:"100px", 
        borderColor : '#e4e2e2'
    },
    unorderedList: {
        paddingInlineStart: '30px'
    },
    listStyle: {
        paddingBottom: '5px'
    },
    topicWrapper: {
        marginTop: '12px',
        paddingTop: '12px', 
        borderTop: '0.5px dashed #d2d0d0'
    },
    subHeading: {
        textAlign:'center',
        fontFamily:'Pangram-Bold',
        minWidth: '33.33%',
    },
}