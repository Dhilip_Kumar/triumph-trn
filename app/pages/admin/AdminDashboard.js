import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import SharedStyles from '../../SharedStyles';
import OrgGroupManagement from './OrgGroupManagement';

class AdminDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displaySection: 'Dashboard'
        }
    }
    
    renderMenuItem(menuItem) {

        return(
            <div className="menu-items" onClick={() => this.setState({displaySection: menuItem})}
                style={this.state.displaySection == menuItem ? styles.highlightedMenuItem : styles.unSelectedMenuItem}> 
                    {menuItem}
            </div>
        )
    }

    renderSideBar() {
        
        return (
            <div className="col-md-3 sidebar">
                {this.renderMenuItem('Dashboard')} <hr />
                {this.renderMenuItem('Group Management')}
            </div>
        )
    }

    renderInnerContainer() {

        switch (this.state.displaySection) {
            case 'Dashboard': return (
                                <div>
                                    <h3 className="mainSectionTitle">Admin Dashboard</h3>
                                    <div>The page is under development.</div>
                                </div>);
            case 'Group Management': return <OrgGroupManagement {...this.props} />;
            default: return <div />;
        }
    }

    render() {
		
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackgroundWithImage}>   
                    <div className="container containerWrapper container-padding">
                        {this.renderSideBar()}
                        <div className="col-md-9 mainSection">
                            {this.renderInnerContainer()}
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }	
}

export default AdminDashboard;

const styles = {

    highlightedMenuItem: {
        fontFamily:'Pangram-Bold',
        color: '#F79569',
        fontSize: 17
    },
    unSelectedMenuItem: {
        fontFamily:'Pangram-Regular',
        color: '#7D7D7D',
        fontSize: 16
    },
};