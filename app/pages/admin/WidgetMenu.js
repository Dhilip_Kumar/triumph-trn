import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import SharedStyles from '../../SharedStyles';

class WidgetMenu extends Component {

    constructor(props) {
        super(props);
    }

    scrollToWidget(widget) {
        var targetWidget = window.document.getElementById('widget_' + widget.widgetID);
        targetWidget.scrollIntoView({behavior: "smooth"});
    }

    toggleVisibility(widget) {
        
        var targetWidget = window.document.getElementById('widget_' + widget.widgetID);
        var togglerImage = window.document.getElementById('widget_menu_visibility_' + widget.widgetID);

        if (targetWidget.style.display == 'block') {
            targetWidget.style.display = 'none';
            togglerImage.src = '/images/hide.png';
        }
        else {
            targetWidget.style.display = 'block';
            togglerImage.src = '/images/visible.png';
        }
    }

    renderInnerDimensionsMenuText(innerDimensions) {

        var textCopy = [];

        innerDimensions.map((innerDimension) => {
            textCopy.push(innerDimension.filter + ' ' + innerDimension.dimension);
        });
        return textCopy.join(" vs. ");
    }

    renderDataPrompt(widget) {
        
        if (widget.widgetType == 'multi-dimension') {
            
            return(
                <div>
                    <hr style={SharedStyles.lineSeparator}></hr>
                    <p>{widget.dataDimension}</p>
                    <div>{this.renderInnerDimensionsMenuText(widget.innerDimensions)}</div>
                </div>
            )
        }
        else if (widget.widgetType == 'data-dimension') {
            
            return(
                <div>
                    <hr style={SharedStyles.lineSeparator}></hr>
                    <p>{widget.dataDimension}</p>
                </div>
            )
        }
        else {
            return null;
        }
    }

    render() {

        if (this.props.showWidgetMenu === true) {

            return (
                <div id='widget-menu-container'>
                    <div style={SharedStyles.widgetListContainer} id='widget-list-container'>
                    {
                        this.props.widgets.map((widget, index) => { 
                            return(
                                <div key={index} style={SharedStyles.widgetContainer} onClick={() => this.scrollToWidget(widget)}>
                                    <div style={styles.menuStyle}>
                                        <div style={{flex:1}}>{widget.widgetTitle}</div>
                                        <div onClick={() => this.toggleVisibility(widget)}><img src='/images/visible.png' id={'widget_menu_visibility_' + widget.widgetID} width={18} /></div>
                                    </div>
                                    {this.renderDataPrompt(widget)}
                                </div>
                            )
                        })
                    }
                    </div>
                </div>
            )
        }
        else {
            return null;
        }
    }
}

export default WidgetMenu;

const styles = {

    menuStyle: {
        fontFamily: 'Pangram-Regular',
        fontSize: '16px',
        display: 'flex',
        alignItems: 'center'
    },
}