import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import User from '../../modules/User';
import Util from '../../modules/Util';
import Globals from '../../modules/Globals';
import SharedStyles from '../../SharedStyles';

class AdminLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            replaceButtonWithActivityIndicator: false
        };
    }


    login() {

        if (this.state.email == "" || this.state.password == "")
            return document.getElementById("errorMsg").innerHTML = "Please enter all the details";

        if (!Util.validateEmail(this.state.email))
            return document.getElementById("errorMsg").innerHTML = "Please enter a valid email address.";

        this.setState({
            replaceButtonWithActivityIndicator: true
        });

        User.login(this.props.coreData, Globals.USER_ROLE.TRN_ORG_ADMIN, 
            { email: this.state.email, password: this.state.password }, (err, result) => {

            if (err) {
                this.setState({
                    replaceButtonWithActivityIndicator: false
                });
                return document.getElementById("errorMsg").innerHTML = err;
            }

            if (result == 'success') {
                this.props.history.push('/admin-dashboard');
            } 
            else {
                this.setState({
                    replaceButtonWithActivityIndicator: false
                });
                return document.getElementById("errorMsg").innerHTML = 'Login failed. Please try again.';
            }
        });
    }

    keypress(e) {
        var THIS = this;
        if (e.key === 'Enter') {
            THIS.login();
        }
    }

    handleEmailChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({email: e.target.value});
    }

    handlePasswordChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({password: e.target.value});
    }

    renderButton() {

        if (this.state.replaceButtonWithActivityIndicator) {
            return (
                <div style={{...SharedStyles.loaderComponent, ...{height: '65px'}}}>
					<img style={styles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
    			</div>
            )
        }
        return (
            <div className="login-submit"><button type="submit" className="submit" id="submit" onClick={this.login.bind(this)}>Login</button></div>
        )
    }

    render() {

        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackgroundWithImage}>
                    <div className="login-section" style={{paddingTop:'40px'}}>
                        <div className="login-section-container" style={SharedStyles.loginContainer}>
                            <p style={SharedStyles.loginSectionTitle}>Admin Login</p>
                            <p className="login-section-heading"> Login with your email and password to access your organization stats.</p>
                            <div className="login-input"><input type="email" name="email" value={this.state.email} onKeyPress={this.keypress.bind(this)} onChange={this.handleEmailChange.bind(this)} placeholder="Enter your email" /></div>
                            <div className="login-input"><input type="password" name="password" placeholder="Enter your password" value={this.state.password} onKeyPress={this.keypress.bind(this)} onChange={this.handlePasswordChange.bind(this)} /></div>
                            <div id="errorMsg" style={{color:'red'}}></div>
                            {this.renderButton()}
                            <p>By continuing, you agree to our <a style={styles.termsCopy} target="_blank" href='https://www.triumphhq.com/terms-of-use/'>Terms</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminLogin;

const styles = {

    termsCopy: {
        color: '#737373',
        textDecorationLine: 'underline'
    },
    loaderImage: {
        width: '35px',
        height: '35px',
        resizeMode: 'contain'
    }
}