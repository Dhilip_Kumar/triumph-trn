import React, { Component } from 'react';
// import Analytics from '../../Modules/Analytics';
import API_Services from '../../modules/API_Services';
import { Modal } from 'react-bootstrap';
import SharedStyles from '../../SharedStyles';
import Util from '../../modules/Util';
import Globals from '../../modules/Globals';
import CryptoJS from "crypto-js";
import _ from '@sailshq/lodash';
import Select from 'react-select';

class OrgGroupManagement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: true,
            isRequestProcessing: false,
            orgGroups: [],
            showUpdateModal: false,
            serviceResponseMessage: '',
            orgGroupObjToUpdate: {},
            surveyListOptions: []
        };
        this.fetchOrgGroups();
    }

    fetchOrgGroups() {

        var params = {
            trnOrganizationID: this.props.coreData && this.props.coreData.userCoreProperties ? this.props.coreData.userCoreProperties.organizationID : ''
        };

        if (!params.trnOrganizationID) return alert('Invalid Request');
        
        API_Services.httpPOST(this.props.coreData, 'getGroupManagementInfo', params, (err, result) => {

            if (err) {
                alert('Network Error. Please try after sometime.');
                return;
            };

            var surveyListOptions = [
                {
                    label: 'Individual Readiness [IIA]',
                    value: 'IIA'
                }
            ];

            /* The "react-select" npm package expects options to be presented in an array of JSON format with keys "label" and "value". Hence cast our DB format to that one. */

            if (result.surveyList && result.surveyList.length > 0) {

                for (let i = 0; i < result.surveyList.length; i++) {
                    surveyListOptions.push({
                        label: result.surveyList[i].sectionTitle,
                        value: result.surveyList[i].sectionID
                    });
                };
            };

            this.setState({
                orgGroups: result ? result.orgGroups : [],
                surveyList: surveyListOptions,
                showActivityIndicator: false
            });
        });
    }

    saveOrgGroup() {

        var orgGroupObjToUpdate = this.state.orgGroupObjToUpdate;

        if (Object.keys(orgGroupObjToUpdate).length == 0) return;

        if (Util.validateTextInput(orgGroupObjToUpdate.groupName) == false) {
            return alert('Please enter a valid group name.');
        }
        else if (Util.validateTextInput(orgGroupObjToUpdate.groupDescription) == false) {
            return alert('Please enter a valid group description.');
        }
        else if (Util.validateTextInput(orgGroupObjToUpdate.groupSecret) == false) {
            return alert('Please enter a valid group secret.');
        }
        else if (!orgGroupObjToUpdate.surveyList || (orgGroupObjToUpdate.surveyList && orgGroupObjToUpdate.surveyList.length <= 0)) {
            return alert('Please select the survey sections.');
        }
        else if (orgGroupObjToUpdate.showIIAReportToTheUser == undefined || orgGroupObjToUpdate.showIIAReportToTheUser == null) {
            return alert('Please select the preference for showing the IIA report to the user.');
        }
        else if (orgGroupObjToUpdate.showSurveyCompletionCodeToTheUser == undefined || orgGroupObjToUpdate.showSurveyCompletionCodeToTheUser == null) {
            return alert('Please select the preference for showing the survey completion code to the user.');
        }
        else if (!orgGroupObjToUpdate.status) {
            return alert('Please select the status of the group.');
        };

        if (!this.state.isRequestProcessing) {
            this.setState({isRequestProcessing: true});
        };

        /* Store the encrypted secret to mantain same survey URL throught. */
        orgGroupObjToUpdate.encryptedSecret = CryptoJS.AES.encrypt(orgGroupObjToUpdate.groupSecret, Globals.CRYPTO_ENCRYPTION_SECRET).toString(); 
        orgGroupObjToUpdate.trnOrganizationID = this.props.coreData && this.props.coreData.userCoreProperties ? this.props.coreData.userCoreProperties.organizationID : '';

        var params = {
            orgGroupObjToUpdate: orgGroupObjToUpdate
        };

        API_Services.httpPOST(this.props.coreData, 'updateTRNOrgGroups', params, (err, updatedRecord) => {

            this.setState({
                serviceResponseMessage: updatedRecord ? 'Record updated successfully!' : err,
                orgGroupObjToUpdate: updatedRecord ? updatedRecord : orgGroupObjToUpdate,
                isRequestProcessing: false
            },
            () => {
                this.fetchOrgGroups();
            });
        });
    }

    updateOrgGroupDetails(item={}) {

        this.setState({
            orgGroupObjToUpdate: item,
            showUpdateModal: true,
            serviceResponseMessage: '', // Reset the message everytime modal opened newly
        });
    }

    handleOrgGroupDetailUpdates(fieldName, value) {
        
        var orgGroupObjToUpdate = Util.cloneArray(this.state.orgGroupObjToUpdate);
        orgGroupObjToUpdate[fieldName] = value;

        this.setState({
            orgGroupObjToUpdate: orgGroupObjToUpdate,
            serviceResponseMessage: ''  // Reset message upon editing again
        });
    }

    /* Change the "react-select" provided format to the desired format. */

    handleSurveySectionChange(selectedSurveySectionList) {

        var surveyList = [];

        for (var i=0; i < selectedSurveySectionList.length; i++) {

            surveyList.push({
                sectionTitle: selectedSurveySectionList[i].label,
                sectionID: selectedSurveySectionList[i].value
            });
        };

        this.handleOrgGroupDetailUpdates('surveyList', surveyList);
    }

    renderOrgGroupDetailFooterOptions() {

        if (this.state.isRequestProcessing) {
            return(
                <div className="loaderContainer" style={{margin:10}}>
                    <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                </div>
            )
        }
        else if (this.state.serviceResponseMessage) {
            return(
                <div style={{...styles.label, ...{color:'#f79569'}}}>{this.state.serviceResponseMessage}</div>
            )
        }
        else {
            return(
                <button style={SharedStyles.primaryButton} onClick={() => this.saveOrgGroup()}>Save</button>
            )
        }
    }

    /* The "react-select" npm package expects options to be presented in an array of JSON format with keys "label" and "value". Hence cast our DB format to that one. */

    renderSelectedSurveySectionValues(groupObj) {

        var selectedSurveySectionValues = [];

        if (groupObj.surveyList && groupObj.surveyList.length > 0) {

            for (var i=0; i < groupObj.surveyList.length; i++) {

                selectedSurveySectionValues.push({
                    label: groupObj.surveyList[i].sectionTitle,
                    value: groupObj.surveyList[i].sectionID
                });
            };
        };
        return selectedSurveySectionValues;
    }

    renderOrgGroupDetails() {

        var orgGroupObjToUpdate = this.state.orgGroupObjToUpdate || {};

        return (
            
            <div>

                <p style={{...styles.label, ...{marginTop:0}}}>Name</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={orgGroupObjToUpdate.groupName}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrgGroupDetailUpdates('groupName', event.target.value)} 
                    placeholder="Name" />

                <p style={{...styles.label}}>Description</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={orgGroupObjToUpdate.groupDescription}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrgGroupDetailUpdates('groupDescription', event.target.value)} 
                    placeholder="Description" />

                <p style={{...styles.label}}>Group Secret</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={orgGroupObjToUpdate.groupSecret}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrgGroupDetailUpdates('groupSecret', event.target.value)} 
                    placeholder="Secret" />

                <p style={{...styles.label}}>Survey List</p>
                <Select
                    value={this.renderSelectedSurveySectionValues(orgGroupObjToUpdate)}
                    onChange={this.handleSurveySectionChange.bind(this)}
                    isMulti={true}
                    options={this.state.surveyList}
                    placeholder={'Select Survey Sections...'}
                    styles={styles.selectBoxStyles}/>

                <p style={styles.label}>Show IIA Report To The User</p>
                <Select
                    value={_.find(Globals.BOOLEAN_OPTIONS, {value: orgGroupObjToUpdate.showIIAReportToTheUser})}
                    onChange={() => this.handleOrgGroupDetailUpdates('showIIAReportToTheUser', !orgGroupObjToUpdate.showIIAReportToTheUser)} 
                    options={Globals.BOOLEAN_OPTIONS}
                    styles={styles.selectBoxStyles}/>

                <p style={styles.label}>Show Completion Code To The User</p>
                <Select
                    value={_.find(Globals.BOOLEAN_OPTIONS, {value: orgGroupObjToUpdate.showSurveyCompletionCodeToTheUser})}
                    onChange={() => this.handleOrgGroupDetailUpdates('showSurveyCompletionCodeToTheUser', !orgGroupObjToUpdate.showSurveyCompletionCodeToTheUser)} 
                    options={Globals.BOOLEAN_OPTIONS}
                    styles={styles.selectBoxStyles}/>
                
                <p style={styles.label}>Status</p>
                <div style={styles.rowContainer}>
                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'groupActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={orgGroupObjToUpdate && orgGroupObjToUpdate.status == 'active' ? true: false}
                        onChange={() => this.handleOrgGroupDetailUpdates('status', 'active')}
                    />
                    <span>Active</span>

                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'groupActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={orgGroupObjToUpdate && orgGroupObjToUpdate.status == 'disabled' ? true: false}
                        onChange={() => this.handleOrgGroupDetailUpdates('status', 'disabled')}
                    />
                    <span>Disabled</span>
                </div>

                <div className="text-center">
                    {this.renderOrgGroupDetailFooterOptions()}
                </div>
            </div>
        )
    }

    getSurveyURLForTheGroup(groupObj) {

        var baseURL = Globals.ENVIRONMENT[Globals.ENVIRONMENT.ENV].BACKEND_BASE_URL;
        var encryptedSecret = groupObj.encryptedSecret ? groupObj.encryptedSecret : CryptoJS.AES.encrypt(groupObj.groupSecret, Globals.CRYPTO_ENCRYPTION_SECRET); 
        return (baseURL + '?gi=' + groupObj.groupID + '&gcs=' + encryptedSecret.toString() + '&s=direct');
    }

    renderModalPopup() {

        return(
            <div className="static-modal">
                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.state.showUpdateModal}
                    onHide={() => this.setState({showUpdateModal: false})}>
                        <Modal.Header closeButton>
                            <Modal.Title>Update Group</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {this.renderOrgGroupDetails()}
                        </Modal.Body>
                </Modal>
            </div>
        )
    }

    renderOrgGroupList() {

        var orgGroups = this.state.orgGroups || [];

        if (orgGroups && orgGroups.length > 0) {

            return (
                <div style={styles.orgListWrapper}>
                    {
                        orgGroups.map((item, i) => {
                            return(
                                <div className="team-body" key={i}>
                                    <div style={styles.rowContainer}>
                                        <div style={styles.teamNameHeader}>{i+1}. {item.groupName}</div>
                                        <div onClick={() => this.updateOrgGroupDetails(item)}>
                                            <img style={styles.editImage} src="/images/EditIcon.png" alt="edit" />
                                        </div>
                                    </div>
                                    <table className="table-curved table-striped" style={styles.tableStyle}>
                                        <thead>
                                            <tr>
                                                <th className="col-md-4" style={styles.tableHeading}>Field</th>
                                                <th className="col-md-8" style={styles.tableHeading}>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>ID</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.groupID}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Name</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.groupName}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Description</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.groupDescription}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Secret</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.groupSecret}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Status</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.status == 'active' ? 'Active' : 'Disabled'}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Survey Sections</td>
                                                <td className="col-md-8" style={styles.tableData}>
                                                    {item.surveyList ? _.map(item.surveyList, 'sectionTitle').join(', ') : ''}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Show IIA Report</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.showIIAReportToTheUser ? 'True' : 'False'}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Show Completion Code</td>
                                                <td className="col-md-8" style={styles.tableData}>{item.showSurveyCompletionCodeToTheUser ? 'True' : 'False'}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4" style={styles.fieldHeader}>Link</td>
                                                <td className="col-md-8" style={styles.tableData}>{this.getSurveyURLForTheGroup(item)}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            )
                        })
                    }
                </div>
            )
        }
        else {
            return(
                <div style={styles.displayCopy}>
                    No Groups have been added.
                </div>
            )
        }
    }

    render() {

        if (this.state.showActivityIndicator) {
            return (
				<div style={SharedStyles.loaderComponent}>
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
        }

        return (
            <div style={styles.mainContainer}>
                <h3 className="mainSectionTitle">Group Management</h3>
                {this.renderOrgGroupList()}
                <div className='text-center'>
                    <button style={SharedStyles.primaryButton} onClick={() => this.updateOrgGroupDetails({}, 'Organization Details')}>Add Group</button>
                </div>
                {this.renderModalPopup()}
            </div>
        );
    }
}

const styles = {

    mainContainer: {
        maxWidth: "1100px",
        width: "100%",
    },
    displayCopy: {
        textAlign: 'center', 
        padding: '20px'
    },
    orgListWrapper: {
        marginTop: '20px',
        marginBottom: '10px'
    },
    tableStyle: {
        width: '100%',
        marginTop: '30px',
        marginBottom: '20px'
    },
    tableHeading: {
        textAlign: 'center',
        padding: '5px',
        fontFamily: 'Pangram-Bold',
        backgroundColor: '#dfdfdf',
    },
    tableData: {
        padding: '10px',
        textAlign: 'center'
    },
    fieldHeader: {
        padding: '10px',
        textAlign: 'center',
        fontFamily: 'Pangram-Regular'
    },
    textArea: {
        width: '100%',
        border: '1px solid #92d6e1',
        borderRadius: '4px',
        padding: '5px 10px',
        minHeight: '42px',
        outline: 'none',
        resize: 'none',
        backgroundColor: '#FFF',
        textAlign: 'left'
    },
    label: {
        marginTop:'15px',
        marginBottom: '15px',
        fontFamily:'Pangram-Regular'
    },
    customRadioBtn: {
        WebkitAppearance: 'none',
        MozAppearance:'none',
        width: '16px',
        height: '16px',
        borderRadius:'50%',
        outline:'none',
        border:'1px solid #1EB0D8',
        marginTop:'0px',
        marginRight:'8px',
        marginLeft:'8px'
    },
    rowContainer: {
        display:'flex',
        flexDirection:'row',
        alignItems:'center'
    },
    editImage: {
        width: 20,
        height: 20,
        marginLeft: 15,
        cursor: 'pointer'
    },
    teamNameHeader: {
        fontFamily: 'Pangram-Bold',
        fontSize: 17,
        paddingTop: 5
    },
    selectBoxStyles: {
        option: (provided, state) => ({
            ...provided,
            '&:hover': {
                backgroundColor: '#FFF5ED',
                color: '#737373'
            },
            backgroundColor: state.isSelected ? '#F79569' : '#FFF',
            color: state.isSelected ? '#FFF' : '#737373',
            outline: 'none'
        }),
        control: (base, state) => ({
            ...base,
            border: '1px solid #92d6e1 !important',
            boxShadow: '0 !important',
            minHeight: '42px',
            overflow: 'auto',
            marginTop: 15,
            marginBottom: 15
        })
    }
}

export default OrgGroupManagement;