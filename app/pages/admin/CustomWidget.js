import React, { Component, PureComponent } from 'react';
import { Modal } from 'react-bootstrap';
import Toggle from 'react-toggle';
import SharedStyles from '../../SharedStyles';
import Select from 'react-select';
import OrgAdmin from '../../modules/OrgAdmin';

var _ = require('@sailshq/lodash');

const widgetType = [
    { label: "Data Dimension", value: "data-dimension" },
    { label: "Multi Dimension", value: "multi-dimension" }
];

/* Currently, we are supporting only below types of filters. */
const filter = [
    { label: "High", value: "High" },
    { label: "Medium", value: "Medium" },
    { label: "Low", value: "Low" },
    { label: "Highest for User", value: "Highest for User" },
    { label: "Lowest for User", value: "Lowest for User" },
];

class CustomWidget extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            widgets: this.props.widgets,
            dataDimensions: [],
            innerDimensions: [],
            orgGroups: [],
            orgSurveys: [],
            stem : true,
            selectedWidgetTypeOption: {},
            selectedDataDimensionOption: {},
            selectedInnerDimensionOption: {},
            selectedInnerDimensions: {},
            selectedOrgGroup: {},
            selectedOrgSurvey: {},
            widgetTitle: '',
            action: 'add',
            showDeleteConfirmation: false,
            showActivityIndicator: true
        }
    }

    componentWillMount() {

        /* Get the data & inner dimensions to let admin create custom widgets. */
        OrgAdmin.getCustomWidgetDependencies(this.props.coreData, (err, dependencies) => {

            if (err) {
                alert('Network Error. Please try again later.');
                return;
            }

            this.setState({
                dataDimensions: dependencies.dataDimensions,
                innerDimensions: dependencies.innerDimensions,
                orgGroups: dependencies.orgGroups,
                orgSurveys: dependencies.orgSurveys,
                showActivityIndicator: false
            },
            () => {
                this.forceUpdate();
            });
        });
    }

    /* To handle edit widget functionality. */

    componentWillReceiveProps(props) {

        if (props.action == 'edit' && props.widgetToUpdate) {

            var widgetToUpdate = props.widgetToUpdate;
            var selectedInnerDimensions = {};

            /* Convert back to JSON obj */

            for (var i=0; i < widgetToUpdate.innerDimensions.length; i++) {
                
                let newInnerDimensionKey = widgetToUpdate.innerDimensions[i].category + " ";
                newInnerDimensionKey += widgetToUpdate.innerDimensions[i].dimension + " ";
                newInnerDimensionKey += widgetToUpdate.innerDimensions[i].filter;
                newInnerDimensionKey = newInnerDimensionKey.replace(/\s+/g, '-').toLowerCase();

                selectedInnerDimensions[newInnerDimensionKey] = widgetToUpdate.innerDimensions[i];
            };

            var selectedOrgSurvey = _.filter(this.state.orgSurveys, (surveys) => {
                return (widgetToUpdate['surveyDataset'].indexOf(surveys.value) > -1);
            });

            var selectedOrgGroup = _.filter(this.state.orgGroups, (groups) => {
                return (widgetToUpdate['groupDataset'].indexOf(groups.value) > -1);
            });

            var selectedWidgetType = widgetType.find(widget => 
                (widgetToUpdate['widgetType'].indexOf(widget.value) > -1));

            var stem = widgetToUpdate.options && _.isObject(widgetToUpdate.options) 
            && widgetToUpdate.options.stem == true || false ? widgetToUpdate.options.stem : false; 

            this.setState({
                widgetTitle: widgetToUpdate.widgetTitle,
                selectedWidgetTypeOption : selectedWidgetType ? selectedWidgetType : {},
                selectedDataDimensionOption: {
                    label: widgetToUpdate.dataDimension,
                    value: widgetToUpdate.dataDimension
                },
                selectedInnerDimensions: selectedInnerDimensions,
                selectedOrgGroup: selectedOrgGroup,
                selectedOrgSurvey: selectedOrgSurvey,
                action: 'edit',
                stem: stem,
                widgetToUpdate: widgetToUpdate
            });
        };
    }

    saveCustomWidget() {

        this.errorMessage('');
        
        /* Validations */
        if (!this.state.selectedWidgetTypeOption) {
            return this.errorMessage("Please select widget type.");
        }
        else if (Object.keys(this.state.selectedOrgGroup).length === 0 || Object.keys(this.state.selectedOrgSurvey).length === 0) {
            return this.errorMessage("Please select target survey & group.");
        }
        else if (Object.keys(this.state.selectedDataDimensionOption).length === 0) {
            return this.errorMessage("Please select data dimension.");
        }
        else if (this.state.selectedWidgetTypeOption.value == "multi-dimension" && Object.keys(this.state.selectedInnerDimensions).length === 0) {
            return this.errorMessage("Please select inner dimension.");
        }
        else if (this.state.selectedWidgetTypeOption.value == "multi-dimension" && Object.keys(this.state.selectedInnerDimensions).length < 2) {
            return this.errorMessage("Please select more than one inner dimension.");
        }
        else if (!this.state.widgetTitle) {
            return this.errorMessage("Please enter widget title");
        }

        var innerDimensions = [];
        var surveyDataset = [];
        var groupDataset = [];

        if (this.state.selectedWidgetTypeOption.value == "multi-dimension") {
            
            for (var key in this.state.selectedInnerDimensions) { /* Convert JSON obj to Array */
                innerDimensions.push(this.state.selectedInnerDimensions[key]);
            };
        }
       
        for (var i=0; i < this.state.selectedOrgGroup.length; i++) {
            groupDataset.push(this.state.selectedOrgGroup[i].value);
        };
        
        for (var i=0; i < this.state.selectedOrgSurvey.length; i++) {
            surveyDataset.push(this.state.selectedOrgSurvey[i].value);
        };

        let customWidget = (this.state.action == 'edit' && this.state.widgetToUpdate) ? this.state.widgetToUpdate: {};

        customWidget.widgetType = this.state.selectedWidgetTypeOption.value;
        customWidget.widgetTitle = this.state.widgetTitle.trim();
        customWidget.dataDimension = this.state.selectedDataDimensionOption.value;
        customWidget.innerDimensions = innerDimensions;
        customWidget.surveyDataset = surveyDataset;
        customWidget.groupDataset = groupDataset;
        customWidget.isDefault = false;
        customWidget.response = false;
        customWidget.options = customWidget.options ? customWidget.options : {};
        customWidget.options.stem = this.state.stem && this.state.stem  == true ? true : false;

        console.log('customWidget...', customWidget );

        this.updateWidgetInServer(customWidget);
    }

    updateWidgetInServer(customWidget) {

        this.setState({showActivityIndicator: true});

        var params = {
            organizationID: this.props.coreData.userCoreProperties.organizationID,
            action: this.state.action,
            widgetToUpdate: customWidget
        };

        OrgAdmin.saveCustomWidget(this.props.coreData, params, (err, response) => {

            if (err || (response && response.status != 'success')) {
                alert('Network Error. Please try again later.');
                return;
            }

            setTimeout(() => {
                window.location.reload(); /* Temporary workaround */
            },
            500);

            // this.setState({
            //     showActivityIndicator: false
            // },
            // () => {
            //     this.clear();
            //     //this.props.addNew(customWidget);
            //     this.props.onClose(false);
            //     setTimeout(() => {
            //         window.location.reload(); /* Temporary workaround */
            //     },
            //     500);
            // });
        });
    }

    handleDelete() {

        this.setState({
            action: 'delete',
            showDeleteConfirmation: false
        },
        () => {
            this.updateWidgetInServer(this.state.widgetToUpdate);
        });
    }

    clear = () => {
        this.errorMessage('');
        this.setState({
            stem:true,
            selectedWidgetTypeOption: {},
            selectedDataDimensionOption: {},
            selectedInnerDimensionOption: {},
            selectedInnerDimensions : {},
            widgetTitle: '',
            action: '',
            selectedOrgGroup: [],
            selectedOrgSurvey: [],
            widgetToUpdate: null
        });
    }

    closeModal = () => {
        this.clear();
        this.props.onClose(false);
    }

    handleOnChangeStem = () => {
        this.errorMessage('');
        this.setState({ stem : !this.state.stem });
        console.log(`selectedStemOption:`, !this.state.stem);
    }

    handleOnChangeWidgetType = selectedWidgetTypeOption => {
        this.errorMessage('');
        this.setState({selectedWidgetTypeOption});
        console.log(`selectedWidgetTypeOption selected:`, selectedWidgetTypeOption);
    }

    handleOnChangeDataDimension = selectedDataDimensionOption => {
        this.errorMessage('');
        this.setState({selectedDataDimensionOption});
        console.log(`selectedDataDimensionOption selected:`, selectedDataDimensionOption);
    }

    handleOnChangeInnerDimension = selectedInnerDimensionOption => {
        this.errorMessage('');
        this.setState({selectedInnerDimensionOption});
        console.log(`selectedDataDimensionOption selected:`, selectedInnerDimensionOption);
    }

    handleOnChangeFilter = selectedFilter => {
        this.errorMessage('');
        this.setState({selectedFilter});
        console.log(`selectedFilter selected:`, selectedFilter);
    }

    handleOnChangeOrgGroups = selectedOrgGroup => {
        
        this.errorMessage('');

        if (!selectedOrgGroup) selectedOrgGroup = [];
        var hasAllSelection = _.find(selectedOrgGroup, {value: 'all'});

        /* If the admin chooses 'all' as the filter option at any time, reset other selected options if any */

        if (hasAllSelection) {
            selectedOrgGroup = [hasAllSelection];
        };
        this.setState({selectedOrgGroup});
        console.log(`selectedOrgGroup selected:`, selectedOrgGroup);
    }

    handleOnChangeOrgSurvey = selectedOrgSurvey => {
        
        this.errorMessage('');

        if (!selectedOrgSurvey) selectedOrgSurvey = [];
        var hasAllSelection = _.find(selectedOrgSurvey, {value: 'all'});

        /* If the admin chooses 'all' as the filter option at any time, reset other selected options if any */

        if (hasAllSelection) {
            selectedOrgSurvey = [hasAllSelection];
        };
        this.setState({selectedOrgSurvey});
        console.log(`selectedOrgSurvey selected:`, selectedOrgSurvey);
    }

    errorMessage(msg) {
        document.getElementById("errorMsg").innerHTML = msg;
    }

    removeInnerDimension = (key) => {
        let selectedInnerDimensions = this.state.selectedInnerDimensions;
        delete selectedInnerDimensions[key];
        this.setState({selectedInnerDimensions});
    }

    addInnerDimension = () =>{
        
        console.log(`Option selected:>>>`, this.state.selectedInnerDimensionOption, this.state.selectedFilter, this.state.selectedInnerDimensions);
        
        if (!this.state.selectedInnerDimensionOption || !this.state.selectedFilter){
            this.errorMessage("Please choose inner dimension and filter");
            return
        };

        let newInnerDimensionKey = this.state.selectedInnerDimensionOption.category + " ";
        newInnerDimensionKey += this.state.selectedInnerDimensionOption.value + " ";
        newInnerDimensionKey += this.state.selectedFilter.value;
        newInnerDimensionKey = newInnerDimensionKey.replace(/\s+/g, '-').toLowerCase();
        
        if (this.state.selectedInnerDimensions && this.state.selectedInnerDimensions[newInnerDimensionKey]){
            return this.errorMessage("Selected inner dimension and filter already exist");
        }

        this.errorMessage('');

        let selectedInnerDimensions = this.state.selectedInnerDimensions;
        
        console.log('selectedInnerDimensions...', this.state.selectedInnerDimensions)
        
        selectedInnerDimensions[newInnerDimensionKey] = {
            category : this.state.selectedInnerDimensionOption.category,
            dimension: this.state.selectedInnerDimensionOption.value ,
            filter   : this.state.selectedFilter.value,
        }
        this.setState({selectedInnerDimensions});
    }

    renderInnerDimensions = () => {
        
        return (
            <div style={styles.selectedInnerDimensions}>
            {
                Object.keys(this.state.selectedInnerDimensions).map((key)=>{
                    return ( 
                        <span style={styles.tag} key={key}>
                            <span>{this.state.selectedInnerDimensions[key].filter + ' ' + this.state.selectedInnerDimensions[key].dimension}</span>
                            <button style={styles.closeTag} onClick={this.removeInnerDimension.bind(this, key)}>
                                x
                            </button>
                        </span>
                    )
                })
            }
            </div>
        )
    }

    renderInnerDimensionsSelection = () => {

        if (this.state.selectedWidgetTypeOption && this.state.selectedWidgetTypeOption.value == "multi-dimension"){
            
            return (
                <React.Fragment>
                    <div style={styles.inputSection}>
                        <p style={styles.sectionTitle}>Inner Dimensions</p>
                        <div style={styles.rowContainer}>
                            <div style={styles.columnSelector}>
                                <Select
                                    onChange={this.handleOnChangeInnerDimension}
                                    options={this.state.innerDimensions}
                                    placeholder={'Select Dimension...'}
                                    styles={styles.selectBoxStyles}/>
                            </div>
                            <div style={styles.columnSelector}>
                                <Select
                                    onChange={this.handleOnChangeFilter}
                                    options={filter}
                                    placeholder={'Select Filter...'}
                                    styles={styles.selectBoxStyles}/>
                            </div>
                            <div style={styles.addBtnContainer}>
                                <button type="button" style={styles.addButton} onClick={this.addInnerDimension.bind(this)}>Add</button>
                            </div>
                        </div>
                    </div>
                    {this.renderInnerDimensions()}
                </React.Fragment>
            )
        }
        return null;
    }

    renderSettings = () => {
        
        return (
            <div style={styles.inputSection}>
                <p style={styles.sectionTitle}>Settings</p>
                <div>
                    <label style={{ display: 'flex' }}>
                        <Toggle 
                            className='custom-toggleBtn' 
                            defaultChecked={this.state.stem} 
                            onChange={this.handleOnChangeStem} />
                        <span style={{ paddingLeft: '10px' }}> Stem </span>
                    </label>
                </div>
            </div>
        );
    }
    
    renderCustomWidget = () => {

        if (this.state.showActivityIndicator) {

            return (
                <div style={SharedStyles.loaderComponent} style={{height:'auto'}}>
                    <div className="loaderContainer" style={{textAlign: 'center'}}>
                        <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                    </div>
                </div>
            )
        }

        if (this.state.showDeleteConfirmation) {

            return (
                <div className='text-center'>
                    Are you sure you want to delete the widget <span style={SharedStyles.widgetTitle}>{this.state.widgetToUpdate.widgetTitle}</span>?
                    <div style={styles.btnContainer}>
                        <button type="button" style={styles.primaryButton} onClick={() => this.setState({showDeleteConfirmation: false})}>Cancel</button>
                        <button type="button" style={{...styles.primaryButton, ...{marginLeft: '15px'}}} onClick={this.handleDelete.bind(this)}>Delete</button>
                    </div>
                </div>
            )
        }

        return(
            <div>
                <div style={styles.inputSection}>
                    <p style={styles.sectionTitle}>Widget Type</p>
                    <Select
                        value={ Object.keys(this.state.selectedWidgetTypeOption).length === 0 ? null : this.state.selectedWidgetTypeOption}
                        onChange={this.handleOnChangeWidgetType}
                        options={widgetType}
                        placeholder={'Select Widget Type...'} 
                        // isDisabled={this.state.action == 'edit' ? true : false}
                        styles={styles.selectBoxStyles}/>
                </div>

                <div style={styles.inputSection}>
                    <p style={styles.sectionTitle}>Widget Title</p>
                    <input
                        value={this.state.widgetTitle}
                        onChange={(event) => this.setState({widgetTitle: event.target.value})} 
                        type='text' style={styles.textArea} 
                        placeholder="Title (e.g. Top Of Mind Things Across Energy States)" />
                </div>

                <div style={styles.inputSection}>

                    <p style={styles.sectionTitle}>Dataset(s)</p>
                    
                    <div style={styles.rowContainer}>
                        <div style={styles.columnSelector}>
                            <Select
                                value={Object.keys(this.state.selectedOrgSurvey).length === 0 ? null : this.state.selectedOrgSurvey}
                                onChange={this.handleOnChangeOrgSurvey}
                                options={this.state.orgSurveys}
                                isMulti={true}
                                placeholder={'Select Survey...'}
                                styles={styles.selectBoxStyles}/>
                        </div>
                        <div style={{...styles.columnSelector, ...{marginRight: '0px'}}}>
                            <Select
                                value={Object.keys(this.state.selectedOrgGroup).length === 0 ? null : this.state.selectedOrgGroup}
                                onChange={this.handleOnChangeOrgGroups}
                                options={this.state.orgGroups}
                                isMulti={true}
                                placeholder={'Select Group...'}
                                styles={styles.selectBoxStyles}/>
                        </div>
                    </div>
                </div>

                <div style={styles.inputSection}>
                    <p style={styles.sectionTitle}>Data Dimension</p>
                    <Select
                        value={Object.keys(this.state.selectedDataDimensionOption).length === 0 ? null : this.state.selectedDataDimensionOption}
                        onChange={this.handleOnChangeDataDimension}
                        options={this.state.dataDimensions}
                        placeholder={'Select Data Dimension...'} 
                        styles={styles.selectBoxStyles}/>
                </div>

                {/* Inner Dimension */}
                {this.renderInnerDimensionsSelection()}

                {/* Other Settings || Configurations */}
                {this.renderSettings()}
                
                <div id="errorMsg" style={{ color: 'red', textAlign: "center" }}></div>
                
                <div style={styles.btnContainer}>
                    <button type="button" style={styles.primaryButton} onClick={this.saveCustomWidget.bind(this)}>Save</button>
                    {this.state.action == 'edit' ?
                        <button type="button" style={{...styles.primaryButton, ...{marginLeft: '15px'}}} onClick={() => this.setState({showDeleteConfirmation: true})}>Delete</button> :
                        <button type="button" style={{...styles.primaryButton, ...{marginLeft: '15px'}}} onClick={this.clear.bind(this)}>Clear</button>
                    }
                </div>
            </div>
        )
    }

    render = () => {
        return (
            <div className="static-modal">
                <Modal
                    show={this.props.showCustomWidgetModal}
                    onHide={this.closeModal.bind(this, false)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Custom Widget</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.renderCustomWidget()}
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

export default CustomWidget;

const styles = {
    
    inputSection: {
        marginBottom: '15px'
    },
    sectionTitle: {
        fontFamily: 'Pangram-Regular'
    },
    textArea: {
        width: '100%',
        border: '1px solid #92d6e1',
        borderRadius: '4px',
        padding: '5px 10px',
        minHeight: '42px',
        outline: 'none',
        resize: 'none',
        backgroundColor: '#FFF',
        textAlign: 'left'
    },
    rowContainer: {
        display: 'flex',
        flexDirection: 'row'
    },
    columnSelector: {
        display: 'flex', 
        flex:1, 
        flexDirection: 'column',
        marginRight: '10px'
    },
    addBtnContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    addButton: {
        padding: '5px 10px',
        borderRadius: '4px',
        backgroundColor: '#96d8e2',
        border: '1px solid #96d8e2',
        color: '#fff'
    },
    btnContainer: {
        marginTop: '20px',
        marginBottom: '15px',
        textAlign: 'center'
    },
    primaryButton: {
        padding: '3px 15px',
        borderRadius: '8px',
        backgroundColor: '#F79569',
        border: '1px solid #F79569',
        color: '#fff'
    },
    selectedInnerDimensions: {
        display: "flex",
        flexWrap: "wrap", 
        justifyContent : "flex-start"
    },
    tag:{
        background: "#AECF7A",
        color: '#FFF',
        border: 0,
        margin: "10px",
        display: "flex",
        // minWidth: 100,
        // maxWidth: 200,
        padding: "3px 10px",
        borderRadius: "6px",
        justifyContent: "space-between",
        textAlign: "center",
        alignItems: "center"
    },
    closeTag: {
        padding: 0,
        backgroundColor: 'transparent',
        border: 'none',
        marginLeft: '8px'
    },
    selectBoxStyles: {
        option: (provided, state) => ({
            ...provided,
            backgroundColor: state.isSelected ? '#F79569' : '#FFF',
            color: state.isSelected ? '#FFF' : '#737373',
            outline: 'none'
        }),
        control: (base, state) => ({
            ...base,
            border: '1px solid #92d6e1 !important',
            boxShadow: '0 !important',
            '&:hover': {
                border: '1px solid #92d6e1 !important'
            },
            minHeight: '42px',
            maxHeight: '75px',
            overflow: 'auto'
        }),
        multiValueLabel: (base, state) => ({
            ...base,
            background: "#AECF7A",
            color: '#FFF',
            maxWidth: '100px',
            borderRadius: '0px',
            borderTopLeftRadius: '6px',
            borderBottomLeftRadius: '6px'
        }),
        multiValueRemove: (base, state) => ({
            ...base,
            background: "#AECF7A",
            color: '#FFF',
            borderRadius: '0px',
            borderTopRightRadius: '6px',
            borderBottomRightRadius: '6px'
        }),
    }
};