import React, { Component } from 'react';
import SurveyModule from '../../modules/Survey';
import Globals from '../../modules/Globals';
import SharedStyles from '../../SharedStyles';
import '../../../assets/styles/assessmentStyles.css';
import moment from 'moment';

class EmostateAssessment extends Component {

	constructor(props) {
		super(props);
		this.state = {
            data: [],
            sectionObj: null,
            updatedCount: 0,
            showActivityIndicator: true
        };
    }

    componentDidMount() {
        this.loadSectionData(this.props.sectionObj);
    }

    componentWillReceiveProps(props) {
        this.loadSectionData(props.sectionObj);
    }

    loadSectionData(sectionObj) {

        if (!sectionObj) return;

        SurveyModule.getSectionDataFromStorage(Globals.ASYNC_STORAGE_KEYS.IIA_DATA).then((assessmentData) => {

            if (assessmentData && assessmentData.statementsAndPrompts) {

                var updatedCount = 0;

                for (var i=0 ; i < assessmentData.statementsAndPrompts.length; i++) {
                    if (assessmentData.statementsAndPrompts[i].isAnswered) {
                        updatedCount += 1;
                    };
                };

                this.setState({
                    data: assessmentData.statementsAndPrompts,
                    sectionObj: assessmentData,
                    updatedCount: updatedCount,
                    showActivityIndicator: false,
                });
            }
            else {

                var sectionObj = this.props.sectionObj;
                sectionObj.sectionStartTime = moment().format();

                this.setState({
                    data: sectionObj.statementsAndPrompts,
                    sectionObj: sectionObj,
                    showActivityIndicator: false,
                });
            };

            /* Since we are using SPA approach where the components replace the previous one, we need to scroll to top of the page programatically */ 
            window.scrollTo(0, 0);
        });
    }

    updateAssessmentEntryValue(assessmentEntry, textResponse, e) {
        
        var value = e.target.value;
        var data = this.state.data;
        var answeredCount = this.state.updatedCount;

        if (assessmentEntry.isAnswered == false) {
            answeredCount = answeredCount + 1;
        }

        data[assessmentEntry.questionId - 1].selectedAnswer = parseInt(value);
        data[assessmentEntry.questionId - 1].score = parseInt(value);
        data[assessmentEntry.questionId - 1].textResponse = textResponse;
        data[assessmentEntry.questionId - 1].isAnswered = true;
        data[assessmentEntry.questionId - 1].associatedQuestion = data[assessmentEntry.questionId - 1].question;

        this.setState({
            data: data,
            updatedCount: answeredCount
        },
        () => {

            var sectionObj = this.state.sectionObj;
            sectionObj.statementsAndPrompts = data;

            //Update local storage
            SurveyModule.setSectionDataInTheStorage(Globals.ASYNC_STORAGE_KEYS.IIA_DATA, sectionObj).then(() => {
                
                var nextQuestion = document.getElementById('Q_' + assessmentEntry.questionId);
                nextQuestion.scrollIntoView({behavior: "smooth"});
            });
        });
    }

    handleSubmit() {

        if (this.state.updatedCount < this.state.data.length) {
            alert("It looks as though you missed a question! Please answer all questions to move forward");
        }
        else {

            /* Update section completion time */
            var sectionObj = this.state.sectionObj;
            sectionObj.sectionEndTime = moment().format();

            SurveyModule.setSectionDataInTheStorage(Globals.ASYNC_STORAGE_KEYS.IIA_DATA, sectionObj).then(() => {
                this.props.handleSectionChange('next');
            });
        }
    }

    renderTips() {
        
        return(
            <div>
                {/* <div className="assessment-intro">
                    <div className="assessment-intro-title">
                        <img src="/images/assessment-intro.png" width={40}/>
                        &nbsp;&nbsp;<span>Individual Readiness</span>
                    </div>
                    <div className="assessment-intro-text">
                        <div className="indent" style={{marginBottom:'10px'}}>
                            Any team or organization can only be as prepared or effective as the individual members, and this section is designed to help get a sense about where the individual team members are at.
                        </div>
                        <div className="indent" style={{marginBottom:'10px'}}>
                            In this section we are going to show you a set of hypothetical statements related to your tendencies. We’d like you to rate them in accordance with if they sound in line with how you would act. Please rate according to a 7-point scale ranging from <i>Very Untrue</i> to <i>Very True</i>.
                        </div>
                        <div className="indent">
                            Try not to think too deeply about each statement, as this section should be answered based on how you would act right now, in regards to how you feel right now. Sometimes, it is in our nature to put how we would like to answer rather than how we actually would, but doing this will only throw your results off! Your initial reaction may be your most honest, so go with that.
                        </div>
                        <div className="indent">
                            Also, don’t worry if you don’t like your answers—these results aren’t set in stone! With our help, when you next take the assessment you should see a great improvement in your results! OK - let’s begin.
                        </div>
                    </div>
                </div> */}

                <div className="assessment-intro">
                    <div className="question-container">
                        <div className="tips-section-title">In this section we are going to show you a set of statements related to your tendencies.</div>
                        <div className="tips-section-title">Please rate them on the 7-point scale ranging from <i>Very Untrue</i> to <i>Very True</i> based on how much they are true for you.</div>
                        <div className="radio-container">
                            <div className="radio-div"><div><input type="radio" className="input-radio selected" disabled={true}/><div>Very Untrue</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Mostly Untrue</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Somewhat Untrue</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Neutral</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Somewhat True</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Mostly True</div></div></div>
                            <div className="radio-div"><div><input type="radio" className="input-radio disabled" disabled={true}/><div>Very True</div></div></div>
                        </div>
                        <div className="tips-container">Answer this section based on how you feel or would act right now.</div>
                    </div>
                </div>
            </div>
        )
    }

    renderAnswers(question) {
        
        var score = question.selectedAnswer;

        if (question.reviseScore) {
            return (
                <div key={question.questionId}>
                    <div className="radio-div first"><div><input type="radio" value= "6" checked= { score == 6 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Very True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption">Very Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "5" checked= { score == 5 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Mostly True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Mostly Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "4" checked= { score == 4 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Somewhat True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Somewhat Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "3" checked= { score == 3 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Neutral')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Neutral</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "2" checked= { score == 2 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Somewhat Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Somewhat True</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "1" checked= { score == 1 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Mostly Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Mostly True</div></div></div>
                    <div className="radio-div last"><div><input type="radio" value= "0" checked= { score == 0 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Very Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption">Very True</div></div></div>
                </div>
            )
        }
        else {
            return (
                <div key={question.questionId}>
                    <div className="radio-div first"><div><input type="radio" value= "0" checked= { score == 0 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Very Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption">Very Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "1" checked= { score == 1 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Mostly Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Mostly Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "2" checked= { score == 2 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Somewhat Untrue')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Somewhat Untrue</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "3" checked= { score == 3 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Neutral')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Neutral</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "4" checked= { score == 4 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Somewhat True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Somewhat True</div></div></div>
                    <div className="radio-div"><div><input type="radio" value= "5" checked= { score == 5 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Mostly True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption desktop">Mostly True</div></div></div>
                    <div className="radio-div last"><div><input type="radio" value= "6" checked= { score == 6 ? true : false} onChange={this.updateAssessmentEntryValue.bind(this, question, 'Very True')} name={question.questionId} className="input-radio-question "/><div className="radio-caption">Very True</div></div></div>
                </div>
            )
        }
    }

    renderQuestions(question, index) {
        
        return (
            <div key={index} id={'Q_' + question.questionId}>
                <div className="questions-section">
                    <div className="question-container">
                        <div style={SharedStyles.questionCount}>{question.questionId} of {this.state.data.length}</div>
                        <div className="question-title">{question.question}</div>
                        <div className="radio-container">
                            {this.renderAnswers(question)}
                        </div>
                    </div>
                    <div className="bottom-div"></div>
                </div>
            </div>
        )
    }

    renderContainer() {

        var questions=[];
        this.state.data.map((item,index) => {
            questions.push(this.renderQuestions(item,index));
        })
        return questions;
    }

    render() {
        
        if (this.state.showActivityIndicator) {
			return (
				<div style={SharedStyles.loaderComponent} className="containerWrapper">
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
		}
		else {
			return (
                <div className="container containerWrapper mobile-container">
                    <div style={{padding:'10px 20px'}}>
                        <div className="emostateAssessment">
                            {this.renderTips()}
                            {this.renderContainer()}
                            <div className="submit-button">
                                {this.state.data.length > 0 ?
                                    <button className="btn sub-button" onClick={this.handleSubmit.bind(this)} type="submit">Next</button>
                                : null}
                            </div>
                        </div>
                    </div>
                </div>
			);
		}
	}
}

export default EmostateAssessment;