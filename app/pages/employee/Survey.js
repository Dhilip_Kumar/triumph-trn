import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import Overview from './Overview';
import EmostateAssessment from './EmostateAssessment';
import MatrixTableSurvey from './MatrixTableSurvey';
import MultipleChoiceQuestions from './MultipleChoiceQuestions';
import OpenEndedQuestions from './OpenEndedQuestions';
import SharedStyles from '../../SharedStyles';
import API_Services from '../../modules/API_Services';
import Globals from '../../modules/Globals';
import SurveyModule from '../../modules/Survey';
import Timer from 'react-compound-timer';
import moment from 'moment';
import queryString from 'query-string';
import CryptoJS from "crypto-js";

class Survey extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: true,
            displayMessage: '',
            displaySection: '',
            surveySections: [],
            activeSectionObj: null,
            activeSectionIndex: -1,
            surveyStartTime: null,
            surveySubmissionResponse: null
        }
    }
    
    componentDidMount() {

        // Get Survey sections configured for the group.

        var params = {
            groupID: this.props.coreData.userCoreProperties.groupID,
            trnOrganizationID: this.props.coreData.userCoreProperties.organizationID
        };

        API_Services.httpPOST(this.props.coreData, 'getSurveyListForTheGroup', params, (err, groupConfigs) => {

            if (err) {
                alert('Network Error. Please try after sometime.');
                return;
            };

            if (groupConfigs && groupConfigs.surveySections && groupConfigs.surveySections.length > 0) {
                
                this.setState({
                    displaySection: 'Overview',
                    surveySections: groupConfigs.surveySections,
                    showActivityIndicator: false
                });
            }
            else {
                this.setState({
                    displayMessage: 'No Active Survey found.',
                    showActivityIndicator: false
                });
            }
        });
    }

    handleSurveySubmission() {

        this.setState({showActivityIndicator: true});

        var surveySections = this.state.surveySections || [];

        /* Retrieve survey data from the local storage */

        SurveyModule.getSurveyResponsesFromStorage(surveySections).then((surveyResponses) => {

            /* Perform secondary validation */

            SurveyModule.validateSurveyResponses(surveyResponses).then((isValid) => {

                if (isValid === true) {

                    /* calculate total survey duration */
                    var surveyEndTime = moment().format();
                    var duration = moment.duration(moment(surveyEndTime).diff(moment(this.state.surveyStartTime)));

                    var postParams = {
                        organizationID: this.props.coreData.userCoreProperties.organizationID,
                        groupID: this.props.coreData.userCoreProperties.groupID,
                        surveyResponses: surveyResponses,
                        surveyStartTime: this.state.surveyStartTime,
                        surveyEndTime: surveyEndTime,
                        totalSurveyDurationInSeconds: duration.asSeconds(),
                        channel: 'direct'
                    };

                    if (location.search) {     
                        
                        var stringURL = location.search.replace(/\+/g, '%2B');
                        let queryParams = queryString.parse(stringURL);
                        
                        if (queryParams.s) {
                            postParams.channel = queryParams.s; // Record source/channel (e.g Linkedin etc..)
                        };
                    }

                    API_Services.httpPOST(this.props.coreData, 'submitSurveyResponses', postParams, (err, response) => {

                        if (err) {
                            alert('Network Error. Please try after sometime.');
                            return;
                        };

                        this.clearSurveySectionData();

                        this.setState({
                            surveySubmissionResponse: response,
                            displaySection: 'SubmissionSuccessful',
                            showActivityIndicator: false
                        });
                    });
                }
                else {
                    alert("It looks as though you missed a question! Please answer all questions to move forward");
                    return;
                }
            });
        });
    }

    clearSurveySectionData() {

        SurveyModule.clearSurveyResponseFromStorage();

        this.setState({
            surveySections: [],
            activeSectionObj: null,
            activeSectionIndex: -1,
            displayMessage: null,
            displaySection: '',
            surveyStartTime: null,
            showActivityIndicator: false
        });
    }

    handleSectionChange(navigateAction) {
        
        var surveySections = this.state.surveySections || [];
        var activeSectionIndex = this.state.activeSectionIndex;
        var nextSectionIndex = -1;
        
        if (navigateAction == 'next') {
            nextSectionIndex = activeSectionIndex + 1; 
        }
        else if (navigateAction == 'prev') {
            nextSectionIndex = activeSectionIndex - 1;
        }
        else {
            return false;
        }

        if (nextSectionIndex == surveySections.length) {
            return this.handleSurveySubmission();
        }
        else if (surveySections[nextSectionIndex]) {
            
            var activeSectionObj = surveySections[nextSectionIndex];
            var displaySection = '';

            if (activeSectionObj && activeSectionObj.sectionID == 'IIA') {
                displaySection = 'InnerInsightAssessment';
            }
            else if (activeSectionObj && (activeSectionObj.responseFormat == 'matrix-table-radio-options' || activeSectionObj.responseFormat == 'matrix-table-checkboxes')) {
                displaySection = 'MatrixTableSurvey';
            }
            else if (activeSectionObj && (activeSectionObj.responseFormat == 'multiple-choice-radio-options' || activeSectionObj.responseFormat == 'multiple-choice-checkboxes')) {
                displaySection = 'MultipleChoiceQuestions';
            }
            else if (activeSectionObj && activeSectionObj.responseFormat == 'open-ended-questions') {
                displaySection = 'OpenEndedQuestions';
            }

            /* As the user can nav back to the first section anytime, consider the very first time (this.state.surveyStartTime == null) user access the first section (nextSectionIndex == 0) for recording surveyStartTime. */

            this.setState({
                surveyStartTime: (nextSectionIndex == 0 && this.state.surveyStartTime == null) ? moment().format() : this.state.surveyStartTime,
                activeSectionIndex: nextSectionIndex,
                activeSectionObj: surveySections[nextSectionIndex],
                displaySection: displaySection
            });
        }
    }

    navToIIAReportPage(reportID) {
        this.props.history.replace('/assessment-report/?reportID=' + reportID);
    }

    renderCompletionMessage() {

        var surveySubmissionResponse = this.state.surveySubmissionResponse;

        return(
            <div style={SharedStyles.loaderComponent} className="container containerWrapper mobile-container">
                
                <div className="loaderContainer" style={{textAlign:'center', marginTop: '-100px'}}>
                    
                    <div><img style={styles.completionImg} src="/images/completion-tick.png" alt="success..." /></div>
                    
                    <div style={styles.completionCopy}>Thanks very much for completing the survey.</div>
                    
                    {surveySubmissionResponse && surveySubmissionResponse.surveyCompletionCode ?
                        <div style={styles.completionCopy}>Here is your completion code for this survey: <span style={styles.completionCode}>{surveySubmissionResponse.surveyCompletionCode}</span></div>
                    : null}
                    
                    {surveySubmissionResponse && surveySubmissionResponse.reportID ?
                        <div style={styles.btnContainer}>
                            <button type="submit" style={styles.button} id="submit" onClick={this.navToIIAReportPage.bind(this, surveySubmissionResponse.reportID)}>View Inner Insight Report</button>
                        </div>
                    : null}
                </div>
            </div>
        )
    }

    renderInnerContainer() {

        if (this.state.showActivityIndicator) {
            return (
				<div style={SharedStyles.loaderComponent} className="container containerWrapper mobile-container">
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
        }

        if (this.state.displaySection == 'Overview') {
            return <Overview surveySections={this.state.surveySections} handleSectionChange={this.handleSectionChange.bind(this)}/>
        }
        else if (this.state.displaySection == 'InnerInsightAssessment') {
            return <EmostateAssessment sectionObj={this.state.activeSectionObj} handleSectionChange={this.handleSectionChange.bind(this)} {...this.props} />
        }
        else if (this.state.displaySection == 'MatrixTableSurvey') {
            return <MatrixTableSurvey sectionObj={this.state.activeSectionObj} handleSectionChange={this.handleSectionChange.bind(this)} {...this.props} />
        }
        else if (this.state.displaySection == 'MultipleChoiceQuestions') {
            return <MultipleChoiceQuestions sectionObj={this.state.activeSectionObj} handleSectionChange={this.handleSectionChange.bind(this)} {...this.props} />
        }
        else if (this.state.displaySection == 'OpenEndedQuestions') {
            return <OpenEndedQuestions sectionObj={this.state.activeSectionObj} handleSectionChange={this.handleSectionChange.bind(this)} {...this.props}/>
        }
        else if (this.state.displaySection == 'SubmissionSuccessful') {
            return this.renderCompletionMessage();
        }
        else {
            return (
                <div style={SharedStyles.loaderComponent} className="container containerWrapper mobile-container">
                    {this.state.displayMessage}
                </div>
            );
        }
    }

    renderBreadCrumb() {

        if (this.state.activeSectionIndex < 1) {
            return <div style={styles.breadCrumbContainer} />;
        };

        return(
            <div style={styles.breadCrumbContainer}>
                <div
                    onClick={() => this.handleSectionChange('prev')} 
                    style={styles.breadCrumb}>
                    <img src='/images/back.png' width={25} style={{marginRight:'5px'}}/>
                        Back
                </div>
            </div>
        )
    }

    renderTopBar() {

        if (this.state.showActivityIndicator || this.state.displaySection == 'Overview' || this.state.displaySection == 'SubmissionSuccessful' || this.state.displayMessage) {
            return <div />;
        };

        return (
            <div className="container containerWrapper" style={{minHeight: 'auto'}}>
                <div style={styles.topBarConatiner}>
                    {this.renderBreadCrumb()}
                    <div style={styles.breadCrumbContainer}>Section {this.state.activeSectionIndex + 1} of {this.state.surveySections.length}</div>
                    <div style={styles.breadCrumbContainer}>
                        {/* <Timer ref={(ref) => this.timerRef = ref}>
                            <Timer.Hours />h : <Timer.Minutes />m : <Timer.Seconds />s
                        </Timer> */}
                    </div>
                </div>
            </div>
        )
    }

    render() {
		
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackground}>   
                    <div>
                        {this.renderTopBar()}
                        {this.renderInnerContainer()}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }	
}

export default Survey;

const styles = {

    topBarConatiner: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
        marginBottom: -15,
        paddingLeft: 15,
        paddingRight: 15,
        fontFamily: 'Pangram-Regular',
        fontSize:17
    },
    breadCrumbContainer: {
        minWidth: 70,
        textAlign: 'center'
    },
    breadCrumb: {
        textDecorationLine: 'underline',
        cursor: 'pointer',
        width: 'fit-content',
        display: 'flex',
        flexDirection: 'row'
    },
    completionImg: {
        width: '80px',
    },
    completionCopy: {
        fontFamily: 'Pangram-Regular',
        fontSize: '20px',
        padding: '0 40px',
        lineHeight: 2
    },
    btnContainer: {
        marginTop: '20px',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#FFF',
        color: '#AECF7A',
        border: '1px solid #AECF7A',
        padding: '10px 20px',
        borderRadius: '12px'
    },
    completionCode: {
        letterSpacing: 3,
        fontSize: 28,
        verticalAlign: 'middle',
        fontFamily: 'Pangram-Bold',
        paddingLeft: 5
    }
}