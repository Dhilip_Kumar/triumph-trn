import React, { Component } from 'react';
import Globals from '../../modules/Globals';
import SurveyModule from '../../modules/Survey';
import SharedStyles from '../../SharedStyles';
import '../../../assets/styles/assessmentStyles.css';
import htmlReactParser from 'html-react-parser';
import moment from 'moment';
import _ from '@sailshq/lodash';

let SelectedCheckbox = '/images/check-box-green-tick.png';
let BlankCheckbox = '/images/blank-check-box.png';

class MatrixTableSurvey extends Component {

    constructor(props) {
		super(props);
		this.state = {
            data: [],
            sectionObj: null,
            updatedCount: 0,
            showActivityIndicator: true
        };
    }

    componentDidMount() {
        this.loadSectionData(this.props.sectionObj);
    }

    componentWillReceiveProps(props) {
        this.loadSectionData(props.sectionObj);
    }

    loadSectionData(sectionObj) {

        if (!sectionObj) return;

        SurveyModule.getSectionDataFromStorage('Section_' + sectionObj.sectionID).then((surveyData) => {

            if (surveyData && surveyData.statementsAndPrompts) {

                var updatedCount = 0;
    
                for (var i=0 ; i < surveyData.statementsAndPrompts.length; i++) {
                    if (surveyData.statementsAndPrompts[i].isAnswered) {
                        updatedCount += 1;
                    };
                };
                
                this.setState({
                    sectionObj: surveyData,
                    data: surveyData.statementsAndPrompts,
                    updatedCount: updatedCount,
                    showActivityIndicator: false,
                });
            }
            else {
    
                /* Record individual section start time */
                sectionObj.sectionStartTime = moment().format();
    
                this.setState({
                    sectionObj: sectionObj,
                    data: sectionObj.statementsAndPrompts,
                    showActivityIndicator: false
                });
            }
    
            /* Since we are using SPA approach where the components replace the previous one, we need to scroll to top of the page programatically */ 
            window.scrollTo(0, 0); 
        });
    }

    updateUserResponse(promptIndex, checkBoxResponse=null, e) {
        
        var data = this.state.data;
        var updatedCount = this.state.updatedCount;

        if (this.state.sectionObj.responseFormat == 'matrix-table-radio-options') {

            var value = e.target.value;

            if (value == '' || value == null || value == undefined) return;

            var finalResponse = isNaN(value) == false ? parseInt(value) : value;

            if (!data[promptIndex].isAnswered) {
                updatedCount = updatedCount + 1;
            };
    
            if (data[promptIndex].requiresScoreInversion && isNaN(finalResponse) == false) {
                
                var scaleUnits = this.state.sectionObj.scaleUnits ? _.map(this.state.sectionObj.scaleUnits, 'value') : [];
                var minScalePoint = isNaN(scaleUnits[0]) == false ? parseInt(scaleUnits[0]) : -1;

                if (minScalePoint == 0) {
                    finalResponse = (scaleUnits.length - 1) - finalResponse;
                }
                else if (minScalePoint == 1) {
                    finalResponse = (scaleUnits.length + 1) - finalResponse; 
                }
            };

            var equivalentTextResponse = _.find(this.state.sectionObj.scaleUnits || [], (scaleUnitObj) => {
                return scaleUnitObj.value == value || scaleUnitObj.value == finalResponse; // Check with parsed value if the values are of numeric type. And also check for direct string match. 
            });

            data[promptIndex].selectedResponse = parseInt(value);
            data[promptIndex].finalResponse = finalResponse;
            data[promptIndex].textResponse = equivalentTextResponse ? equivalentTextResponse.label : '';
            data[promptIndex].isAnswered = true;
        }
        else if (this.state.sectionObj.responseFormat == 'matrix-table-checkboxes') {
        
            if (checkBoxResponse == '' || checkBoxResponse == null || checkBoxResponse == undefined) return;

            var userResponses = data[promptIndex].selectedResponse || [];
            var valueIndex = userResponses.indexOf(checkBoxResponse);

            if (!data[promptIndex].isAnswered) {
                updatedCount = updatedCount + 1;
            };

            if (valueIndex == -1) {
                userResponses.push(checkBoxResponse); // User select the checkbox item for the first time.
            }
            else {
                userResponses.splice(valueIndex, 1); // User select the checkbox item for the second time. Thereby to achieve toggle feature, we are removing that response.
            };

            data[promptIndex].selectedResponse = userResponses;
            data[promptIndex].isAnswered = true;
        };

        this.setState({
            data: data,
            updatedCount: updatedCount
        },
        () => {

            var sectionObj = this.props.sectionObj;
            sectionObj.statementsAndPrompts = data;

            //Update local storage
            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                
                var nextQuestion = document.getElementById('Q_' + (promptIndex + 0) + '_survey_prompt');
            
                if (nextQuestion && checkBoxResponse == null) {
                    nextQuestion.scrollIntoView({behavior: "smooth"});
                };
            });            
        });
    }

    renderInstructionContainer() {
        
        return(
            <div>
                {/* <div className="assessment-intro">
                    <div className="assessment-intro-title">
                        <img src="/images/assessment-intro.png" width={40}/>
                        &nbsp;&nbsp;<span>{this.state.sectionObj.sectionTitle}</span>
                    </div>
                    <div className="assessment-intro-text">
                        {htmlReactParser(this.state.sectionObj.overviewBlock)}
                    </div>
                </div> */}
                <div className="assessment-intro" style={{marginBottom:0}}>
                    <div className="question-container">
                        <div className="tips-section-title">{htmlReactParser(this.state.sectionObj.sectionIntro)}</div>
                        { this.state.sectionObj.introTipText ? 
                            <div className="tips-container" style={{marginTop:15}}>TIP: {htmlReactParser(this.state.sectionObj.introTipText)}</div>
                        : null}
                    </div>
                </div>
            </div>
        )
    }

    renderInputOptions(promptObj, ind, viewPort) {

        var inputOptions = [];
        var scaleUnits = this.state.sectionObj.scaleUnits || [];
        var scaleVisibility = this.state.sectionObj.useMobileLayoutForDesktop ? 'visible' : 'visible-xs';

        scaleUnits.map((scaleObj, scaleIndex) => {

            if (this.state.sectionObj.responseFormat == 'matrix-table-radio-options') {
                inputOptions.push(
                    <div className="radio-div" style={styles.radioBtnStyle} key={'prompt_ind' + ind + '_label_ind_' + scaleIndex}>
                        <input type="radio" value={scaleObj.value} checked={promptObj.selectedResponse == scaleObj.value ? true : false} onChange={this.updateUserResponse.bind(this, ind, null)} name={viewPort+'-radio-input-index-'+ind} className="input-radio-question "/>
                        <div className={scaleVisibility} style={styles.scaleUnitLabel}>{scaleObj.label}</div>
                    </div>
                );
            }
            else if (this.state.sectionObj.responseFormat == 'matrix-table-checkboxes') {
                
                var isSelected = false;
                
                if (promptObj.selectedResponse && promptObj.selectedResponse.indexOf(scaleObj.value) > -1) {
                    isSelected = true;
                };
                
                inputOptions.push(
                    <div className="radio-div" style={styles.radioBtnStyle} key={'prompt_ind' + ind + '_label_ind_' + scaleIndex} 
                        onClick={this.updateUserResponse.bind(this, ind, scaleObj.value)}>
                            {isSelected == true ? <img src={SelectedCheckbox} style={styles.checkBoxStyle} /> : <img src={BlankCheckbox} style={styles.checkBoxStyle} /> }
                            <div className={scaleVisibility} style={styles.scaleUnitLabel}>{scaleObj.label}</div>
                    </div>
                );
            }
        });

        return (
            <div className="radio-container" style={{...styles.radioBtnWrapper, ...{flexWrap:'wrap'}}}>
                {inputOptions}
            </div>
        );
    }

    renderRows() {

        var rows = [];

        this.state.data.map((promptObj, ind) => {
            
            /* 7pt scalings will be rendered in mobile layout even for desktop for better visual experience */

            if (this.state.sectionObj.useMobileLayoutForDesktop == true) {

                /* Radio option layout for both desktop and mobile view */
                rows.push(
                    <tr key={'Q_' + ind + '_row_prompt'} id={'Q_' + ind + '_survey_prompt'}>
                        <td colSpan='12' className="questions-section" style={styles.questionSection}>
                            <div style={styles.spacer}></div>
                            <div className="question-container" style={styles.questionContainer}>
                                <div style={SharedStyles.questionCount}>{ind+1} of {this.state.data.length}</div>
                                <div className="question-title">{promptObj.prompt}</div>
                                {this.renderInputOptions(promptObj, ind, 'mobile')}
                            </div>
                            <div className="bottom-div"></div>
                        </td>
                    </tr>
                )
            }
            else {

                /* Matrix table layout for desktop view */
                rows.push(
                    <tr key={ind} className='hidden-xs'>
                        <td style={{...styles.promptQtn, ...{minWidth: 35}}}>{ind + 1}. &nbsp;</td>
                        <td colSpan='5' style={styles.promptQtn}>{promptObj.prompt}</td>
                        <td colSpan='6'>
                            {this.renderInputOptions(promptObj, ind, 'desktop')}
                        </td>
                    </tr>
                );

                /* Radio option layout for mobile view */
                rows.push(
                    <tr key={'Q_' + ind + '_mobile_row_prompt'} id={'Q_' + ind + '_survey_prompt'} className='visible-xs'>
                        <td colSpan='12' className="questions-section" style={styles.questionSection}>
                            <div style={styles.spacer}></div>
                            <div className="question-container" style={styles.questionContainer}>
                                <div style={SharedStyles.questionCount}>{ind+1} of {this.state.data.length}</div>
                                <div className="question-title">{promptObj.prompt}</div>
                                {this.renderInputOptions(promptObj, ind, 'mobile')}
                            </div>
                            <div className="bottom-div"></div>
                        </td>
                    </tr>
                )
            };
        });

        return rows;
    }

    renderContainer() {
        
        if (this.state.sectionObj && this.state.sectionObj.scaleUnits && this.state.data.length > 0) {
            
            return (
                <table style={styles.tableContainer}>
                    {this.state.sectionObj.useMobileLayoutForDesktop == false ?
                        <thead>
                            <tr className='hidden-xs'>
                                <td colSpan='6'></td>
                                <td colSpan='6'>
                                    <div className="radio-container" style={{...styles.radioBtnWrapper, ...{marginTop:15}}}>
                                        {this.state.sectionObj.scaleUnits.map((scaleObj, scaleIndex) => {
                                            return(
                                                <div className="radio-div" style={styles.radioBtnStyle} key={'scale_header_ind_' + scaleIndex}>
                                                    <div style={styles.scaleHeaders}>{scaleObj.label}</div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </td>
                            </tr>
                        </thead>
                    : null }
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            )
        }
        return <div />;
    }

    handleSubmit() {

        if (this.state.updatedCount < this.state.data.length) {
            alert("It looks as though you missed a question! Please answer all questions to move forward");
        }
        else {

            /* Update section completion time */
            var sectionObj = this.state.sectionObj;
            sectionObj.sectionEndTime = moment().format();

            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                this.props.handleSectionChange('next');
            });
        }
    }

    render() {
        
        if (this.state.showActivityIndicator) {
			return (
				<div style={SharedStyles.loaderComponent} className="containerWrapper">
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
		}
		else {
			return (
                <div className="container containerWrapper mobile-container">
                    <div style={{padding:'10px 20px'}}>
                        <div className="emostateAssessment">
                            {this.renderInstructionContainer()}
                            {this.renderContainer()}
                            <div className="submit-button">
                                {this.state.data.length > 0 ?
                                    <button className="btn sub-button" onClick={this.handleSubmit.bind(this)} type="submit">Next</button>
                                : null}
                            </div>
                        </div>
                    </div>
                </div>
			);
		}
	}
}

export default MatrixTableSurvey;

const styles = {

    tableContainer: {
        //marginTop: 15,
        width: '100%'
    },
    scaleHeaders: {
        fontFamily: 'Pangram-Bold',
        textAlign: 'center',
        fontSize: 17,
        paddingBottom: 5
    },
    radioBtnWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    radioBtnStyle: {
        textAlign: 'center'
    },
    promptQtn: {
        fontSize: 16,
        fontFamily: 'Pangram-Regular'
    },
    scaleUnitLabel: {
        fontSize: 13,
        color: '#737373',
        paddingTop: 5
    },
    questionSection: {
        backgroundColor: '#fff'
    },
    spacer: {
        height: 30
    },
    questionContainer: {
        backgroundColor: '#f6f6f6',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    checkBoxStyle: {
        width: 25,
        height: 25
    }
};