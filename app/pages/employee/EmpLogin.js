import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import User from '../../modules/User';
import Globals from '../../modules/Globals';
import SharedStyles from '../../SharedStyles';
import queryString from 'query-string';
import CryptoJS from "crypto-js";

class EmpLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groupID: "",
            groupSecret: "",
            replaceButtonWithActivityIndicator: false
        };
    }

    componentWillMount() {
        
        if (User.isLoggedIn()) {
            
            var coreData = User.getUserCoreData();
            
            if (coreData.userCoreProperties && coreData.userCoreProperties.userRole == 'employee') {
                this.props.history.push('/survey');
            }
            else {
                this.props.history.goBack(); // Prevent multiple logins with different roles
            } 
        }
        else if (location.search) {     
        
            var stringURL = location.search.replace(/\+/g, '%2B');
            let queryParams = queryString.parse(stringURL);
            
            if (queryParams.gi && queryParams.gcs) {
                
                var bytes = CryptoJS.AES.decrypt(queryParams.gcs, Globals.CRYPTO_ENCRYPTION_SECRET);
                var decryptedSecret = bytes.toString(CryptoJS.enc.Utf8);

                this.setState({
                    groupID: queryParams.gi,
                    groupSecret: decryptedSecret
                },
                () => {
                    this.login(); // Initiate login process automatically for the users accessing the survey using encryptedURL with login details.
                });
            };
        }
    }

    login() {

        if (this.state.groupID == "" || this.state.groupSecret == "")
            return document.getElementById("errorMsg").innerHTML = "Please enter all the details";

        this.setState({
            replaceButtonWithActivityIndicator: true
        });

        User.login(this.props.coreData, Globals.USER_ROLE.TRN_ORG_EMP,
            { groupID: this.state.groupID, groupSecret: this.state.groupSecret }, (err, result) => {

            if (err) {
                this.setState({
                    replaceButtonWithActivityIndicator: false
                });
                return document.getElementById("errorMsg").innerHTML = err;
            }

            if (result == 'success') {
                this.props.history.push('/survey' + location.search);
            } 
            else {
                this.setState({
                    replaceButtonWithActivityIndicator: false
                });
                return document.getElementById("errorMsg").innerHTML = 'Login failed';
            }
        });
    }

    keypress(e) {
        var THIS = this;
        if (e.key === 'Enter') {
            THIS.login();
        }
    }

    handleGroupIdChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({groupID: e.target.value});
    }

    handleSecretChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({groupSecret: e.target.value});
    }
    
    renderButton() {

        if (this.state.replaceButtonWithActivityIndicator) {
            return (
                <div style={{...SharedStyles.loaderComponent, ...{height: '65px'}}}>
					<img style={styles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
    			</div>
            )
        }
        return (
            <div className="login-submit"><button type="submit" className="submit" id="submit" onClick={this.login.bind(this)}>Login</button></div>
        )
    }

    render() {
		
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackgroundWithImage}>
                    <div className="login-section" style={{paddingTop:'40px'}}>
                        <div className="login-section-container" style={SharedStyles.loginContainer}>
                            <p style={SharedStyles.loginSectionTitle}>Employee Login</p>
                            <p className="login-section-heading"> Login with your group ID and its secret code to take a survey!</p>
                            <div className="login-input"><input type="text" name="text" value={this.state.groupID} onKeyPress={this.keypress.bind(this)} onChange={this.handleGroupIdChange.bind(this)} placeholder="Enter your group ID" /></div>
                            <div className="login-input"><input type="password" name="secret" placeholder="Enter your group secret code" value={this.state.groupSecret} onKeyPress={this.keypress.bind(this)} onChange={this.handleSecretChange.bind(this)} /></div>
                            <div id="errorMsg" style={{color:'red'}}></div>
                            {this.renderButton()}
                            <p>By continuing, you agree to our <a style={styles.termsCopy} target="_blank" href='https://www.triumphhq.com/terms-of-use/'>Terms</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }	
}

export default EmpLogin;

const styles = {

    termsCopy: {
        color: '#737373',
        textDecorationLine: 'underline'
    },
    loaderImage: {
        width: '35px',
        height: '35px',
        resizeMode: 'contain'
    }
}