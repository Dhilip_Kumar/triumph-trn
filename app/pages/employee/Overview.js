import React, { Component } from 'react';
import htmlReactParser from 'html-react-parser';

class Overview extends Component {

    constructor(props) {
        super(props);
    }
    
    navToNext() {
        this.props.handleSectionChange('next');
    }

    render() {
        
        var surveySections = this.props.surveySections || [];

        return (
            <div className="container containerWrapper container-padding">
                <div style={styles.overviewConatiner}>
                    <div style={styles.sectionHeading}>Overview</div>
                    <div style={styles.displayCopy}>This survey consists of a handful of sections and should take you 12-15 minutes. There are no right or wrong answers.</div>
                    <div style={styles.displayCopy}><i>Your responses are anonymous and will not be identified with you in any way. You must be at least 18 years old to participate.</i></div>
                    {/* <ol>
                        {surveySections.map((section, key) => {
                            return(
                                <li key={key}>
                                    <div><strong>{section.sectionTitle}: </strong>{htmlReactParser(section.overviewBlock)}</div>
                                </li>
                            )
                        })}
                    </ol> */}
                </div>
                {surveySections.length > 0 ?
                    <div style={styles.btnContainer}>
                        <button type="submit" style={styles.button} id="submit" onClick={this.navToNext.bind(this)}>Let's Get Started</button>
                    </div> 
                : null}
                <div style={styles.termsConatiner}>
                    By continuing, you agree to our <a href='https://www.triumphhq.com/terms-of-use/' target='blank' style={styles.anchorLink}>Terms</a>.
                </div>
            </div>
        )
    }	
}

export default Overview;

const styles = {

    overviewConatiner: {
        padding: '15px 20px',
        borderRadius: '12px',
        backgroundColor: '#AECF7A',
        color: '#FFF',
        fontSize: '17px'
    },
    sectionHeading: {
        fontFamily: 'Pangram-Bold',
        textAlign: 'center',
        paddingBottom: '15px',
        fontSize: '20px'
    },
    btnContainer: {
        marginTop: '40px',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#FFF',
        color: '#AECF7A',
        border: '1px solid #AECF7A',
        padding: '10px 20px',
        borderRadius: '12px'
    },
    displayCopy: {
        paddingBottom: 15
    },
    termsConatiner: {
        textAlign: 'center',
        marginTop: 25
    },
    anchorLink: {
        textDecorationLine: 'underline',
        color: '#737373'
    }
}