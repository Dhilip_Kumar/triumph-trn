import React, { Component } from 'react';
import Globals from '../../modules/Globals';
import SurveyModule from '../../modules/Survey';
import SharedStyles from '../../SharedStyles';
import '../../../assets/styles/assessmentStyles.css';
import htmlReactParser from 'html-react-parser';
import Select from 'react-select';
import moment from 'moment';
import _ from '@sailshq/lodash';

let SelectedCheckbox = '/images/check-box-green-tick.png';
let BlankCheckbox = '/images/blank-check-box.png';

class OpenEndedQuestions extends Component {

    constructor(props) {
		super(props);
		this.state = {
            data: [],
            sectionObj: null,
            updatedCount: 0,
            showActivityIndicator: true
        };
    }

    componentDidMount() {
        this.loadSectionData(this.props.sectionObj);
    }

    componentWillReceiveProps(props) {
        this.loadSectionData(props.sectionObj);
    }

    loadSectionData(sectionObj) {

        if (!sectionObj) return;

        SurveyModule.getSectionDataFromStorage('Section_' + sectionObj.sectionID).then((surveyData) => {

            if (surveyData && surveyData.statementsAndPrompts) {

                var updatedCount = 0;

                for (var i=0 ; i < surveyData.statementsAndPrompts.length; i++) {
                    if (surveyData.statementsAndPrompts[i].isAnswered) {
                        updatedCount += 1;
                    };
                };
                
                this.setState({
                    sectionObj: surveyData,
                    data: surveyData.statementsAndPrompts,
                    updatedCount: updatedCount,
                    showActivityIndicator: false,
                });
            }
            else {

                /* Record individual section start time */
                sectionObj.sectionStartTime = moment().format();

                this.setState({
                    sectionObj: sectionObj,
                    data: sectionObj.statementsAndPrompts,
                    showActivityIndicator: false
                });
            }

            /* Since we are using SPA approach where the components replace the previous one, we need to scroll to top of the page programatically */ 
            window.scrollTo(0, 0); 
        });
    }

    updateUserResponse(promptIndex, checkBoxResponse=null, e) {
        
        var data = this.state.data;
        var updatedCount = this.state.updatedCount;

        if (data[promptIndex].responseFormat == 'single-line-radio-options' || data[promptIndex].responseFormat == 'multi-line-radio-options') {

            var value = e.target.value;

            if (value == '' || value == null || value == undefined) return;

            var finalResponse = isNaN(value) == false ? parseInt(value) : value;

            if (data[promptIndex].requiresScoreInversion && isNaN(finalResponse) == false) {
                
                var scaleUnits = data[promptIndex].scaleUnits ? _.map(data[promptIndex].scaleUnits, 'value') : [];
                var minScalePoint = isNaN(scaleUnits[0]) == false ? parseInt(scaleUnits[0]) : -1;

                if (minScalePoint == 0) {
                    finalResponse = (scaleUnits.length - 1) - finalResponse;
                }
                else if (minScalePoint == 1) {
                    finalResponse = (scaleUnits.length + 1) - finalResponse; 
                }
            };

            var equivalentTextResponse = _.find(data[promptIndex].scaleUnits || [], (scaleUnitObj) => {
                return scaleUnitObj.value == value || scaleUnitObj.value == finalResponse; // Check with parsed value if the values are of numeric type. And also check for direct string match. 
            });
            
            data[promptIndex].selectedResponse = value;
            data[promptIndex].finalResponse = finalResponse;
            data[promptIndex].textResponse = equivalentTextResponse && equivalentTextResponse.label != 'Other' ? equivalentTextResponse.label : '';
        }
        else if (data[promptIndex].responseFormat == 'single-line-checkboxes' || data[promptIndex].responseFormat == 'multi-line-checkboxes') {
        
            if (checkBoxResponse == '' || checkBoxResponse == null || checkBoxResponse == undefined) return;

            var userResponses = data[promptIndex].selectedResponse || [];
            var valueIndex = userResponses.indexOf(checkBoxResponse);

            if (valueIndex == -1) {
                userResponses.push(checkBoxResponse); // User select the checkbox item for the first time.
            }
            else {
                userResponses.splice(valueIndex, 1); // User select the checkbox item for the second time. Thereby to achieve toggle feature, we are removing that response.
            };

            data[promptIndex].selectedResponse = userResponses;
        }
        else if (data[promptIndex].responseFormat == 'single-line-text-input' || data[promptIndex].responseFormat == 'multi-line-text-input') {

            var value = e.target.value;
            data[promptIndex].textResponse = value;
        };

        if (!data[promptIndex].isAnswered) {
            updatedCount = updatedCount + 1;
            data[promptIndex].isAnswered = true;
        };

        this.setState({
            data: data,
            updatedCount: updatedCount
        },
        () => {

            var sectionObj = this.props.sectionObj;
            sectionObj.statementsAndPrompts = data;

            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                /* Automatic scrolling is disabled for OpenEnded Section */
            });
        });
    }

    updateOtherOptionTextResponse(promptIndex, e) {

        var data = this.state.data;
        var value = e.target.value;
        data[promptIndex].textResponse = value;

        this.setState({
            data: data
        },
        () => {

            var sectionObj = this.props.sectionObj;
            sectionObj.statementsAndPrompts = data;

            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                /* Automatic scrolling is disabled for OpenEnded Section */
            });
        });
    }

    handleTextWithFocusAreaResponse(promptIndex, key, category, targetItem) {
        
        var value = category == 'focusAreaResponse' ? targetItem.value : targetItem.target.value;
        var data = this.state.data;
        var updatedCount = this.state.updatedCount;

        if (!data[promptIndex].responseSet) {
            data[promptIndex].responseSet = []; // Initialize responseSet for the first time.
        };

        if (data[promptIndex].responseSet.length > key && data[promptIndex].responseSet[key]) {
            data[promptIndex].responseSet[key][category] = value;
        }
        else { 
            data[promptIndex].responseSet[key] = {};
            data[promptIndex].responseSet[key][category] = value;
        }; 

        if (!data[promptIndex].isAnswered) {
            updatedCount = updatedCount + 1;
            data[promptIndex].isAnswered = true;
        };

        this.setState({
            data: data,
            updatedCount: updatedCount
        },
        () => {
            
            var sectionObj = this.props.sectionObj;
            sectionObj.statementsAndPrompts = data;

            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                /* Automatic scrolling is disabled for OpenEnded Section */
            });
        });
    }

    renderInstructionContainer() {
        
        return(
            <div>
                {/* <div className="assessment-intro">
                    <div className="assessment-intro-title">
                        <img src="/images/assessment-intro.png" width={40}/>
                        &nbsp;&nbsp;<span>{this.state.sectionObj.sectionTitle}</span>
                    </div>
                    <div className="assessment-intro-text">
                        {htmlReactParser(this.state.sectionObj.overviewBlock)}
                    </div>
                </div> */}
                <div className="assessment-intro">
                    <div className="question-container">
                        <div className="tips-section-title">{htmlReactParser(this.state.sectionObj.sectionIntro)}</div>
                        { this.state.sectionObj.introTipText ? 
                            <div className="tips-container" style={{marginTop:15}}>TIP: {htmlReactParser(this.state.sectionObj.introTipText)}</div>
                        : null}
                    </div>
                </div>
            </div>
        )
    }

    renderInputOptions(promptObj, ind) {

        if (promptObj.responseFormat == 'single-line-text-input') {

            return(
                <div style={{display: 'flex'}}>
                    <input
                        type={promptObj.inputType ? promptObj.inputType : 'text'}
                        style={styles.textInputResponse} 
                        placeholder="Enter your response here"
                        value={promptObj.textResponse}
                        onChange={this.updateUserResponse.bind(this, ind, null)}>    
                    </input>
                </div>
            );
        }
        else if (promptObj.responseFormat == 'multi-line-text-input') {

            return(
                <div style={{display: 'flex'}}>
                    <textarea
                        data-gramm_editor="false"
                        rows={promptObj.numberOfAllowedRows}
                        style={styles.textInputResponse} 
                        placeholder="Enter your response here"
                        value={promptObj.textResponse}
                        onChange={this.updateUserResponse.bind(this, ind, null)}>    
                    </textarea>
                </div>
            );
        }
        else if (promptObj.responseFormat == 'single-line-radio-options') {
            
            var radioOptions = [];
            var scaleUnits = promptObj.scaleUnits || [];
            
            scaleUnits.map((scaleObj, scaleIndex) => {

                radioOptions.push(
                    <div className="radio-div" key={'prompt_ind' + ind + '_label_ind_' + scaleIndex}>
                        <input type="radio" value={scaleObj.value} checked={promptObj.selectedResponse == scaleObj.value ? true : false} onChange={this.updateUserResponse.bind(this, ind, null)} name={'radio-input-index-'+ind} className="input-radio-question "/>
                        <div style={styles.scaleUnitLabel}>{scaleObj.label}</div>
                    </div>
                );
            });

            return (
                <div className="radio-container" style={styles.radioBtnWrapper}>
                    {radioOptions}
                </div>
            );      
        }
        else if (promptObj.responseFormat == 'multi-line-radio-options') {
            
            var radioOptions = [];
            var scaleUnits = promptObj.scaleUnits || [];
            
            scaleUnits.map((scaleObj, scaleIndex) => {

                radioOptions.push(
                    <div style={styles.radioBtnStyle} key={'prompt_ind' + ind + '_label_ind_' + scaleIndex}>
                        <input type="radio" value={scaleObj.value} checked={promptObj.selectedResponse == scaleObj.value ? true : false} onChange={this.updateUserResponse.bind(this, ind, null)} name={'radio-input-index-'+ind} className="input-radio-question "/>
                        <div style={{...styles.scaleUnitLabel, ...{paddingLeft: 15}}}>{scaleObj.label}</div>
                    </div>
                );

                /* If the user selected 'Other' as radio-option response, show text-input-box to specify their response. */
                
                if (scaleObj.label == 'Other' && promptObj.selectedResponse == scaleObj.value) {

                    radioOptions.push(
                        <div style={{display:'flex'}} key={'prompt_ind' + ind + '_label_ind_' + scaleIndex + '_text_input'}>
                            <input
                                type={'text'}
                                style={{...styles.textInputResponse, ...{margin: '5px 15px 10px 48px'}}} 
                                placeholder="Please specify"
                                value={promptObj.textResponse}
                                onChange={this.updateOtherOptionTextResponse.bind(this, ind)}>    
                            </input>
                        </div>
                    );
                };
            });

            return (
                <div className="radio-container">
                    {radioOptions}
                </div>
            );      
        }
        else if (promptObj.responseFormat == 'single-line-checkboxes') {
            
            var checkboxes = [];
            var scaleUnits = promptObj.scaleUnits || [];

            scaleUnits.map((scaleObj, scaleIndex) => {

                var isSelected = false;
                
                if (promptObj.selectedResponse && promptObj.selectedResponse.indexOf(scaleObj.value) > -1) {
                    isSelected = true;
                };

                checkboxes.push(
                    <div className="radio-div" key={'prompt_ind' + ind + '_label_ind_' + scaleIndex} 
                        onClick={this.updateUserResponse.bind(this, ind, scaleObj.value)}>
                            <div>{isSelected == true ? <img src={SelectedCheckbox} style={styles.checkBoxStyle} /> : <img src={BlankCheckbox} style={styles.checkBoxStyle} />}</div>
                            <div style={{...styles.scaleUnitLabel, ...{paddingTop:10}}}>{scaleObj.label}</div>
                    </div>
                );
            });

            return (
                <div className="radio-container" style={styles.radioBtnWrapper}>
                    {checkboxes}
                </div>
            );      
        }
        else if (promptObj.responseFormat == 'multi-line-checkboxes') {
            
            var checkboxes = [];
            var scaleUnits = promptObj.scaleUnits || [];

            scaleUnits.map((scaleObj, scaleIndex) => {

                var isSelected = false;
                
                if (promptObj.selectedResponse && promptObj.selectedResponse.indexOf(scaleObj.value) > -1) {
                    isSelected = true;
                };

                checkboxes.push(
                    <div style={styles.radioBtnStyle} key={'prompt_ind' + ind + '_label_ind_' + scaleIndex} 
                        onClick={this.updateUserResponse.bind(this, ind, scaleObj.value)}>
                            <div>{isSelected == true ? <img src={SelectedCheckbox} style={styles.checkBoxStyle} /> : <img src={BlankCheckbox} style={styles.checkBoxStyle} />}</div>
                            <div style={{...styles.scaleUnitLabel, ...{paddingLeft:15}}}>{scaleObj.label}</div>
                    </div>
                );
            });

            return (
                <div className="radio-container">
                    {checkboxes}
                </div>
            );      
        }
        else if (promptObj.responseFormat == 'text-with-focus-area-responses') {
            
            var multipleResponseSet = [];

            var focusAreas = promptObj.focusAreas.map(val => ({ 
                label: val, 
                value: val 
            }));

            for (let i = 0; i < promptObj.numberOfResponseSetsToBeRendered; i++) {
                
                let txtResponse = '', focusAreaResponse = null;

                if (promptObj.responseSet && promptObj.responseSet[i] && promptObj.responseSet[i].textResponse) {
                    txtResponse = promptObj.responseSet[i].textResponse;
                };

                if (promptObj.responseSet && promptObj.responseSet[i] && promptObj.responseSet[i].focusAreaResponse) {
                    focusAreaResponse = { 
                        label : promptObj.responseSet[i].focusAreaResponse, 
                        value : promptObj.responseSet[i].focusAreaResponse 
                    };
                };

                multipleResponseSet.push(
                    
                    <div style={styles.multipleResponseSetRow} key={'multi_set_prompt_index_' + ind + '_set_index' + i}>
                        <div style={styles.focusAreaSelectionContainer}>
                            <Select 
                                onChange={this.handleTextWithFocusAreaResponse.bind(this, ind, i, 'focusAreaResponse')}
                                options={focusAreas} 
                                defaultValue={focusAreaResponse}
                                isSearchable={false}
                                placeholder={'Select Focus Area'} 
                                styles={styles.selectBoxStyles}/>
                        </div>
                        <input
                            type={'text'}
                            style={styles.multiRespSetTextResponse} 
                            placeholder="Enter your response here"
                            value={txtResponse}
                            onChange={this.handleTextWithFocusAreaResponse.bind(this, ind, i, 'textResponse')}> 
                        </input>
                    </div>
                )
            };
            return (
                <div style={{padding: '20px 0 10px'}}>
                    {multipleResponseSet}
                </div>
            );
        }
        else {
            return null;
        }
    }

    renderRows() {

        var rows = [];

        this.state.data.map((promptObj, ind) => {

            rows.push(
                <div key={'Q_' + ind + '_row_prompt'} id={'Q_' + ind + '_survey_prompt'}>
                    <div className="questions-section" style={styles.questionSection}>
                        <div className="question-container" style={styles.questionContainer}>
                            <div style={{...SharedStyles.questionCount}}>{ind+1} of {this.state.data.length}</div>
                            {promptObj.prompt ?
                                <div className="question-title">{promptObj.prompt}</div>
                            : null}
                            {promptObj.tip ? <div style={styles.tips}>{promptObj.tip} </div> : null }
                            {this.renderInputOptions(promptObj, ind)}
                        </div>
                        <div className="bottom-div"></div>
                    </div>
                </div>
            )
        });

        return rows;
    }

    handleSubmit() {

        if (this.state.updatedCount < this.state.data.length) {
            alert("It looks as though you missed a question! Please answer all questions to move forward");
        }
        else {

            /* Update section completion time */
            var sectionObj = this.state.sectionObj;
            sectionObj.sectionEndTime = moment().format();
            
            SurveyModule.setSectionDataInTheStorage('Section_' + sectionObj.sectionID, sectionObj).then(() => {
                this.props.handleSectionChange('next');
            });
        }
    }

    render() {
        
        if (this.state.showActivityIndicator) {
			return (
				<div style={SharedStyles.loaderComponent} className="containerWrapper">
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
		}
		else {
			return (
                <div className="container containerWrapper mobile-container">
                    <div style={{padding:'10px 20px'}}>
                        <div className="emostateAssessment">
                            {this.renderInstructionContainer()}
                            {this.renderRows()}
                            <div className="submit-button">
                                {this.state.data.length > 0 ?
                                    <button className="btn sub-button" style={{marginBottom:100}} onClick={this.handleSubmit.bind(this)} type="submit">Next</button>
                                : null}
                            </div>
                        </div>
                    </div>
                </div>
			);
		}
	}
}

export default OpenEndedQuestions;

const styles = {

    radioBtnWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    radioBtnStyle: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: '0 10px',
        textAlign: 'left',
        marginBottom: 10
    },
    promptQtn: {
        fontSize: 16,
        fontFamily: 'Pangram-Regular'
    },
    scaleUnitLabel: {
        fontSize: 16,
        color: '#737373'
    },
    questionSection: {
        backgroundColor: '#fff'
    },
    questionContainer: {
        backgroundColor: '#f6f6f6',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    checkBoxStyle: {
        width: 25,
        height: 25
    },
    questionCount: {
        fontFamily: 'Pangram-Regular',
        textDecorationLine: 'underline'
    },
    textInputResponse: {
        margin: 15,
        display: 'flex',
        flex: 1,
        borderRadius: '10px',
        border: '1px solid #1EB0D8',
        padding: '10px',
        overflow: 'hidden',
        textAlign: 'left',
        backgroundColor: '#fff',
        color: '#737373',
        resize: 'none'
    },
    tips: {
        color : "#737373",
        padding: 5
    },
    multipleResponseSetContainer : {
        padding : '15px 20px',
        margin : '0 auto'
    },
    multipleResponseSetRow : {
        display: "flex",
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingBottom: 15,
        paddingLeft: 15,
        paddingRight: 15
    },
    focusAreaSelectionContainer: {
        display: 'flex', 
        flex: 0.2
    },
    selectBoxStyles: {
        container: (base) => ({
            ...base,
            width: '100%'
        }),
        dropdownIndicator: (base) => ({
            ...base,
            color: '#FFF',
            padding: '5px'
        }),
        indicatorSeparator: () => ({}),
        option: (provided, state) => ({
            ...provided,
            backgroundColor: state.isSelected ? '#1EB0D8' : '#FFF',
            color: state.isSelected ? '#FFF' : '#737373',
            outline: 'none',
            '&:hover': {
                backgroundColor: '#1EB0D8 !important',
                color: '#FFF'
            },
        }),
        control: (base) => ({
            ...base,
            border: 'none !important',
            borderRadius: '10px',
            outline: 'none',
            boxShadow: '0 !important',
            '&:hover': {
                border: 'none !important'
            },
            color: '#FFF',
            background:"#1EB0D8",
            height: '50px',
            minWidth: '100px',
            flex: 1,
            borderBottomRightRadius : "0 !important",
            borderTopRightRadius : "0 !important"
        }),
        singleValue: (base) => ({
            ...base,
            color: '#FFF',
            padding: '5px'
        }),
        placeholder: (base) => ({
            ...base,
            color: '#FFF',
            padding: '5px'
        })
    },
    multiRespSetTextResponse: {
        margin: 0,
        flex: 0.8,
        borderRadius: '10px',
        borderBottomLeftRadius: '0px',
        borderTopLeftRadius: '0px',
        border: '1px solid #1EB0D8',
        padding: '10px',
        overflow: 'hidden',
        textAlign: 'left',
        height: '50px',
        backgroundColor: '#fff',
        color: '#737373'
    }
};