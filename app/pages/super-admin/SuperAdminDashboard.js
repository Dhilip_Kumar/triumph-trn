import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import SharedStyles from '../../SharedStyles';
import '../../../assets/styles/sharedStyles.css';
import '../../../assets/styles/home.css';
import OrgManagement from './OrgManagement';
import DataExport from './DataExport';

class SuperAdminDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displaySection: 'Org Management'
        }
    }
    
    renderMenuItem(menuItem) {

        return(
            <div className="menu-items" onClick={() => this.setState({displaySection: menuItem})}
                style={this.state.displaySection == menuItem ? styles.highlightedMenuItem : styles.unSelectedMenuItem}> 
                    {menuItem}
            </div>
        )
    }

    renderSideBar() {
        
        return (
            <div className="col-md-3 sidebar">
                {this.renderMenuItem('Org Management')} <hr />
                {this.renderMenuItem('Data Export')}
            </div>
        )
    }

    renderInnerContainer() {

        switch (this.state.displaySection) {
            case 'Org Management': return <OrgManagement {...this.props} />;
            case 'Data Export': return <DataExport {...this.props} />;
            default: return <div />;
        }
    }

    render() {
		
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackgroundWithImage}>   
                    <div className="container containerWrapper container-padding">
                        {this.renderSideBar()}
                        <div className="col-md-9 mainSection">
                            {this.renderInnerContainer()}
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }	
}

export default SuperAdminDashboard;

const styles = {
    
    highlightedMenuItem: {
        fontFamily:'Pangram-Bold',
        color: '#F79569',
        fontSize: 17
    },
    unSelectedMenuItem: {
        fontFamily:'Pangram-Regular',
        color: '#7D7D7D',
        fontSize: 16
    }
};