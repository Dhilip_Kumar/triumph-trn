import React, { Component } from 'react';
// import Analytics from '../../Modules/Analytics';
import API_Services from '../../modules/API_Services';
import { Modal } from 'react-bootstrap';
import SharedStyles from '../../SharedStyles';
import Util from '../../modules/Util';
import OrgAdminManagement from './OrgAdminManagement';

class OrgManagement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: true,
            isRequestProcessing: false,
            organizations: [],
            showUpdateModal: false,
            modalSection: '',
            serviceResponseMessage: '',
            organizationObjToUpdate: {}
        };
        this.fetchOrganizations();
    }

    fetchOrganizations() {

        API_Services.httpGET(this.props.coreData, 'fetchTRNOrganizations', (err, foundOrganizations) => {

            if (err) {
                alert('Network Error. Please try after sometime.');
                return;
            };

            this.setState({
                organizations: foundOrganizations,
                showActivityIndicator: false
            });
        });
    }

    saveOrganization() {

        var organizationObjToUpdate = this.state.organizationObjToUpdate;

        if (Object.keys(organizationObjToUpdate).length == 0) return;

        if (Util.validateTextInput(organizationObjToUpdate.organizationName) == false) {
            return alert('Please enter a valid organization name.');
        }
        else if (!organizationObjToUpdate.status) {
            return alert('Please select the status of the organization.');
        };

        if (!this.state.isRequestProcessing) {
            this.setState({isRequestProcessing: true});
        };

        var params = {
            organizationObjToUpdate: organizationObjToUpdate
        };

        API_Services.httpPOST(this.props.coreData, 'updateTRNOrganization', params, (err, updatedRecord) => {

            this.setState({
                serviceResponseMessage: updatedRecord ? 'Record updated successfully!' : err,
                organizationObjToUpdate: updatedRecord ? updatedRecord : organizationObjToUpdate,
                isRequestProcessing: false
            },
            () => {
                this.fetchOrganizations();
            });
        });
    }

    updateOrganizationDetails(item={}, section) {

        this.setState({
            organizationObjToUpdate: item,
            showUpdateModal: true,
            serviceResponseMessage: '', // Reset the message everytime modal opened newly
            modalSection: section
        });
    }

    handleOrganizationDetailUpdates(fieldName, value) {
        
        var organizationObjToUpdate = Util.cloneArray(this.state.organizationObjToUpdate);
        organizationObjToUpdate[fieldName] = value;

        this.setState({
            organizationObjToUpdate: organizationObjToUpdate,
            serviceResponseMessage: ''  // Reset message upon editing again
        });
    }

    renderOrgDetailFooterOptions() {

        if (this.state.isRequestProcessing) {
            return(
                <div className="loaderContainer" style={{margin:10}}>
                    <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                </div>
            )
        }
        else if (this.state.serviceResponseMessage) {
            return(
                <div style={{...styles.label, ...{color:'#f79569'}}}>{this.state.serviceResponseMessage}</div>
            )
        }
        else {
            return(
                <button style={SharedStyles.primaryButton} onClick={() => this.saveOrganization()}>Save</button>
            )
        }
    }

    renderOrgDetails() {

        return (
            
            <div>

                <p style={{...styles.label, ...{marginTop:0}}}>Name</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={this.state.organizationObjToUpdate.organizationName}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrganizationDetailUpdates('organizationName', event.target.value)} 
                    placeholder="Organization Name" />

                <p style={{...styles.label}}>Description</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={this.state.organizationObjToUpdate.description}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrganizationDetailUpdates('description', event.target.value)} 
                    placeholder="Description" />

                <p style={styles.label}>Status</p>

                <div style={styles.rowContainer}>
                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'orgActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={this.state.organizationObjToUpdate && this.state.organizationObjToUpdate.status == 'active' ? true: false}
                        onChange={() => this.handleOrganizationDetailUpdates('status', 'active')}
                    />
                    <span>Active</span>

                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'orgActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={this.state.organizationObjToUpdate && this.state.organizationObjToUpdate.status == 'disabled' ? true: false}
                        onChange={() => this.handleOrganizationDetailUpdates('status', 'disabled')}
                    />
                    <span>Disabled</span>
                </div>
                
                <div className="text-center">
                    {this.renderOrgDetailFooterOptions()}
                </div>
            </div>
        )
    }

    renderModalBody() {

        switch (this.state.modalSection) {
            case 'Organization Details': return this.renderOrgDetails();
            case 'Organization Admin': return <OrgAdminManagement orgObj={this.state.organizationObjToUpdate} {...this.props} />;
            default: return <div />;
        };
    }

    renderModalPopup() {

        return(
            <div className="static-modal">
                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.state.showUpdateModal}
                    onHide={() => this.setState({showUpdateModal: false})}>
                        <Modal.Header closeButton>
                            <Modal.Title>{this.state.modalSection}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {this.renderModalBody()}
                        </Modal.Body>
                </Modal>
            </div>
        )
    }

    renderOrgList() {

        var orgList = this.state.organizations || [];

        if (orgList && orgList.length > 0) {

            return (
                <div style={styles.orgListWrapper}>
                    <div className="team-body">
                        <table className="table-curved" style={styles.tableStyle}>
                            <thead>
                                <tr>
                                    <th className="col-md-6" style={styles.tableHeading}>Name</th>
                                    <th className="col-md-2" style={styles.tableHeading}>Status</th>
                                    <th className="col-md-4" colSpan={2} style={styles.tableHeading}>Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                {   
                                    orgList.map((item, i) => {
                                        return(
                                            <tr key={i}>
                                                <td className="col-md-6" style={styles.tableData}>{item.organizationName}</td>
                                                <td className="col-md-2" style={styles.tableData}>{item.status}</td>
                                                <td className="col-md-2" style={styles.editLink} onClick={() => this.updateOrganizationDetails(item, 'Organization Details')}>Edit</td>
                                                <td className="col-md-2" style={styles.editLink} onClick={() => this.updateOrganizationDetails(item, 'Organization Admin')}>Admin</td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        }
        else {
            return(
                <div style={styles.displayCopy}>
                    No Organizations have been added.
                </div>
            )
        }
    }

    render() {

        if (this.state.showActivityIndicator) {
            return (
				<div style={SharedStyles.loaderComponent}>
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
        }

        return (
            <div style={styles.mainContainer}>
                <h3 className="mainSectionTitle">TRN Organizations</h3>
                {this.renderOrgList()}
                <div className='text-center'>
                    <button style={SharedStyles.primaryButton} onClick={() => this.updateOrganizationDetails({}, 'Organization Details')}>Add Organization</button>
                </div>
                {this.renderModalPopup()}
            </div>
        );
    }
}

const styles = {

    mainContainer: {
        maxWidth: "1100px",
        width: "100%",
    },
    displayCopy: {
        textAlign: 'center', 
        padding: '20px'
    },
    orgListWrapper: {
        marginTop: '20px',
        marginBottom: '10px'
    },
    tableStyle: {
        width: '100%',
        marginTop: '30px',
        marginBottom: '20px'
    },
    tableHeading: {
        textAlign: 'center',
        padding: '5px',
        fontFamily: 'Pangram-Bold',
        backgroundColor: '#dfdfdf',
    },
    editLink: {
        textAlign: 'center',
        padding: '5px',
        fontFamily: 'Pangram-Regular',
        textDecorationLine: 'underline',
        color: '#1EB0D8',
        cursor: 'pointer'
    },
    tableData: {
        padding: '10px',
        textAlign: 'center'
    },
    textArea: {
        width: '100%',
        border: '1px solid #92d6e1',
        borderRadius: '4px',
        padding: '5px 10px',
        minHeight: '42px',
        outline: 'none',
        resize: 'none',
        backgroundColor: '#FFF',
        textAlign: 'left'
    },
    label: {
        marginTop:'15px',
        marginBottom: '15px',
        fontFamily:'Pangram-Regular'
    },
    customRadioBtn: {
        WebkitAppearance: 'none',
        MozAppearance:'none',
        width: '16px',
        height: '16px',
        borderRadius:'50%',
        outline:'none',
        border:'1px solid #1EB0D8',
        marginTop:'0px',
        marginRight:'8px',
        marginLeft:'8px'
    },
    rowContainer: {
        display:'flex',
        flexDirection:'row',
        alignItems:'center'
    },
}

export default OrgManagement;