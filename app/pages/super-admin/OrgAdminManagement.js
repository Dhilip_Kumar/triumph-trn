import React, { Component } from 'react';
// import Analytics from '../../Modules/Analytics';
import SharedStyles from '../../SharedStyles';
import API_Services from '../../modules/API_Services';
import Util from '../../modules/Util';

class OrgAdminManagement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: true,
            isRequestProcessing: false,
            serviceResponseMessage: '',
            adminObjToUpdate: {}
        };
        this.fetchOrgAdmins();
    }

    fetchOrgAdmins() {
        
        var params = {
            trnOrganizationID: this.props.orgObj.trnOrganizationID
        };

        API_Services.httpPOST(this.props.coreData, 'fetchTRNOrgAdmins', params, (err, foundAdmins) => {

            if (err) {
                alert('Network Error. Please try after sometime.');
                return;
            };

            this.setState({
                adminObjToUpdate: foundAdmins && foundAdmins[0] ? foundAdmins[0] : {}, // Currently we are supporting only one admin per organization.
                showActivityIndicator: false
            });
        });
    }

    saveAdminDetails() {

        var adminObjToUpdate = Util.cloneArray(this.state.adminObjToUpdate);

        if (Object.keys(adminObjToUpdate).length == 0) return;

        if (Util.validateTextInput(adminObjToUpdate.name) == false) {
            return alert('Please enter a valid admin name.');
        }
        else if (Util.validateEmail(adminObjToUpdate.email) == false) {
            return alert('Please enter a valid email.');
        }
        else if (!adminObjToUpdate.status) {
            return alert('Please select the status of the admin.');
        }; 

        if (!this.state.isRequestProcessing) {
            this.setState({isRequestProcessing: true});
        };

        var params = {
            trnOrganizationID: this.props.orgObj.trnOrganizationID,
            adminObjToUpdate: adminObjToUpdate
        };

        API_Services.httpPOST(this.props.coreData, 'updateTRNOrgAdmin', params, (err, updatedRecord) => {

            this.setState({
                serviceResponseMessage: updatedRecord ? 'Record updated successfully!' : err,
                adminObjToUpdate: updatedRecord ? updatedRecord : adminObjToUpdate,
                isRequestProcessing: false
            });
        });
    }

    handleOrganizationAdminDetailUpdates(fieldName, value) {
        
        var adminObjToUpdate = Util.cloneArray(this.state.adminObjToUpdate);
     
        adminObjToUpdate[fieldName] = value;

        this.setState({
            adminObjToUpdate: adminObjToUpdate,
            serviceResponseMessage: '' // Reset message upon editing again
        });
    }

    renderOrgAdminFooterOptions() {

        if (this.state.isRequestProcessing) {
            return(
                <div className="loaderContainer" style={{margin:10}}>
                    <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                </div>
            )
        }
        else if (this.state.serviceResponseMessage) {
            return(
                <div style={{...styles.label, ...{color:'#f79569'}}}>{this.state.serviceResponseMessage}</div>
            )
        }
        else {
            return(
                <button style={SharedStyles.primaryButton} onClick={() => this.saveAdminDetails()}>Save</button>
            )
        }
    }

    renderOrgAdminDetails() {

        var adminObjToUpdate = this.state.adminObjToUpdate || {};
        var preventedBoxStyle = this.state.isRequestProcessing || adminObjToUpdate.adminID ? styles.preventedBoxStyle : {};

        return (
            
            <div>
                
                <p style={{...styles.label, ...{marginTop:0}}}>Name</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing}
                    value={adminObjToUpdate.name}
                    style={styles.textArea}
                    onChange={(event) => this.handleOrganizationAdminDetailUpdates('name', event.target.value)} 
                    placeholder="Admin Name" />

                <p style={{...styles.label}}>Email</p>
                <input
                    type='text'
                    readOnly={this.state.isRequestProcessing || adminObjToUpdate.adminID}
                    value={adminObjToUpdate.email}
                    style={{...styles.textArea, ...preventedBoxStyle}}
                    onChange={(event) => this.handleOrganizationAdminDetailUpdates('email', event.target.value)} 
                    placeholder="Admin Email" />

                <p style={{...styles.label}}>Status</p>
                <div style={styles.rowContainer}>
                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'orgAdminActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={adminObjToUpdate.status == 'active' ? true: false}
                        onChange={() => this.handleOrganizationAdminDetailUpdates('status', 'active')}
                    />
                    <span>Active</span>

                    <input
                        type="radio"
                        disabled={this.state.isRequestProcessing}
                        className="feedback-radio"
                        name={'orgAdminActiveStatus'}
                        style = {styles.customRadioBtn}
                        checked={adminObjToUpdate.status == 'disabled' ? true: false}
                        onChange={() => this.handleOrganizationAdminDetailUpdates('status', 'disabled')}
                    />
                    <span>Disabled</span>
                </div>

                <div className="text-center">
                    {this.renderOrgAdminFooterOptions()}
                </div>
            </div>
        )
    }

    render() {
        
        if (this.state.showActivityIndicator) {
            return (
				<div style={{...SharedStyles.loaderComponent, ...{height: 'auto'}}}>
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
        }

        return(
            this.renderOrgAdminDetails()
        )
    }
}

const styles = {

    mainContainer: {
        maxWidth: "1100px",
        width: "100%",
    },
    displayCopy: {
        textAlign: 'center', 
        padding: '20px'
    },
    orgListWrapper: {
        marginTop: '20px',
        marginBottom: '10px'
    },
    tableStyle: {
        width: '100%',
        marginTop: '30px',
        marginBottom: '20px'
    },
    tableHeading: {
        textAlign: 'center',
        padding: '5px',
        fontFamily: 'Pangram-Bold',
        backgroundColor: '#dfdfdf',
    },
    editLink: {
        textAlign: 'center',
        padding: '5px',
        fontFamily: 'Pangram-Regular',
        textDecorationLine: 'underline',
        color: '#1EB0D8',
        cursor: 'pointer'
    },
    tableData: {
        padding: '10px',
        textAlign: 'center'
    },
    textArea: {
        width: '100%',
        border: '1px solid #92d6e1',
        borderRadius: '4px',
        padding: '5px 10px',
        minHeight: '42px',
        outline: 'none',
        resize: 'none',
        backgroundColor: '#FFF',
        textAlign: 'left'
    },
    label: {
        marginTop:'15px',
        marginBottom: '15px',
        fontFamily:'Pangram-Regular'
    },
    customRadioBtn: {
        WebkitAppearance: 'none',
        MozAppearance:'none',
        width: '16px',
        height: '16px',
        borderRadius:'50%',
        outline:'none',
        border:'1px solid #1EB0D8',
        marginTop:'0px',
        marginRight:'8px',
        marginLeft:'8px'
    },
    rowContainer: {
        display:'flex',
        flexDirection:'row',
        alignItems:'center'
    },
    preventedBoxStyle: {
        backgroundColor: '#F6F6F6', 
        cursor: 'not-allowed'
    }
}

export default OrgAdminManagement;