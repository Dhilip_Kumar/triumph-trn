import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import User from '../../modules/User';
import SharedStyles from '../../SharedStyles';

class SuperAdminLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            replaceButtonWithActivityIndicator: false
        };
    }


    login() {

        if (this.state.email == "" || this.state.password == "") {
            return document.getElementById("errorMsg").innerHTML = "Please enter the valid credentials.";
        }
        var coreData = this.props.coreData;
        coreData.userCoreProperties.accessToken = 'temp_dummy_token';
        coreData.userCoreProperties.userRole = 'super-admin';
        User.setUserCoreData(coreData);
        this.props.history.push('/super-admin-dashboard');
    }

    keypress(e) {
        var THIS = this;
        if (e.key === 'Enter') {
            THIS.login();
        }
    }

    handleEmailChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({email: e.target.value});
    }

    handlePasswordChange(e) {
        document.getElementById("errorMsg").innerHTML = "";
        this.setState({password: e.target.value});
    }
    
    renderButton() {

        if (this.state.replaceButtonWithActivityIndicator) {
            return (
                <div>
                    <img class='buttonLoader' src="/images/loading_indicator.gif" alt="Loader" />
                </div>
            )
        }
        return (
            <div className="login-submit"><button type="submit" className="submit" id="submit" onClick={this.login.bind(this)}>Login</button></div>
        )
    }

    render() {
		
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackgroundWithImage}>
                    <div className="login-section" style={{paddingTop:'40px'}}>
                        <div className="login-section-container" style={SharedStyles.loginContainer}>
                            <p style={SharedStyles.loginSectionTitle}>Super Admin Login</p>
                            <div className="login-input"><input type="email" name="email" value={this.state.email} onKeyPress={this.keypress.bind(this)} onChange={this.handleEmailChange.bind(this)} placeholder="Enter your email" /></div>
                            <div className="login-input"><input type="password" name="password" placeholder="Enter your password" value={this.state.password} onKeyPress={this.keypress.bind(this)} onChange={this.handlePasswordChange.bind(this)} /></div>
                            <div id="errorMsg" style={{color:'red'}}></div>
                            {this.renderButton()}
                            <p>Note: This window is only for Triumph Officials.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }	
}

export default SuperAdminLogin;

const styles = {

    termsCopy: {
        color: '#737373',
        textDecorationLine: 'underline'
    }
}