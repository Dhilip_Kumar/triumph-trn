import React, { Component } from 'react';
import moment from "moment";
import 'moment-timezone';
import _ from "@sailshq/lodash";
import Select from 'react-select';
import { CSVLink, CSVDownload } from "react-csv";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import API_Services from '../../modules/API_Services';
import SuperAdmin from '../../modules/SuperAdmin.js';
import SharedStyles from '../../SharedStyles';

class DataExport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: false,
            startDate: new Date().setDate(new Date().getDate() - 7),
            endDate: new Date(),
            trnOrgGroups: [
                {
                    label: 'All Group Reports',
                    value: 'ALL'
                }
            ],
            surveySections: [
                {
                    label: 'Individual Readiness [IIA]',
                    value: 'IIA'
                }
            ],
            trnOrganizations: [
                {
                    label: 'All Organization Reports',
                    value: 'All'
                }
            ]
        };
    }

    componentDidMount() {
        this.CSVHeaders = SuperAdmin.getHeaderForCsvDownload();
        this.getDataExportFilterOptions();
    }

    getDataExportFilterOptions() {

        if (!this.state.showActivityIndicator) {
            this.setState({showActivityIndicator: true});
        };

        API_Services.httpGET(this.props.coreData, 'getDataExportFilterOptions', (error, data) => {

            if (error) {
                return alert(error);
            };

            if (data && data.organizations && data.surveySections && data.groups) {

                var trnOrganizations = this.state.trnOrganizations;
                var trnOrgGroups = this.state.trnOrgGroups;
                var surveySections = this.state.surveySections;

                for (let i = 0; i < data.organizations.length; i++) {
                    trnOrganizations.push({
                        label: data.organizations[i].organizationName,
                        value: data.organizations[i].trnOrganizationID
                    });
                };

                for (let i = 0; i < data.surveySections.length; i++) {
                    surveySections.push({
                        label: data.surveySections[i].sectionTitle,
                        value: data.surveySections[i].sectionID
                    });
                };

                for (let i = 0; i < data.groups.length; i++) {
                    trnOrgGroups.push({
                        label: data.groups[i].groupName,
                        value: data.groups[i].groupID,
                        organizationID: data.groups[i].trnOrganizationID,
                    });
                };

                this.setState({
                    trnOrgGroups: trnOrgGroups,
                    trnOrganizations: trnOrganizations,
                    surveySections: surveySections,
                    showActivityIndicator: false
                });
            };
        }); 
    }

    fetchReport() {

        // validate
        if (!this.state.surveySection) {
            alert("Please select survey section.");
            return;
        }

        if (!this.state.trnOrganization) {
            alert("Please select organization.");
            return;
        }

        if (!this.state.trnOrgGroup) {
            alert("Please select groups.");
            return;
        }

        if (!this.state.showActivityIndicator) {
            this.setState({showActivityIndicator: true});
        };

        var params = {
            trnOrganizationID: this.state.trnOrganization.value,
            trnOrgGroupID: this.state.trnOrgGroup.value,
            sectionID: this.state.surveySection.value,
            startDate: moment(this.state.startDate).tz('America/Los_Angeles').startOf('day').toISOString(),
            endDate: moment(this.state.endDate).tz('America/Los_Angeles').endOf('day').toISOString()
        };

        API_Services.httpPOST(this.props.coreData, 'getSurveyReportsForAnalysis', params, (error, surveyReports) => {

            if (error) {
                return alert(error);
            };

            if (surveyReports) {
                this.setState({
                    surveyReports: surveyReports,
                    showActivityIndicator: false
                });
            };
        }); 
    }

    renderCSVdownloadBtn() {

        if (this.state.surveyReports && this.state.surveyReports.length > 0) {

            var surveySection = this.state.surveySection && this.state.surveySection.value ? this.state.surveySection.value : null;
            var customHeaders = surveySection &&  this.CSVHeaders[surveySection]? this.CSVHeaders[surveySection] : null;

            return (
                <CSVLink
                    style={styles.reportButton} 
                    headers={customHeaders} 
                    filename={"Data-Export.csv"}
                    data={this.state.surveyReports}>Download</CSVLink>
            );
        }
        else {
            return null;
        }
    }

    renderReportSection() {

        var recordCounts = (this.state.surveyReports && this.state.surveyReports.length > 0) ? this.state.surveyReports.length : 0;

        return (
            <div>
                <div style={{...styles.rowContainer, ...{margin:'20px 0'}}}>
                    <div style={{...styles.highlightText,...{margin:'0'}}}>
                        Total # of records: {recordCounts}
                    </div>
                </div>
                {this.renderCSVdownloadBtn()}
            </div>
        );
    }

    trnOrganizationsOnChange = (selectedOrg) => {

        this.setState({
            trnOrganization: selectedOrg,
            trnOrgGroup: null,
            surveyReports: []
        });
    }

    renderFilterOptions() {

        return(
            <div>
                <p style={styles.highlightText}>Report date range:</p>
                
                <div style={styles.rowContainer}>
                    <div style={styles.highlightText}>From:</div>
                    <DatePicker
                        selected={this.state.startDate}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onChange={(date) => this.setState({startDate: date})}
                        maxDate={new Date()}
                        placeholderText="Start Date"
                        className={'custom-date-input'}
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                    />

                    <div style={styles.highlightText}>To:</div>
                    <DatePicker
                        selected={this.state.endDate}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onChange={(date) => this.setState({endDate: date})}
                        minDate={this.state.startDate}
                        maxDate={new Date()}
                        placeholderText="End Date"
                        className={'custom-date-input'}
                        disabled={this.state.startDate ? false : true}
                        peekNextMonth
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                    />
                </div>

                <p style={styles.highlightText}>Select Organization:</p>
                <Select
                    value={this.state.trnOrganizations.filter(obj => this.state.trnOrganization && this.state.trnOrganization.value && obj.value === this.state.trnOrganization.value)}
                    onChange={this.trnOrganizationsOnChange}
                    options={this.state.trnOrganizations}
                    placeholder={'Select Organization...'}
                    styles={styles.selectBoxStyles}/>

                <p style={styles.highlightText}>Select Groups:</p>
                <Select
                    value={this.state.trnOrgGroups.filter(obj => this.state.trnOrgGroup && this.state.trnOrgGroup.value && obj.value === this.state.trnOrgGroup.value)}
                    onChange={(obj) => this.setState({trnOrgGroup: obj, surveyReports: []})}
                    options={this.state.trnOrgGroups.filter(obj => this.state.trnOrganization && this.state.trnOrganization.value && (this.state.trnOrganization.value == 'All' || this.state.trnOrganization.value == obj.organizationID))}
                    placeholder={'Select Groups...'}
                    styles={styles.selectBoxStyles}/>

                <p style={styles.highlightText}>Select Survey Section:</p>
                <Select
                    value={this.state.surveySections.filter(obj => this.state.surveySection && this.state.surveySection.value && obj.value === this.state.surveySection.value)}
                    onChange={(obj) => this.setState({surveySection: obj, surveyReports: []})}
                    options={this.state.surveySections}
                    placeholder={'Select Survey Section...'}
                    styles={styles.selectBoxStyles}/>

                <button 
                    style={styles.reportButton} 
                    onClick={() => this.fetchReport()} 
                    disabled={(this.state.startDate && this.state.endDate) ? false : true}>
                        Fetch Report
                </button>
                <hr/>
            </div>
        );
    }

    render() {

        if (this.state.showActivityIndicator) {
            return (
				<div style={SharedStyles.loaderComponent}>
					<div className="loaderContainer">
						<img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
					</div>
    			</div>
			)
        }

        return (
            <div style={styles.mainContainer}>
                <h3 className="mainSectionTitle">Data Export</h3>
                {this.renderFilterOptions()}
                {this.renderReportSection()}
            </div>
        );
    }
}

const styles = {

    mainContainer: {
        maxWidth: "1100px",
        width: "100%",
    },
    highlightText: {
        fontFamily: 'Pangram-Regular',
        marginTop: '12px',
        marginBottom: '12px'
    },
    rowContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: '15px 5px'
    },
    reportButton: {
        color: '#F79569',
        backgroundColor: '#FFF',
        border: '1px solid #F79569',
        fontSize: '14px',
        borderRadius: '5px',
        padding: '5px 15px',
        marginTop: 20,
        textDecorationLine: 'none'
    },
    selectBoxStyles: {
        option: (provided, state) => ({
            ...provided,
            '&:hover': {
                backgroundColor: '#FFF5ED',
                color: '#737373'
            },
            backgroundColor: state.isSelected ? '#F79569' : '#FFF',
            color: state.isSelected ? '#FFF' : '#737373',
            outline: 'none'
        }),
        control: (base, state) => ({
            ...base,
            border: '1px solid #F79569 !important',
            boxShadow: '0 !important',
            minHeight: '42px',
            maxHeight: '75px',
            overflow: 'auto'
        })
    }
}

export default DataExport;