import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';
import { Modal } from 'react-bootstrap';

var sampleResp = [ 
    {
        "valueName" : "Compassion",
        "valueScore" : 2.5,
        "definition" : "Those who value compassion attempt to feel what others are going through so they may offer help. They seek to expand their awareness beyond their own problems and needs, as they understand we thrive as a collective when we are compassionate. However, self-compassion is equally as important, as this allows us to also thrive as individuals. Compassion is having the desire to help and acting on it, whether that be with someone else, or our own internal needs."
    }, 
    {
        "valueName" : "Kindness & Caring",
        "valueScore" : 3,
        "definition" : "Kindness is a virtue, and if passed on, can have tremendous knock-on effects. A simple smile or helping hand to a stranger down on their luck can have huge positive implications for both them and the people they meet. Caring, in turn, emerges from knowing the value and importance of other people and things beyond ourselves, and the more kind and caring we are to others, the more kindness and caring we find in our own lives. True kindness is the willingness to help even when it’s not convenient to do so."
    }, 
    {
        "valueName" : "Understanding",
        "valueScore" : 2.5,
        "definition" : "Sometimes, all it requires is a sympathetic ear and a sensitivity to another’s feelings. We may all be cut from the same cloth, but the experiences we all go through can differ wildly. Those who value understanding are able to think outside the confines of their own experiences and appreciate not every circumstance is the same. When we understand ourselves, we are better able to ignite our own growth by making more grounded life decisions. By understanding the world we live in, we can see our place in it, giving us greater significance and a stronger feel of purpose."
    }, 
    {
        "valueName" : "Courage",
        "valueScore" : 3.5,
        "definition" : "Courage is the ability to do what you are too afraid to do, and those who value courage understand that courage is the gateway to a more fulfilled life. When we fail to act, we miss out on so many potential opportunities. Until you step outside your comfort zone, you have no idea what could be waiting for you on the other side. Remember - fortune favors the brave."
    }, 
    {
        "valueName" : "Letting Go",
        "valueScore" : 4.5,
        "definition" : "It is easier to forget than it is to forgive, but letting go is to move past the things that hold us back so we can thrive and truly blossom. We all make mistakes from time to time, and those who value Letting Go understand that holding onto resentment can actually hinder our progress as it draws our focus to unhealthy negativity, rather than to the task at hand. The same goes for yourself. Forgive yourself your past digressions, move on, and become a better version of yourself."
    }, 
    {
        "valueName" : "Inclusivity",
        "valueScore" : 2.5,
        "definition" : "It is a wonderful virtue to take pleasure in helping others while also taking care of our own selves. Those who value inclusivity understand that people who receive help are much more likely to pay it forward. To practice inclusivity is to set in motion a continuous positive impact which improves the human condition. It is not about ignoring our own needs, however, but rather putting others’ needs above our non-essential wants."
    }, 
    {
        "valueName" : "Authenticity & Truthfulness",
        "valueScore" : 3,
        "definition" : "Authenticity & Truthfulness begin with being honest with ourselves, but this can be hard as many times we are not even aware when we’re being self-deceptive. It’s by replacing this kind of self-deception with authenticity and truthfulness that we can truly start living with integrity (being true to who we are) and kick our growth engine into high gear. Stretching the truth is like building on a weak foundation. Not being authentic can lead to negative feelings about ourselves, which in turn affects our confidence and belief in our own abilities."
    }, 
    {
        "valueName" : "Simplicity",
        "valueScore" : 3,
        "definition" : "It is easy to overcomplicate most aspects of our lives. We are encouraged to keep building our empires and collecting things as we pursue a myriad of different possibilities at the same time. Those who value simplicity understand that it’s about getting to the heart of what matters while ignoring the peripheral. Simplicity is about not spreading ourselves too thin so that we can truly enjoy what’s essential."
    }, 
    {
        "valueName" : "Humility",
        "valueScore" : 3,
        "definition" : "It is important to understand our value, but humility allows us to see others’ true worth, which goes a long way in terms of building strong personal and professional relationships. Those who value humility understand the importance of completing accomplishments for oneself and are more likely to take pleasure in the task itself, rather than seeking validation from others. Without humility, we’re blindsided by our shortcomings and errors, which in turn hinders our own progress."
    }, 
    {
        "valueName" : "Accepting & Non-Judgement",
        "valueScore" : 3,
        "definition" : "People who value acceptance are able to acknowledge that we all have our differences, and these are our strengths. They are able to observe without judgment and evaluate reality for what it is. When we accept our world (and those in it) for what it truly is, we are able to work together, look forward, and become stronger for it. When we work with those who are different than ourselves, we complement each others’ strengths and cover each others’ weaknesses. Discovery comes from accepting who we are and allows us to begin creating an amazing future for ourselves as well as for our world."
    }, 
    {
        "valueName" : "Abundance",
        "valueScore" : 2,
        "definition" : "People with an abundance attitude take pleasure in sharing their own knowledge, life lessons, and even resources with others, as they are fully aware that there is plenty in this world for everyone to be able to succeed. Those who value it believe in a win-win approach; understanding the infinite potential we all hold, and knowing that through generosity we are able to think bigger, and achieve so much more."
    }, 
    {
        "valueName" : "Being Present",
        "valueScore" : 4.5,
        "definition" : "For those who value being present, they understand that it is better to live in the here and now. Being present when with someone else makes connecting and bonding much more natural and effortless, and in the context of work, improves our focus and concentration and allows us to do our best work. Those who value being present understand that past mistakes are there to be learned from and not dwelled on, and that more often than not, what we worry about in the future never actually happens. They value what they can control. They value what they can enjoy right now. They value the present."
    }, 
    {
        "valueName" : "Dedication & Surrender",
        "valueScore" : 2.5,
        "definition" : "To truly dedicate oneself to a task or a cause is to surrender to it; by which we mean we do what is needed to be done rather than simply sticking to things that we like or prefer doing. When you commit to something bigger than yourself, something you truly believe in, you’re able to rise to a much higher level of awareness and existence. Those who value dedication understand that true attainment and success come from a willingness to walk the path, no matter what the circumstances."
    }, 
    {
        "valueName" : "Being Responsible",
        "valueScore" : 3.5,
        "definition" : "The path of responsibility is harder to tread due to the pressure involved, but usually leads to much greater rewards. Those who value responsibility understand that in most situations, someone is required to step up and take the helm, even when that helm is a burden. We all have limited resources specific to ourselves, and being responsible means using them mindfully. Being Responsible starts with accepting that our own thoughts, words, and actions shape our lives. External circumstances are merely the backdrop, so blaming them gets us nowhere. Being responsible is about accepting power and treating it with the respect it deserves."
    }, 
    {
        "valueName" : "Positivity & Spirit",
        "valueScore" : 4,
        "definition" : "Those who embrace positivity and exude spirit take everything in their stride, and are able to view any situation through a positive lens. Rather than forever looking for what they don’t have, they are able to be grateful for what they do. Those who value positivity and spirit can see the huge importance this simple change of perspective can have: defeats become learning experiences, obstacles become growth opportunities, and successes become internalized."
    }, 
    {
        "valueName" : "Self-Awareness",
        "valueScore" : 2,
        "definition" : "Without awareness, there can be no understanding. And without understanding, we can’t be in the driver’s seat, can we? Our ability to do the best in our lives is predicated on our ability to understand ourselves, and that cannot happen without a sufficient level of Self-Awareness. It’s the common thread underlying all other values such as Understanding, Self-Mastery, Courage, Being Responsible, Being Present, and so on."
    }, 
    {
        "valueName" : "Freedom",
        "valueScore" : 2.5,
        "definition" : "Freedom is a way of thinking and living outside the limiting confines and structures we create around ourselves. When we don’t link our identity, state of mind, thoughts, and happiness to external things, we are able to enjoy them even more. It is one of the core things we seek in our lives, and ironically, it’s something that only we have the ability to give ourselves. Those who value freedom value the ability to be able to choose—or in many cases, carve out—their own path in life. Freedom is the ability to ask ‘why not?’ and to be grounded in truth."
    }, 
    {
        "valueName" : "Self-Mastery",
        "valueScore" : 1.5,
        "definition" : "Life is never linear; it is made of many twists, turns, ups, downs, and obstacles. Those who value Self-Mastery seek composure even when at their lowest, darkest points, but also to be able to experience all of the joy from their highest, greatest achievements. It is a blend of discipline, patience, deep inner reflection, and resolve. This keen knowledge of all of their workings means they are able to weather any storm, which ultimately means they are capable of anything."
    }, 
    {
        "valueName" : "Openness",
        "valueScore" : 3.5,
        "definition" : "When someone practices openness, they are less likely to tie their fate to certain pre-specified outcomes. Thus, the more open we are, the freer and happier we can become. Openness can be driven by (and can lead to) curiosity, so those who practice it are usually more open to new experiences. This means new doors become open to us, and we understand that there is more to life than what we’ve seen and experienced so far."
    }, 
    {
        "valueName" : "Innocence",
        "valueScore" : 3,
        "definition" : "When we talk of Innocence, we remove prejudice, judgment, ego, and malice from our thoughts. Those who value innocence have an inherent belief in the goodness of others. There is simplicity in thinking and a sense of wonder—the beginner's mindset. We may fumble and tumble, but we get up and try again. There is no bruised ego to nurse and no entitlement to preferred outcomes created from layers of ‘grown-up’ habits."
    }, 
    {
        "valueName" : "Excellence",
        "valueScore" : 4.5,
        "definition" : "When we love someone or something, we want to do better than good, don't we? That's excellence at play right there. Here we're not excited by the idea of cutting corners, but instead, we're motivated to set high standards and to push boundaries. That's because we care and want to do our best."
    }
];

class ValuesReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalPopupShow: true,
            valuesReport: sampleResp
        }
    }

    renderValuesIcon(sectionID) {

        if (sectionID == 1) {
            return(<div><img src='/images/wheelhouse_value_icon.png' width={50} /></div>);
        }
        else if (sectionID == 2) {
            return(<div><img src='/images/keystone_value_icon.png' width={50} /></div>);
        }
        else {
            return null;
        }
    }

    selectContainerBackgroundColor(ind) {

        var containerStyle = JSON.parse(JSON.stringify(styles.valueContainer)); // To avoid object by reference, Stringify and Parse the JSON style. 

        if (ind == 0) containerStyle.backgroundColor = '#fdc057';
        else if (ind == 1) containerStyle.backgroundColor = '#AECF7A';
        else if (ind == 2) containerStyle.backgroundColor = '#92d6e1';

        return containerStyle;
    }

    renderValues(sectionID) {

        var valuesReport = this.state.valuesReport;

        if (sectionID == 1) {
            valuesReport = valuesReport.slice(0, 3);
        }
        else if (sectionID == 2) {
            valuesReport = valuesReport.slice(3, 6);
        }

        return (
            <div>
                {valuesReport.map((item, ind) => {
                    return (
                        <div style={this.selectContainerBackgroundColor(ind)} key={'emostate_' + item.stateName}>
                            <div style={{textAlign: 'center'}}>
                                {this.renderValuesIcon(sectionID)}
                                <p style={styles.headerText}>{item.valueName}</p>
                            </div>
                            <div>{item.definition}</div>
                        </div>
                    )
                })}
            </div>
        )
    }

    render() {

        return(

            <div className="static-modal">

                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.props.activeModal == 'Values'} 
                    onHide={() => this.props.closeModal()}>

                    <Modal.Header closeButton>
                        <Modal.Title>Values Report</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={SharedStyles.modalConatiner}>   

                        <div style={styles.sectionHeading}>
                            <p><img src='/images/Value_Icon.png' width={100}></img></p>
                            <p>Core Values</p>
                        </div>

                        <p>In this section, we are going to go through the Core Values that drive you. But before that, you may be asking: what exactly do you mean by Core Values? We’re glad you asked!</p>

                        <div style={styles.valueIntoConatiner}>
                            Our Values are the lens through which we experience our lives. As is most often the case, this happens unconsciously, based on years of previous experiences and a multitude of environmental and personality factors. Our interpretation - or judgement - of what's good, bad, right, and wrong is heavily influenced by a set of values that we are sensitive to.
                        </div>

                        <p>Now you understand what they are, lets take a look at your Core Values.</p>
                         
                        <div style={{textAlign: 'center'}}>
                            <div style={styles.subHeaderText}>Your top 3 Values are: </div>
                        </div>

                        {this.renderValues(1)}

                        <div style={{textAlign: 'center'}}>
                            <div style={styles.subHeaderText}>Your bottom 3 Values are: </div>
                        </div>

                        {this.renderValues(2)}

                        <p style={{marginTop:15}}>So there they are - the Core Values that drive your life. The practical use of these Core Values can be seen in the context of relationships and the conflicts that we experience as a part of these relationships from time to time. Conflicts usually occur when someone violates one or more of your Core Values. It could even be you who 'causes' the conflict by violating one of your own Core Values. Also worth noting is that the process described above usually happens subconsciously. Furthermore, without realizing it, we may set high standards for others to meet even though we may ourselves not excel in our own Core Values. This can lead to scenarios where someone ticks us off for no particular reason. Thus, the more mindful we can be of our own Core Values, the more conscious we can be in how we respond to others’ words and actions as well as other external events.</p>
                    
                    </Modal.Body>
                    
                </Modal>

            </div>
        )
    }
}

export default ValuesReport;

const styles = {

    sectionHeading: {
        fontFamily: 'Pangram-Regular',
        color: '#92d6e1',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    valueIntoConatiner: {
        backgroundImage: `url('/images/Value_Background.png')`,
        color: '#fff',
        backgroundPosition: 'center',
        padding: 10,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        borderRadius: 10,
        fontFamily: 'Niconne-Regular',
        fontSize: 25,
        lineHeight: 1.25,
        marginTop: 15,
        marginBottom: 15
    },
    headerText: {
        fontFamily: 'Pangram-Bold',
        fontSize: '18px'
    },
    subHeaderText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '18px',
        margin: '20px 0px 5px'
    },
    valueContainer: {
        marginTop: '15px',
        padding: '15px',
        borderRadius: '10px',
        color: '#FFF'
    }
};