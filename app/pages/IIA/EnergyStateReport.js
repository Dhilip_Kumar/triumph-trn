import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';
import { Modal } from 'react-bootstrap';

class EnergyStateReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalPopupShow: true,
            energyResponses: [
                {
                    energyState: 'Hyper',
                    percent: 4.86
                },
                {
                    energyState: 'Inert',
                    percent: 3.61
                },
                {
                    energyState: 'Balanced',
                    percent: 2.54
                }
            ]
        }
    }

    renderEnergyState() {

        var energyStates = [];

        {this.state.energyResponses.map((item, ind) => {
            
            // The avg.score is computed on a 10pt scale. Multiply it with 100 to compute bar width.
            let width = (parseFloat(item.percent) * 10) + '%';

            if (item.energyState == 'Balanced') {
                energyStates.push(
                    <div key={ind}>
                        <div style={{...styles.percentileText, ...{color:'#AECF7A'}}}>Balanced - {item.percent} / 10</div>
                        <div style={{...styles.percentileBar, ...{width: width, backgroundColor: '#AECF7A'}}}></div>
                    </div>
                );
            }
            else if (item.energyState == 'Inert') {
                energyStates.push(
                    <div key={ind}>
                        <div style={{...styles.percentileText, ...{color:'#a9dde2'}}}>Inert - {item.percent} / 10</div>
                        <div style={{...styles.percentileBar, ...{width: width, backgroundColor: '#a9dde2'}}}></div>
                    </div>
                );
            }
            else if (item.energyState == 'Hyper') {
                energyStates.push(
                    <div key={ind}>
                        <div style={{...styles.percentileText, ...{color:'#ED7499'}}}>Hyper - {item.percent} / 10</div>
                        <div style={{...styles.percentileBar, ...{width: width, backgroundColor: '#ED7499'}}}></div>
                    </div>
                )
            }
        })}
        
        return(
            <div>{energyStates}</div>
        )
    }

    renderEnergyStateDescriptions(widget) {

        return(
            
            this.state.energyResponses.map((item, ind) => {
                    
                if (item.energyState == 'Balanced') {
                    return (
                        <div style={styles.balancedTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Balanced</p>
                                <img src='/images/Balanced_State_Transparent.png' width={70} style={{marginTop:'-10px'}}></img>
                            </div>
                            <div>In this Energy State, life can be blissful and productive. You can have a greater level of clarity about things that matter and know what you want out of life. You know you’re going to reach your lofty objectives, if you haven’t already, and don’t need others to say nice things to make you feel good about yourself. The deeper the state of Balance, the more every moment is joyful and every person wonderful, and the more you want to share the richness of life with everyone and everything around you. Balanced is the Energy State in which we operate the best.</div>
                        </div>
                    )
                }
                else if (item.energyState == 'Inert') {
                    return (
                        <div style={styles.inertTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Inert</p>
                                <img src='/images/Inert_State_Transparent.png' width={65} style={{marginBottom:'5px'}}></img>
                            </div>
                            <div>In this Energy State, you can be weak and lack initiative. You might experience confusion and lack the ability to make confident decisions. You might feel pressures from life that block you from taking positive action. The more time you spend in the Inert Energy State, the more you feel stalled—unable or unwilling to progress—preventing you from living your life to its fullest potential.</div>
                        </div>
                    )
                }
                else if (item.energyState == 'Hyper') {
                    return (
                        <div style={styles.hyperTextContainer} key={'energy_state_desc' + item.energyState}>
                            <div style={{textAlign:'center'}}>
                                <p style={styles.headerText}>Hyper</p>
                                <img src='/images/Hyper_State_Transparent.png' width={70}></img>
                            </div>
                            <div>In this Energy State, you can be wasteful, inefficient, and insensitive. You might be driven to pursue the things you do not have while losing sight of the things you already possess. You see your goals so clearly and approach them with such energy that if others get in your way, it can make you mad. You may feel that others are mostly either with you or against you. It can also become annoying or even frustrating when others bring up contrary or opposing ideas. </div>
                        </div>
                    )
                }
            })
        )
    }

    render() {

        return(

            <div className="static-modal">

                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.props.activeModal == 'EnergyState'} 
                    onHide={() => this.props.closeModal()}>

                    <Modal.Header closeButton>
                        <Modal.Title>Energy State Report</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={SharedStyles.modalConatiner}>   

                        <div style={styles.sectionHeading}>
                            <p><img src='/images/EnergyState_Icon.png' width={100}></img></p>
                            <p>Energy State</p>
                        </div>

                        <p>An 'Energy State' is the place from which you approach your life at any given point in time. Your Energy State is either Inert, Balanced, or Hyper.</p>
                        <p>The idea is that we should strive to operate in a Balanced Energy State, as this is our most efficient, psychologically healthy state. Making decisions when in a balanced state will be the best we are capable of, as we are able to exhibit calm and control. By initiating actions from the Balanced Energy State, we can achieve significantly more and better results with significantly less effort. Whichever state we operate in affects all other aspects of our life.</p>
                        <p>Let's take a look at your Energy State Percentiles, and see where you currently operate.</p>

                        {this.renderEnergyState()}

                        <p style={{marginTop: 20}}>While the ideal is to always be in the Balanced energy state, for most of us the reality is that we flow from one state to another based on the situation and our tendencies. This report reflect the amount of time you spend in the various Energy States. As such, our aim would be to go lower and lower on Hyper and Inert, and higher on Balanced.</p>
                        <p>Let's look at what the different Energy States specifically mean:</p>
                    
                        {this.renderEnergyStateDescriptions()}
                        
                    </Modal.Body>
                    
                </Modal>

            </div>
        )
    }
}

export default EnergyStateReport;

const styles = {

    sectionHeading: {
        fontFamily: 'Pangram-Regular',
        color: '#AECF7A',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    hyperTextContainer: {
        backgroundColor: '#ED7499',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    balancedTextContainer: {
        backgroundColor: '#AECF7A',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    inertTextContainer: {
        backgroundColor: '#a9dde2',
        color: '#FFF',
        padding: '15px',
        borderRadius: '10px',
        marginTop: '15px'
    },
    headerText: {
        fontFamily: 'Pangram-Bold',
        fontSize: '18px',
        color:"#fff"
    },
    subHeaderText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '18px',
        margin: '20px 0px 5px'
    },
    viewMoreBtn: {
        background:"#AECF7A",
        color : "#fff",
        borderRadius: '8px',
        padding: '5px 15px',
        marginTop: '10px',
        marginBottom: '5px',
        fontFamily: 'Pangram-Regular'
    },
    percentileText: {
        fontFamily:'Pangram-Bold',
        fontSize:16,
        paddingTop:10,
        paddingBottom:10,
    },
    percentileBar: {
        height:20,
        borderRadius:5,
        borderWidth:1,
        borderColor: 'transparent'
    }
}