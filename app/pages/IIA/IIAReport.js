import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import SharedStyles from '../../SharedStyles';
import API_Services from '../../modules/API_Services';

import EnergyStateReport from '../IIA/EnergyStateReport';
import EmoStateReport from '../IIA/EmoStateReport';
import ValuesReport from '../IIA/ValuesReport';
import ThriveMeterReport from '../IIA/ThriveMeterReport';

import queryString from 'query-string';

class IIAReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showActivityIndicator: true,
            activeModal: null
        }
    }

    componentDidMount() {

        var reportID = null;

        if (location.search) {     
        
            var stringURL = location.search.replace("+", "%2B");
            let queryParams = queryString.parse(stringURL);
            
            if (queryParams.reportID) {
                reportID = queryParams.reportID;
            }
        };

        if (!reportID) return;

        API_Services.httpPOST(this.props.coreData, 'getIIAReport', {reportID: reportID}, (err, report) => {

            if (err) {
                alert('Network Error. Please try after sometime.');
                return;
            };

            if (!report || (report && !report.results)) {
                alert('Invalid Request');
                return;
            };

            this.setState({
                recommendedTracks: report ? report.recommendedTracks : {},
                showActivityIndicator: false
            });
        });
    }

    closeModal() {
        this.setState({activeModal: null});
    }

    renderEnergyStateSummary() {

        return(
            <div style={{...styles.stateSummaryContainer, ...{backgroundColor: '#AECF7A'}}} 
                onClick={() => this.setState({activeModal: 'EnergyState'})}>
                <div style={styles.sectionHeadingText}>Energy State</div>
                <div style={styles.rowContainer}>
                    <div style={styles.colContainer}>
                        <img style={styles.sectionIcon} src="/images/Inert_State_Transparent.png" />
                        <div style={styles.sectionText}>Inert</div>
                        <div style={styles.innerSubContainer}>
                            <div>7 / 10</div>
                        </div>
                    </div>
                    <div style={styles.colContainer}>
                        <img style={styles.sectionIcon} src="/images/Balanced_State_Transparent.png" />
                        <div style={styles.sectionText}>Balanced</div>
                        <div style={styles.innerSubContainer}>
                            <div>7 / 10</div>
                        </div>
                    </div>
                    <div style={styles.colContainer}>
                        <img style={styles.sectionIcon} src="/images/Hyper_State_Transparent.png" />
                        <div style={styles.sectionText}>Hyper</div>
                        <div style={styles.innerSubContainer}>
                            <div>7 / 10</div>
                        </div>
                    </div>
                </div>
                <div style={styles.viewMoreText}>View More</div>
            </div>
        )
    }

    renderValuesSummary() {

        return(
            <div style={{...styles.stateSummaryContainer, ...{backgroundColor: '#92d6e1'}}}
                onClick={() => this.setState({activeModal: 'Values'})}>
                <div style={styles.sectionHeadingText}>Values</div>
                <div style={styles.colContainer}>
                    <div style={styles.sectionText}>Top 3 Values</div>
                    <div style={{...styles.innerSubContainer, ...{width:'97%', marginBottom: 10}}}>
                        <div>Enthusiasm | Courage | Love</div>
                    </div>
                </div>
                <div style={styles.colContainer}>
                    <div style={styles.sectionText}>Bottom 3 Values</div>
                    <div style={{...styles.innerSubContainer, ...{width:'97%'}}}>
                        <div>Competing | Desire | Regret and Grief</div>
                    </div>
                </div>
                <div style={styles.viewMoreText}>View More</div>
            </div>
        )
    }

    renderEmoStateSummary() {

        return(
            <div style={{...styles.stateSummaryContainer, ...{backgroundColor: '#f6946a'}}}
                onClick={() => this.setState({activeModal: 'EmoState'})}>
                <div style={styles.sectionHeadingText}>EmoState</div>
                <div style={styles.colContainer}>
                    <div style={styles.sectionText}>Top Balanced States</div>
                    <div style={{...styles.innerSubContainer, ...{width:'97%', marginBottom: 10}}}>
                        <div>Enthusiasm | Courage | Love</div>
                    </div>
                </div>
                <div style={styles.colContainer}>
                    <div style={styles.sectionText}>Inert and/or Hyper States</div>
                    <div style={{...styles.innerSubContainer, ...{width:'97%'}}}>
                        <div>Competing | Desire | Regret and Grief</div>
                    </div>
                </div>
                <div style={styles.viewMoreText}>View More</div>
            </div>
        )
    }

    renderThriveMeterSummary() {

        return(
            <div style={{...styles.stateSummaryContainer, ...{backgroundColor: '#FAB95E'}}}
                onClick={() => this.setState({activeModal: 'ThriveMeter'})}>
                <div style={styles.sectionHeadingText}>Thrive Meter</div>
                <div style={{...styles.rowContainer, ...{marginTop:15}}}>
                    <div style={{...styles.sectionText, ...{flex:1, textAlign: 'center'}}}>Core Enablers</div>
                    <div style={{...styles.innerSubContainer, ...{marginBottom: 10}}}>
                        <div>5.42 / 10</div>
                    </div>
                </div>
                <div style={{...styles.rowContainer}}>
                    <div style={{...styles.sectionText, ...{flex:1, textAlign: 'center'}}}>Inner Compass</div>
                    <div style={{...styles.innerSubContainer, ...{marginBottom: 10}}}>
                        <div>6.12 / 10</div>
                    </div>
                </div>
                <div style={{...styles.rowContainer}}>
                    <div style={{...styles.sectionText, ...{flex:1, textAlign: 'center'}}}>Good Living</div>
                    <div style={{...styles.innerSubContainer, ...{marginBottom: 10}}}>
                        <div>3.41 / 10</div>
                    </div>
                </div>
                <div style={{...styles.rowContainer}}>
                    <div style={{...styles.sectionText, ...{flex:1, textAlign: 'center'}}}>Being Extraordinary</div>
                    <div style={{...styles.innerSubContainer, ...{marginBottom: 10}}}>
                        <div>5.40 / 10</div>
                    </div>
                </div>
                <div style={styles.viewMoreText}>View More</div>
            </div>
        )
    }

    renderInnerContainer() {
        
        if (this.state.showActivityIndicator) {
            return (
                <div style={SharedStyles.loaderComponent}>
                    <div className="loaderContainer">
                        <img style={SharedStyles.loaderImage} src="/images/loading_indicator.gif" alt="loading..." />
                    </div>
                </div>
            )
        }

        return(
            <div style={styles.innerContainer}>
                
                {/* <div style={styles.headerRow}>
                    <img style={styles.headerImage} src="/images/Assessment_Header.png" />
                    <div style={styles.headerText}>Inner Insight</div>
                </div> */}

                <div style={styles.headerRow}>
                    <p><img src='/images/Thrive_Icon.png' width={100}></img></p>
                    <p>Thrive Meter</p>
                </div>

                <p>You're already on the path to understanding your values and taking control of your inner state to best drive your own personal growth.</p>
                <p>Below we are going to go through your results and give you personalized feedback on how to reach your full potential.</p>
                
                {/* {this.renderEnergyStateSummary()}
                <EnergyStateReport activeModal={this.state.activeModal} closeModal={this.closeModal.bind(this)} {...this.props} />
                
                {this.renderValuesSummary()}
                <ValuesReport activeModal={this.state.activeModal} closeModal={this.closeModal.bind(this)} {...this.props} />
                
                {this.renderEmoStateSummary()}
                <EmoStateReport activeModal={this.state.activeModal} closeModal={this.closeModal.bind(this)} {...this.props} />

                {this.renderThriveMeterSummary()} */}
                <ThriveMeterReport
                    {...this.props} 
                    activeModal={this.state.activeModal} 
                    closeModal={this.closeModal.bind(this)}
                    recommendedTracks={this.state.recommendedTracks} />

                <div style={styles.promoContainer}>
                    <p style={styles.promoTitle}>Get Triumph Mastery</p>
                    <p>If you haven't already, download the Triumph Mastery app on your mobile device:</p>
                    <div style={{marginTop:'15px'}}>
                        <a href='https://itunes.apple.com/app/triumph-journal/id1386457723' target='_blank'>
                            <img src='https://d2qoy7rbg08fi8.cloudfront.net/ImageAssets/App_Store_Icon_Triumph_Journal.png' width='125px'/>
                        </a>
                        <a href='https://play.google.com/store/apps/details?id=com.triumphhq.TriumphJournal' target='_blank' style={{marginLeft: '12px'}}>
                            <img src='https://d2qoy7rbg08fi8.cloudfront.net/ImageAssets/Play_Store_Logo_Triumph_Journal.png' width='125px'/>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
    
    render() {
  
        return (
            <div>
                <NavBar {...this.props} />
                <div style={SharedStyles.containerBackground}>   
                    <div className="container containerWrapper">
                        {this.renderInnerContainer()}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default IIAReport;

const styles = {

    innerContainer: {
        textAlign: 'justify',
        padding: '10px 20px',
        lineHeight: 1.5
    },
    headerRow: {
        fontFamily: 'Pangram-Regular',
        color: '#FAB95E',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    headerImage: {
        width: 30,
        height: 40,
        resizeMode: 'contain'
    },
    headerText: {
        paddingLeft: 10,
        color: '#1EB0D8',
        fontFamily: 'Pangram-Bold',
        fontSize: 19
    },
    stateSummaryContainer: {
        borderRadius:10,
        borderWidth:1,
        margin: '15px 0',
        padding: 10,
        cursor: 'pointer'
    },
    sectionHeadingText: {
        fontFamily:'Pangram-Bold',
        fontSize: 18,
        paddingTop: 5,
        paddingBottom: 10,
        color: '#ffffff',
        textAlign:'center'
    },
    rowContainer: {
        flex:1,
        display: 'flex',
        flexDirection:'row',
        width:'100%'
    },
    colContainer: {
        flex:1,
        display: 'flex',
        flexDirection:'column',
        alignItems:'center',
        justifyContent: 'center'
    },
    sectionIcon: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
    },
    sectionText: {
        color:'#fff',
        fontSize: 17,
        padding: 5
    },
    innerSubContainer: {
        flex:1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap:'wrap',
        borderRadius:5,
        borderWidth:1,
        padding: 5,
        backgroundColor: '#fff',
        borderColor: '#fff',
        width: '70%',
        textAlign: 'center',
        fontFamily: 'Pangram-Regular'
    },
    viewMoreText: {
        color:'#fff',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft:13,
        paddingRight:13,
        marginTop:10,
        textAlign: 'right',
        textDecorationLine: 'underline'
    },
    promoContainer: {
        border: '1px solid #F6F6F6',
        padding: '15px',
        borderRadius: '10px',
        backgroundColor:'#F6F6F6',
        fontSize:'16px',
        marginTop:20,
        marginBottom: 20,
        textAlign: 'center'
    },
    promoTitle: {
        color:'#F79569',
        fontFamily: 'Pangram-Bold',
        fontSize: '17px'
    },
};