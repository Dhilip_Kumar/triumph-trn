import React, { Component } from 'react';
import SharedStyles from '../../SharedStyles';
import { Modal } from 'react-bootstrap';

var sampleResp = [
    {
        "stateName": "Enthusiasm",
        "average": "3.40",
        "energyState": "Balanced",
        "stateDefinition": "Whenever you see something that inspires you, you're quick to get back to work. You want every day to be better than the last, but understand that good results require work. You bond easily with others that share your growth mentality."
    },
    {
        "stateName": "Courage",
        "average": "3.28",
        "energyState": "Balanced",
        "stateDefinition": "A tight spot is no problem for you, and you're usually able to take it in your stride. After all, this isn't the first time you've weathered the storm, and you know it won't be the last. Where others might show signs of stopping, you're able to grit your teeth, smile and push on."
    },
    {
        "stateName": "Love",
        "average": "3.08",
        "energyState": "Balanced",
        "stateDefinition": "You are quick to allow others into your group, and enjoy working with people who are similar to you. You usually focus on a person's positives rather than their negatives. You find it easy to apologize - when you are in the wrong."
    },
    {
        "stateName": "Competing",
        "average": "3.57",
        "energyState": "Hyper",
        "stateDefinition": "When others succeed, your first reaction is likely to compare their success to your own. If you are ahead, you take it as validation of your own success, but if you are behind you can feel as though you need to step up your game."
    },
    {
        "stateName": "Desire",
        "average": "3.40",
        "energyState": "Hyper",
        "stateDefinition": "When working with others you can feel uncomfortable when they do things differently to you. You occasionally ignore instructions in favor of your own methods - even if others disagree with you."
    },
    {
        "stateName": "Regret and Grief",
        "average": "3.25",
        "energyState": "Inert",
        "stateDefinition": "If things don't go your way, you can quickly become disappointed. You occasionally have moments where you feel like abandoning your work altogether, and starting something new. You often focus on what 'could have been' too often, when you should be looking at 'what could be.'"
    }
];

class EmoStateReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            emostateReport: sampleResp,
            balancedPercentile: 0,
            inertPercentile: 0,
            hyperPercentile: 0
        }
    }

    renderEnergyStateIcon(energyState) {

        if (energyState == 'Balanced') {
            return(<div><img src='/images/Balanced_State_Transparent.png' width={70} style={{marginTop:'-10px'}} /></div>);
        }
        else if (energyState == 'Inert') {
            return(<div><img src='/images/Inert_State_Transparent.png' width={65} style={{marginBottom:'5px'}} /></div>);
        }
        else if (energyState == 'Hyper') {
            return(<div><img src='/images/Hyper_State_Transparent.png' width={70}/></div>);
        }
        else {
            return null;
        }
    }

    selectContainerBackgroundColor(ind) {

        var containerStyle = JSON.parse(JSON.stringify(styles.emostateContainer)); // To avoid object by reference, Stringify and Parse the JSON style. 

        if (ind == 0) containerStyle.backgroundColor = '#F9A575';
        else if (ind == 1) containerStyle.backgroundColor = '#92d6e1';
        else if (ind == 2) containerStyle.backgroundColor = '#fdc057';

        return containerStyle;
    }

    renderEmoStates(sectionID) {

        var emoStates = this.state.emostateReport;

        if (sectionID == 1) {
            emoStates = emoStates.slice(0, 3);
        }
        else if (sectionID == 2) {
            emoStates = emoStates.slice(3, 6);
        }

        return (
            <div>
                {emoStates.map((item, ind) => {
                    return (
                        <div style={this.selectContainerBackgroundColor(ind)} key={'emostate_' + item.stateName}>
                            <div style={{textAlign: 'center'}}>
                                {this.renderEnergyStateIcon(item.energyState)}
                                <p style={styles.headerText}>{item.stateName} {item.energyState == 'Balanced' ? '' : '(' + item.energyState + ')'}</p>
                            </div>
                            <div>{item.stateDefinition}</div>
                        </div>
                    )
                })}
            </div>
        )
    }

    render() {

        return(

            <div className="static-modal">

                <Modal
                    dialogClassName='modal-dialog modal-lg'
                    show={this.props.activeModal == 'EmoState'} 
                    onHide={() => this.props.closeModal()}>

                    <Modal.Header closeButton>
                        <Modal.Title>EmoState Report</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={SharedStyles.modalConatiner}>   

                        <div style={styles.sectionHeading}>
                            <p><img src='/images/EmoState_Icon.png' width={100}></img></p>
                            <p style={{color: '#f9a576'}}>EmoState</p>
                        </div>

                        <p>In this section, we are going to look at your 'EmoState', or to put it simpler, the top-level emotions that drive the various traits we humans have. They are states of feeling that result in physical and psychological changes which influence our behavior. We refer to them as EmoState Traits which fall in one of the following three categories:</p>

                        <div style={styles.energyStateWrapper}>
                            <div style={styles.energyStateContainer}>
                                <p><img src='/images/INERT.png' width={75}></img></p>
                                <p>When we are weakest, easiest to fail</p>
                            </div>

                            <div style={styles.energyStateContainer}>
                                <p><img src='/images/BALANCED.png' width={75}></img></p>
                                <p>Where we operate best</p>
                            </div>

                            <div style={styles.energyStateContainer}>
                                <p><img src='/images/HYPER.png' width={75}></img></p>
                                <p>When we allow strong emotions to cloud our judgement</p>
                            </div>
                        </div>
                        
                        <p>Picture it like a tightrope walker. On one end of the balancing pole are all of your Inert traits, while on the other end are all of your hyper traits. You know where you need to go, but the only way to move forward is to keep your balance. If you go too far either way, you are likely to stumble. Keeping everything balanced is key.</p>
                        <p>Now on to your results. Below are the top 6 which make up your EmoState, and ultimately, effect most decisions you make in your life.</p>

                        <div style={{textAlign: 'center'}}>
                            <div style={styles.subHeaderText}>Your top Balanced States are: </div>
                        </div>

                        <p>First, let's take a look at your Top Balanced EmoStates. As mentioned before, these are the states in which we operate best.</p>

                        {this.renderEmoStates(1)}

                        <p style={{marginTop: '15px'}}>Next, we're going to take a look at some of the Inert and Hyper States in which you scored highly, as these are the areas we will target to improve. Don't worry - we all have areas in our life in which we can improve, so to have these states is completely normal. The great news is, by isolating the different States, we are able to improve them! Let's take a look...</p>

                        <div style={{textAlign: 'center'}}>
                            <div style={styles.subHeaderText}>Your Inert and/or Hyper States that we have isolated are: </div>
                        </div>

                        {this.renderEmoStates(2)}

                    </Modal.Body>
                    
                </Modal>

            </div>
        )
    }
}

export default EmoStateReport;

const styles = {

    sectionHeading: {
        fontFamily: 'Pangram-Regular',
        color: '#AECF7A',
        textAlign: 'center',
        fontSize: '18px',
        marginBottom: '15px'
    },
    headerText: {
        fontFamily: 'Pangram-Bold',
        fontSize: '18px'
    },
    subHeaderText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '18px',
        margin: '20px 0px 5px'
    },
    energyStateText: {
        fontFamily: 'Pangram-Regular',
        fontSize: '17px',
        color:'#fff'
    },
    energyStateWrapper: {
        display:'flex', 
        flexWrap: 'wrap', 
        margin: '15px 0px'
    },
    energyStateContainer: {
        flex:1,
        margin: '10px',
        textAlign: 'center',
        padding: '10px',
        backgroundColor: '#fef2ec',
        backgroundColor: '#FFF',
        borderRadius: '10px',
        fontSize: '15px'
    },
    emostateContainer: {
        marginTop: '15px',
        padding: '15px',
        borderRadius: '10px',
        color: '#FFF'
    },
    emoStateText : { 
        fontFamily: 'Pangram-Regular', 
        padding:"5px 20px", 
        width:'fit-content', 
        borderRadius:"10px", 
        background:"white", 
        color: '#f8c055', 
        //maxWidth:"350px",
        textAlign:"center"
    },
    overviewContainer: {
        margin: '20px 0', 
        background :'#f8c055', 
        borderRadius : '12px', 
        padding : '20px', 
        display : 'flex', 
        flexDirection:"column", 
        textAlign: 'center', 
        alignItems:'center'
    },
    viewMoreBtn: {
        background:"#AECF7A",
        color : "#fff",
        borderRadius: '8px',
        padding: '5px 15px',
        marginBottom: '5px'
    }
}