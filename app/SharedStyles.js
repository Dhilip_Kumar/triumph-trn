module.exports = {

    containerBackground: {
        minHeight: '94vh',
        marginTop: '60px',
        backgroundColor: '#FFF5ED'
    },
    containerBackgroundWithImage: {
        backgroundImage: 'url(/images/Inverse_Logo.png)',
        backgroundSize: 'cover',
        backgroundPosition: '50%',
        backgroundAttachment: 'fixed',
        minHeight: '94vh',
        marginTop: '60px'
    },
    loginContainer: {
		padding: '20px 10px',
		maxWidth: '768px',
		border: '1px solid #a7a7a7',
    },
    loaderComponent: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '85vh'
    },
    loaderImage: {
        width: '45px',
        height: '45px',
        resizeMode: 'contain'
    },
    loginSectionTitle:{
        fontFamily: 'Pangram-Regular',
        textDecorationLine: 'underline',
        fontSize: '18px'
    },
    primaryButton: {
        textAlign: 'center',
        marginRight: '5px',
        marginTop: '10px',
        marginBottom: '10px',
        padding: '5px 10px',
        border: '1px solid #F79569',
        color: '#F79569',
        borderRadius: '8px',
        backgroundColor: '#FFF'
    },

    /* Assessment */

    questionCount: {
        marginBottom: '-5px',
        paddingTop: '25px',
        color: '#737373'
    },
    
    /* Admin Reports */

    widgetWrapper: {
        padding: '15px',
        borderRadius: '10px',
        border: '1px solid #d2d0d0',
        marginTop: '20px',
        textAlign: 'justify',
        letterSpacing: '0.5px',
        backgroundColor: '#F6F6F6',
        lineHeight: 1.5,
        overflowX: 'auto'
    },
    modalConatiner: {
        padding: '15px',
        textAlign: 'justify',
        letterSpacing: '0.5px',
        backgroundColor: '#F6F6F6',
        lineHeight: 1.5,
        overflowX: 'auto'
    },
    widgetSectionHeading: {
        fontFamily: 'Pangram-Regular',
        padding: '10px 0px',
        fontSize: '18px',
        marginLeft: '15px'
    },
    menuTogglerText: {
        fontSize: '13px',
        margin: '0 20px',
        color: '#FAB95E',
        textDecorationLine: 'underline',
        cursor: 'pointer'
    },
    widgetTitle: {
        fontFamily: 'Pangram-Regular',
        fontSize: '16px'
    },
    widgetListContainer: {
        borderRadius: '10px',
        border: '0.5px solid #E9E9E9',
        backgroundColor: '#F6F6F6',
        width: '300px',
        maxHeight: '75vh',
        marginBottom: '15px',
        overflowY: 'auto',
        boxShadow: '2px 2px 2px 2px #F6F6F6'
    },
    widgetContainer: {
        backgroundColor: '#FFF',
        borderRadius: '10px',
        cursor: 'pointer',
        margin: '10px',
        padding: '10px',
        textAlign: 'center',
        fontSize: '15px'
    },
    lineSeparator: {
        margin: '10px'
    },
    widgetResponseContainer: {
        padding: '0 20px',
        height: '75vh',
        overflowY: 'auto',
        width: '100%'
    },
    widgetResponse: {
        overflowX: 'auto',
        marginBottom: '20px'
    },
    tableStyle: {
        width:'100%',
        marginTop:'20px',
        marginBottom: '20px'
    },
    tableHeading: {
        textAlign:'center',
        fontFamily:'OpenSans-SemiBoldItalic',
        backgroundColor: '#F6F6F6',
    },
    innerDimensionHeading: {
        textAlign:'center',
        fontFamily:'Pangram-Bold',
        minWidth: '33.33%'
    },
    plainText: {
        fontFamily:'Pangram-Light',
        fontSize: '15px'
    }
}