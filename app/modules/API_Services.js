var Util = require('./Util');
var Globals = require('./Globals');

const endPoints = {

    /* Related to Survey */
    getIIAReport: 'Survey/getIIAReport',
    getSurveyListForTheGroup: 'Survey/getSurveyListForTheGroup',
    submitSurveyResponses: 'Survey/submitSurveyResponses',

    /* Related to Super Admin */
    fetchTRNOrganizations: 'SuperAdmin/fetchTRNOrganizations',
    updateTRNOrganization: 'SuperAdmin/updateTRNOrganization',
    fetchTRNOrgAdmins: 'SuperAdmin/fetchTRNOrgAdmins',
    updateTRNOrgAdmin: 'SuperAdmin/updateTRNOrgAdmin',
    getSurveyReportsForAnalysis: 'SuperAdmin/getSurveyReportsForAnalysis',
    getDataExportFilterOptions: 'SuperAdmin/getDataExportFilterOptions',

    /* Related to TRN Org Admin */
    getGroupManagementInfo: 'TRNOrganizationAdmin/getGroupManagementInfo',
    updateTRNOrgGroups: 'TRNOrganizationAdmin/updateTRNOrgGroups'
};

const errorCodesWithText = [409];

class API_Services {

    static httpGET(coreData, serviceName, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, endPoints[serviceName]);
        
        console.log('apiURL: ' + apiURL);

        fetch(apiURL)
        .then(response => {

            if (!response.ok) {

                if (errorCodesWithText.indexOf(response.status) > -1) {
                    response.text().then((text) => {
                        callback(text, null);
                    })
                    .catch((err) => {
                        console.log("Error getting response.text: ", err);
                        callback('Something went wrong. Please try after sometime.', null);
                    });
                }
                else {
                    callback('Something went wrong. Please try after sometime.', null);
                };
                return;
            }
            response.json().then((json) => {
                // Pass the list to the callback function
                callback(null, json);
            });
        })
        .catch(error => {
            console.log('Something bad happened ' + error);
            callback('Something went wrong. Please try after sometime.', null);
        });
    } 

    static httpPOST(coreData, serviceName, params, callback) {

        params.client = Globals.APP_NAME;

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, endPoints[serviceName]);
        
        console.log('apiURL: ' + apiURL);
        console.log('params: ' + JSON.stringify(params));

        fetch(apiURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params),
        })
        .then(response => {

            if (!response.ok) {
                
                if (errorCodesWithText.indexOf(response.status) > -1) {
                    response.text().then((text) => {
                        callback(text, null);
                    })
                    .catch((err) => {
                        console.log("Error getting response.text: ", err);
                        callback('Something went wrong. Please try after sometime.', null);
                    });
                }
                else {
                    callback('Something went wrong. Please try after sometime.', null);
                };
                return;
            }
            response.json().then((json) => {
                // Pass the list to the callback function
                callback(null, json);
            });
        })
        .catch(error => {
            console.log('Something bad happened ' + error);
            callback('Something went wrong. Please try after sometime.', null);
        });
    } 
}

module.exports = API_Services;