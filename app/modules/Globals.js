module.exports = {

    APP_NAME: "Triumph - TRN",

    /* ASYNC STORAGE KEYS */
	ASYNC_STORAGE_KEYS: {
        USER_CORE_PROPERTIES: 'AS_TRN_APP_USER_PROPERTIES',
        CORE_DATA: 'TRN_CORE_DATA',
        IIA_DATA: 'TRN_IIA_DATA',
        OPEN_ENDED_SECTION_DATA: 'TRN_OPEN_ENDED_QUESTION_DATA'
    },
    
    /* ENVIRONMENT */
    ENVIRONMENT: {

        ENV: process.env.NODE_ENV == 'development' ? 'DEV': (process.env.TARGET_ENV == 'staging' ? 'STAGING' : 'PRODUCTION'),

        DEV: {
            BACKEND_BASE_URL: 'http://localhost:1337/', 
        },
        STAGING: {
            BACKEND_BASE_URL: 'https://trn.triumphunt.com/',
        },
        PRODUCTION: {
            BACKEND_BASE_URL: 'https://app.triumphhq.com/trn/',
        },
    },

    USER_ROLE : { 
        TRN_SUPER_ADMIN : "super-admin",
        TRN_ORG_ADMIN : "admin",
        TRN_ORG_EMP : "employee",
    },

    CRYPTO_ENCRYPTION_SECRET: 'sdf@fgh-mn$hjkl',

    BOOLEAN_OPTIONS: [
        {
            label: 'True',
            value: true
        },
        {
            label: 'False',
            value: false
        }
    ]
};
