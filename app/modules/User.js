var moment = require('moment');
var Globals = require('./Globals');
var Util = require('./Util');

class User {

    static setUserCoreData(coreData) {
        localStorage.setItem(Globals.ASYNC_STORAGE_KEYS.CORE_DATA, JSON.stringify(coreData));
    }

    static getUserCoreData() {
        var coreData = JSON.parse(localStorage.getItem(Globals.ASYNC_STORAGE_KEYS.CORE_DATA));
        if (coreData == '' || coreData == null || coreData == undefined) {
            coreData = this.initializeCoreData();
        }
        return coreData;
    }

    static initializeCoreData() {
        var coreData = {};
        coreData.userCoreProperties = {};
        coreData.userCoreProperties.accessToken = '';
        coreData.userCoreProperties.userID = '';
        coreData.userCoreProperties.userRole = '';        
        coreData.userCoreProperties.userName = '';
        coreData.userCoreProperties.userEmail = '';
        coreData.userCoreProperties.organizationID = '';
        coreData.deviceName = navigator.appName;
        coreData.deviceId = navigator.appVersion;
        return coreData;
    }

    static isLoggedIn() {
        
        var coreData = this.getUserCoreData();
        var isLoggedIn = false;
        
        if (coreData && coreData.userCoreProperties) {
            if (coreData.userCoreProperties.accessToken != '' && coreData.userCoreProperties.accessToken != null && coreData.userCoreProperties.accessToken != undefined) {
                isLoggedIn = true;
            }
        } 
        return isLoggedIn;
    }

    static getUserRole() {
        
        var coreData = this.getUserCoreData();
        var userRole = '';
        
        if (coreData && coreData.userCoreProperties) {
            if (coreData.userCoreProperties.accessToken != '' && coreData.userCoreProperties.accessToken != null && coreData.userCoreProperties.accessToken != undefined) {
                userRole = coreData.userCoreProperties.userRole || '';
            }
        } 
        return userRole;
    }

    static login(coreData, role, inputData, callback) {

        let url = '';

        if (role == Globals.USER_ROLE.TRN_ORG_ADMIN) {
            url = 'trn/admin/login';
        }
        else if (role == Globals.USER_ROLE.TRN_ORG_EMP) {
            url = 'trn/emp/login'; // group login
        }
        else if (role == Globals.USER_ROLE.TRN_SUPER_ADMIN) {
            url = 'trn/super-admin/login';
        }
        
        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, url);

        console.log('Login apiURL::', apiURL);

        fetch(apiURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData),
        })
        .then((response) => {

            if (!response.ok) {
                response.text()
                    .then((text) => {
                        callback(text, null);
                    })
                    .catch((err) => {
                        console.log("Error getting response.text: ", err);
                        callback(err, null);
                    });
                return;
            }
            
            response.json().then((responseObj) => {

                coreData.userCoreProperties.userID = responseObj.userID;
                coreData.userCoreProperties.accessToken = responseObj.accessToken;
                coreData.userCoreProperties.userName = responseObj.username;
                coreData.userCoreProperties.userEmail = responseObj.email;
                coreData.userCoreProperties.userRole = responseObj.userRole;

                if (responseObj.organizationID) coreData.userCoreProperties.organizationID = responseObj.organizationID;
                if (responseObj.organizationName) coreData.userCoreProperties.organizationName = responseObj.organizationName;

                if (responseObj.userRole == Globals.USER_ROLE.TRN_ORG_EMP){ 
                    coreData.userCoreProperties.groupID = responseObj.groupID;
                }

                this.setUserCoreData(coreData);
                callback(null, 'success');
            })
        })
        .catch((error) => {
          console.log("Error completing the operation: ", error);
          alert(
              'Connectivity Issue',
              "Are you connected to the Internet? If yes, can you please try again in a few minutes.",
            )// Alert End
        });
    } // login

    static logout(callback) {
        
        let coreData = this.getUserCoreData();
        
        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'trn/logout');
        console.log('Logout apuURL::', apiURL);

        fetch(apiURL)
        .then((response) => {
            if (!response.ok) {
                response.text()
                    .then((text) => {
                        callback(text, null);
                    })
                    .catch((err) => {
                        console.log("Error getting response.text: ", err);
                        callback(err, null);
                    });
                return;
            }
            localStorage.clear();
            callback(null, response);
        })
        .catch((error) => {
          console.log("Error completing the operation: ", error);
          localStorage.clear();
            callback(error, null);
        });
    } // logout
}

module.exports = User;