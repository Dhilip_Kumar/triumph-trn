var Util = require('./Util');

class OrgAdmin {

    static getOrganizationDashboardStats(coreData, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/getOrganizationDashboardStats');

        console.log('apiURL: ' + apiURL);
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({organizationID: coreData.userCoreProperties.organizationID}),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of getOrganizationDashboardStats

    static getCustomWidgetDependencies(coreData, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/getCustomWidgetDependencies');

        console.log('apiURL: ' + apiURL);
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({organizationID: coreData.userCoreProperties.organizationID}),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of getCustomWidgetDependencies

    static getInnerDimensionWidgetReport(coreData, section, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/getInnerDimensionWidgetReport');

        console.log('apiURL: ' + apiURL);
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                organizationID: coreData.userCoreProperties.organizationID,
                section: section
            }),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of getInnerDimensionWidgetReport

    static getMultiDimensionalWidgetReport(coreData, params, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/getMultiDimensionalWidgetReport');

        console.log('apiURL: ' + apiURL);

        params.organizationID = coreData.userCoreProperties.organizationID;
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({...params}),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of getMultiDimensionalWidgetReport

    static getDataDimensionalWidgetReport(coreData, params, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/getDataDimensionalWidgetReport');

        console.log('getDataDimensionalWidgetReport apiURL: ' + apiURL);
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({...params}),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of getDataDimensionalWidgetReport

    static saveCustomWidget(coreData, params, callback) {

        var apiURL = Util.generateBaseAPIURLWithUserToken(coreData, 'TRNOrganizationAdmin/saveCustomWidget');

        console.log('apiURL: ' + apiURL);
        
        fetch(apiURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify(params),
        })
        .then(response => response.json())
        .then((responseJSON) => {
            callback(null, responseJSON);
        })
        .catch((error) => {
            console.warn(error);
            callback(error, null);
        });
    } // End of saveCustomWidget
}

module.exports = OrgAdmin;