import _ from "@sailshq/lodash";

class SuperAdmin {

    // construct headers for prompts
    static constructHeadersForPrompts(sectionID) {

        var surveySectionPromptCount = this.getCurrentSectionsPromptCount()[sectionID] || [];
        var headers = [];

        for (let i = 1; i <= surveySectionPromptCount; i++) {
            
            const PROMPT_ID = 'PROMPT-' + i.toString()
            headers.push({ label: PROMPT_ID, key: PROMPT_ID });
            headers.push({ label: PROMPT_ID + "-USER-RESPONSE", key: PROMPT_ID + "-USER-RESPONSE" });
            headers.push({ label: PROMPT_ID + "-FINAL-RESPONSE", key: PROMPT_ID + "-FINAL-RESPONSE" });
            headers.push({ label: PROMPT_ID + "-TEXT-RESPONSE", key: PROMPT_ID + "-TEXT-RESPONSE" });
        };

        return headers;
    } // End of constructHeadersForPrompts

    static getCurrentSectionsPromptCount(){

        var config = {
           'V1': {          // total number of prompts
                'IIA' : 42, // IIA
                '1' : 10,   // Generalized Self-Efficacy survey
                '2' : 8,    // Grit-S survey
                '3' : 7,    // GAD-7 survey
                '4' : 3,    // Mini-SPIN for GSAD survey
                '5' : 21,   // Beck Depression Inventory (BDI)
                '6' : 27,   // Life Mastery Scale
                '7' : 9,    // General Demographics
                '8' : 4,    // Open Ended Section
                '9' : 44,   // Big Five Personality Traits survey
            }
        };

        return config['V1']; // current
    }

    static getHeaderForCsvDownload(){

        var initialHeaderColumn = { label: "REPORT-ID-1", key: "REPORT-ID" };
        
        var headers = {
            'IIA' : [initialHeaderColumn], // IIA
            '1' : [initialHeaderColumn], // Generalized Self-Efficacy survey
            '2' : [initialHeaderColumn], // Grit-S survey
            '3' : [initialHeaderColumn], // GAD-7 survey
            '4' : [initialHeaderColumn], // Mini-SPIN for GSAD survey
            '5' : [initialHeaderColumn], // Beck Depression Inventory (BDI)
            '6' : [initialHeaderColumn], // Life Mastery Scale
            '7' : [initialHeaderColumn], // General Demographics
            '8' : [initialHeaderColumn], // Open Ended Section
            '9' : [initialHeaderColumn], // Big Five Personality Traits survey
        };

        var otherIIAHeaders = [ // VALUE SCORE, ENERGY STATES, EMO STATES, THRIVE METER
            { label: "VALUE-ABUNDANCE", key: "VALUE-ABUNDANCE" },
            { label: "VALUE-ACCEPTING-&-NON-JUDGEMENT", key: "VALUE-ACCEPTING-&-NON-JUDGEMENT" },
            { label: "VALUE-BEING-PRESENT", key: "VALUE-BEING-PRESENT" },
            { label: "VALUE-BEING-RESPONSIBLE", key: "VALUE-BEING-RESPONSIBLE" },
            { label: "VALUE-COMPASSION", key: "VALUE-COMPASSION" },
            { label: "VALUE-SELF-MASTERY", key: "VALUE-SELF-MASTERY" },
            { label: "VALUE-COURAGE", key: "VALUE-COURAGE" },
            { label: "VALUE-DEDICATION-&-SURRENDER", key: "VALUE-DEDICATION-&-SURRENDER" },
            { label: "VALUE-LETTING-GO", key: "VALUE-LETTING-GO" },
            { label: "VALUE-FREEDOM", key: "VALUE-FREEDOM" },
            { label: "VALUE-AUTHENTICITY-&-TRUTHFULNESS", key: "VALUE-AUTHENTICITY-&-TRUTHFULNESS" },
            { label: "VALUE-HUMILITY", key: "VALUE-HUMILITY" },
            { label: "VALUE-INNOCENCE", key: "VALUE-INNOCENCE" },
            { label: "VALUE-KINDNESS-&-CARING", key: "VALUE-KINDNESS-&-CARING" },
            { label: "VALUE-OPENNESS", key: "VALUE-OPENNESS" },
            { label: "VALUE-POSITIVITY-&-SPIRIT", key: "VALUE-POSITIVITY-&-SPIRIT" },
            { label: "VALUE-INCLUSIVITY", key: "VALUE-INCLUSIVITY" },
            { label: "VALUE-SIMPLICITY", key: "VALUE-SIMPLICITY" },
            { label: "VALUE-UNDERSTANDING", key: "VALUE-UNDERSTANDING" },
            { label: "VALUE-SELF-AWARENESS", key: "VALUE-SELF-AWARENESS" },
            { label: "VALUE-EXCELLENCE", key: "VALUE-EXCELLENCE" },

            { label: "EMO-RESOURCEFULNESS-&-SELF-SUFFICIENCY", key: "EMO-RESOURCEFULNESS-&-SELF-SUFFICIENCY" },
            { label: "EMO-ENTHUSIASM", key: "EMO-ENTHUSIASM" },
            { label: "EMO-ACCEPTANCE", key: "EMO-ACCEPTANCE" },
            { label: "EMO-LOVE", key: "EMO-LOVE" },
            { label: "EMO-PEACE-&-SERENITY", key: "EMO-PEACE-&-SERENITY" },
            { label: "EMO-CLARITY", key: "EMO-CLARITY" },
            { label: "EMO-INSPIRATION", key: "EMO-INSPIRATION" },
            { label: "EMO-PATIENCE", key: "EMO-PATIENCE" },
            { label: "EMO-PERSEVERANCE", key: "EMO-PERSEVERANCE" },
            { label: "EMO-DESIRE-&-GREED", key: "EMO-DESIRE-&-GREED" },
            { label: "EMO-COMPETING", key: "EMO-COMPETING" },
            { label: "EMO-ANGER", key: "EMO-ANGER" },
            { label: "EMO-PRIDE", key: "EMO-PRIDE" },
            { label: "EMO-CONTEMPT-&-SPITE", key: "EMO-CONTEMPT-&-SPITE" },
            { label: "EMO-FEAR-&-INSECURITY", key: "EMO-FEAR-&-INSECURITY" },
            { label: "EMO-SHAME", key: "EMO-SHAME" },
            { label: "EMO-GUILT", key: "EMO-GUILT" },
            { label: "EMO-DOUBT", key: "EMO-DOUBT" },
            { label: "EMO-DESPAIR", key: "EMO-DESPAIR" },
            { label: "EMO-REGRET-AND-GRIEF", key: "EMO-REGRET-AND-GRIEF" },
            { label: "EMO-FRUSTRATION", key: "EMO-FRUSTRATION" },

            { label: "EMO-GRIT", key: "EMO-GRIT" },
            { label: "EMO-GENERALIZED-ANXIETY-DISORDER", key: "EMO-GENERALIZED-ANXIETY-DISORDER" },
            { label: "EMO-GENERALIZED-SOCIAL-ANXIETY-DISORDER", key: "EMO-GENERALIZED-SOCIAL-ANXIETY-DISORDER" },
            { label: "EMO-GENERALIZED-SELF-EFFICACY", key: "EMO-GENERALIZED-SELF-EFFICACY" },
            { label: "EMO-DEPRESSION", key: "EMO-DEPRESSION" },
            { label: "EMO-BIG-FIVE---CONSCIENTIOUSNESS", key: "EMO-BIG-FIVE---CONSCIENTIOUSNESS" },
            { label: "EMO-BIG-FIVE---AGREEABLENESS", key: "EMO-BIG-FIVE---AGREEABLENESS" },
            { label: "EMO-BIG-FIVE---EXTRAVERSION", key: "EMO-BIG-FIVE---EXTRAVERSION" },
            { label: "EMO-BIG-FIVE---OPENNESS-TO-EXPERIENCE", key: "EMO-BIG-FIVE---OPENNESS-TO-EXPERIENCE" },
            { label: "EMO-BIG-FIVE---NEUROTICISM", key: "EMO-BIG-FIVE---NEUROTICISM" },
            { label: "EMO-ENTREPRENEURIAL-VISION", key: "EMO-ENTREPRENEURIAL-VISION" },
            { label: "EMO-ENTREPRENEURIAL-SIMPLICITY-&-FOCUS", key: "EMO-ENTREPRENEURIAL-SIMPLICITY-&-FOCUS" },
            { label: "EMO-ENTREPRENEURIAL-LEADERSHIP", key: "EMO-ENTREPRENEURIAL-LEADERSHIP" },
            
            { label: "ENERGY-BALANCED", key: "ENERGY-BALANCED" },
            { label: "ENERGY-HYPER", key: "ENERGY-HYPER" },
            { label: "ENERGY-INERT", key: "ENERGY-INERT" },

            { label: "TM-CORE-ENABLER", key: "TM-CORE-ENABLER" },
            { label: "TM-INNER-COMPASS", key: "TM-INNER-COMPASS" },
            { label: "TM-GOOD-LIVING", key: "TM-GOOD-LIVING" },
            { label: "TM-BEING-EXTRAORDINARY", key: "TM-BEING-EXTRAORDINARY" },
        ];

        var commonHeaders = [
            { label: "REPORT-VERSION", key: "REPORT-VERSION" },
            { label: "COMPLETION-CODE", key: "COMPLETION-CODE" },
            { label: "TRN-ORG-ID", key: "TRN-ORG-ID" },
            { label: "GROUP-ID", key: "GROUP-ID" },
            { label: "CHANNEL", key: "CHANNEL" },
            { label: "SECTION-START-TIME", key: "SECTION-START-TIME" },
            { label: "SECTION-END-TIME", key: "SECTION-END-TIME" },
            { label: "SURVEY-START-TIME", key: "SURVEY-START-TIME" },
            { label: "SURVEY-END-TIME", key: "SURVEY-END-TIME" },
            { label: "TOTAL-SURVEY-DURATION-IN-SECONDS", key: "TOTAL-SURVEY-DURATION-IN-SECONDS" }
        ];

        /* Construct headers for every section */

        // Construct IIA headers
        var IIAHeaders = this.constructHeadersForPrompts('IIA');
        headers['IIA'] = _.union(headers['IIA'], IIAHeaders, otherIIAHeaders, commonHeaders);

        // Construct Generalized Self-Efficacy survey
        var GSESHeaders = this.constructHeadersForPrompts('1');
        headers['1'] = _.union(headers['1'], GSESHeaders, commonHeaders);

        // Construct Grit-S survey
        var GritSHeaders = this.constructHeadersForPrompts('2');
        headers['2'] = _.union(headers['2'], GritSHeaders, commonHeaders);

        // Construct GAD-7 survey
        var GAD7Headers = this.constructHeadersForPrompts('3');
        headers['3'] = _.union(headers['3'], GAD7Headers, commonHeaders);

        // Construct Mini-SPIN for GSAD survey
        var MiniSpinHeaders = this.constructHeadersForPrompts('4');
        headers['4'] = _.union(headers['4'], MiniSpinHeaders, commonHeaders);

        // Construct Beck Depression Inventory (BDI)
        var BDIHeaders = this.constructHeadersForPrompts('5');
        headers['5'] = _.union(headers['5'], BDIHeaders, commonHeaders);

        // Construct Life Mastery Scale
        var LMSHeaders = this.constructHeadersForPrompts('6');
        headers['6'] = _.union(headers['6'], LMSHeaders, commonHeaders);        

        // Construct General Demographics
        var GDHeaders = this.constructHeadersForPrompts('7');
        headers['7'] = _.union(headers['7'], GDHeaders, commonHeaders);

        // Construct Open Ended Section
        var OESHeaders = this.constructHeadersForPrompts('8');
        headers['8'] = _.union(headers['8'], OESHeaders, commonHeaders);

        // Construct Big Five Personality Traits survey
        var BFPTHeaders = this.constructHeadersForPrompts('9');
        headers['9'] = _.union(headers['9'], BFPTHeaders, commonHeaders);

        return headers;
    }
};

export default SuperAdmin;