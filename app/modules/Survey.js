var Util = require('./Util');
var User = require('./User');
var Globals = require('./Globals');
var moment = require('moment');

class Survey {

    static getSurveyResponsesFromStorage(surveySections) {

        var surveyResponses = [];

        return new Promise((resolve, reject) => {

            if (!surveySections) return reject('Invalid Input');

            for (var i=0; i < surveySections.length; i++) {

                var asyncStorageKey = '';

                if (surveySections[i] && surveySections[i].sectionID == 'IIA') {
                    asyncStorageKey = Globals.ASYNC_STORAGE_KEYS.IIA_DATA;
                }
                else if (surveySections[i] && surveySections[i].sectionID) {
                    asyncStorageKey = 'Section_' + surveySections[i].sectionID;
                };

                surveyResponses.push(this.getSectionDataFromStorage(asyncStorageKey));
            };

            Promise.all(surveyResponses).then((values) => {
                console.log('survey data...', values);
                resolve(values);
            });
        });
    } // End of getSurveyResponsesFromStorage

    static validateSurveyResponses(surveyResponses) {

        return new Promise((resolve, reject) => {

            if (!surveyResponses) return reject('Invalid Input');

            for (var j=0; j <= surveyResponses.length; j++) {

                if (j == surveyResponses.length) {
                    return resolve(true);
                };

                var sectionData = surveyResponses[j].statementsAndPrompts;

                for (var k=0; k < sectionData.length; k++) {

                    if (!sectionData[k].isAnswered) {
                        return resolve(false);
                    }
                }
            };
        });
    } // End of validateSurveyResponses

    static clearSurveyResponseFromStorage() {

        var storageKeys = Object.keys(localStorage);

        if (storageKeys && storageKeys.length > 0) {

            for (var i=0 ; i < storageKeys.length; i++) {
                
                var key = storageKeys[i];

                if (key == Globals.ASYNC_STORAGE_KEYS.USER_CORE_PROPERTIES || key == Globals.ASYNC_STORAGE_KEYS.CORE_DATA) {
                    continue;
                }
                else {
                    localStorage.removeItem(key);
                };
            };
        };
    } // End of clearSurveyResponseFromStorage

    static getSectionDataFromStorage(key) {

        return new Promise((resolve, reject) => {
            
            if (!key) reject('Invalid Input');
            
            var value = localStorage.getItem(key);

            // Introduce some time delay before resolving promise to make sure that the data fetched properly.
            setTimeout(resolve, 100, JSON.parse(value));
        });
    } // End of getDataFromStorage

    static setSectionDataInTheStorage(key, data) {

        return new Promise((resolve, reject) => {
            
            if (!key || !data) reject('Invalid Input');
            
            localStorage.setItem(key, JSON.stringify(data));

            // Introduce some time delay before resolving promise to make sure that the data gets stored properly.
            setTimeout(resolve, 100, true);
        });
    } // End of getDataFromStorage
};

module.exports = Survey;