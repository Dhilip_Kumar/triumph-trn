var Globals = require('./Globals');

class Util {

    static generateBaseAPIURLWithUserToken(coreData, endpoint) {
        var apiURL = Globals.ENVIRONMENT[Globals.ENVIRONMENT.ENV].BACKEND_BASE_URL;
        apiURL += endpoint;
        apiURL += '?userID=' + coreData.userCoreProperties.userID;
        apiURL += '&accessToken=' + coreData.userCoreProperties.accessToken;
        apiURL += '&deviceID=' + coreData.deviceId;
        apiURL += '&platform=' + 'web';
        apiURL += '&client=' + Globals.APP_NAME;
        return apiURL;
    }

    static cloneArray(baseArray) {
        
        if (null == baseArray || "object" != typeof baseArray) return baseArray;
        
        var copy = baseArray.constructor();

        for (var attr in baseArray) {
            if (baseArray.hasOwnProperty(attr)) copy[attr] = this.cloneArray(baseArray[attr]);
        };
        return copy;
    }

    static validateTextInput(textInput) {

        if (typeof textInput != 'string' || textInput.length < 1) 
            return false;
        else
            return true;
    }

    static validatePassword(pass) {
        var regex =  new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        return regex.test(pass);
    }

    static validateEmail(email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }
}

module.exports = Util;