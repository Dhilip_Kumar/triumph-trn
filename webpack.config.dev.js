const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'development',

  devtool: 'cheap-module-eval-source-map',

  entry: [
    'babel-polyfill',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/dev-server',
    './app/main.js'
  ],

  output: {
    path: path.join(__dirname, '/assets/'),
    filename: 'bundle.js',
    pathinfo: true,
    publicPath: 'http://localhost:8080/'
  },

  resolve: {
    modules: [path.join(__dirname, '..', 'app'), 'node_modules'],
    alias: {
      assets: 'assets',
      styles: 'assets/styles/',
      components: 'assets/components/',
      fonts: 'assets/fonts/',
      images: 'assets/images/'
    },
    extensions: ['.webpack.js', '.web.js', '.js', '.jsx']
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      __ENV__: process.env.NODE_ENV
    })
  ],

  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader?sourceMap' },
          { loader: 'sass-loader?sourceMap' }
        ]
      },
      {
        test: /\.(ttf|eot|svg|woff)?$/,
        exclude: /node_modules/,
        include: path.join(__dirname, 'assets/fonts/'),
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/public/fonts/',
              publicPath: 'http://localhost:8080/fonts/'
            }
          }
        ]
      },
      { test: /\.(woff2?|jpe?g|png|gif|ico)$/, 
        use: 'file-loader?name=./assets/images/[name].[ext]' },
      {
        test: /\.(jsx|js)?$/,
        exclude: /node_modules/,
        use:{loader: 'babel-loader',
        options: {
          presets: ['react', 'stage-2']
        }},
        include: path.join(__dirname, 'app'),
      }
    ],

    noParse: /\.min\.js/
  }
};
